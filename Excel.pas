unit Excel;

{
Notes about attributes used by this unit:
   A system attribute will be added to the drawing for each workbook/sheet referenced by the drawing:
    Attrib name:  SpdhXLZZZ9
                      where ZZZ9 is a sequential number (starting at 1 and incremented for each
                      additional workbok/sheet that is referenced (attributes are not deleted if
                      the workbook/sheet ceases to be used).
    Attrib shstr: FullFilePathAndName|SheetName
                      (file name and sheet name separated by a pipe character)
                      Note that if the specified file name starts with '|pathxls|'
                      then that needs to be replaced with the 'Insert Excel Spreadsheet'
                      path configered in DataCAD

   Another system attribute is added with the Unit Size of dimensions in workbook/sheet: This
    Attrib name:  SpdhXLZZZ9DU
                      (same as above attribute name, but with 'UD' appended
    Attrib dis:   Unit size in datacad distance units (1/32")
                      (e.g. if measurements in the spreadsheet are in feet this will be 384)

   One or more (max 10) attributes may also added to map the spreadsheet columns to field tags.
    Attrib name:  SpdhXLZZZ9C9
                      where SpdhXLZZZ9 matches the corresponding attribute above. 'C' is a hard-coded
                      value and the last 9 is a sequential number from 0..9.
                      If the spreadsheet has 42 or less columns there will only be one such attribute
                      (ending it '0').  Extra attributes are added to cater for more columns (up to max
                      of 420 columns).
    Attrib shstr: The string is space filled with tag names every 6 characters (i.e. if column1 is mapped
                  to a tag then the tag will be in characters 1-6, column 2 tag will be characters 7-12
                  and so on).

   Default spreadsheet column mapping is saved in one or more (max 10) attributes
    Attrib name:  SpdhXLdftC9
                       where the first 10 characters are constant and the last character is a sequential
                       number from 0..9/
    Attrib shstr: The string is space filled with tag names every 6 characters as for the above.
}

interface

  uses ActiveX, Forms, strutil, Dialogs, Settings, ImportExcel, URecords, SysUtils, CommonStuff,
  Language, UInterfaces, UConstants, vcl.Controls, System.math, Vcl.Grids, Rectangle, Report,
  System.Generics.Collections, CreateSpaces, System.Variants, ComObj, Version, Catgries,
  ExportSelect, Vcl.ComCtrls, ExcelDefaults, ExcelHelper, UserFields;

  PROCEDURE ExcelImport;

  PROCEDURE ExcelExport;

  PROCEDURE SetExcelDefaults;

implementation

TYPE
  xlMapping = array [1..420] of str6;

FUNCTION TagValue (tag : string; Ent : entity; AtrData : StandardAtr; DisUnit : double;
                   RptTotArea, DwgTotArea : double;
                   var strvalue : string;
                   var numvalue : double) : variant;
/// Note : for string values numvalue is set to NAN
VAR
  i : integer;
  tempshortstring : shortstring;
  done : boolean;
  BracedTag : string;
  atr : attrib;
  UserField : UserFieldDef;
BEGIN
  strvalue := '';
  numvalue := NAN;
  result := null;
  if length (tag) = 0 then
    exit;

  // meaning of {grp} tag can change depending on specified sub-total grouping
  if tag.ToUpper = 'GRP' then begin
    case l^.RptSubtotBy of
      tag_lyr : tag := Tags[tag_lyr, 1];
      tag_cat : tag := Tags[tag_cat, 1];
    end;
  end;

  BracedTag := '{' + tag.ToUpper + '}';
  done := false;
  for i := 1 to NumTags do begin
    if BracedTag = Tags[i, 1].ToUpper then begin
        case i of
          tag_nbr :
            begin
              strvalue := string(AtrData.Num);
              done := true;
            end;
          tag_name :
            begin
              strvalue := TrimRight(string(AtrData.Name));
              done := true;
            end;
          tag_hdim :
            begin
              numvalue := AtrData.hdim / DisUnit;
              done := true;
            end;
          tag_vdim :
            begin
              numvalue := AtrData.vdim / DisUnit;
              done := true;
            end;
          tag_sdim :
            begin
              numvalue := min(AtrData.hdim, AtrData.vdim) / DisUnit;
              done := true;
            end;
          tag_ldim :
            begin
              numvalue := max(AtrData.hdim, AtrData.vdim) / DisUnit;
              done := true;
            end;
          tag_perim :
            begin
              numvalue := (AtrData.OutlinePerim + AtrData.VoidPerim) / DisUnit;
              done := true;
            end;
          tag_operim :
            begin
              numvalue := AtrData.OutlinePerim / DisUnit;
              done := true;
            end;
          tag_vperim :
            begin
              numvalue := AtrData.VoidPerim / DisUnit;
              done := true;
            end;
          tag_dunit :
            begin
              if disunit = DimUnitSize (UNITS_CLIP) then
                // spreadsheet is using same units as specified for Clipboard export
                strvalue := DisUnitDisplay (UNITS_CLIP)
              else begin
                if realequal (disunit, 384) then
                  strvalue := GetLbl(355)  //ft
                else if realequal (disunit, 32) then
                  strvalue := GetLbl(322)  //inches
                else if realequal (disunit, 1.2598425196850393700787401574803) then
                  strvalue := GetLbl(356)  //mm
                else if realequal (disunit, 12.598425196850393700787401574803) then
                  strvalue := GetLbl(357)  //cm
                else if realequal (disunit, 1259.8425196850393700787401574803) then
                  strvalue := GetLbl(258)  //m
                else
                  strvalue := '?units?';
              end;
              done := true;
            end;
          tag_area :
            begin
              numvalue := AtrData.area / AreaUnitSize (UNITS_CLIP, MainAreaRec);
              done := true;
            end;
          tag_oarea :
            begin
              numvalue := AtrData.GrossArea / AreaUnitSize (UNITS_CLIP, MainAreaRec);
              done := true;
            end;
          tag_varea :
            begin
              numvalue := (AtrData.GrossArea - AtrData.area) /  AreaUnitSize (UNITS_CLIP, MainAreaRec);
              done := true;
            end;
          tag_aunit :
            begin
              strvalue := AreaUnitDisplay (UNITS_CLIP, MainAreaRec);
              done := true;
            end;
          tag_parea :
            begin
              if l^.AltWallArea then
                numvalue := (AtrData.VoidPerimArea + AtrData.OutlinePerimArea) / AreaUnitSize (UNITS_CLIP, AltAreaRec)
              else
                numvalue := (AtrData.VoidPerimArea + AtrData.OutlinePerimArea) / AreaUnitSize (UNITS_CLIP, MainAreaRec);
              done := true;
            end;
          tag_oparea:
            begin
              if l^.AltWallArea then
                numvalue := AtrData.OutlinePerimArea / AreaUnitSize (UNITS_CLIP, AltAreaRec)
              else
                numvalue := AtrData.OutlinePerimArea / AreaUnitSize (UNITS_CLIP, MainAreaRec);
              done := true;
            end;
          tag_vparea:
            begin
              if l^.AltWallArea then
                numvalue := AtrData.VoidPerimArea / AreaUnitSize (UNITS_CLIP, AltAreaRec)
              else
                numvalue := AtrData.VoidPerimArea / AreaUnitSize (UNITS_CLIP, MainAreaRec);
              done := true;
            end;
          tag_aaunit:
            begin
              strvalue := AreaUnitDisplay (UNITS_CLIP, AltAreaRec);
              done := true;
            end;
          tag_vol:
            begin
              numvalue := AtrData.Volume / VolUnitSize (UNITS_CLIP);
              done := true;
            end;
          tag_vunit:
            begin
              strvalue := VolUnitDisplay (UNITS_CLIP);
              done := true;
            end;
          tag_rareapct:
            begin
              numvalue := AtrData.area/RptTotArea;   //'{rarea%}
              done := true;
            end;
          tag_dareapct:
            begin
              numvalue := AtrData.area/DwgTotArea;   //'{darea%}
              done := true;
            end;
          tag_lyr:
            begin     //{lyr}
              getLongLayerName (Ent.lyr, tempshortstring);
              strvalue := string(tempshortstring);
              done := true;
            end;
          tag_cat:
            begin
              strvalue := string(AtrData.Category);
              done := true;
            end;
          tag_grp:
            begin
              //do nothing (if group had been specified then tag would have been chagned)
              done := true;
            end;
          tag_flr:
            begin
              numvalue := Ent.plbase / DisUnit;
              done := true;
            end;
          tag_ceiling:
            begin
              numvalue := (Ent.plnhite - Ent.plbase) / DisUnit;
              done := true;
            end;
        end;
    end;
    if done then begin
      if isNAN (numValue) then
        result := strvalue
      else
        result := numValue;
    end
    else begin
      // look for user defined tag
        if atr_entfind (Ent, 'SpdhCF'+atrname(tag), atr) then begin
        result := '';
        case atr.atrtype of
          atr_str : strValue := '''' +  string(atr.str);
          atr_dis : numValue := atr.dis/DisUnit;
          atr_int : numValue := atr.int;
          atr_rl  : NumValue := atr.rl;
          atr_ang : NumValue := atr.ang/pi*180;
          atr_Area: if l^.AltCustomArea then
                      NumValue := atr.rl / AreaUnitSize (UNITS_CLIP, AltAreaRec)
                    else
                      NumValue := atr.rl / AreaUnitSize (UNITS_CLIP, MainAreaRec);
        end;
      end
      else if atr_sysfind ('SpdhUF'+atrname(tag), atr) then begin
            UserField := DecodeUserFieldAtr (atr);
            case UserField.numeric of
              str : StrValue := '''' + string(UserField.defaultstr);
              int : NumValue := UserField.defaultint;
              rl  : NumValue := UserField.defaultrl;
              dis : NumValue := UserField.defaultdis/DisUnit;
              ang : NumValue := UserField.defaultang/pi*180;
              UserFieldType.area : begin
                      if l^.AltCustomArea then
                        NumValue := UserField.defaultarea / AreaUnitSize (UNITS_CLIP, AltAreaRec)
                      else
                        NumValue := UserField.defaultarea / AreaUnitSize (UNITS_CLIP, MainAreaRec)
                    end
            end;
      end;
      if isNAN (numValue) then
        result := strvalue
      else
        result := numValue;
    end;
  end;
END;



  FUNCTION DfltExcelDimUnit : double;
  VAR
    dimRec : DimensionsRec;
  BEGIN
    result := 1;

    dimRec := l^.DimensionSettings[UNITS_CLIP];
    if dimRec.DimTyp = SameAsRpt then
      dimRec := l^.DimensionSettings[UNITS_RPT];
    if dimRec.DimTyp = SameAsLbl then
      dimRec := l^.DimensionSettings[UNITS_LBL];
    case dimRec.DimTyp of
      Current: begin
          case PGSaveVar^.scaletype of
            0,1,2	: result := onefoot;	//feet
            3,8,9,10	: result :=  MCONV;		//meters
            4,5	: result :=  thirtytwo;	//inches
            6:	result := MCONV / 100;	//cm
            7: result := MCONV / 1000;	//mm
          end;
        end;
      FeetInch: result := thirtytwo;	//inches
      DecFt: result := onefoot;	//feet
      Mtr:  result := MCONV;
      Cm: 	result := MCONV / 100;	//cm
      Mm:	result := MCONV / 1000;	//mm
      CustomDim: result := dimRec.CustUnitSize;
    end;

  END;


  PROCEDURE SetExcelDefaults;
  VAR
    FmExcelDefaults: TFmExcelDefaults;
  BEGIN
    FmExcelDefaults := TFmExcelDefaults.Create(Application);
    try
      FmExcelDefaults.ShowModal;
      if FmExcelDefaults.ModalResult = mrOK then begin
        SaveStr ('SpdhXLDeflt', FmExcelDefaults.edFileName.Text);
        SaveInt('SpdhXLDltRow', FmExcelDefaults.DcadNumericEdit1.IntValue);
        SaveColAssignment(atrname('SpdhXLdftC'), FmExcelDefaults.sgColMapping, 0);
     end;
    finally
      FmExcelDefaults.Free;
    end;
  END;


  FUNCTION GetGridValue (Grid : TStringGrid;
                          tag : string; row : integer;
                          var value : string) : boolean;    overload;
  VAR
    Col : integer;
    svVal : string;
  BEGIN
    result := false;
    svVal := value;
    Col := 1;
    if not tag.StartsWith('{') then
      tag := '{' + tag + '}';
    while (Col < Grid.ColCount) do begin
      if Grid.Cells[Col,0] = tag then begin
        value := Grid.Cells[Col, row];
        if length (value) = 0 then begin
          result := false;
          value := svVal;
        end
        else
          result := true;
        exit;
      end;
      Col := Col+1;
    end;
  END;

  FUNCTION GetGridValue (Grid : TStringGrid;
                          tag : string; row : integer;
                          var value : shortstring) : boolean;    overload;
  VAR
    s : string;
  BEGIN
    result := GetGridValue (Grid, tag, row, s);
    if result then
      value := shortstring(s);
  END;

  FUNCTION GetGridValue (Grid : TStringGrid;
                          tag : string; row : integer;
                          var value : integer) : boolean;    overload;
  VAR
    s : string;
    svVal : integer;
  BEGIN
    svVal := value;
    result := GetGridValue (Grid, tag, row, s);
    if result then try
      value := strtoint (s);
    except
      result := false;
      value := svVal;
    end;
  END;

  FUNCTION GetGridValue (Grid : TStringGrid;
                          tag : string; row : integer;
                          var value : double) : boolean;    overload;
  VAR
    s : string;
    svVal : double;
  BEGIN
    svVal := value;
    result := GetGridValue (Grid, tag, row, s);
    if result then try
      value := strtofloat (s);
    except
      result := false;
      value := SvVal;
    end;
  END;


  PROCEDURE ExcelImport;
  VAR
    FmImportXL : TImportXL;
    OpenDlg : TOpenDialog;
    s : string;
    ss : shortstring;
    i, row : integer;
    ExcelNdx : SmallInt;
    atr : attrib;
    done : boolean;
    ColAtrName : atrName;
    filename : string;
    ExcelPath : shortstring;
    p : point;
    xdim, ydim : double;
    UserFieldValues  : TList<UserFieldValue>;
    UserFieldVal : UserFieldValue;
    FloorZ, CeilingHt : double;
    Name, Number, Category : string;
    view : view_type;
    DimUnit : double;
    catndx : integer;
    SvFillFlag : boolean;
    SvFillClr : longint;
    ExistingExcel : boolean;
    adr : lgl_addr;


    FUNCTION FindCatCustomVal : boolean;
    VAR
      i : integer;
    BEGIN
       result := false;
       if catndx < 0 then
         exit;
       for i := 0 to l^.Categories[catndx].Custom.Count-1 do begin
         if (Uppercase (string(l^.Categories[catndx].Custom[i].tag)) = UpperCase (string(UserFieldVal.tag))) and
         (l^.Categories[catndx].Custom[i].numeric = UserFieldVal.numeric) then begin
           result := true;
           UserFieldValues.Add(l^.Categories[catndx].Custom[i]);
           exit;
         end;
       end;
    END;


  BEGIN
    OpenDlg := TOpenDialog.Create(Application);
    try
      ExistingExcel := false; //initialise to some value to prevent compiler warning ...
      OpenDlg.Filter := 'Excel Files (*.xls, *.xlsx)|*.xls;*.xlsx';
      OpenDlg.FilterIndex := 1;
      GetPath (ExcelPath, Pathxls);
      s := string(GetSvdStr ('SpdhLastXl', ExcelPath));
      if ansipos ('|pathxls|', s) = 1 then begin  //ExcelPath has been replaced with a tag ... need to substitute with actual Excelpath
        delete (s, 1, 9);
        insert (string(ExcelPath), s, 1);
      end;
      OpenDlg.InitialDir := ExtractFilePath (s);
      OpenDlg.FileName := ExtractFileName (s);
      OpenDlg.Options := [ofHideReadOnly, ofFileMustExist];
      if OpenDlg.Execute then begin
        screen.cursor:=crHourglass;
        filename := OpenDlg.FileName;
        if ansipos (UpperCase(string(ExcelPath)), UpperCase(filename)) = 1 then begin
          delete (filename, 1, length(ExcelPath));
          insert (string('|pathxls|'), filename, 1);
        end;
        SaveStr ('SpdhLastXl', filename);

        FmImportXL := TImportXL.Create(Application);
        try
          GetMsg(69, s); //Import Spaces from
          FmIMportXL.Caption := s + ' ' + OpenDlg.FileName;
          FmImportXL.DfltDimension := DfltExcelDimUnit;
          try

            try
              FmIMportXL.ExcelApplication := GetActiveOleObject('Excel.Application');
              ExistingExcel := true;
            except
              FmIMportXL.ExcelApplication := CreateOleObject('Excel.Application');
              ExistingExcel := false;
            end;


            try
              if not ExistingExcel then
                FmIMportXL.ExcelApplication.Visible := false;
              FmIMportXL.wb := FmIMportXL.ExcelApplication.Workbooks.Open(string(OpenDlg.FileName));
              for i := 1 to FmIMportXL.wb.WorkSheets.Count do begin
                FmIMportXL.cbSheets.Items.Add(FmIMportXL.wb.WorkSheets[i].Name);
              end;
              FmIMportXL.cbSheets.ItemIndex := 0;

              l^.ent2.txtstr := shortstring (filename);   //used to construct attrib name in forms PopulateGrid method
              FmIMportXL.PopulateGrid;

              screen.cursor:= crDefault;

              FmImportXL.ShowModal;
              if FmImportXL.ModalResult = mrOk then begin

                // find or create an attribute with file|sheet name.
                ExcelNdx := FileSheetNdx (str255 (filename + '|' + FmImportXL.cbSheets.Text), true);

                if ExcelNdx > 0 then begin
                  // add or update attribute(s) that stores columm mappings
                  SaveColAssignment(atrname (atrname('SpdhXL' + inttostr(ExcelNdx)) + 'C'), FmImportXl.StringGrid1, 1);
                end;

                // Create spaces
                SvFillFlag := l^.doFill;
                SvFillClr := l^.FillColours[1];
                UserFieldValues  := TList<UserFieldValue>.create;

                if l^.UserFields.Count > 0 then for i := 0 to l^.UserFields.Count-1 do begin
                  UserFieldVal.tag := l^.UserFields[i].tag;
                  UserFieldVal.numeric := l^.UserFields[i].numeric;
                  case UserFieldVal.numeric of
                    str : begin
                            UserFieldVal.str := l^.UserFields[i].defaultstr;
                          end;
                    int : begin
                            UserFieldVal.int := l^.UserFields[i].defaultint;
                          end;
                    rl  : begin
                            UserFieldVal.rl := l^.UserFields[i].defaultrl;
                          end;
                    dis : begin
                            UserFieldVal.dis := l^.UserFields[i].defaultdis;
                          end;
                    ang : begin
                            UserFieldVal.ang := l^.UserFields[i].defaultang;
                          end;
                  end;

                  UserFieldValues.Add(UserFieldVal);
                end;

                DimUnit := FmImportXL.dceCustomUnits.NumValue;
                try
                  view_getcurr (view);
                  p.x := view.xr;
                  p.y := view.yr;
                  p.z := zbase;
                  for row := FmImportXl.StringGrid1.Selection.Top to FmImportXl.StringGrid1.Selection.Bottom do begin
                    if GetGridValue (FmImportXl.StringGrid1, '{hdim}', row, xdim) and
                       GetGridValue (FmImportXl.StringGrid1, '{vdim}', row, ydim) and
                       (xdim>0) and (ydim>0) then begin

                      if not GetGridValue (FmImportXl.StringGrid1, '{name}', row, name) then
                        name := '';
                      if not GetGridValue (FmImportXl.StringGrid1, '{nbr}', row, number) then
                        number := '';
                      if not GetGridValue (FmImportXl.StringGrid1, '{cat}', row, category) then
                        category := '';


                      if GetGridValue (FmImportXl.StringGrid1, '{fllvl}', row, FloorZ) then
                        FloorZ := FloorZ * DimUnit
                      else
                        FloorZ := zbase;

                      CatNdx := FindCategory (category);

                      if (catndx < 0) and (Category > '') then begin
                        AddCategoryViaForm(Category, UserFieldValues, catndx);
                        if catndx >=0 then begin
                          l^.FillColours[1] := l^.Categories[catndx].Standard.FillColor;
                          l^.doFill := l^.Categories[catndx].Standard.DoFill;
                        end;
                      end;

                      CeilingHt := zhite - zbase;
                      if GetGridValue (FmImportXl.StringGrid1, '{ceilng}', row, CeilingHt) then
                        CeilingHt := CeilingHt * DimUnit
                      else if (CatNdx >= 0) then begin
                        if not l^.Categories[catndx].Standard.UseZHt then
                          CeilingHt := l^.Categories[catndx].Standard.Height1;
                      end;

                      if (catndx < 0) then begin
                        l^.FillColours[1] := SvFillClr;
                        l^.doFill := SvFillFlag;
                      end
                      else begin
                        l^.doFill := l^.Categories[catndx].Standard.DoFill;
                        l^.FillColours[1] := l^.Categories[catndx].Standard.FillColor;
                      end;

                      // populate user defined fields
                      UserFieldValues.Clear;
                      if l^.UserFields.Count > 0 then begin
                        for i := 0 to l^.UserFields.Count-1 do begin
                          UserFieldVal.numeric := l^.UserFields[i].numeric;
                          UserFieldVal.tag := l^.UserFields[i].tag;
                          case UserFieldVal.numeric of
                            str : begin
                                    if not GetGridValue (FmImportXl.StringGrid1, string(UserFieldVal.tag),
                                                         row, UserFieldVal.str) then
                                      if not FindCatCustomVal then
                                        UserFieldVal.str := l^.UserFields[i].defaultstr;
                                    UserFieldValues.Add (UserFieldVal);
                                  end;
                            int : begin
                                    if not GetGridValue (FmImportXl.StringGrid1, string(UserFieldVal.tag),
                                                         row, UserFieldVal.int) then
                                      if not FindCatCustomVal then
                                        UserFieldVal.int := l^.UserFields[i].defaultint;
                                    UserFieldValues.Add (UserFieldVal);
                                  end;
                            rl  : begin
                                    if not GetGridValue (FmImportXl.StringGrid1, string(UserFieldVal.tag),
                                                         row, UserFieldVal.rl) then
                                      if not FindCatCustomVal then
                                        UserFieldVal.rl := l^.UserFields[i].defaultrl;
                                    UserFieldValues.Add (UserFieldVal);
                                  end;
                            dis : begin
                                    if GetGridValue (FmImportXl.StringGrid1, string(UserFieldVal.tag),
                                                   row, UserFieldVal.dis) then
                                      UserFieldVal.dis := UserFieldVal.dis * DimUnit
                                    else if not FindCatCustomVal then
                                      UserFieldVal.dis := l^.UserFields[i].defaultdis;
                                    UserFieldValues.Add (UserFieldVal);
                                  end;
                            ang : begin
                                    if GetGridValue (FmImportXl.StringGrid1, string(UserFieldVal.tag),
                                                   row, UserFieldVal.ang) then begin
                                      case pgSaveVar^.angstyl of
                                        4 : begin end; //radians, no conversion needed
                                        5 : UserFieldVal.ang := UserFieldVal.ang * Pi / 200  //Gradians
                                        else UserFieldVal.ang := UserFieldVal.ang * Pi / 180; //Degrees
                                      end;
                                    end
                                    else if not FindCatCustomVal then
                                      UserFieldVal.ang := l^.UserFields[i].defaultang;
                                    UserFieldValues.Add (UserFieldVal);
                                  end;
                          end;
                        end;
                      end;

                      stopgroup;
                      xdim := xdim * DimUnit;
                      ydim := ydim * DimUnit;
                      l^.ent := CreateDimRectangle (p, xdim, ydim, 0, 4);
                      p.x := p.x + xdim;
                      if not l^.DefineByCenter then
                        p.x := p.x + l^.WallWidth;

                      ProcessOffsetLine (true, not l^.DefineByCenter,
                                 l^.CreateAtSurface, l^.CreateAtCenter, l^.WallWidth);

                      CreateSpace (0, FloorZ, CeilingHt, Name, Number, Category, UserFieldValues, ExcelNdx, row);

                      ProcessVisibility (not l^.DefineByCenter,
                                         l^.CreateAtSurface, l^.CreateAtCenter);
                      DrawLabel;
                      GroupLabel;
                    end;
                  end;
                finally
                  UserFieldValues.Free;
                  l^.doFill := SvFillFlag;
                  l^.FillColours[1] := SvFillClr;
                end;


                if l^.RecalcReport then begin
                  i := 0;
                  while (i < 1000) and atr_sysfind ('SpdhR' +  atrname(IntToZeroFilledStr(i, 3)), atr) do begin
                    if (atr.atrtype = atr_str) and (atr.str[1] = 'A') then begin
                      doReport (false, atr.str);
                    end;
                    i := i+1;
                  end;
                  adr := atr_lyrfirst (getlyrcurr);
                  while atr_get (atr, adr) do begin
                    adr := atr_next (atr);
                    if (string(atr.name)).StartsWith('SpdhR') then begin
                      if atr_sysFind (atr.name, atr) then
                        doReport (false, atr.str);
                    end;
                  end;

                end;

              end;
            except
              on E : Exception do
                lpmsg_ok (62, [shortstring(E.Message)]); //Problem processing Excel file|(check file is valid and that Excel is installed on this computer)
            end;
          finally
              try
                if not (ExistingExcel and FmImportXL.ExcelApplication.Visible) then
                  FmIMportXL.wb.Close;
              except
              end;
              try
                FmImportXL.ExcelApplication := null;
              except
              end;
          end;
        finally
          FmImportXL.wb := null;
          FmImportXL.free;
        end;

      end;

    finally
      OpenDlg.Free;
    end;

  END;



  PROCEDURE ExcelExport;
  VAR
    FmExport : TfExportSelect;
    FileFound : boolean;
    xls, wb : OLEvariant;
    ExistingExcel : boolean;
    i, RowNum : integer;
    savedUpdating, SaveAlerts, abort : boolean;



    FUNCTION FindSheet (const SheetName : string; var ndx : integer): boolean;
    VAR
      i, Count : integer;
      ThisSheetName : string;
    BEGIN
      result := true;
      ndx := 0;
      Count := wb.WorkSheets.Count;
      for i := 1 to Count do begin
        ThisSheetName := wb.WorkSheets[i].Name;
        if ThisSheetName = SheetName then begin
          ndx := i;
          exit;
        end;
      end;
      result := false;
    END;  //FindSheet


    FUNCTION ProcessFileExport (FileSheet : string ; UseForAll : boolean;
                                RowsAbove : integer; DeleteReplace, AfterLast : boolean) : boolean;
    VAR
      FileName, SheetName, TempStr : string;
      TempNum : double;
      ndx, i, j, FlSheetNdx : integer;
      tempent, pct_ent : entity;
      atr, atr1, pct_Atr : attrib;
      pStandardAtr, pPctAtr : ^StandardAtr;
      disUnit : double;
      ExistData, CellRow : variant;
      ColMapping : xlMapping;
      MaxCol : integer;
      first : boolean;
      Range : OLEVariant;
      TempRec : SpaceListRecType;
      needRptPct, needDwgPct : boolean;
      DwgTotArea, RptTotArea : double;

      PROCEDURE PopulateColMapping (const xlIndex : integer;
                                    var MappingArray : xlMapping;
                                    var MaxCol : integer;
                                    out usesRptAreaPct, usesDwgAreaPct : boolean);
      var
        atr : attrib;
        i, atrNum : integer;
        rareapct_tag, dareapct_tag : str6;

      BEGIN
        rareapct_tag := str6(copy (Tags[tag_rareapct, 1], 2, length(Tags[tag_rareapct, 1])-2));
        dareapct_tag := str6(copy (Tags[tag_dareapct, 1], 2, length(Tags[tag_dareapct, 1])-2));
        usesRptAreaPct := false;
        usesDwgAreaPct := false;
        atrNum := 0;
        MaxCol := 0;
        while atr_sysfind (atrname('SpdhXL' + inttostr(xlIndex) + 'C' + IntToStr(atrNum)), atr) do begin
          MaxCol := (atrNum+1)*42;
          for i := 1 to 42 do begin
            MappingArray[atrNum*42 + i] := shortstring(trim(copy (string(atr.shstr), i*6-5, 6)));
            if MappingArray[atrNum*42 + i] = rareapct_tag then
              usesRptAreaPct := true;
            if MappingArray[atrNum*42 + i] = dareapct_tag then
              usesDwgAreaPct := true;
          end;
          inc (atrNum);
        end;
        while (length(MappingArray[MaxCol]) = 0) and (MaxCol > 1) do
          inc (MaxCol, -1);
      END; //PopulateColMapping

      FUNCTION FindNextSheet : boolean;
      VAR
        i, pipendx : integer;
      BEGIN
        result := false;
        for i := 1 to FmExport.SpaceList.Count-1 do begin
          if FmExport.SpaceList[i].Selected and FmExport.SpaceList[i].xlName.StartsWith(FileName) then begin
            pipendx := pos ('|', FileSheet);
            if pipendx > 0 then begin
              SheetName := copy (FileSheet, ndx+1, 256);
              result := true;
              exit;
            end
          end;
        end;
      END;   //FindNextSheet



    BEGIN
      result := false;
      disUnit := 1; //initialise to some value to prevent warning ... will be set properly later
      FileSheet := FileSheet.Replace(' | ', '|');  // May have spaces around the pipe character if exporting to existing link
      i := pos ('|', FileSheet);
      if i > 0 then begin
        FileName := copy (FileSheet, 1, i-1);
        SheetName := copy (FileSheet, i+1, 256);
      end
      else
        exit;

      try
        wb := xls.Workbooks.Item(FileName);
      except
        wb := xls.Workbooks.Open(FileName);
      end;

      try
        if VarIsNull (wb) then begin
          lpMsg_Error (62, [shortstring(FileName)]); //Problem processing Excel file|(check file is valid and that Excel is installed on this computer)||$
          exit;
        end;

        if not findSheet (SheetName, ndx) then begin
          wb.Worksheets.Add(After := wb.Worksheets[wb.Worksheets.Count]);
          ndx := wb.Worksheets.Count;
          wb.Worksheets[ndx].Name := SheetName;
        end;

        if UseForAll then begin
          RowNum := RowsAbove+1;
          if AfterLast then begin
            i := wb.Worksheets[ndx].Cells.Find('*',EmptyParam,EmptyParam,EmptyParam,xlByRows,xlPrevious,EmptyParam,EmptyParam).Row;
            RowNum := RowNum + i;
          end;
          // set up this file/sheet for every selected record.  Also set row number
          with FmExport do for i := 0 to SpaceList.Count-1 do begin
            TempRec := SpaceList[i];
            if TempRec.Selected then begin
              TempRec.xlName := FileSheet;
              TempRec.xlRow := RowNum;
              RowNum := RowNum + 1;
              SpaceList[i] := TempRec;
            end;
          end;
        end;

        FlSheetNdx := 0; //initialize this and following to some value to prevent compiler warnings
        RptTotArea := zero;
        DwgTotArea := zero;
        repeat
          with FmExport do begin
            first := true;
            for i := 0 to SpaceList.Count-1 do begin
              if SpaceList[i].Selected and (SpaceList[i].xlName.Replace(' | ', '|') = FileSheet) then begin
                SetSpaceListSelected (i, false);   // set flag to false so we don't process this row again
                if ent_get (tempent, SpaceList[i].adr) and
                   atr_entfind (tempent, STANDARD_ATR_NAME, atr) and (atr.atrtype = atr_str255) then begin
                  pStandardAtr := addr(atr.shstr);

                  if first then begin
                    if UseForAll then
                      FlSheetNdx := FileSheetNdx (str255(FileSheet), false)
                    else
                      FlSheetNdx := pStandardAtr.ExcelNdx;
                    PopulateColMapping (FlSheetNdx, ColMapping, MaxCol, needRptPct, needDwgPct);

                    RptTotArea := 0;
                    if needRptPct then begin
                      for j := 0 to SpaceList.Count-1 do
                        if (SpaceList[j].Selected or (j=i)) // need to check j=i because SpaceList[i].Selected will have been set to false to prevent it being processed twice even though it was originally true
                           and ent_get (pct_ent, SpaceList[j].adr) and
                           atr_entfind (pct_ent, STANDARD_ATR_NAME, pct_atr) and (pct_atr.atrtype = atr_str255)
                        then begin
                          pPctAtr := addr (pct_atr.shstr);
                          RptTotArea := RptTotArea + pPctAtr^.area;
                        end;
                      ent_get (tempent, SpaceList[i].adr);
                    end;

                    DwgTotArea := 0;
                    if needDwgPct then begin
                      for j := 0 to SpaceList.Count-1 do
                        if ent_get (pct_ent, SpaceList[j].adr) and
                           atr_entfind (pct_ent, STANDARD_ATR_NAME, pct_atr) and (pct_atr.atrtype = atr_str255)
                        then begin
                          pPctAtr := addr (pct_atr.shstr);
                          DwgTotArea := DwgTotArea + pPctAtr^.area;
                        end;
                      ent_get (tempent, SpaceList[i].adr);
                    end;

                    CellRow := VarArrayCreate([1, MaxCol], varVariant);
                    if atr_sysfind (atrname('SpdhXL' + inttostr(pStandardAtr.ExcelNdx) + 'UD'), atr1) and (atr1.atrtype = atr_dis) then
                      disunit := atr1.dis
                    else
                      disunit := DimUnitSize (UNITS_CLIP);
                    first := false;
                  end;

                  if UseForAll and UpdateLinks and (FlSheetNdx > 0) then begin
                    pStandardAtr.ExcelRow := SpaceList[i].xlRow;
                    pStandardAtr.ExcelNdx := FlSheetNdx;
                    atr_update (atr);
                  end;

                  Range := wb.WorkSheets[ndx].Range[wb.WorkSheets[ndx].Cells[SpaceList[i].xlRow , 1],
                                  wb.WorkSheets[ndx].Cells[SpaceList[i].xlRow, MaxCol]];
                  ExistData := Range.Value;

                  for j := 1 to MaxCol do begin
                    CellRow[j] := TagValue (string(ColMapping[j]), tempent,
                                            pStandardAtr^, disUnit, RptTotArea, DwgTotArea,
                                            TempStr, TempNum);
                    if (CellRow[j] = null) and (not DeleteReplace) then
                      CellRow[j] := ExistData[1, j];
                  end;
                  Range.value := CellRow;
                end;
              end;
            end;
          end;
        until not FindNextSheet;
      except
        on E : Exception do begin
          lpmsg_ok (62, [shortstring(E.Message)]); //Problem processing Excel file|(check file is valid and that Excel is installed on this computer)
          abort := true;
          exit;
        end;
      end;
      Range := Unassigned;
      try
        wb.save;
      except
        on E : Exception do begin
          lpmsg_ok (77, [shortstring(E.message)]);
        end;
      end;
      if not (FmExport.chbOpenExcel.checked or ExistingExcel) then begin
        wb.close;
        wb := Unassigned;
      end;

    END; //ProcessFileExport

  BEGIN
    FmExport := TfExportSelect.Create(Application);
    try
      if FmExport.ShowModal <> mrCancel then begin
        savedUpdating := true;   // initialise to some value to prevent compiler warning
        screen.cursor := crHourglass;
        if FmExport.ModalResult IN [mr_ExportSpecify, mr_ExportLinked]  then with FmExport do begin
          try
            xls := GetActiveOleObject('Excel.Application');
            ExistingExcel := true;
            SaveAlerts := xls.DisplayAlerts;
          except
            xls := CreateOleObject('Excel.Application');
            ExistingExcel := false;
            SaveAlerts := FmExport.chbOpenExcel.checked;
          end;

          try
            if VarIsNull(xls) then begin
              lpMsg_Error(62, ['']);
              exit;
            end;
            xls.DisplayAlerts := false;
            if not ExistingExcel then
              xls.Visible := FmExport.chbOpenExcel.checked;

            if (xls.WindowState = xlMinimized) then begin
              if FmExport.chbOpenExcel.checked then
                xls.WindowState := xlNormal
              else begin
                savedUpdating := xls.ScreenUpdating;
                xls.ScreenUpdating := true;
              end;

            end;

            abort :=false;
            if FmExport.ModalResult = mr_ExportLinked then begin
              filefound := true;
              for i := 0 to SpaceList.Count-1 do
                if SpaceList[i].Selected and (SpaceList[i].xlRow < 1) then
                  filefound := false;
                if not filefound then
                  lMsg_OK(125);   //Some of the selected spaces are not associated with an existing spreadsheet|They will not be exported.
              repeat
                FileFound := false;
                i := 0;
                while (not FileFound) and (i < SpaceList.Count) do
                  if SpaceList[i].Selected and (SpaceList[i].xlRow > 0) then begin
                    ProcessFileExport (SpaceList[i].xlName, false, 0, false, false);
                    FileFound := true;
                  end
                  else
                    inc (i);
              until abort or (not FileFound)
            end
            else if FmExport.ModalResult = mr_ExportSpecify then begin
              //set up file and row information in SpaceList
              ProcessFileExport (ExportFileName, true, FmExport.RowsAbove, FmExport.DeleteReplace, FmExport.AfterLast);
            end;

          finally
            try
              xls.DisplayAlerts := SaveAlerts;
              if FmExport.chbOpenExcel.checked then begin
                xls.visible := true;
                if xls.WindowState = xlMinimized then
                  xls.WindowState := xlNormal;
              end
              else if not ExistingExcel then begin
                if xls.WindowState = xlMinimized then
                  xls.ScreenUpdating := savedUpdating;
                xls := Unassigned;
                wb := Unassigned;
              end;
            except
            end;
          end;
        end
      end;

    finally
      FmExport.Free;
      screen.Cursor := crDefault;
    end;
  END;

end.
