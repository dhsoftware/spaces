unit NewCatgry;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CommonStuff, Vcl.ExtCtrls, Vcl.StdCtrls, UInterfaces,
  Vcl.Menus, ClrUtil, CeilingOptions, DcadEdit, URecords, System.Generics.Collections, Language,
  Settings, UDCLibrary;

type
  CatCustField = record
    Lbl : TLabel;
    Dft : TCheckBox;
    Input : TDcadEdit;
//    key : atrname;
  end;

type
  TfNewCategory = class(TForm)
    lblCat: TLabel;
    lblCatName: TLabel;
    lblUpdtCfm: TLabel;
    chbCatSolidFill: TCheckBox;
    pCatColour: TPanel;
    MainMenu1: TMainMenu;
    CustomFillColours1: TMenuItem;
    pCatCeiling: TPanel;
    Options1: TMenuItem;
    btnOK: TButton;
    sbCatUserFields: TScrollBox;
    lblUserFieldName1: TLabel;
    lblUserFieldName2: TLabel;
    chbUserDefault1: TCheckBox;
    dceUserFieldValue1: TDcadEdit;
    chbUserDefault2: TCheckBox;
    dceUserFieldValue2: TDcadEdit;
    lblUserFields: TLabel;
    lblUserFieldName: TLabel;
    lblUserFieldDflt: TLabel;
    lblUserFieldValue: TLabel;
    Shape2: TShape;
    shLine: TShape;
    procedure FormCreate(Sender: TObject);
    procedure CustomFillColours1Click(Sender: TObject);
    procedure pCatColourClick(Sender: TObject);
    procedure chbCatSolidFillClick(Sender: TObject);
    procedure chbUserDefaultClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
//    CatNum : integer;
    fCeilingOptions : TfCeilingOptions;
    CatCustFields : array of CatCustField;
    procedure SetUserFieldDflt (ndx : integer);
    procedure CreateCatCustFields (UserFieldValues  : TList<UserFieldValue>);
  end;

var
  fNewCategory: TfNewCategory;

implementation

{$R *.dfm}

procedure TfNewCategory.SetUserFieldDflt (ndx : integer);
begin
    case l^.UserFields[ndx].numeric of
      str : begin
              CatCustFields[ndx].Input.Text := string(l^.UserFields[ndx].defaultstr);
            end;
      int : begin
              CatCustFields[ndx].Input.IntValue := l^.UserFields[ndx].defaultint;
            end;
      rl  : begin
              CatCustFields[ndx].Input.NumValue := l^.UserFields[ndx].defaultrl;
            end;
      dis : begin
              CatCustFields[ndx].Input.NumValue := l^.UserFields[ndx].defaultdis;
            end;
      ang : begin
              CatCustFields[ndx].Input.NumValue := l^.UserFields[ndx].defaultang;
            end;
    end;
end;


procedure TfNewCategory.chbUserDefaultClick(Sender: TObject);
var
  ndx : integer;
begin
  ndx := (Sender as TCheckbox).tag - 1;
  CatCustFields[ndx].Input.Enabled := not (Sender as TCheckbox).Checked;
  if (Sender as TCheckbox).Checked then
    SetUserFieldDflt (ndx);
end;

procedure TfNewCategory.CreateCatCustFields (UserFieldValues  : TList<UserFieldValue>);
var
  i : integer;
begin
  if l^.UserFields.Count = 0 then begin
    lblUserFields.Visible := false;
    shLine.Visible := false;
    lblUserFieldName.Visible := false;
    lblUserFieldDflt.Visible := false;
    lblUserFieldValue.Visible := false;
    lblUserFieldName1.Visible := false;
    chbUserDefault1.Visible := false;
    dceUserFieldValue1.Visible := false;
    lblUserFieldName2.Visible := false;
    chbUserDefault2.Visible := false;
    dceUserFieldValue2.Visible := false;
    exit;
  end;
  if l^.UserFields.Count = 1 then begin
    lblUserFieldName2.Visible := false;
    chbUserDefault2.Visible := false;
    dceUserFieldValue2.Visible := false;
  end;

  setlength (CatCustFields, l^.UserFields.Count);
  for i := 0 to (l^.UserFields.Count - 1) do begin
    if i = 0 then begin
      CatCustFields[0].Lbl := lblUserFieldName1;
      CatCustFields[0].Dft := chbUserDefault1;
      CatCustFields[0].Input := dceUserFieldValue1;
    end
    else if i = 1 then begin
      CatCustFields[1].Lbl := lblUserFieldName2;
      CatCustFields[1].Dft := chbUserDefault2;
      CatCustFields[1].Input := dceUserFieldValue2;
    end
    else begin
      CatCustFields[i].Lbl := TLabel.Create(lblUserFieldName1.Owner);
      CatCustFields[i].Lbl.Parent := lblUserFieldName1.Parent;
      CatCustFields[i].Lbl.Top := lblUserFieldName2.Top + (i-1)*(lblUserFieldName2.Top - lblUserFieldName1.Top);
      CatCustFields[i].Lbl.Left := lblUserFieldName1.Left;
      CatCustFields[i].Lbl.AutoSize := false;
      CatCustFields[i].Lbl.Width := lblUserFieldName1.Width;
      CatCustFields[i].Lbl.Height := lblUserFieldName1.Height;
      CatCustFields[i].Lbl.Name := 'lblUserFieldName' + IntToStr(i+1);
      CatCustFields[i].Lbl.Anchors := lblUserFieldName1.Anchors;

      CatCustFields[i].Dft := TCheckBox.Create(chbUserDefault1.Owner);
      CatCustFields[i].Dft.Parent := chbUserDefault1.Parent;
      CatCustFields[i].Dft.Top := CatCustFields[i].Lbl.Top;
      CatCustFields[i].Dft.Left := chbUserDefault1.Left;
      CatCustFields[i].Dft.Width := chbUserDefault1.Width;
      CatCustFields[i].Dft.Height := chbUserDefault1.Height;
      CatCustFields[i].Dft.Name := 'chbUserDefault' + IntToStr(i+1);
      CatCustFields[i].Dft.OnClick := chbUserDefault1.OnClick;
      CatCustFields[i].Dft.Caption := '';
      CatCustFields[i].Dft.Anchors := chbUserDefault1.Anchors;
      CatCustFields[i].Dft.Tag := i+1;
      CatCustFields[i].Dft.OnClick := chbUserDefault1.OnClick;

      CatCustFields[i].Input := TDcadEdit.Create(dceUserFieldValue1.Owner);
      CatCustFields[i].Input.Parent := dceUserFieldValue1.Parent;
      CatCustFields[i].Input.Top := CatCustFields[i].Lbl.Top - 2;
      CatCustFields[i].Input.Left := dceUserFieldValue1.Left;
      CatCustFields[i].Input.Width := dceUserFieldValue1.Width;
      CatCustFields[i].Input.Anchors := dceUserFieldValue1.Anchors;
      CatCustFields[i].Input.Height := dceUserFieldValue1.Height;
      CatCustFields[i].Input.Name := 'dceUserFieldValue' + IntToStr(i+1);
    end;

    CatCustFields[i].Lbl.Caption := string(l^.UserFields[i].desc);

    case l^.UserFields[i].numeric of
      str : begin
              CatCustFields[i].Input.NumberType := DCADEdit.TextStr;
              CatCustFields[i].Input.Text := string(UserFieldValues[i].str);
              CatCustFields[i].Dft.Checked := (UserFieldValues[i].str = l^.UserFields[i].defaultstr);
            end;
      int : begin
              CatCustFields[i].Input.NumberType := DCADEdit.IntegerNum;
              CatCustFields[i].Input.IntValue := UserFieldValues[i].int;
              CatCustFields[i].Dft.Checked := (UserFieldValues[i].int = l^.UserFields[i].defaultint);
            end;
      rl  : begin
              CatCustFields[i].Input.NumberType := DCADEdit.DecimalNum;
              CatCustFields[i].Input.NumValue := UserFieldValues[i].rl;
              CatCustFields[i].Dft.Checked := (UserFieldValues[i].rl = l^.UserFields[i].defaultrl);
            end;
      dis : begin
              CatCustFields[i].Input.NumberType := DCADEdit.Distance;
              CatCustFields[i].Input.NumValue := UserFieldValues[i].dis;
              CatCustFields[i].Dft.Checked := (UserFieldValues[i].dis = l^.UserFields[i].defaultdis);
            end;
      ang : begin
              if pgSaveVar^.angstyl = 3 then
                CatCustFields[i].Input.NumberType := DCADEdit.DecDegrees
              else
                CatCustFields[i].Input.NumberType := DCADEdit.DegMinSec;
              CatCustFields[i].Input.NumValue := UserFieldValues[i].ang;
              CatCustFields[i].Dft.Checked := (UserFieldValues[i].ang = l^.UserFields[i].defaultang);
            end;
    end;
    CatCustFields[i].Input.Enabled := not CatCustFields[i].Dft.Checked;
  end;
end;


procedure TfNewCategory.chbCatSolidFillClick(Sender: TObject);
begin
  pCatColour.Visible := chbCatSolidFill.Checked;
end;

procedure TfNewCategory.CustomFillColours1Click(Sender: TObject);
begin
  l^.Options.CustomClr := not l^.Options.CustomClr;
  SaveOptions;
  CustomFillColours1.Checked := l^.Options.CustomClr;
end;

procedure TfNewCategory.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  SaveFormPos (TForm(Sender));
end;

procedure TfNewCategory.FormCreate(Sender: TObject);
begin
  SetFormPos (TForm(Sender));

  LabelCaption (141, lblCat); //Category Name:
  LabelCaption (142, lblUpdtCfm); //Update/Confirm default values for this new category:
  CheckCaption (114, chbCatSolidFill); //Use Solid Fill
  PanelLblParams(106, [], pCatColour);  //Select Colour|Select Fill Colour
  LabelCaption (129, lblUserFields);  //User Defined Fields
  LabelCaption (143, lblUserFieldName); //Field Name
  LabelCaption (144, lblUserFieldDflt);  //User Default
  LabelCaption (145, lblUserFieldValue);  //Value
  ButtonLblParams(108, [], btnOK);  //OK
  MenuItemCaption(110, Options1);  //Options
  MenuItemCaption (126, CustomFillColours1);  //Custom Fill Colours

  chbCatSolidFill.Checked := l^.doFill;
  pCatColour.Visible := l^.doFill;
  if l^.FillColours[1] = 0 then begin
    pCatColour.Color := DCADRGBtoRGB (LineColor);
    FillClrPnlCaption (pCatColour, LineColor);
  end
  else begin
    pCatColour.Color := DCADRGBtoRGB (l.FillColours[1]);
    FillClrPnlCaption (pCatColour, l.FillColours[1]);
  end;
  pCatColour.Font.color := PnlFontClr (pCatColour.Color);

  fCeilingOptions := TfCeilingOptions.Create(self);
  fCeilingOptions.Parent := pCatCeiling;
  fCeilingOptions.Top := 0;
  fCeilingOptions.Left := 0;
  fCeilingOptions.Color := pCatCeiling.Color;
  fCeilingOptions.Visible := true;

  CustomFillColours1.Checked := l^.Options.CustomClr;
end;

procedure TfNewCategory.pCatColourClick(Sender: TObject);
var
  ClrD : TColorDialog;
  TempClr  : longint;
begin
    if l^.Options.CustomClr then begin
      ClrD := TColorDialog.Create(Application);
      try
        ClrD.Color := pCatColour.Color;
        ClrD.Options := [cdFullOpen];
        if ClrD.Execute then begin
          pCatColour.Color := ClrD.Color;
          TempClr := RGBtoDCADRGB (pCatColour.Color);
          pCatColour.Caption := string(ColourName (TempClr));
        end;
      finally
        ClrD.Free;
      end;
    end
    else begin
      TempClr := RGBtoDCADRGB (pCatColour.Color);
      GetColorIndex(TempClr);
      pCatColour.Color := DCADRGBtoRGB (TempClr);
      pCatColour.Caption := string(ColourName (TempClr));
    end;
end;

end.
