unit LoadFromAttribs;

interface

uses CommonStuff, URecords, UInterfaces, UConstants, UInterfacesRecords,
     SysUtils, StrUtils, System.Generics.Collections, Catgries, UserFields;

procedure DoLoadFromAttribs;

implementation

procedure DoLoadFromAttribs;

type
  UserFieldNumValue = record
    Num : str2;
    value : UserFieldValue;
  end;

var
  addr : lgl_addr;
  atr : attrib;
  Cat, NewCat : CategoryDef;
  UFD : UserFieldDef;
  UFV : UserFieldNumValue;
  Ndx, i : integer;
  badatr, catExists : boolean;
  UnasignedUFVs : TList<UserFieldNumValue>;
begin
  l^.UserFields := TList<UserFieldDef>.Create;
  CreateCategories;
  UnasignedUFVs := TList<UserFieldNumValue>.Create;
  addr := atr_sysfirst;
  while atr_get (atr, addr) do begin
    if AnsiStartsStr('SpdhUF', string(atr.name)) and (Length(atr.name) > 6) then begin
      {User Defined Field Record}
//      UFD.tag := copy (atr.name, 7, 6);
      UFD := DecodeUserFieldAtr (atr);
{      UFD.desc := copy (atr.str, 2, 80);
      case atr.str[1] of
        'I' : UFD.numeric := int;
        'R' : UFD.numeric := rl;
        'D' : UFD.numeric := dis;
        'A' : UFD.numeric := ang;
         else UFD.numeric := str;
      end;  }
      l^.UserFields.Add(UFD);
    end
    else if AnsiStartsStr('SpdhCat' ,string(atr.name)) then begin
      {OLD Category Record}
      Cat.num := shortstring(atr.name[8]) + shortstring(atr.name[9]);
      Cat.Standard.str := atr.str;
      Cat.Custom := TList<UserFieldValue>.Create;
      catExists := false;
      if Cat.Standard.Name > '' then begin
        for i := 0 to l^.Categories.Count-1 do begin
          if l^.Categories.Items[i].Standard.Data.name = Cat.Standard.name then
            catExists := true;
        end;
        if not catExists then begin
          NewCat.Standard.Data.version := 80;
          NewCat.Standard.Data.UseZHt := Cat.Standard.UseZHt;
          NewCat.Standard.Data.DoFill := Cat.Standard.DoFill;
          NewCat.Standard.Data.FillOpacity := 255;
          NewCat.Standard.Data.FillColor := Cat.Standard.FillColor;
          NewCat.Standard.Data.Height1 := Cat.Standard.Height1;
          NewCat.Standard.Data.Height2 := Cat.Standard.Height2;
          NewCat.Standard.Data.Height3 := Cat.Standard.Height3;
          NewCat.Standard.Data.Slope1 := Cat.Standard.Slope1;
          NewCat.Standard.Data.Slope2 := Cat.Standard.Slope2;
          NewCat.Standard.Data.name := Cat.Standard.name;
          NewCat.Standard.Data.CeilingType := Cat.Standard.CeilingType;
          NewCat.num := Cat.Num;
          NewCat.Custom := Cat.Custom;
          NewCat.updated := false;
          l^.Categories.Add (NewCat);
        end;
        {remove OLD category record, replace with new one}
        if atr_get (atr, atr.addr) then
          atr_delsys (atr);
        atr_init (atr, atr_str);
        atr.name := 'SdhCat' + Cat.Num;
        atr.str := NewCat.Standard.str;
        atr_add2sys (atr);
      end
      else
        atr_delsys (atr);
    end
    else if AnsiStartsStr('SdhCat' ,string(atr.name)) then begin
        {NEW Category Record}
      Cat.num := shortstring(atr.name[7]) + shortstring(atr.name[8]);
      Cat.Standard.str := atr.str;
      Cat.Custom := TList<UserFieldValue>.Create;
      catExists := false;
      for i := 0 to l^.Categories.Count-1 do begin
        if l^.Categories.Items[i].Standard.Data.name = Cat.Standard.Data.name then
          catExists := true;
      end;
      if not catExists then
        l^.Categories.Add (Cat);
    end
    else if AnsiStartsStr('SdhC' ,string(atr.name)) then begin
      {User defined value for category}
      UFV.num := copy (atr.name, 5, 2);        //' + atr.name[5] + atr.name[6];
      UFV.value.tag := copy (atr.name, 7, 6);
      badatr := false;
      case atr.atrtype of
        atr_str : begin
                    UFV.value.numeric := str;
                    UFV.value.str := atr.str;
                  end;
        atr_int : begin
                    UFV.value.numeric := int;
                    UFV.value.int := atr.int;
                  end;
        atr_rl  : begin
                    UFV.value.numeric := rl;
                    UFV.value.rl := atr.rl;
                  end;
        atr_dis : begin
                    UFV.value.numeric := dis;
                    UFV.value.dis := atr.dis;
                  end;
        atr_ang : begin
                    UFV.value.numeric := ang;
                    UFV.value.ang := atr.ang;
                  end;
        atr_area: begin
                    UFV.value.numeric := UserFieldType.area;
                    UFV.value.area := atr.rl;
                  end;
        else badatr := true;
      end;


      if not badatr then begin
        if GetCatIndex (UFV.Num, Ndx) then begin
          l^.Categories.Items[Ndx].Custom.Add(UFV.value)
        end
        else begin
          UnasignedUFVs.Add(UFV);
        end;
      end;
    end;

    addr := atr_next (atr);
  end;

  if UnasignedUFVs.Count > 0 then begin
    for i := 0 to UnasignedUFVs.Count-1 do
      if GetCatIndex (UnasignedUFVs[i].Num, Ndx) then
        l^.Categories.Items[Ndx].Custom.Add(UnasignedUFVs[i].value);
  end;
  UnasignedUFVs.Free;

  l^.Categories.Sort;

end;


end.
