unit StrUtil;


interface
uses SysUtils, DateUtils, CommonStuff, strutils, Settings, math, UInterfaces,
     URecords, vcl.graphics, WinAPI.Windows;

FUNCTION BlankString (s : string): boolean;   overload;
FUNCTION BlankString (s : shortstring): boolean;   overload;

FUNCTION cvstint (str : shortstring; var i : integer) : boolean;
PROCEDURE AppendDateTimeStamp (var Str : String;
                               DateTime : TDateTime;
															 doDate : boolean;
															 doTime : boolean;
															 TimeColons : boolean;
															 spacebetween : boolean;
															 dohundredths : boolean);  overload;
PROCEDURE AppendDateTimeStamp (var Str : shortstring;
                               DateTime : TDateTime;
															 doDate : boolean;
															 doTime : boolean;
															 TimeColons : boolean;
															 spacebetween : boolean;
															 dohundredths : boolean);  overload;
PROCEDURE AppendDateTimeStamp (var Str : String;
															 doDate : boolean;
															 doTime : boolean;
															 TimeColons : boolean;
															 spacebetween : boolean;
															 dohundredths : boolean);  overload;
PROCEDURE AppendDateTimeStamp (var Str : ShortString;
															 doDate : boolean;
															 doTime : boolean;
															 TimeColons : boolean;
															 spacebetween : boolean;
															 dohundredths : boolean);  overload;

FUNCTION IncrementNumber (number : shortstring; increment : integer) : string;

PROCEDURE AreaString (area : real; AreaSource : AreaSourceTyp; Destination : integer;
                      var str_area : string; var str_units : string);
PROCEDURE DisString (dis : real; Destination : integer;
                     var s_dis : string; var s_units : string);
PROCEDURE VolString (r_vol : real; Destination : integer;
                     var s_vol : string; var s_units : string);
FUNCTION AngString (ang : real) : shortstring;

FUNCTION IntToZeroFilledStr (num, len : integer) : string;

FUNCTION DisStr (dis : double) : shortstring;
FUNCTION PointDisStr (pnt : URecords.point) : shortstring;


implementation

FUNCTION BlankString (s : string): boolean;
BEGIN
  result := length(Trim(s)) = 0;
END;

FUNCTION BlankString (s : shortstring): boolean;
BEGIN
  result := length(s) = 0;
  if not result then
    result := BlankString(string(s));
END;

FUNCTION cvstint (str : shortstring; var i : integer) : boolean;
BEGIN
  try
    i := strtoint (string(str));
  except
    result := false;
    exit;
  end;
  result := true;
END;

FUNCTION AngString (ang : real) : shortstring;
/// this function added as DataCAD's cvangst procedure appears to always round to
/// nearest degree when andstyl is 3 (decimal degrees).
/// This function decimal places as required
VAR
  d : double;
BEGIN
  if pgSaveVar^.angstyl = 3 then begin
    d := ang*180/Pi;
    result := shortstring(floattostrf (d, ffGeneral, 8, 2));
    if AnsiContainsText (string(result), FormatSettings.DecimalSeparator) then begin
      while result[length(result)] = '0' do
        delete (result, length(result), 1);
      if result[length(result)] in [FormatSettings.DecimalSeparator] then
        delete (result, length(result), 1);
    end;
  end
  else
    cvangst (ang, result);
END;

PROCEDURE AppendDateTimeStamp (var Str : String;
                               DateTime : TDateTime;
															 doDate : boolean;
															 doTime : boolean;
															 TimeColons : boolean;
															 spacebetween : boolean;
															 dohundredths : boolean);  overload;
VAR
	y,m,d,h,mi,s,ms, hu	: word; //year, month, day, hours, minutes, seconds, millisecs, hundredths
	tempstr				: string;


	PROCEDURE Append2DigitStr (const num : word);
	// actually allows more than 2 digits, but inserts a zero at the front of the string if it is less than 2 digits
	BEGIN
		tempstr := inttostr(num);
		if length(tempstr)<2 then begin
			tempstr := '0' + tempstr;
		end;
		 str := str + tempstr;
	END; //Append2DigitStr;

BEGIN
	if not (doDate or doTime) then begin
		exit;
	end;
	DecodeDateTime (DateTime, y, m, d, h,mi, s, ms);
  y := y mod 100;
	if doDate then begin
		Append2DigitStr (y);
		Append2DigitStr (m);
		Append2DigitStr (d);
		if doTime and spacebetween then begin
			 str := str + ' ';
		end;
	end;
	if DoTime then begin
		Append2DigitStr (h);
		if timecolons then begin
			 str := str + ':';
		end;
		Append2DigitStr (mi);
		if timecolons then begin
			 str := str + ':';
		end;
		Append2DigitStr (s);
		if timecolons and dohundredths then begin
			 str := str + ':';
		end;
		if dohundredths then begin
      hu := ms div 100;
			Append2DigitStr (hu);
		end;
	end;
END; //AppendDateTimeStamp;

PROCEDURE AppendDateTimeStamp (var Str : String;
															 doDate : boolean;
															 doTime : boolean;
															 TimeColons : boolean;
															 spacebetween : boolean;
															 dohundredths : boolean);
BEGIN
  AppendDateTimeStamp (Str, now, doDate, doTime, TimeColons, spacebetween, dohundredths);
END; //AppendDateTimeStamp;

PROCEDURE AppendDateTimeStamp (var Str : shortstring;
                               DateTime : TDateTime;
															 doDate : boolean;
															 doTime : boolean;
															 TimeColons : boolean;
															 spacebetween : boolean;
															 dohundredths : boolean);
VAR
  s : string;
BEGIN
  s := string(Str);
  AppendDateTimeStamp (s, DateTime, doDate, doTime, TimeColons, spacebetween, dohundredths);
  Str:= ShortString (s);
END;


PROCEDURE AppendDateTimeStamp (var Str : ShortString;
															 doDate : boolean;
															 doTime : boolean;
															 TimeColons : boolean;
															 spacebetween : boolean;
															 dohundredths : boolean);
VAR
  s : String;
BEGIN
  s := string(Str);
  AppendDateTimeStamp (s, Now, doDate, doTime, TimeColons, spacebetween, dohundredths);
  Str:= ShortString (s);
END;


FUNCTION IncrementNumber (number : shortstring; increment : integer) : string;
VAR
  i, j : integer;
  sn, se: string;
BEGIN
  if increment = 0 then begin
    result := string (number);
    exit;
  end;
  i := length (number);
  if i=0 then begin
    result := '';
    exit;
  end;
  se := '';
  while (i > 0) and (not (number[i] in ['0'..'9'])) do begin
    se := se + string(number[i]);
    delete (number, i, 1);
    i := i-1;
  end;
  if length(number) = 0 then begin
    result := ansireversestring (se);
    exit;
  end;

  i := length (number);
  while (i > 0) and (number[i] in ['0'..'9']) do begin
    sn := sn + string(number[i]);
    delete (number, i, 1);
    i := i-1;
  end;

  sn := ansireversestring (sn);
  j := length (sn);
  if j > 0 then begin
    i := StrToInt(sn);
    i := i + increment;
    sn := IntToStr(i);
    while length(sn) < j do
      sn := '0' + sn;
  end;

  result := string(number) + sn + ansireversestring (se);
END;


PROCEDURE InsertCommas (var str : string);
VAR
	i : integer;
BEGIN
	i := length (str) - 3;
	while i > 0 do begin
		insert (',', str, i+1);
		i := i-3;
	end;
END; // InsertCommas


FUNCTION FractionalString (const num : Double;
                           Fractions : FractionType;
                           Rounding : RoundingType) : string;
VAR
	WholeNum : double;
	fract : double;
	iFract : integer;
	Commas : boolean;
  Denominator : integer;

BEGIN
  Denominator := round(math.Power (2.0, ord(Fractions)));
	Commas := GetSvdBln ('dhSPCommas', true);
	WholeNum := System.Int (num);
  Fract := Frac (num);
	case Rounding of
		RoundNear : iFract := round (fract*Denominator);
    RoundDown : iFract := trunc (fract*Denominator);
    RoundUp   : iFract := round (fract*Denominator + 0.4999999999999);
  end;
	if (iFract = Denominator) then begin
		WholeNum := System.int (WholeNum +1.0001);
		result := WholeNum.ToString;
		if commas then
			InsertCommas (result);
		exit;
  end
	else begin
		while (iFract mod 2 = 0) and (iFract > 0) do begin
			iFract := iFract div 2;
			Denominator := Denominator div 2;
		end;
    result := WholeNum.ToString;
		if Commas then
			InsertCommas (result);
    if iFract > 0 then
      result := result + ' ' + iFract.ToString + '/' + Denominator.toString;
	end;
END; //FractionalString


FUNCTION DecimalString (const num : double; SigDig : integer; RemTrail0 : boolean;
                        Rounding : RoundingType) : string;
VAR
	i, j : integer;
	Commas, RndUp, RndDone : boolean;
BEGIN
	Commas := GetSvdBln ('dhSPCommas', true);
	result := num.ToString;
	i := ansipos ('.', result);
	if i = 0 then begin
		if (SigDig > 0) and (not RemTrail0) then begin
			result := result + '.';
			for j := 1 to SigDig do
        result := result + '0';
		end;
	end
  else begin
		if length (result) > (i + SigDig) then begin
			case Rounding of
        RoundNear:
          RndUp := result[i+SigDig+1] > '4';
        RoundUp : begin
          RndUp := false;
          for j := (i+SigDig+1) to length(result) do
            if result[j] > '0' then
              RndUp := true;
        end
        else RndUp := false;
      end;
      SetLength(result, i+SigDig);
      if RndUp then begin
        RndDone := false;
        for i := length(result) downto 1 do begin
          if not RndDone then begin
            if result[i] <> '.' then begin
              if result[i] < '9' then begin
                result[i] := char (ord(result[i]) + 1);
                RndDone := true;
              end
              else begin
                result[i] := '0';
              end;
            end;
          end;
        end;
        if not RndDone then begin
          result := '1' + result;
        end;
      end;

		end;
		if RemTrail0 then begin
			while result.EndsWith ('0') do begin
				setlength (result, length(result)-1);
			end;
    end
    else begin
      i := ansipos ('.', result);
      while length (result) < (i + SigDig) do
        result := result + '0';
		end;
		if result.EndsWith ('.') then begin
			setlength (result, length(result)-1);
		end;
	end;
	if Commas then begin
		i := ansipos ('.', result);
		if i = 0 then begin
			i := length(result) - 3;
		end else begin
			i := i-4;
		end;
		while i > 0 do begin
			insert (',', result, i+1);
			i := i-3;
		end;
	end;
END; //Decimalresulting;

FUNCTION FeetInches (const num : Real; Rounding : RoundingType) : string;
VAR
	r_feet, r_inches : double;
	i_feet, i_inches : integer;
BEGIN
	r_feet := num/(32*12);
	i_feet := trunc(r_feet);
	r_inches := (r_feet-i_feet) * 12.0;

  case Rounding of
	  RoundUp :  i_inches := round (r_inches + 0.49999999);
    RoundDown : i_inches := trunc (r_inches + 0.00000001)
    else i_inches := round (r_inches);
	end;
	if i_inches = 12 then begin
		i_feet := i_feet + 1;
		i_inches := 0;
	end;
	result := i_feet.ToString;
	if GetSvdBln ('dhSPCommas', true) then begin
		InsertCommas (result);
	end;
  result := result + '''-' + i_inches.ToString + '"';
END; //FeetInches;


PROCEDURE AreaString (area : real; AreaSource : AreaSourceTyp; Destination : integer;
                      var str_area : string; var str_units : string);

VAR
  RecordToUse : AreaRecords;
  MyDestination : integer;

	FUNCTION StringArea (area : real) : string;
	BEGIN
		if abs(area) < 0.001 then begin
			result := '0';
		end
    else if l^.AreaSettings[RecordToUse, MyDestination].Fractions = Fr_Decimal then begin
			result := DecimalString (area,
                              l^.AreaSettings[RecordToUse, MyDestination].DecPlaces,
                              l^.AreaSettings[RecordToUse, MyDestination].RemTrailingZero,
                              l^.AreaSettings[RecordToUse, MyDestination].Rounding);
		end
    else begin
			result := FractionalString (area,
                                  l^.AreaSettings[RecordToUse, MyDestination].Fractions,
                                  l^.AreaSettings[RecordToUse, MyDestination].Rounding);
		end;
	END; // StringArea

BEGIN
  CASE AreaSource OF
    Flr     : RecordToUse := MainAreaRec;
    Wall    : if l^.AltWallArea then
                RecordToUse := AltAreaRec
              else
                RecordToUse := MainAreaRec;
    CustArea: if l^.AltCustomArea then
                RecordToUse := AltAreaRec
              else
                RecordToUse := MainAreaRec;
    Ceiling : if l^.AltCeilingArea then
                RecordToUse := AltAreaRec
              else
                RecordToUse := MainAreaRec;
    Door    : if l^.AltWallArea then
                RecordToUse := AltAreaRec
              else
                RecordToUse := MainAreaRec;
    Window  : if l^.AltWallArea then
                RecordToUse := AltAreaRec
              else
                RecordToUse := MainAreaRec;
  END;
  if (RecordToUse = AltAreaRec) and (l^.AreaSettings[AltAreaRec, Destination].AreaTyp = AltSameAsMain) then
    RecordToUse := MainAreaRec;

  MyDestination := Destination;
  if l^.AreaSettings[RecordToUse, Destination].AreaTyp = AreaSameRpt then
    MyDestination := UNITS_RPT;
  if l^.AreaSettings[RecordToUse, MyDestination].AreaTyp = AreaSameLbl then
    MyDestination := UNITS_LBL;

  with l^.AreaSettings[RecordToUse, MyDestination] do begin
    if AreaTyp = CustomArea then begin
      str_area := StringArea (area / CustUnitSize);
      str_units := string(CustUnitDisplay);
    end
    else begin
      Case AreaTyp of
        SqFt    : str_area := StringArea (area / SQFTCONV);
        Squares : str_area := StringArea (area / SQCONV);
        Acres   : str_area := StringArea (area / ACRECONV);
        SqMtr   : str_area := StringArea (area / SQMCONV);
        Hectares: str_area := StringArea (area / HECTARECONV);
      End;
      str_units := string (DecUnitDisplay);
    end;
  end;

END; //AreaString;


PROCEDURE DisString (dis : real; Destination : integer;
                     var s_dis : string; var s_units : string);
VAR
	d : double;
  MyDestination, i : integer;
  ss : shortstring;
BEGIN
  if l^.DimensionSettings[Destination].DimTyp = SameAsLbl then begin
    if l^.DimensionSettings[UNITS_LBL].DimTyp = SameAsRpt then
      MyDestination := UNITS_RPT
    else
      MyDestination := UNITS_LBL
  end
  else if l^.DimensionSettings[Destination].DimTyp = SameAsRpt then begin
    if l^.DimensionSettings[UNITS_RPT].DimTyp = SameAsLbl then
      MyDestination := UNITS_LBL
    else
      MyDestination := UNITS_RPT
  end
  else
    MyDestination := Destination;

 with l^.DimensionSettings[MyDestination] do begin
    case DimTyp of
      CustomDim : begin
          s_units := string(CustUnitDisplay);
          if Fractions = Fr_Decimal then
            s_dis := DecimalString (dis/CustUnitSize, DecPlaces, RemTrailingZero, Rounding)
          else
            s_dis := FractionalString (dis/CustUnitSize, Fractions, Rounding);
        end;
      Current : begin
          cvdisst(dis, ss);
          s_dis := string(ss);
          s_units := '';
          if PGSavevar^.stakfrac and (MyDestination = UNITS_LBL) and (PGSaveVar^.scaletype=0) then begin
            i := ansipos ('/', s_dis);
            if i > 0 then begin
              //add an escape character to tell label creation routine to create stacked fraction
              repeat i := i-1 until (s_dis[i] = ' ') or (i=1);
              insert (char(27), s_dis, i);
            end;
          end;
        end;
      FeetInch : begin
          s_units := '';
          s_dis := FeetInches(dis, Rounding)
        end
      else begin
        s_units := string (DecUnitDisplay);
        case DimTyp of
          DecFt : d := dis/384;
          Mtr : d := dis/1259.8425196850393700787401574803;
          Cm : d := dis/12.598425196850393700787401574803;
          Mm : d := dis/1.2598425196850393700787401574803
          else d := 0;  // should never happen
        end;
        if Fractions = Fr_Decimal then
          s_dis := DecimalString (d, DecPlaces, RemTrailingZero, Rounding)
        else
          s_dis := FractionalString (d, Fractions, Rounding);
      end;
    end;
  end;
END; //DisString;


PROCEDURE VolString (r_vol : real; Destination : integer;
                     var s_vol : string; var s_units : string);
VAR
	v : double;
  MyDestination : integer;

BEGIN
  MyDestination := Destination;
  if l^.DimensionSettings[Destination].DimTyp = SameAsRpt then
    MyDestination := UNITS_RPT;
  if l^.DimensionSettings[MyDestination].DimTyp = SameAsLbl then
    MyDestination := UNITS_LBL;
  with l^.VolSettings[MyDestination] do
    if VolTyp = CustomVol then begin
      s_units := string(CustUnitDisplay);
      if Fractions = Fr_Decimal then
        s_vol := DecimalString (r_vol/power(CustUnitSize, 3), DecPlaces, RemTrailingZero, Rounding)
      else
        s_vol := FractionalString (r_vol/power(CustUnitSize, 3), Fractions, Rounding);
    end
    else begin
      s_units := string (DecUnitDisplay);
      case VolTyp of
        CuFt : v := r_vol/FT3CONV;
        CuYard : v := r_vol/YD3CONV;
        Litre : v := r_vol/LITRCONV;
        CuMtr : v := r_vol/M3CONV
        else v := 0;  // should never happen
      end;
      if Fractions = Fr_Decimal then
        s_vol := DecimalString (v, DecPlaces, RemTrailingZero, Rounding)
      else
        s_vol := FractionalString (v, Fractions, Rounding);
    end;
END; //VolString;


FUNCTION IntToZeroFilledStr (num, len : integer) : string;
BEGIN
  result := IntToStr(num);
  while length (result) < len do
    result := '0' + result;
END;


FUNCTION DisStr (dis : double) : shortstring;
BEGIN
  cvdisst (dis, result);
  if dis < 0 then
    result := '-' + result;
END;

FUNCTION PointDisStr (pnt : URecords.point) : shortstring;
BEGIN
  result := 'x:' + DisStr (Pnt.x) + '  y:' + DisStr (Pnt.y) +
             '  z:' + DisStr (Pnt.z);
END;



END.
