unit Reports;

interface

uses UInterfaces, UInterfacesRecords, URecords, UConstants, CommonStuff, System.Generics.Collections;

PROCEDURE HiliteSelected (var mode : mode_type; turnON : boolean);


implementation

PROCEDURE HiliteSelected (var mode : mode_type; turnON : boolean);
VAR
  ent : entity;
  adr : lgl_addr;
  i : integer;
  unhilite_list : TList<lgl_addr>;
BEGIN
  mode_enttype (mode, entpln);
  mode_atr (mode, STANDARD_ATR_NAME);
  unhilite_list := TList<lgl_addr>.create;
  try
    adr := ent_first (mode);
    while ent_get (ent, adr) do begin
      adr := ent_next (ent, mode);
      hi_lite (turnON, ent, true, true);
      if turnON then begin
        if not lyr_ison (ent.lyr) then begin
          ent.color := clrltred;
          ent.ltype := ltype_dashed;
          ent.spacing := pixsize * 15;
          ent_draw_dl (ent, drmode_white, false);
        end;
        ssAdd (hilite_ss, ent);
      end
      else begin
        if not lyr_ison (ent.lyr) then begin  // remove 'not on' entities first, as otherwise co-incident 'on' entities may effectively be undrawn
          hi_lite (false, ent, true, false);
          ent.ltype := ltype_dashed;
          ent.spacing := pixsize * 15;
          ent_draw_dl (ent, drmode_black, false);
        end
        else begin  // add to list for later processing
          unhilite_list.Add(ent.addr);
        end;
      end;
    end;
    if unhilite_list.Count > 0 then for i := 0 to unhilite_list.Count-1 do begin
      if ent_get (ent, unhilite_list[i]) then begin
        hi_lite (false, ent, true, true);
      end;
    end;
  finally
    unhilite_list.free;
  end;
  if not turnON then begin
    unhilite_all (true, true, false);
    ssClear (hilite_ss);
  end;
END;

end.
