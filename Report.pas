unit Report;

{
Attributes associated with Reports:

  SpdhR999 (where 999 is sequential number starting at 0 for the first report added)
    - This is added as a System atr_str attribute.The string will start with a 2 character code
      the first character indicates the selection method used to create the report and the second
      indicates subtotal grouping:
        First character of string:   C = Currently Active Layer
                                     O = On Layers
                                     A = All Layers
                                     R = By Area
        Second character of string:  N = No sub-totals
                                     L = Sub-total by Layer
                                     C = Sup-total by Category

      If a report is deleted then the system attribute remains, but with a blank string. It is
      important that the attribute remains as it would break the logic to find subsequent reports
      if it were removed.
    - This is also added to layers (where selection if 'On Layers' or 'Current Layer') or entities
      (where selection is 'Area') that are included in a report.
  SpdhR999ZZZ9 (where the first 999 is sequential number starting at 0 as above. ZZZ9 is the
                line number in the report)
    - This is added as an entity atr_str attribute to every entity in each report.  For entlin
      entities the line number will be zero and the value will be a blank string, but for text
      entities it contains the following record:
          Row Type :   byte (6=Title, 7=ColHeading, 8=SubHeading, 9=Data, 10=SubTots, 11=Totals)
          Column Num in Rpt : smallint
          Entity Tag : atrname (for Data Lines ... blank if not applicable)
          Pattern : str60;
}


interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, UInterfaces, UInterfacesRecords, URecords,
  UConstants, CommonStuff, System.Generics.Collections, Language, Settings, StrUtil, Math,
  CreateSpaces, System.Generics.Defaults, UserFields, ClipBrd, Version, Constants;

type
  TDtlRptOptions = class(TForm)
    lblRptName: TLabel;
    lblRptTitle: TLabel;
    btnCopyName: TButton;
    Label3: TLabel;
    rbSubtotsByLyr: TRadioButton;
    rbSubtotsByCat: TRadioButton;
    rbNoSubtots: TRadioButton;
    cbRptName: TComboBox;
    btnCreate: TButton;
    Button2: TButton;
    edRptTitle: TEdit;
    btnNoTitle: TButton;
    memNoSelection: TMemo;
    chbClipboard: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure btnCopyNameClick(Sender: TObject);
    procedure cbRptNameChange(Sender: TObject);
    procedure btnNoTitleClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    function NameInList : boolean;
  end;

FUNCTION ReportHeading (col : integer) : string;
FUNCTION ReportSubHeading : string;
FUNCTION ReportDataPattern (col : integer) : string;
FUNCTION ReportSubTotPattern (col : integer) : string;
FUNCTION ReportTotPattern (col : integer) : string;
PROCEDURE SpaceAroundText (RowType : integer; VAR SpaceAbove, SpaceBelow, SpaceLR : double);

PROCEDURE HiliteSelected (var mode : mode_type; turnON : boolean);

PROCEDURE DoReport (showform : boolean; TagsAndName : shortstring);

PROCEDURE DoAllReports;


implementation

{$R *.dfm}

CONST
  TITLEROW = 6;
  COLHEADROW = 7;
  SUBHEADROW = 8;
  DATAROW = 9;
  SUBTOTSROW = 10;
  TOTSROW = 11;

  DummyStr = '!@#$dh';

TYPE
  EntAtrStrRec = RECORD
    {00} Len : byte;
    {02} RowType : byte;  //6=Title, 7=ColHeading, 8=SubHeading, 9=Data, 10=SubTots, 11=Totals
    {04} ColNum : smallint;
    {06} Tag : atrname;
    {20} Pattern : str60;
  END;


FUNCTION ReportAtrName (RptName : string; out RptNum, SelectMethod : integer) : atrname;
VAR
  i : integer;
  atr : attrib;
BEGIN
  i := 0;
  result := '';
  while (i < 1000) and atr_sysfind ('SpdhR' +  atrname(IntToZeroFilledStr(i, 3)), atr) do begin
    if atr.atrtype = atr_str then begin
      if RptName.ToUpper = ((string(atr.str)).ToUpper).Substring(2) then begin
        result := atr.name;
        RptNum := i;
        case atr.str[1] of
          'C' : SelectMethod := RPT_ACTIVE;
          'O' : SelectMethod := RPT_ON;
          'A' : SelectMethod := RPT_ALL;
          'R' : SelectMethod := RPT_AREA
          else  SelectMethod := RPT_NONE;
        end;
        exit;
      end;
    end;
    i := i+1;
  end;
  if i < 1000 then begin
    RptNum := i;
    SelectMethod := -1;
    result := 'SpdhR' + atrname(IntToZeroFilledStr(i, 3));
  end
  else begin
    result := '';
    RptNum := -1;
  end;
END;



PROCEDURE SpaceAroundText (RowType : integer; VAR SpaceAbove, SpaceBelow, SpaceLR : double);
CONST
  DummySpace = 999.97;
  DefaultSpace = 0.25;
BEGIN
  SpaceAbove := getsvdRl ('SpdhSpAbv' + inttostr(RowType), DummySpace);
  if RealEqual (SpaceAbove, DummySpace) then
    // look for old Space Planner setting
    case RowType of
      TITLEROW : SpaceAbove := GetSvdRl ('dhSPRepTlAbv', DefaultSpace);
      COLHEADROW : SpaceAbove := GetSvdRl ('dhSPRepHdAbv', DefaultSpace);
      SUBHEADROW : SpaceAbove := GetSvdRl ('dhSPRepSHAbv', DefaultSpace);
      DATAROW : SpaceAbove := GetSvdRl ('dhSPRepLiAbv', DefaultSpace);
      SUBTOTSROW : SpaceAbove := GetSvdRl ('dhSPRepSTAbv', DefaultSpace);
      TOTSROW: SpaceAbove := GetSvdRl ('dhSPRepToAbv', DefaultSpace)
      else SpaceAbove := DefaultSpace;
    end;

  SpaceBelow := getsvdRl ('SpdhSpBlw' + inttostr(RowType), DummySpace);
  if RealEqual (SpaceBelow, DummySpace) then
    // look for old Space Planner setting
    case RowType of
      TITLEROW : SpaceBelow := GetSvdRl ('dhSPRepTlBlw', DefaultSpace);
      COLHEADROW : SpaceBelow := GetSvdRl ('dhSPRepHdBlw', DefaultSpace);
      SUBHEADROW : SpaceBelow := GetSvdRl ('dhSPRepSHBlw', DefaultSpace);
      DATAROW : SpaceBelow := GetSvdRl ('dhSPRepLiBlw', DefaultSpace);
      SUBTOTSROW : SpaceBelow := GetSvdRl ('dhSPRepSTBlw', DefaultSpace);
      TOTSROW : SpaceBelow := GetSvdRl ('dhSPRepToBlw', DefaultSpace);
      else SpaceBelow := 0.25;
    end;

  SpaceLR := getsvdRl ('SpdhSpLR6', One);
END;  // SpaceAroundText

PROCEDURE ReplaceObsoleteTags (var pattern : string);
VAR
  tagpos : integer;
BEGIN
  // in case obsolete {area%} tag exists, replace with new tag
  tagpos := ansipos ('{area%}', pattern);
  if tagpos > 0 then begin
    delete (pattern, tagpos, 7);
    insert (Tags[tag_rareapct, 1], pattern, tagpos);
  end;
END;

FUNCTION ReportHeading (col : integer) : string;
BEGIN
    result := string (GetSvdStr ('SpdhRptHed' + inttostr(col), DummyStr));
    if result = DummyStr then
      result := string (GetSvdStr ('dhSPRptHed' + inttostr(col), DummyStr));
    if result = DummyStr then
      case col of
        1 : result := 'Room #';
        2 : result := 'Room Name';
        3 : result := 'Dimensions';
        4 : result := 'Area';
        5 : result := 'Percent of Area';
        6 : result := 'Perimeter';
        7 : result := 'Volume';
        else result := '';
      end;
END;

FUNCTION ReportSubHeading : string;
BEGIN
  result := string(GetSvdStr ('SpdhRptSubHd', DummyStr));
  if result = DummyStr then
    result := string(GetSvdStr ('dhSPRptSubHd', '{grp}'));
END;

FUNCTION ReportDataPattern (col : integer) : string;
BEGIN
  result := string(GetSvdStr ('SpdhRptDat' + inttostr(col), DummyStr));
  if result = DummyStr then
    result := string(GetSvdStr ('dhSPRptCol' + inttostr(col), DummyStr));
  if result = DummyStr then
    case col of
      1 : result := Tags [tag_nbr, 1];
      2 : result := Tags [tag_name, 1];
      3 : result := Tags [tag_ldim, 1] + ' x ' +Tags [tag_sdim, 1];
      4 : result := Tags [tag_area, 1] + ' ' +Tags [tag_aunit, 1];
      5 : result := Tags [tag_rareapct, 1];
      6 : result := Tags [tag_perim, 1];
      7 : result := Tags [tag_vol, 1] + ' ' +Tags [tag_vunit, 1];
      else result := '';
    end;

  ReplaceObsoleteTags(result);
END;

FUNCTION ReportSubTotPattern (col : integer) : string;
VAR
  tagpos : integer;
BEGIN
  result := string(GetSvdStr ('SpdhRptSuT' + inttostr(col), DummyStr));
  if result = DummyStr then
    result := string(GetSvdStr ('dhSPRptST' + inttostr(col), DummyStr));
  if result = DummyStr then
    case col of
      3 : result := Tags [tag_grp, 1] + ' Totals:';
      4 : result := Tags [tag_area, 1] + ' ' + Tags [tag_aunit, 1];
      5 : result := Tags [tag_rareapct, 1];
      6 : result := Tags [tag_perim, 1];
      7 : result := Tags [tag_vol, 1] + ' ' + Tags [tag_vunit, 1];
      else result := '';
    end;

  // just in case {lyr} or {cat} tag exists, replace with {grp} tag (lyr/cat tags should not be used for sub-totals)
  tagpos := ansipos (Tags[tag_lyr, 1], result);
  if tagpos > 0 then begin
    delete (result, tagpos, length(Tags[tag_lyr, 1]));
    insert (Tags[tag_grp, 1], result, tagpos);
  end;
  tagpos := ansipos (Tags[tag_cat, 1], result);
  if tagpos > 0 then begin
    delete (result, tagpos, length(Tags[tag_cat, 1]));
    insert (Tags[tag_grp, 1], result, tagpos);
  end;

  ReplaceObsoleteTags(result);
END;

FUNCTION ReportTotPattern (col : integer) : string;

BEGIN
  result := string(GetSvdStr ('SpdhRptTot' + inttostr(col), DummyStr));
  if result = DummyStr then
    result := string(GetSvdStr ('dhSPRptTot' + inttostr(col), DummyStr));
  if result = DummyStr then
    case col of
      3 : result := 'TOTALS:';
      4 : result := Tags [tag_area, 1] + ' ' + Tags [tag_aunit, 1];
      5 : result := Tags [tag_rareapct, 1];
      6 : result := Tags [tag_perim, 1];
      7 : result := Tags [tag_vol, 1] + ' ' + Tags [tag_vunit, 1];
      else result := '';
    end;

  ReplaceObsoleteTags(result);
END;

FUNCTION NothingSelected : boolean;
VAR
  adr : lgl_addr;
  ent : entity;
BEGIN
  result := (l^.ReportSelection = 0);
  if not result then begin
    adr := ent_first (l^.RptMode);
    result := not ent_get (ent, adr);
  end;
END;

PROCEDURE DoReport (showform : boolean; TagsAndName : shortstring);
TYPE
  ReportRow = RECORD
    RowType : byte;  //6=Title, 7=ColHeading, 8=SubHeading, 9=Data, 10=SubTots, 11=Totals
    StAtr : StandardAtr;
    Lyr : string;
    floorz, celingz : double;
    adr : lgl_addr;
    Columns : array [1..10] of string;
    ColAdrs : array [1..10] of entaddr;
    FirstNonBlank : smallint;
    RowHeight : double;
  END;
VAR
  DtlRptOptions : TDtlRptOptions;
  botleft, topright, ReportTopLeft, ReportTopRight, p1, p2, p3, p4 : point;
  TopLeftSet : boolean;  //, haslines : boolean;
  ypos, xpos : double;
  RptNum, SelectMethod, i, j, row, col, lastfound, groupfirst : integer;
  SysAtrName : atrname;
  pEntAtrStr :  ^EntAtrStrRec;
  EntsToReuse : TList<lgl_addr>;
  AtrToReuse : attrib;
  mode : mode_type;
  adr : lgl_addr;
  atr : attrib;
  s : string;
  ent, firstent : entity;
  ReportRows : TList<ReportRow>;
  Comparison: TComparison<ReportRow>;
  TempRow : ReportRow;
  Subheading : string;
  ss : shortstring;
  pStandardAtr : ^StandardAtr;
  SubTotAtr, TotAtr : StandardAtr;
  Patterns, SubTotPatterns, TotPatterns : array [1..10] of shortstring;
  numbersNumeric : boolean;
  RowType : byte;
  ColWidths : array [1..10] of double;
  HeadingWidth : double;
  svLyr, tempLyr : lyraddr;
  doLines : boolean;
  AvailableWidth, CombinedWidth : double;
  spaceabove : array [TITLEROW .. TOTSROW] of double;
  spacebelow : array [TITLEROW .. TOTSROW] of double;
  SpaceLR : double;
  DwgTotArea, RptTotArea : double;
  darea_required : boolean;
  OriginalSelection : boolean;
  UserField : UserFieldDef;
  tempstr : shortstring;
//  LyrName, LyrName1 : string;
  ssLyrName : shortstring;
  lyrfound : boolean;
  rlyr : Rlayer;
  ClipbrdStr : string;
  RptTitle : str255;
  TotCount, SubTotCount : word;

  // Text Style variables
  TxtFont : string;
  dflttxtsize, TxtHeight, Aspect, Slant : double;
  Weight : integer;
  TxtClr : integer;

  FUNCTION ContainsGroupTagOrNoTags (ptn : string) : boolean;
  /// returns false if ptn potentially tags but does not contains the either the {count} or {grp} tag
  VAR
    i : integer;
  BEGIN
    i := pos (Tags[tag_grp, 1], ptn);
    if i = 0 then
      i := pos (Tags[tag_count, 1], ptn);
    if i > 0 then
      result := true
    else if ptn.Contains('{') and ptn.Contains('}') then
      result := false
    else
      result := true;
  END; //ContainsGroupTagOrNoTags

  PROCEDURE SetAtrZero (var Atr : StandardAtr);
  BEGIN
    Atr.area := zero;
    Atr.OutlinePerim := zero;
    Atr.VoidPerim := zero;
    Atr.OutlinePerimArea := zero;
    Atr.VoidPerimArea := zero;
    Atr.volume := zero;
    Atr.CeilingArea := zero;
    Atr.GrossArea := zero;
    Atr.GrossVolume := zero;
    Atr.Name := '';
    Atr.Category := '';
    Atr.Num := '';
  END;

  PROCEDURE AddAtr (var Atr : StandardAtr;
                    const IncAtr : StandardAtr);
  BEGIN
    Atr.area := Atr.area + IncAtr.area;
    Atr.OutlinePerim := Atr.OutlinePerim + IncAtr.OutlinePerim;
    Atr.VoidPerim := Atr.VoidPerim + IncAtr.VoidPerim;
    Atr.OutlinePerimArea := Atr.OutlinePerimArea + IncAtr.OutlinePerimArea;
    Atr.VoidPerimArea := Atr.VoidPerimArea + IncAtr.VoidPerimArea;
    Atr.volume := Atr.volume + IncAtr.volume;
    Atr.CeilingArea := Atr.CeilingArea + IncAtr.CeilingArea;
    Atr.GrossArea := Atr.GrossArea + IncAtr.GrossArea;
    Atr.GrossVolume := Atr.GrossVolume + IncAtr.GrossVolume;
  END;

  PROCEDURE CustomTots (VAR Col : string; rowfrom, rowto : integer);
  VAR
    tagstart, tagend, row, atrtype : integer;
    tag : string;
    tempent : entity;
    atr : attrib;
    total : double;
    s, s1 : string;
  BEGIN
    tagstart := Col.IndexOf ('{');
    while tagstart >= 0 do begin
      tagend := Col.IndexOf('}',tagstart);  // dont forget that IndexOf returns zero-based index
      if tagend < tagstart+2 then
        tagstart := -1
      else begin
        tag := Col.Substring(tagstart+1, tagend-tagstart-1);
        total := zero;
        atrtype := atr_str; // just set it to a type we don't use to start with (so we know a suitable atrib has been found if it changes)
        for row := rowfrom to rowto do begin
          if (ReportRows[row].RowType = DATAROW) and ent_get(tempent, ReportRows[row].adr) then begin
            if atr_entfind (tempent, 'SpdhCF'+atrname(tag), atr) then begin
              atrtype := atr.atrtype;
              case atr.atrtype of
                atr_int : total := total + atr.int;
                atr_rl  : total := total + atr.rl;
                atr_dis : total := total + atr.dis;
                atr_ang : total := total + atr.ang;
                atr_area: total := total + atr.rl;
              end;
            end
            else if atr_sysfind ('SpdhUF'+atrname(tag), atr)then begin
              UserField := DecodeUserFieldAtr (atr);
              case UserField.numeric of
                int : begin
                        atrtype := atr_int;
                        total := total + UserField.defaultint;
                      end;
                rl  : begin
                        atrtype := atr_rl;
                        total := total + UserField.defaultrl;
                      end;
                dis : begin
                        atrtype := atr_dis;
                        total := total + UserField.defaultdis;
                      end;
                ang : begin
                        atrtype := atr_ang;
                        total := total + UserField.defaultang;
                      end;
                area : begin
                        atrtype := atr_Area;
                        total := total + UserField.defaultarea;
                      end;
              end;
            end;
          end;
        end;
        case atrtype of
          atr_int : begin
                      Delete (Col, Tagstart+1, tagend-tagstart+2);
                      Insert (inttostr (round(total)), Col, Tagstart+1);
                    end;
          atr_rl  : begin
                      Delete (Col, Tagstart+1, tagend-tagstart+2);
                      Insert (floattostr (total), Col, Tagstart+1);
                    end;
          atr_dis : begin
                      Delete (Col, Tagstart+1, tagend-tagstart+2);
                      DisString (total, UNITS_RPT, s, s1);
                      Insert (s, Col, Tagstart+1);
                    end;
          atr_ang : begin
                      Delete (Col, Tagstart+1, tagend-tagstart+2);
                      cvangst (total, ss);
                      Insert (string(ss), Col, Tagstart+1);
                    end;
          atr_area: begin
                      Delete (Col, Tagstart+1, tagend-tagstart+2);
                      AreaString (total, CustArea, UNITS_RPT, s, s1);
                      Insert (s, Col, Tagstart+1);
                    end;
        end;

        tagstart := tagend+1;
      end;
    end;
  END;

  FUNCTION ReportLineEnt (p1, p2 : point) : entaddr;
  VAR
    ent : entity;
  BEGIN
    if (p1.x > l^.Pnt2.x+abszero) and (p2.x > l^.Pnt2.x+abszero) then
      exit;
    if (EntsToReuse.Count > 0) and ent_get(ent, EntsToReUse[0]) then begin
      EntsToReuse.Delete(0);
      ent_draw (ent, drmode_black);
      ent.enttype := entlin;
      ent.linpt1 := p1;
      ent.linpt2 := p2;
    end
    else begin
      ent_init (ent, entlin);
      ent.linpt1 := p1;
      ent.linpt2 := p2;
      ent_add (ent);
    end;

    if l^.RptLineClr > 0 then
      ent.color := l^.RptLineClr
    else
      ent.color := CurrentState^.color; //need to set in case we are re-using an entity that already has a diffent colour
    ent.Width := 1;
    ent.ltype := ltype_solid;
    ent_update (ent);


    adr := atr_entfirst (ent);     // if re-using an entity then alse re-use its attrib
    setnil (AtrToReuse.addr);
    while atr_get (atr, adr) do begin
      adr := atr_next (atr);
      if (string(atr.name)).StartsWith('SpdhR') then begin
        if isnil (AtrToReuse.addr) then
          AtrToReuse := atr
        else
          atr_delent (ent, atr);
      end
    end;

    if not atr_get (atr, AtrToReuse.addr) then begin
      atr_init (atr, atr_str);
      atr.str := 'temp';
      atr_add2ent (ent, atr);
    end;
    pEntAtrStr := addr (atr.str);
    pEntAtrStr^.Len := 80;
    pEntAtrStr^.RowType := 0;
    pEntAtrStr^.ColNum := 0;
    pEntAtrStr^.Tag := '';
    pEntAtrStr^.Pattern := '';
    atr.name := SysAtrName + '0';
    atr_update (atr);

    if TopLeftSet then
      ent_draw (ent, drmode_white);

    result := ent.addr;
  END;

  FUNCTION ReportTextEnt (const TxtStr : string; var MaxWidth, MaxHeight : double) : entaddr;
  VAR
    reusing : boolean;
    ent : entity;
    MyFont : string;
    minpnt, maxpnt : point;
  BEGIN
    if blankstring (TxtStr) then begin
      setnil (result);
      exit;
    end;

    if isnil (firstent.addr) then begin
      //always create a new entity in a new group the very first time
      reusing := false;
      stopgroup;
    end
    else
      reusing := (EntsToReuse.Count > 0);
    if reusing then begin
      ent_get (ent, EntsToReuse[0]);
      EntsToReuse.Delete(0);
      ent_draw (ent, drmode_black);
      ent.enttype := enttxt;
    end
    else
      ent_init (ent, enttxt);

    ent.txtstr := shortstring(TxtStr);
    if not reusing then
      ent_add (ent);
    ent.txttype.backwards := false;
    ent.txttype.upsidedown := false;
    ent.txttype.justification := TEXT_JUST_LEFT;
    ent.txttype.VerticalAlignment := TEXT_VALIGN_BASELINE;
    ent.txtang := 0;
    ent.txtsiz := TxtHeight;
    ent.txtaspect := Aspect;
    ent.txtslant := Slant;
    ent.Width := Weight;
    ent.color := TxtClr;
    MyFont := TxtFont;
    if MyFont.EndsWith('(TTF)') then begin
      SetLength(MyFont, Length(MyFont)-6);
      ent.txtdisplayttf.dooutline := true;
      ent.txtdisplayttf.dofill := true;
      ent.txtdisplayttf.OutlineClr := TxtClr;
      ent.txtdisplayttf.FillClr := TxtClr;
      ent.TxtWinfont := font_ttf;
    end
    else begin
      ent.TxtWinfont := font_shx;
    end;
    ent.txtfon := shortstring(MyFont);

    ent_update (ent);
    if isnil (firstent.addr) then
      ent_get (firstent, ent.addr)
    else begin
      ent_get (firstent, firstent.addr);
      ent_get (ent, ent.addr);
      ent_relink (firstent, ent);
    end;

    ent_extent (ent, minpnt, maxpnt);
    MaxHeight := max (MaxHeight, maxpnt.y - minpnt.y);
    MaxWidth := max (MaxWidth, maxpnt.x - minpnt.x);

    result := ent.addr;
  END;  // ReportTextEnt


BEGIN
  if NothingSelected then begin
    if not atr_sysfind ('SpdhR000', atr) then begin
      lmsg_ok (84); //Select spaces to report on first (using options F1 thru F4 on this menu);
      exit;
    end;
    OriginalSelection := true;
  end
  else
    OriginalSelection := false;
  DtlRptOptions := TDtlRptOptions.Create(Application);
  try
    if OriginalSelection then begin
      DtlRptOptions.cbRptName.Style := csDropDownList;
      DtlRptOptions.cbRptName.ItemIndex := 0;
      GetMsg (102, s);  //Note: As no spaces are selected, the original selection criteria of the report will be used
      DtlRptOptions.memNoSelection.Lines.Add(s);
      DtlRptOptions.memNoSelection.Visible := true;
      ButtonLblParams (82, [], DtlRptOptions.btnCreate);  //Replace Report|Replace an Existing Report using current report settings
    end;

    if showform then
      DtlRptOptions.ShowModal
    else begin
      DtlRptOptions.cbRptName.Text := (string(TagsAndName)).Substring(2);
      case TagsAndName[2] of
        'L' : DtlRptOptions.rbSubtotsByLyr.Checked := true;
        'C' : DtlRptOptions.rbSubtotsByCat.Checked := true
        else  DtlRptOptions.rbNoSubtots.Checked := true;
      end;
      case TagsAndName[1] of
        'C' : l^.ReportSelection := RPT_ACTIVE;
        'O' : l^.ReportSelection := RPT_ON;
        'A' : l^.ReportSelection := RPT_ALL
        else l^.ReportSelection := RPT_AREA;
      end;
      DtlRptOptions.ModalResult := mrOK;
    end;

    topleftSet := false;
    numbersNumeric := true;
    svLyr := getlyrcurr;
    EntsToReuse := TList<lgl_addr>.Create;
    ReportRows := TList<ReportRow>.Create;

    with DtlRptOptions do try
      if ModalResult = mrOK then begin
        if rbSubtotsByLyr.Checked then
          l^.RptSubtotBy := tag_lyr
        else if rbSubtotsByCat.Checked then
          l^.RptSubtotBy := tag_cat
        else
          l^.RptSubtotBy := 0;
        SaveInt (SpdhRptSbTot, l^.RptSubtotBy);

        if showForm then
          SysAtrName := ReportAtrName (cbRptName.Text, RptNum, SelectMethod)
        else
          SysAtrName := ReportAtrName ((string(TagsAndName)).Substring(2), RptNum, SelectMethod);

        if OriginalSelection then begin
          // use report selection attributes to set up RptMode
          ssClear (hilite_ss);
          mode_init (l^.RptMode);
          Case SelectMethod of
            RPT_ALL : mode_lyr (l^.RptMode, lyr_all);
            RPT_ON, RPT_ACTIVE :
                 begin
                    TempLyr := lyr_first;
                    while not lyr_nil (TempLyr) do begin
                      if atr_lyrfind (TempLyr, SysAtrName, atr) then begin
                        mode_init (mode);
                        mode_1lyr (mode, TempLyr);
                        mode_enttype (mode, entpln);
                        mode_atr (mode, STANDARD_ATR_NAME);
                        adr := ent_first (mode);
                        while ent_get (ent, adr) do begin
                          ssAdd (hilite_ss, ent);
                          adr := ent_next (ent, mode);
                        end;
                      end;
                      TempLyr := lyr_next (TempLyr);
                    end;
                    mode_ss (l^.RptMode, hilite_ss);
                 end
            else begin
                    mode_init (mode);
                    mode_lyr (mode, lyr_all);
                    mode_atr (mode, SysAtrName);
                    mode_enttype (mode, entpln);
                    adr := ent_first (mode);
                    while ent_get (ent, adr) do begin
                      ssAdd (hilite_ss, ent);
                      adr := ent_next (ent, mode);
                    end;
                    mode_ss (l^.RptMode, hilite_ss);
                 end;
          end;

          mode_enttype (l^.RptMode, entpln);
          mode_atr (l^.RptMode, STANDARD_ATR_NAME);

        end
        else begin
          // remove existing report selection attributes from entities
          // delete or add report selection attribute for layers as applicable

          TempLyr := lyr_first;
          while not lyr_nil (TempLyr) do begin
            if atr_lyrfind (TempLyr, SysAtrName, atr) then begin
              if not (((l^.ReportSelection = RPT_ON) and lyr_ison (TempLyr)) or
                      ((l^.ReportSelection = RPT_ACTIVE) and addr_equal (getlyrcurr, TempLyr)) or
                      (l^.ReportSelection = RPT_ALL)) then
              atr_dellyr (TempLyr, atr);
            end
            else if ((l^.ReportSelection = RPT_ON) and lyr_ison (TempLyr)) or
                    ((l^.ReportSelection = RPT_ACTIVE) and addr_equal (getlyrcurr, TempLyr)) or
                    (l^.ReportSelection = RPT_ALL)
            then begin
              atr_init (atr, atr_int);
              atr.name := SysAtrName;
              atr.int := RptNum;
              atr_add2lyr (TempLyr, atr);
            end;

            TempLyr := lyr_next (TempLyr);
          end;

          mode_init (mode);
          mode_lyr (mode, lyr_all);
          mode_enttype (mode, entpln);
          mode_atr (mode, SysAtrName);
          adr := ent_first (mode);
          while ent_get (ent, adr) do begin
            adr := ent_next (ent, mode);
            if atr_entfind (ent, SysAtrName, atr) then
              atr_delent (ent, atr);
          end;
        end;

        doLines := (l^.RptLineClr >= 0);

        darea_required := false;
        // set patterns for data, sub-tot and tot rows
        for i := 1 to 10 do begin
          Patterns[i] := shortstring(ReportDataPattern(i));
          if (not darea_required) and (ansipos (string(Patterns[i]), Tags[tag_dareapct, 1]) > 0) then
            darea_required := true;
          SubTotPatterns[i] := shortstring(ReportSubTotPattern(i));
          if (not darea_required) and (ansipos (string(SubTotPatterns[i]), Tags[tag_dareapct, 1]) > 0) then
            darea_required := true;
          TotPatterns[i] := shortstring(ReportTotPattern(i));
          if (not darea_required) and (ansipos (string(TotPatterns[i]), Tags[tag_dareapct, 1]) > 0) then
            darea_required := true;
        end;

        DwgTotArea := Zero;
        if darea_required then begin      // need to calculate total area of all spaces in drawing to calculate percentages
          mode_init (mode);
          mode_lyr (mode, lyr_all);
          mode_atr (mode, STANDARD_ATR_NAME);
          mode_enttype (mode, entpln);
          adr := ent_first (mode);
          while ent_get (ent, adr) do begin
            if atr_entfind (ent, STANDARD_ATR_NAME, atr) and (atr.atrtype = atr_str255) then begin
              pStandardAtr := Addr (atr.shstr);
              DwgTotArea := DwgTotArea + pStandardAtr^.area;
            end;
            adr := ent_next (ent, mode);
          end;
        end;

        RptTotArea := zero;
        adr := ent_first (l^.RptMode);
        while ent_get (ent, adr) do begin
          adr := ent_next (ent, l^.RptMode);
          if atr_entfind (ent, STANDARD_ATR_NAME, atr) then begin
            pStandardAtr := addr (atr.shstr);
            RptTotArea := RptTotArea + pStandardAtr.area;

            if l^.ReportSelection in [RPT_AREA, RPT_AREA_ADD, RPT_AREA_DEL] then begin
              atr_init (atr, atr_int);
              atr.name := SysAtrName;
              atr.int := RptNum;
              atr_add2ent (ent, atr);
            end;
          end;
        end;

        setpoint (ReportTopLeft, 0.0);

        RptTitle := str255(edRptTitle.Text);

        if NameInList then begin
          //get top left of current report and add entities to list to reuse
          i := 0;
          lastfound := 0;
          repeat
            mode_init (mode);
            mode_lyr (mode, lyr_all);
            mode_atr (mode, SysAtrName + atrname(IntToStr(i)));     //set mode to find all entities on this row
            adr := ent_first (mode);
            while ent_get (ent, adr) do begin
              if lyr_ison (ent.lyr) then
                ent_draw (ent, drmode_black);

              lastfound := i;
//              EntsToReuse.Add(adr);

              if ent.enttype = enttxt then begin
                txtbox (ent, p1, p2, p3, p4);
                botleft.x := min (p1.x, min(p2.x, min (p3.x, p4.x)));
                botleft.y := min (p1.y, min(p2.y, min (p3.y, p4.y)));
                botleft.z := p1.z;
                topright.x := max (p1.x, max(p2.x, max (p3.x, p4.x)));
                topright.y := max (p1.y, max(p2.y, max (p3.y, p4.y)));
                topright.z := p1.z;
                if (not showform) and
                   atr_entfind (ent, SysAtrName + atrname(IntToStr(i)), atr) and
                   (atr.atrtype = atr_str255)
                then begin
                   pEntAtrStr := addr (atr.shstr);
                   if pEntAtrStr^.RowType = TITLEROW then
                    RptTitle := ent.txtstr;
                end;
              end
              else if ent.enttype = entlin then begin
                botleft.x := min (ent.linpt1.x, ent.linpt2.x);
                botleft.y := min (ent.linpt1.y, ent.linpt2.y);
                botleft.z := ent.linpt1.z;
                topright.x := max (ent.linpt1.x, ent.linpt2.x);
                topright.y := max (ent.linpt1.y, ent.linpt2.y);
                topright.z := ent.linpt1.z;
              end;
              if ent.enttype in [entlin, enttxt] then begin
                if TopLeftSet then begin
                  ReportTopLeft.x := min (botleft.x, ReportTopLeft.x);
                  ReportTopLeft.y := max (topright.y, ReportTopLeft.y);
                end
                else begin
                  lyr_set (ent.lyr);
                  ReportTopLeft.x := botleft.x;
                  ReportTopLeft.y := topright.y;
                  if ent.enttype = enttxt then
                    ReportTopLeft.z := ent.TxtBase
                  else if ent.enttype = entlin then
                    ReportTopLeft.z := ent.linpt1.z;
                  TopLeftSet := true;
                end;
              end;

              adr := ent_next (ent, mode);
              ent_del (ent);
            end;
            i := i+1;
          until i > lastfound + 5;  //allow a buffer of 5 in case they have manually deleted rows of the report (if they have deleted more than 5 then subsequent rows will become unlinked)
          atr_sysfind (SysAtrName, atr)
        end
        else begin
          atr_init (atr, atr_str);
          atr.name := SysAtrName;
        end;
        case l^.ReportSelection of
          RPT_ACTIVE : atr.str := 'C';
          RPT_ON : atr.str := 'O';
          RPT_ALL : atr.str := 'A';
          RPT_AREA, RPT_AREA_ADD, RPT_AREA_DEL : atr.str := 'R'
          else atr.str := atr.str[1];
        end;
        if rbSubtotsByLyr.Checked then
          atr.str := atr.str + 'L'
        else if rbSubtotsByCat.Checked then
          atr.str := atr.str + 'C'
        else
          atr.str := atr.str + 'N';
        atr.str := atr.str + shortstring (cbRptName.Text);
        if showform then begin
          if NameInList then
            atr_update (atr)
          else
            atr_add2sys (atr);
        end;

        if not TopLeftSet then begin  //we are not updating an existing report
          ssLyrName := GetSvdStr('SpdhRptLyr', '');
          if not BlankString(sslyrname) then begin
            if lyr_find (ssLyrName, templyr) then begin
              lyr_get (templyr, rlyr);
              if rlyr.group <> -1 then
                lpmsg_ok (87, [ssLyrName]) // Layer '$' is LOCKED.||Report will be placed on currently active layer.
              else
                lyr_set (tempLyr);
            end
            else begin
              if lyr_create (ssLyrName, tempLyr) then begin
                // NOTE: There is a bug in lyr_create. It does not return the layer address.
                //       So need to find the just created layer and set it.
                lyr_find (ssLyrName, templyr);
                lyr_set (templyr);
              end
              else begin
                lpMsg_OK(86, [ssLyrName]);
                lyr_set (svLyr);
              end;
            end;
          end;
        end;  // not TopLeftSet

        lyr_get (getlyrcurr, rlyr);
        if rlyr.group <> -1 then begin
          lmsg_ok (20);   //NOT CREATED (Layer is locked).
          exit;
        end;



        botleft := ReportTopLeft;  // set to an initial value that is greater than the eventual expected value
        topright := ReportTopLeft; // set to an initial value that is less then eventual expected value

        adr := ent_first (l^.RptMode);
        while ent_get (l^.ent, adr) do begin       //note: use l^.ent as it is referenced in SubstituteTags procedure
          adr := ent_next (l^.ent, l^.RptMode);
          if atr_entfind (l^.ent, STANDARD_ATR_NAME, atr) then begin
            pStandardAtr := addr (atr.shstr);
            TempRow.StAtr := pStandardAtr^;
            if numbersNumeric then try
              StrToFloat(string(TempRow.StAtr.Num));
            except
              numbersNumeric := false;
            end;
            getLongLayerName (l^.ent.lyr, ss);
            TempRow.Lyr := string(ss);
            tempRow.floorz := l^.ent.plbase;
            tempRow.celingz := l^.ent.plnhite;
            TempRow.adr := l^.ent.addr;

            for i := 1 to 10 do begin
             TempRow.columns[i] := SubstituteTags (Patterns[i], pStandardAtr^, TempRow.Lyr, TempRow.floorz, TempRow.CelingZ,
                                                   RptTotArea, DwgTotArea, 0, UNITS_RPT, true, false);
            end;

            if rbSubtotsByLyr.Checked then
              TempRow.StAtr.Category := '';  // don't need category any more after SubstituteTags call above, so clear it so it won't be used in sub-headings or sub-totals

            TempRow.RowType := DATAROW;
            ReportRows.Add(TempRow);
          end;
        end;

        if ReportRows.Count < 1 then
          exit;  //should never happen
        totCount := ReportRows.Count;


        Comparison := function(const LeftRec, RightRec: ReportRow): integer
                      begin
                        result := 0;
                        if rbSubtotsByLyr.Checked then begin
                          if LeftRec.Lyr > RightRec.Lyr then
                            result := 1
                          else if RightRec.Lyr > LeftRec.Lyr then
                            result := -1;
                        end
                        else if rbSubtotsByCat.Checked then begin
                          if LeftRec.StAtr.Category > RightRec.StAtr.Category then
                            result := 1
                          else if RightRec.StAtr.Category > LeftRec.StAtr.Category then
                            result := -1;
                        end;

                        if result = 0 then begin
                          if numbersNumeric then begin
                            if StrToFloat(string(LeftRec.StAtr.Num)) > StrToFloat(string(RightRec.StAtr.Num)) then
                              result := 1
                            else if StrToFloat(string(RightRec.StAtr.Num)) > StrToFloat(string(LeftRec.StAtr.Num)) then
                              result := -1;
                          end
                          else begin
                            if LeftRec.StAtr.Num > RightRec.StAtr.Num then
                              result := 1
                            else if RightRec.StAtr.Num > LeftRec.StAtr.Num then
                              result := -1;
                          end;
                        end;

                        if result = 0 then begin
                          if LeftRec.StAtr.Name > RightRec.StAtr.Name then
                            result := 1
                          else if RightRec.StAtr.Name > LeftRec.StAtr.Name then
                            result := -1;
                        end;
                      end;

        ReportRows.Sort(TComparer<ReportRow>.Construct(Comparison));

        //set up TempRow as column headings and insert it at top of ReportRows
        TempRow.RowType := COLHEADROW;
        TempRow.Lyr := '';
        TempRow.StAtr.Category := '';
        for i := 1 to 10 do begin
          TempRow.Columns[i] := ReportHeading(i);
        end;
        ReportRows.Insert(0, TempRow);

        if rbNoSubtots.Checked then begin
          SetAtrZero (TotAtr);
          for i := 1 to ReportRows.Count-1 do begin
            if ReportRows[i].RowType = DATAROW then
              AddAtr (TotAtr, ReportRows[i].StAtr);
          end;
        end
        else begin
          // set up TempRow as a sub-total row
          TempRow.RowType := SUBTOTSROW;
          TempRow.Lyr := '';
          TempRow.StAtr.Category := '';

          // zero SubTotAtr, TotAtr
          SetAtrZero (SubTotAtr);
          SetAtrZero (TotAtr);
          if rbSubtotsByCat.Checked then begin
            SubTotAtr.Category := ReportRows[1].StAtr.Category;
            if Blankstring (SubTotAtr.Category) then
              GetMsg (122, SubTotAtr.Category);   //Uncategorised
          end
          else
            SubTotAtr.Category := '';

          // insert sub-total rows into ReportRows
          i := 2;
          groupfirst := 0;
          SubTotCount := 1;
          while i < ReportRows.Count do begin
            AddAtr (SubTotAtr, ReportRows[i-1].StAtr);
            if (rbSubtotsByLyr.Checked and (ReportRows[i].Lyr <> ReportRows[i-1].Lyr)) or
               (rbSubtotsByCat.Checked and (ReportRows[i].StAtr.Category <> ReportRows[i-1].StAtr.Category))
            then begin
              for col := 1 to 10 do begin
                j := ansipos (TempRow.columns[col], Tags[tag_grp, 1]);
                if j > 0 then begin
                  delete (TempRow.columns[col], j, length(Tags[tag_grp, 1]));
                  if rbSubtotsByLyr.Checked then
                    insert (ReportRows[i-1].Lyr, TempRow.columns[col], j)
                  else
                    insert (string(ReportRows[i-1].StAtr.Category), TempRow.columns[col], j);
                end;
                j := ansipos (TempRow.columns[col], Tags[tag_lyr, 1]);
                if j > 0 then begin
                  delete (TempRow.columns[col], j, length(Tags[tag_lyr, 1]));
                  insert (ReportRows[i-1].Lyr, TempRow.columns[col], j);
                end;
                TempRow.columns[col] := SubstituteTags (SubTotPatterns[col], SubTotAtr, ReportRows[i-1].Lyr, ReportRows[i-1].floorz,
                                                        ReportRows[i-1].celingz, RptTotArea, DwgTotArea, SubTotCount, UNITS_RPT, false, false);
                CustomTots (TempRow.columns[col], groupfirst, i-1);
              end;
              ReportRows.Insert(i, TempRow);
              subTotCount := 0;


              AddAtr (TotAtr, SubTotAtr);
              SetAtrZero (SubTotAtr);
              groupfirst := i+1;
              if rbSubtotsByCat.Checked then begin
                SubTotAtr.Category := ReportRows[groupfirst].StAtr.Category;
                if Blankstring (SubTotAtr.Category) then
                  GetMsg (122, SubTotAtr.Category);   //Uncategorised
              end;
              i := i+2;
            end
            else
              i := i+1;
              SubTotCount := SubTotCount + 1;
          end;


          AddAtr (SubTotAtr, ReportRows[ReportRows.Count-1].StAtr);

          for j := 1 to 10 do begin
            TempRow.columns[j] := SubstituteTags (SubTotPatterns[j], SubTotAtr, ReportRows[ReportRows.Count-1].Lyr,
                                                  ReportRows[ReportRows.Count-1].FloorZ, ReportRows[ReportRows.Count-1].CelingZ,
                                                  RptTotArea, DwgTotArea, SubTotCount,
                                                  UNITS_RPT, false, false);
            CustomTots (TempRow.Columns[j], groupfirst, ReportRows.Count-1);
          end;
          ReportRows.Add(TempRow);
          AddAtr (TotAtr, SubTotAtr);

          //set up and insert subheadings
          TempRow.RowType := SUBHEADROW;
          TempRow.Lyr := '';
          TempRow.StAtr.Category := '';
          Subheading := ReportSubHeading;
          Patterns[1] := shortstring(Subheading);
          for i := 2 to 10 do
            TempRow.Columns[i] := '';

          i := 1;
          while i < ReportRows.Count do begin
            if (ReportRows[i].RowType = DATAROW) and (ReportRows[i-1].RowType <> DATAROW) then begin
              TempRow.StAtr := ReportRows[i].StAtr;
              if rbSubtotsByCat.Checked and Blankstring (TempRow.StAtr.Category) then
                  GetMsg (122, TempRow.StAtr.Category);   //Uncategorised
              TempRow.Columns[1] := SubstituteTags (Patterns[1], TempRow.StAtr, ReportRows[i].Lyr,
                                                    ReportRows[i].FloorZ, ReportRows[i].CelingZ, RptTotArea, DwgTotArea, 0,
                                                    UNITS_RPT, false, false);
              ReportRows.Insert(i, TempRow);
              i := i + 2;
            end
            else begin
              i := i + 1;
            end;
          end;
        end;

        //set up and add totals row
        TempRow.RowType := TOTSROW;
        for col := 1 to 10 do begin
          TempRow.Columns[col] := SubstituteTags (TotPatterns[col], TotAtr, 'Tag invalid for this Row',
                                                zero, zero, RptTotArea, DwgTotArea, TotCount, UNITS_RPT, false, false);
          CustomTots (TempRow.Columns[col], 1, ReportRows.Count-1);
        end;
        ReportRows.Add(TempRow);

        //set up and add title row
        if not BlankString (RptTitle) then begin
          TempRow.RowType := TITLEROW;
          TempRow.Columns[1] := string(RptTitle);
          for i := 2 to 10 do
            TempRow.Columns[i] := '';
          ReportRows.Insert(0, TempRow);
        end;


        //create (or re-use) entities
        CheckTxtScale;
        dflttxtsize := DefaultTxtSize;
        for i := 1 to 10 do ColWidths[i] := 0.0;
        HeadingWidth := 0.0;
        setnil (firstent.addr);
        for RowType := TITLEROW to TOTSROW do begin
          TxtFont := string (GetSvdStr ('SpdhSl' + inttostr(RowType) + 'Font', ''));
          if BlankString(TxtFont) then begin
            if RowType < 10 then
              TxtFont := string (GetSvdStr ('dhSPStl' + inttostr(RowType) + 'Font', PGsaveVar^.fontname))
            else
              TxtFont := string (PGsaveVar^.fontname);
          end;

          TxtHeight := GetSvdRl ('SpdhSl' + inttostr(RowType) + 'Siz', 0);
          if TxtHeight = 0 then
            TxtHeight := GetSvdRl ('dhSPStl' + inttostr(RowType) + 'Siz', dflttxtsize);
          TxtHeight := ActualTextSize (TxtHeight);

          SpaceAroundText(RowType, SpaceAbove[RowType], SpaceBelow[RowType], SpaceLR);
          SpaceAbove[RowType] := SpaceAbove[RowType] * TxtHeight;
          SpaceBelow[RowType] := SpaceBelow[RowType] * TxtHeight;

          Aspect := GetSvdRl ('SpdhSl' + inttostr(RowType) + 'Asp', 0.0);
          if RealEqual(Aspect, 0.0) then
            Aspect := GetSvdRl ('dhSPStl' + inttostr(RowType) + 'Asp', 1.0);

          Slant := getsvdRl ('SpdhSl' + inttostr(RowType) + 'Sln', 999.0);
          if Slant = 999.0 then begin
            if RowType < 10 then
              Slant := getsvdRl ('dhSPStl' + inttostr(RowType) + 'Sln', 0.0)
            else
              Slant := 0.0;
          end;

          Weight := getsvdInt ('SpdhSl' + inttostr(RowType) + 'Wgt', -999);
          if weight = -999 then
            if RowType < 10 then
              weight := getsvdInt ('dhSPStl' + inttostr(RowType) + 'Wgt', 1)
            else
              weight := 1;

          TxtClr := GetSvdInt ('SpdhSl' + inttostr(RowType) + 'Clr', 0);
          if TxtClr = 0 then
            if RowType < 10 then
              TxtClr := GetSvdInt ('dhSPStl' + inttostr(RowType) + 'Clr', clrWhite)
            else
              TxtClr := clrWhite;

          for row := 0 to ReportRows.Count-1 do if ReportRows[row].RowType = RowType then begin
            TempRow := ReportRows[row];
            if TempRow.RowType in [TITLEROW, SUBHEADROW] then begin
              TempRow.ColAdrs[1] := ReportTextEnt (TempRow.Columns[1], HeadingWidth, TempRow.RowHeight);
              if isnil (TempRow.ColAdrs[1]) then
                TempRow.FirstNonBlank := 0
              else
                TempRow.FirstNonBlank := 1;
            end
            else begin
              col := 1;
              TempRow.FirstNonBlank := 0;
              AvailableWidth := 0;
              while (col < 10) and blankstring (TempRow.Columns[col]) do begin
                AvailableWidth := AvailableWidth + ColWidths[Col];
                col := col+1;
              end;
              TempRow.FirstNonBlank := col;

              if TempRow.RowType in [TOTSROW, SUBTOTSROW] then begin
                if TempRow.RowType = SUBTOTSROW then
                  Patterns[col] := SubTotPatterns[Col]
                else
                  Patterns[col] := TotPatterns[Col];

                if ContainsGroupTagOrNoTags (string(Patterns[Col])) then begin
                  AvailableWidth := AvailableWidth + ColWidths[Col];
                  CombinedWidth := AvailableWidth;
                  TempRow.ColAdrs[col] := ReportTextEnt (TempRow.Columns[col], CombinedWidth, TempRow.RowHeight);
                  if CombinedWidth > AvailableWidth then
                    ColWidths[Col] := ColWidths[Col] + CombinedWidth - AvailableWidth;
                  col := col + 1;
                end;
                while col < 10 do begin
                  TempRow.ColAdrs[col] := ReportTextEnt (TempRow.Columns[col], ColWidths[Col], TempRow.RowHeight);
                  col := col+1;
                end;
              end   // TOTSROW, SUBTOTSROW
              else begin
                for col := 1 to 10 do begin
                  TempRow.ColAdrs[col] := ReportTextEnt (TempRow.Columns[col], ColWidths[Col], TempRow.RowHeight);
                end;
              end;
            end;
            ReportRows[row] := TempRow;
          end;
        end;

        SpaceLR := SpaceLR * ReportRows[0].RowHeight;  //SpaceLR is a factor to be applied to title row size
        row := 0;
        while row < ReportRows.Count do begin
          if (ReportRows[row].RowType in [TITLEROW, SUBHEADROW]) and isnil (ReportRows[row].ColAdrs[1]) then
            ReportRows.Delete(row)
          else
            row := row+1;
        end;

        ReportTopRight := ReportTopLeft;
        for col := 1 to 10 do begin
          ReportTopRight.x := ReportTopRight.x + ColWidths[col] + 2*SpaceLR;
        end;
        if not doLines then
          ReportTopRight.x := ReportTopRight.x - 2*SpaceLR;    // do not need space before first col or after last col
        col := 10;
        while (col > 1) and (ColWidths[col] = zero) do begin
          ReportTopRight.x := ReportTopRight.x - 2*SpaceLR;
          col := col-1
        end;


        ypos := ReportTopLeft.y;

        l^.Pnt1 := ReportTopLeft;  // use l^.Pnt1/2 to store report extents
        l^.Pnt2 := ReportTopLeft;

        if doLines then
          ypos := ypos - SpaceAbove[ReportRows[0].RowType];
        ypos := ypos - ReportRows[0].RowHeight;
        xpos := ReportTopLeft.x;
        for row := 0 to ReportRows.Count-1 do begin
          for col := 1 to 10 do begin
            if col = 1 then begin
              xpos := ReportTopLeft.x;
              if doLines then
                xpos := xpos + SpaceLR;
            end
            else begin
              xpos := xpos + ColWidths[col-1] + 2*SpaceLR;
            end;
            if ent_get (ent, ReportRows[row].ColAdrs[col]) then begin
              if (ReportRows[row].RowType in [SUBTOTSROW, TOTSROW]) and (col>1) and
                 (col = ReportRows[row].FirstNonBlank)
              then begin
                ent.txtpnt.x := xpos + ColWidths[col];
                ent.txtpnt.y := ypos;
                ent.txttype.justification := TEXT_JUST_RIGHT;
              end
              else begin
                ent.txtpnt.x := xpos;
                ent.txtpnt.y := ypos;
                case ReportRows[row].RowType of
                  TITLEROW, SUBHEADROW:
                    begin
                      if ReportRows[row].RowType = TITLEROW then
                        i := 11
                      else
                        i := 12;
                      case l^.RptTxtAlign[i] of
                        'C' : begin
                                ent.txtpnt.x := (ReportTopLeft.x + ReportTopRight.x) / 2;
                                ent.txttype.justification := TEXT_JUST_CENTER;
                              end;
                        'R' : begin
                                ent.txtpnt.x := ReportTopRight.x;
                                if doLines then
                                  ent.txtpnt.x := ent.txtpnt.x - SpaceLR;
                                ent.txttype.justification := TEXT_JUST_RIGHT;
                              end;
                      end;
                    end
                  else begin
                      case l^.RptTxtAlign[col] of
                        'C' : begin
                                ent.txtpnt.x := xpos + ColWidths[col] / 2;
                                ent.txttype.justification := TEXT_JUST_CENTER;
                              end;
                        'R' : begin
                                ent.txtpnt.x := xpos + ColWidths[col];
                                ent.txttype.justification := TEXT_JUST_RIGHT;
                              end;
                      end;
                    end;
                end;
              end;
              ent_update (ent);

              //add (or re-use) attribute identifying this entity as part of the report;
              adr := atr_entfirst (ent);     // if re-using an entity then alse re-use its attrib
              setnil (AtrToReuse.addr);
              while atr_get (atr, adr) do begin
                adr := atr_next (atr);
                if (string(atr.name)).StartsWith('SpdhR') then begin
                  if isnil (AtrToReuse.addr) then
                    AtrToReuse := atr
                  else
                    atr_delent (ent, atr);
                end;
              end;
              if not atr_get (atr, AtrToReuse.addr) then begin  // create a new attrib
                atr_init (atr, atr_str255);
                atr.name := 'SpdhR';
                atr.shstr := 'temp';
                atr_add2ent (ent, atr);
              end;
              pEntAtrStr := addr (atr.shstr);
              pEntAtrStr^.Len := 255;
              pEntAtrStr^.RowType := ReportRows[row].RowType;
              pEntAtrStr^.ColNum := col;
              if pEntAtrStr^.RowType = DATAROW then
                pEntAtrStr^.Tag := ReportRows[row].StAtr.tag
              else
                pEntAtrStr^.Tag := '';
              case ReportRows[row].RowType of
                TITLEROW, COLHEADROW : pEntAtrStr^.Pattern := str60(ent.txtstr);
                SUBHEADROW : pEntAtrStr^.Pattern := str60(SubHeading);
                DATAROW : pEntAtrStr^.Pattern := str60(Patterns[col]);
                SUBTOTSROW : pEntAtrStr^.Pattern := str60(SubTotPatterns[col]);
                TOTSROW : pEntAtrStr^.Pattern := str60(TotPatterns[col]);
              end;
              atr.name := SysAtrName + atrname(inttostr(row));
              atr_update (atr);
//              atr_add2ent (ent, atr);

              if TopLeftSet then
                ent_draw (ent, drmode_white);
              l^.Pnt2.x := max (l^.Pnt1.x, xpos + ColWidths[col]);
              l^.Pnt2.y := min (l^.Pnt1.y, ypos);
            end;
          end;

          ypos := ypos - SpaceBelow[ReportRows[row].RowType];
          if row < (ReportRows.Count-1) then begin
            ypos := ypos - SpaceAbove[ReportRows[row+1].RowType] - ReportRows[row+1].RowHeight;
          end;
        end;

        if dolines then begin
          //NOTE: I use ent.plynt as a convenient array of points to use in the
          //      following calculations.  It is a convenient array only, and has
          //      nothing to do with a polygon entity as such.
          //      Points 1 & 2 define the top line and initially points 3 & 4 are
          //      set equal to them.  Pnts 3 & 4 are moved down for each line until
          //      at the end of the row loop they define the bottom line (and by
          //      inference points 1 & 4 then define the left vertical line and
          //      points 2 & 3 the right vertical line).
          //      Points 11 thru 19 define the tops of vertical column lines and
          //      points 21 thru 29 define the corresponding bottoms of those lines.

          //create top horizontal line
          l^.Pnt2.x := l^.Pnt2.x + SpaceLR;
          ent.plypnt[1].x := botleft.x;
          ent.plypnt[1].y := topright.y;
          ent.plypnt[1].z := zero;
          ent.plypnt[2].x := l^.pnt2.x;
          ent.plypnt[2].y := topright.y;
          ent.plypnt[2].z := zero;
          ent.plypnt[3] := ent.plypnt[2];
          ent.plypnt[4] := ent.plypnt[1];
          ReportLineEnt(ent.plypnt[1], ent.plypnt[2]);

          // create horizontal lines below each report line
          for row := 0 to ReportRows.count-1 do begin
            ent.plypnt[3].y := ent.plypnt[3].y - ReportRows[row].RowHeight -
                               spaceabove[ReportRows[row].RowType] -
                               spacebelow[ReportRows[row].RowType];
            ent.plypnt[4].y := ent.plypnt[3].y;
            ReportLineEnt(ent.plypnt[4], ent.plypnt[3]);
          end;

          // create vertical lines at each side of report
          ReportLineEnt(ent.plypnt[1], ent.plypnt[4]);
          ReportLineEnt(ent.plypnt[2], ent.plypnt[3]);

          // set up horizontal spacing of other vertical lines
          ent.plypnt[10] := ent.plypnt[1];
          for col := 1 to 9 do begin
            ent.plypnt[10 + col].x := ent.plypnt[9 + col].x + ColWidths[col] + 2*SpaceLR;
            ent.plypnt[10 + col].y := ent.plypnt[9 + col].y;
            ent.plypnt[10 + col].z := ent.plypnt[9 + col].z;
            ent.plypnt[20 + col] := ent.plypnt[10 + col];
          end;

          // iterate through rows and create vertical lines as required
          ypos := topright.y;
          for row := 0 to ReportRows.Count-1 do begin
            ypos := ypos - ReportRows[row].RowHeight - spaceabove[ReportRows[row].RowType] - spacebelow[ReportRows[row].RowType];
            case ReportRows[row].RowType of
              TITLEROW :
                for i := 11 to 19 do begin
                  ent.plypnt[i].y := ypos;
                  ent.plypnt[i+10].y := ypos;
                end;
              DATAROW, COLHEADROW :
                for i := 21 to 29 do
                  ent.plypnt[i].y := ypos;
              SUBHEADROW :
                for i := 11 to 19 do begin
                  if ent.plypnt[i].y > (ent.plypnt[i+10].y - abszero) then
                     ReportLineEnt(ent.plypnt[i], ent.plypnt[i+10]);
                  ent.plypnt[i].y := ypos;
                  ent.plypnt[i+10].y := ypos;
                end;
              SUBTOTSROW, TOTSROW :
                for Col := 1 to 9 do begin
                  i := Col+10;
                  if ReportRows[row].FirstNonBlank > Col then begin
                    if ent.plypnt[i].y > (ent.plypnt[i+10].y - abszero) then
                      ReportLineEnt(ent.plypnt[i], ent.plypnt[i+10]);
                    ent.plypnt[i].y := ypos;
                  end;
                  ent.plypnt[i+10].y := ypos;
                end;
            end;
            for i := 11 to 19 do
              if ent.plypnt[i].y > (ent.plypnt[i+10].y - abszero) then
                ReportLineEnt(ent.plypnt[i], ent.plypnt [i+10]);
          end;

        end;

        if not TopLeftSet then begin
          mode_init (l^.lblmode);
          mode_group (l^.lblmode, firstent);
          NextState(state_ReportPos);
        end;

        if DtlRptOptions.chbClipboard.Checked then begin
          ClipbrdStr := '';
          for i := 0 to ReportRows.Count-1 do  begin
            col := 10;
            while (col>1) and Blankstring(ReportRows[i].Columns[col]) do
              col := col-1;
            for j := 1 to col do begin
              ClipbrdStr := ClipbrdStr + ReportRows[i].Columns[j];
              if j = col then
                ClipbrdStr := ClipbrdStr + sLineBreak
              else
                ClipbrdStr := ClipbrdStr + #9;
            end;
          end;
          try
            Clipboard.AsText := ClipbrdStr;
          except
            lmsg_ok (101); //Could not copy to clipboard
          end;
        end;
      end; //modalresult = mrok

      while EntsToReuse.Count > 0 do begin
        if ent_get (ent, EntsToReuse[0]) then begin
          ent_draw (ent, drmode_black);
          ent_del (ent);
        end;
        EntsToReuse.Delete(0);
      end;
    finally
      EntsToReuse.Free;
      ReportRows.Free;
      lyr_set (svLyr);
    end;

  finally
    DtlRptOptions.Free;
    if OriginalSelection then
      ssClear (hilite_ss);
  end;
END;

PROCEDURE HiliteSelected (var mode : mode_type; turnON : boolean);
VAR
  ent : entity;
  adr : lgl_addr;
  i : integer;
  unhilite_list : TList<lgl_addr>;
BEGIN
  mode_enttype (mode, entpln);
  mode_atr (mode, STANDARD_ATR_NAME);
  unhilite_list := TList<lgl_addr>.create;
  try
    adr := ent_first (mode);
    while ent_get (ent, adr) do begin
      adr := ent_next (ent, mode);
      hi_lite (turnON, ent, true, true);
      if turnON then begin
        if not lyr_ison (ent.lyr) then begin
          ent.color := clrltred;
          ent.ltype := ltype_dashed;
          ent.spacing := pixsize * 15;
          ent_draw_dl (ent, drmode_white, true);
        end;
        ssAdd (hilite_ss, ent);
      end
      else begin
        if not lyr_ison (ent.lyr) then begin  // remove 'not on' entities first, as otherwise co-incident 'on' entities may effectively be undrawn
          hi_lite (false, ent, true, false);
          ent.ltype := ltype_dashed;
          ent.spacing := pixsize * 15;
          ent_draw_dl (ent, drmode_black, true);
        end
        else begin  // add to list for later processing
          unhilite_list.Add(ent.addr);
        end;
      end;
    end;
    if unhilite_list.Count > 0 then for i := 0 to unhilite_list.Count-1 do begin
      if ent_get (ent, unhilite_list[i]) then begin
        hi_lite (false, ent, true, true);
      end;
    end;
  finally
    unhilite_list.free;
  end;
  if not turnON then begin
    unhilite_all (true, true, false);
    ssClear (hilite_ss);
  end;
END;

procedure TDtlRptOptions.btnCopyNameClick(Sender: TObject);
begin
  edRptTitle.Text := cbRptName.Text;
end;

function TDtlRptOptions.NameInList : boolean;
var
  i : integer;
begin
  result := cbRptName.ItemIndex >= 0;
  if (not result) and (cbRptName.Items.Count > 0) then
    for i := 0 to cbRptName.Items.Count-1 do begin
      if cbRptName.Items[i].ToUpper = (string(cbRptName.Text)).ToUpper then begin
        result := true;
        exit;
      end;
    end;
end;

procedure TDtlRptOptions.btnNoTitleClick(Sender: TObject);
begin
  edRptTitle.Text := '';
end;

procedure TDtlRptOptions.cbRptNameChange(Sender: TObject);
var
  mode : mode_type;
  adr : lgl_addr;
  ent : entity;
  attrname : atrName;
  atr : attrib;
  pEntAtrStr : ^EntAtrStrRec;

begin
  if BlankString (cbRptName.Text) then
    btnCreate.Enabled := false
  else begin
    btnCreate.Enabled := true;
    if NameInList then begin
      edRptTitle.Text := '';
      ButtonLblParams (82, [], btnCreate);  //Replace Report|Replace an Existing Report using current report settings
      attrname := 'SpdhR' + atrname(IntToZeroFilledStr(cbRptName.itemIndex, 3 ));
      if atr_sysfind (attrname, atr) and (atr.atrtype = atr_str) and (length(atr.str) > 2) then begin
        case atr.str[2] of
           'L' : rbSubtotsByLyr.Checked := true;
           'C' : rbSubtotsByCat.Checked := true
           else rbNoSubtots.Checked := true;
        end;
      end;

      attrname := attrname + '0';
      mode_init (mode);
      mode_lyr (mode, lyr_all);
      mode_enttype (mode, enttxt);
      mode_atr (mode, attrname);
      adr := ent_first (mode);
      if ent_get (ent, adr) and atr_entfind (ent, attrname, atr) and (atr.atrtype = atr_str255) then begin
        pEntAtrStr := addr (atr.shstr);
        if pEntAtrStr.RowType = TITLEROW then
          edRptTitle.Text := string(ent.txtstr)
      end;
    end
    else
      ButtonLblParams (81, [], btnCreate); //Create New Report|Create a New Report and place it on the current layer
  end;
end;

procedure TDtlRptOptions.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  SaveFormPos (TForm(Sender));
end;

procedure TDtlRptOptions.FormCreate(Sender: TObject);
var
  i : integer;
  atr : attrib;
  s : shortstring;
  mode : mode_type;
  adr : lgl_addr;
  ent : entity;
  entatrname : atrname;
  pEntAtrStr : ^entatrstrrec;
  first : boolean;
  NumNotFound : integer;
  LastFound : integer;
begin
  SetFormPos (TForm(Sender));
  i := 0;
  first := true;
  LastFound := -1;
  NumNotFound := 0;
  while (i < 1000) and (NumNotFound < 10) do begin
    if atr_sysfind ('SpdhR' + atrname(IntToZeroFilledStr(i, 3 )), atr) and (atr.atrtype = atr_str)
    then begin
      NumNotFound := 0;
      LastFound := i;
      cbRptName.Items.Add((string(atr.str)).Substring(2));   //note: first character of atr.str indicates type of selection and is not part of the name
      if first then begin
        first := false;
        case atr.str[2] of
           'L' : rbSubtotsByLyr.Checked := true;
           'C' : rbSubtotsByCat.Checked := true
           else rbNoSubtots.Checked := true;
        end;

        entatrname := 'SpdhR' + atrname(IntToZeroFilledStr(i, 3 )) + '0';
        mode_init (mode);
        mode_lyr (mode, lyr_all);
        mode_enttype (mode, enttxt);
        mode_atr (mode, entatrname);
        adr := ent_first (mode);
        if ent_get (ent, adr) and atr_entfind (ent, entatrname, atr) and (atr.atrtype = atr_str255) then begin
          pEntAtrStr := addr (atr.shstr);
          if pEntAtrStr^.RowType = TITLEROW then
            edRptTitle.Text := string(ent.txtstr)
          else
            edRptTitle.Text := '';
        end
        else
          edRptTitle.Text := '';
      end;
    end
    else
      NumNotFound := NumNotFound + 1;

    i := i+1;
  end;

  if NothingSelected then begin
    cbRptName.ItemIndex := 0
  end
  else begin
    cbRptName.Text := 'Report ' + IntToStr(LastFound+2);

    s := GetSvdStr ('SpdhRptTitle', DummyStr);
    if s = DummyStr then
      s := GetSvdStr ('dhSPRptTitle', '');
    edRptTitle.Text := string(s);

    case l^.RptSubtotBy of
      tag_lyr : rbSubtotsByLyr.Checked := true;
      tag_cat : rbSubtotsByCat.Checked := true
      else rbNoSubtots.Checked := true;
    end;
  end;

  LabelCaption (315, lblRptName);      //Report Name:
  LabelCaption (316, lblRptTitle);     //Report Title:
  RadioCaption (309, rbNoSubTots);     //No Subtotals
  RadioCaption (310, rbSubtotsByLyr);  //By Layer
  RadioCaption (311, rbSubtotsByCat);  //By Category
  CheckCaption (312, chbClipboard);    //To Clipboard

  ButtonLblParams (81, [], btnCreate); //Create New Report|Create a New Report and place it on the current layer
  ButtonLblParams (83, [], btnCopyName);  //Copy Name|Copy Report Name to Report Title
  ButtonLblParams (84, [], btnNoTitle);  //No Title|Delete Report Title
end;

PROCEDURE DoAllReports;
VAR
  i : integer;
  atr : attrib;
  major, minor, patch, build : integer;
BEGIN
  if (l^.DCADver = 0) and DataCADVersion (major, minor, patch, build) then begin
    if DataCADVersionEqOrGtr (21, 0, 1, 0) then
      l^.DCADver := 3
    else if DataCADVersionEqOrGtr (17, 0, 0, 0) then
      l^.DCADver := 2
    else l^.DCADver := 1;
  end;

  if l^.DCADver >= 3 then
    undoStartTransaction;

  i := 0;
  while (i < 1000) and atr_sysfind ('SpdhR' +  atrname(IntToZeroFilledStr(i, 3)), atr) do begin
    if atr.atrtype = atr_str then begin
      doReport (false, atr.str);
    end;
    i := i+1;
  end;

  if l^.DCADver >= 3 then
    undoEndTransaction;

  END;


end.
