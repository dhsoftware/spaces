unit Lyr;

interface
  uses URecords;

{$TYPEINFO ON}
  type
    TLyr = Class(TObject)
       private
          MyAddr : lyrAddr;
       published
         constructor Create(Addr : lyraddr);
         property Addr : lyraddr
             read MyAddr;
    end;
{$TYPEINFO OFF}

implementation
 constructor TLyr.Create(Addr : lyraddr);
 begin
   MyAddr  := Addr;
 end;


end.