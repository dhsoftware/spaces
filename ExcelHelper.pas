unit ExcelHelper;

interface

  uses Settings, URecords, SysUtils, CommonStuff, VCL.Grids, VCL.Menus,
  Language, UInterfaces, UConstants;

  FUNCTION xlsColHeading (colnum : integer) : string;
  FUNCTION FileSheetNdx (const FileSheet : str255; CreateIfNotExists : boolean) : integer;
  PROCEDURE SaveColAssignment (SettingPrefix : atrname; CONST sg : TStringGrid; StartCol : integer);   overload;
  PROCEDURE SaveColAssignment (filesheet : string; CONST sg : TStringGrid; StartCol : integer);   overload;
  PROCEDURE SetMapGridHeadings (VAR MapGrid : TStringGrid;
                                CONST SavePrefix : string;
                                FirstCol : integer);
  PROCEDURE BuildExportPopUpMenu (VAR menu : TPopupMenu);


implementation


  FUNCTION xlsColHeading (colnum : integer) : string;
  BEGIN
    result := chr (ord('A') + (colnum-1) mod 26);
    if colnum > 26 then begin
      result := chr (ord('A') + (colnum-1) div 26 - 1) + result;
    end;
  END;   //xlsColHeading


  PROCEDURE SetMapGridHeadings (VAR MapGrid : TStringGrid;
                                CONST SavePrefix : string;
                                FirstCol : integer);
  VAR
    i, col : integer;
    s : string;
  BEGIN
    for i := 1 to 420 do begin
      col := i-1 +FirstCol;
      if col < MapGrid.ColCount then begin
        if (i mod 42) = 1 then
          s := string (GetSvdStr (SavePrefix + inttostr(i div 42), ''));
        if length(s) > ((i-1) mod 42)*6 then begin
          MapGrid.Cells[i-1, 0] := '{' + trim(copy (s, ((i-1) mod 42)*6+1, 6)) + '}';
          if MapGrid.Cells[i-1, 0] = '{}' then
            MapGrid.Cells[i-1, 0] :=  xlsColHeading(i);
        end
        else
          MapGrid.Cells[i-1, 0] := xlsColHeading(i);
      end;
    end;
  END;   //SetMapGridHeadings


  PROCEDURE BuildExportPopUpMenu (VAR menu : TPopupMenu);
  VAR
    i : integer;
    s : string;
    menuItem : TMenuItem;
    UserField : UserFieldDef;
  BEGIN
    for i := 1 to NumTags do begin
      menuItem := TMenuItem.Create(menu);
      menuItem.Caption := Tags[i, 1] + '  ' + Tags[i, 2];
      //assign it a custom integer value..
      menuItem.Tag := i;
      menu.Items.Add(menuItem);
    end;

    i := NumTags+1;
    for UserField in l^.UserFields do begin
      menuItem := TMenuItem.Create(menu);
      menuItem.Caption := string ('{' + UserField.tag + '}  ' + UserField.desc);
      i := i+1;
      menuItem.Tag := i;
      menu.Items.Add(menuItem);
    end;

    menuItem := TMenuItem.Create(menu);
    GetMsg (68, s); //Clear
    menuItem.Caption := s;
    menuItem.Tag := -1;
    menu.Items.Add(menuItem);
  END;

  FUNCTION FileSheetNdx (const FileSheet : str255; CreateIfNotExists : boolean) : integer;
  VAR
    atr : attrib;
    done : boolean;
  BEGIN
    // find or create an attribute with file|sheet name.
    result := 1;
    done := false;
    repeat
      atr.name := 'SpdhXL' + shortstring (IntToStr(result));
      if atr_sysfind (atr.name, atr) then begin
        if (atr.atrtype = atr_str255) and (atr.shstr = FileSheet) then
          done := true
        else
          inc (result);
      end
      else begin
        done := true;
        if CreateIfNotExists then begin
          atr_init (atr, atr_str255);
          atr.name := 'SpdhXL' + shortstring(IntToStr(result));
          atr.shstr := FileSheet;
          atr_add2sys (atr);
        end
        else
          result := 0;
      end;
    until done or (result > 9999);
    if result > 9999 then
      result := 0;
  END;


  PROCEDURE SaveColAssignment (SettingPrefix : atrname; CONST sg : TStringGrid; StartCol : integer);
  VAR
    i : integer;
    s : str255;
    s8 : str8;
    isDefault : boolean;
  BEGIN
    isDefault := (SettingPrefix = 'SpdhXLdftC');
    for i := StartCol to sg.ColCount-1 do begin
      if ((i-StartCol) mod 42) = 0 then
        s := str255(stringofchar (' ', 255));
      if (i < sg.ColCount) and sg.Cells[i, 0].StartsWith('{') then begin
        s8 := str8(sg.Cells[i, 0]);
        move (s8[2], s[((i-StartCol) mod 42)*6 + 1], length(s8)-2);
      end;
      if (((i-StartCol) mod 42) = 41) or (i = sg.ColCount-1) then begin
        if length(trim(string(s))) > 0 then
          SaveStr (SettingPrefix + atrname (inttostr((i - StartCol) div 42)), s, isDefault)
        else
          DeleteSetting (SettingPrefix + atrname (inttostr((i - StartCol) div 42)));
      end;
    end;

  END;

  PROCEDURE SaveColAssignment (filesheet : string; CONST sg : TStringGrid; StartCol : integer);
  VAR
    ndx : integer;
    SettingPrefix : atrname;
  BEGIN
    ndx := FileSheetNdx (str255(FileSheet), true);
    SettingPrefix := 'SpdhXL' + atrname(inttostr (ndx)) + 'C';
    SaveColAssignment (SettingPrefix, sg, StartCol);
  END;

  end.
