unit UserDefined;

interface

uses SysUtils, Settings, URecords, UInterfaces, UConstants, System.IOUtils;

FUNCTION UserDefinedFileName : string;

implementation

FUNCTION UserDefinedFileName : string;
var
  MacroPath : shortstring;
  PrevFile : string;
BEGIN
  getpath (MacroPath, pathmcr);
  result := string(MacroPath) + 'SpacesUserDefinedFields.xml';
  PrevFile := PrevMacroPath + 'SpacesUserDefinedFields.xml';

  if CompareText (result, PrevFile) <> 0 then begin
    if not fileexists (result) and fileexists (PrevFile) then begin
      TFile.Copy (PrevFile, result);
    end;
  end;

END;

end.
