unit SelectSpace;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, URecords, System.Generics.Collections,
  UInterfaces, CommonStuff, UConstants, Settings, Language;

type
  TSpaceSelect = class(TForm)
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    Button1: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    RadioButtons : array of TRadioButton;

    procedure SetOptions (Spaces : TList<lgl_addr>);
    function Selected : integer;
  end;

var
  SpaceSelect: TSpaceSelect;

implementation

{$R *.dfm}

procedure TSpaceSelect.SetOptions (Spaces : TList<lgl_addr>);
var
  LineSpace : integer;
  i : integer;
  pStandardAtrStr : ^StandardAtrStr;
  ent : entity;
  atr : attrib;
begin
  if Spaces.Count < 2 then
    exit;   // should never happen ... this form should only be displayed when there is more than 1 space to choose from
  LineSpace := RadioButton2.top - RadioButton1.top;
  setLength (RadioButtons, Spaces.Count);
  RadioButtons[0] := RadioButton1;
  RadioButtons[1] := RadioButton2;
  if Spaces.Count > 2 then for i := 2 to (Spaces.Count-1) do begin
    RadioButtons[i] := TRadioButton.Create(self);
    RadioButtons[i].Parent := RadioButton1.Parent;
    RadioButtons[i].Top := RadioButtons[i-1].Top + LineSpace;
    RadioButtons[i].Left := RadioButton1.Left;
    RadioButtons[i].Width := RadioButton1.Width;
    RadioButtons[i].Height := RadioButton1.Height;
    RadioButtons[i].Anchors := RadioButton1.Anchors;
  end;

  for i := 0 to Spaces.Count-1 do begin
    if ent_get (ent, Spaces[i]) and
       atr_entfind (ent, STANDARD_ATR_NAME, atr) and
       (atr.atrtype = atr_str255)  then begin
      pStandardAtrStr := Addr (atr.shstr);
      RadioButtons[i].Caption := string (pStandardAtrStr^.fields.Num + ' - ' + pStandardAtrStr^.fields.Name);
    end
    else
      RadioButtons[i].Caption := 'error';
  end;

  SpaceSelect.Height := RadioButtons[1].top + (Spaces.Count+2) * LineSpace;
end;

procedure TSpaceSelect.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  SaveFormPos (TForm(Sender));
end;

procedure TSpaceSelect.FormCreate(Sender: TObject);
begin
  SetFormPos(TForm(Sender));
  (TForm(Sender)).Caption := GetMsg (129);
end;

function TSpaceSelect.Selected : integer;
begin
  for result := Length (RadioButtons)-1 downto 0 do
    if RadioButtons[result].Checked then
      exit;
  result := 0;
end;

end.
