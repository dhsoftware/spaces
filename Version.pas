unit Version;

interface

(*function FileDescription: String;
function LegalCopyright: String;
function DateOfRelease: String; // Proprietary
function ProductVersion: String;   *)
function FileVersion: String;

function DataCADVersion (var major, minor, patch, build : integer) : boolean;
function DataCADVersionEqOrGtr (const major, minor, patch, build : integer) : boolean;

implementation

uses
  Winapi.Windows, System.SysUtils, System.Classes, Math, UInterfaces, UConstants;


function GetVersionInfo(AIdent: String): String;

type
  TLang = packed record
    Lng, Page: WORD;
  end;

  TLangs = array [0 .. 10000] of TLang;

  PLangs = ^TLangs;

var
  BLngs: PLangs;
  BLngsCnt: Cardinal;
  BLangId: String;
  RM: TMemoryStream;
  RS: TResourceStream;
  BP: PChar;
  BL: Cardinal;
  BId: String;

begin
  // Assume error
  Result := '';

  RM := TMemoryStream.Create;
  try
    // Load the version resource into memory
    RS := TResourceStream.CreateFromID(HInstance, 1, RT_VERSION);
    try
      RM.CopyFrom(RS, RS.Size);
    finally
      FreeAndNil(RS);
    end;

    // Extract the translations list
    if not VerQueryValue(RM.Memory, '\\VarFileInfo\\Translation', Pointer(BLngs), BL) then
      Exit; // Failed to parse the translations table
    BLngsCnt := BL div sizeof(TLang);
    if BLngsCnt <= 0 then
      Exit; // No translations available

    // Use the first translation from the table (in most cases will be OK)
    with BLngs[0] do
      BLangId := IntToHex(Lng, 4) + IntToHex(Page, 4);

    // Extract field by parameter
    BId := '\\StringFileInfo\\' + BLangId + '\\' + AIdent;
    if not VerQueryValue(RM.Memory, PChar(BId), Pointer(BP), BL) then
      Exit; // No such field

    // Prepare result
    Result := BP;
  finally
    FreeAndNil(RM);
  end;
end;
(*
function FileDescription: String;
begin
  Result := GetVersionInfo('FileDescription');
end;

function LegalCopyright: String;
begin
  Result := GetVersionInfo('LegalCopyright');
end;

function DateOfRelease: String;
begin
  Result := GetVersionInfo('DateOfRelease');
end;

function ProductVersion: String;
begin
  Result := GetVersionInfo('ProductVersion');
end;
             *)
function FileVersion: String;
begin
  Result := GetVersionInfo('FileVersion');
end;


function DataCADVersion (var major, minor, patch, build : integer) : boolean;
var
  ExePath : shortstring;
  ExeName : string;
  VerInfoSize, VerValueSize, Dummy: DWORD;
  VerInfo: Pointer;
  VerValue: PVSFixedFileInfo;
begin
  UInterfaces.GetPath (ExePath, UConstants.PathInstall);
  ExeName := string(ExePath) + 'DCADWIN.EXE';
  VerInfoSize := GetFileVersionInfoSize(PChar(ExeName), Dummy);
  GetMem(VerInfo, VerInfoSize);
  GetFileVersionInfo(PChar(ExeName), 0, VerInfoSize, VerInfo);
  result := VerQueryValue(VerInfo, '\', Pointer(VerValue), VerValueSize);

  if result then with VerValue^ do begin
    Major := dwFileVersionMS shr 16;
    Minor := dwFileVersionMS and $FFFF;
    Patch := dwFileVersionLS shr 16;
    build := dwFileVersionLS and $FFFF;
  end;
  FreeMem(VerInfo, VerInfoSize);
end;

function DataCADVersionEqOrGtr (const major, minor, patch, build : integer) : boolean;
var
  ActualMajor, ActualMinor, ActualPatch, ActualBuild : integer;
begin
  result := false;
  if DataCADVersion (ActualMajor, ActualMinor, ActualPatch, ActualBuild) then begin
    if ActualMajor > Major then begin
      result := true;
      exit;
    end
    else if ActualMajor < Major then begin
      exit;
    end;
    if ActualMinor > Minor then begin
      result := true;
      exit;
    end
    else if ActualMinor < Minor then begin
      exit;
    end;
    if ActualPatch > Patch then begin
      result := true;
      exit;
    end
    else if ActualPatch < Patch then begin
      exit;
    end;
    result := ActualBuild >= Build;
  end;
end;

end.
