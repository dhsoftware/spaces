unit FileUtil;

interface

  USES  SysUtils, System.Classes, UInterfaces, UConstants, CommonStuff;

  PROCEDURE DeleteTempDwgFile (VAR TempFile : lglFile);
  PROCEDURE OpenTempDwgFile (VAR TempFile : lglFile; forWriting : boolean);

  FUNCTION FilesTheSame (const File1, File2: string): boolean;

implementation
  PROCEDURE OpenTempDwgFile (VAR TempFile : lglFile; forWriting : boolean);
  VAR
    Path, DwgFileName : shortstring;
  BEGIN
    getpath (Path, pathtmp);
    dwgname (DwgFileName);
    AssignFile (TempFile, string(Path) + string(DwgFileName) + '_Spaces.tmp');
    if ForWriting then
      rewrite (TempFile)
    else
      reset (tempfile);
  END;

  PROCEDURE DeleteTempDwgFile (VAR TempFile : lglFile);
  VAR
    Path, DwgFileName : shortstring;
  BEGIN
    getpath (Path, pathtmp);
    dwgname (DwgFileName);
    DeleteFile (string(Path) + string(DwgFileName) + '_Spaces.tmp');
  END;

  FUNCTION FilesTheSame (const File1, File2: string): boolean;
  VAR
    ms1, ms2: TMemoryStream;
    WSFile1, WSFile2: WideString;
  BEGIN
    result := False;
    WSFile1 := File1;
    WSFile2 := File2;
    ms1 := TMemoryStream.Create;
    try
      ms1.LoadFromFile(pWideChar(WSFile1));
      ms2 := TMemoryStream.Create;
      try
        ms2.LoadFromFile(pWideChar(WSFile2));
        if ms1.Size = ms2.Size then
          result := CompareMem(ms1.Memory, ms2.memory, ms1.Size);
      finally
        ms2.Free;
      end;
    finally
      ms1.Free;
    end
  END;

end.
