unit CopyDist;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Language, Vcl.StdCtrls, DcadEdit, System.Math, CommonStuff,
  UConstants, Vcl.ExtCtrls, UInterfaces, Settings;

type
  TfmCopyDist = class(TForm)
    Label1: TLabel;
    dceX: TDcadEdit;
    Label2: TLabel;
    dceY: TDcadEdit;
    Label3: TLabel;
    dceZ: TDcadEdit;
    Label4: TLabel;
    dceD: TDcadEdit;
    Label5: TLabel;
    dceA: TDcadEdit;
    btnOK: TButton;
    btnCancel: TButton;
    Shape1: TShape;
    lblDestLyr: TLabel;
    cbLayer: TComboBox;
    btnInvertX: TButton;
    btnInvertY: TButton;
    btnInvertZ: TButton;
    btnInvertA: TButton;
    procedure FormCreate(Sender: TObject);
    procedure XYchange(Sender: TObject);
    procedure DAchange(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure btnInvertXClick(Sender: TObject);
    procedure btnInvertYClick(Sender: TObject);
    procedure btnInvertZClick(Sender: TObject);
    procedure btnInvertAClick(Sender: TObject);
  private
    { Private declarations }
    init : boolean;
  public
    { Public declarations }
  end;


implementation

{$R *.dfm}

procedure TfmCopyDist.btnInvertAClick(Sender: TObject);
begin
  dceA.NumValue := dceA.NumValue + pi;
end;

procedure TfmCopyDist.btnInvertXClick(Sender: TObject);
begin
  dceX.NumValue := -dceX.NumValue;
end;

procedure TfmCopyDist.btnInvertYClick(Sender: TObject);
begin
  dceY.NumValue := -dceY.NumValue;
end;

procedure TfmCopyDist.btnInvertZClick(Sender: TObject);
begin
  dceZ.NumValue := -dceZ.NumValue;
end;

procedure TfmCopyDist.btnOKClick(Sender: TObject);
begin
  if not dceX.valid then
    dceX.SetFocus
  else if not dceY.valid then
    dceY.SetFocus
  else if not dceZ.valid then
    dceZ.SetFocus
  else if not dceD.valid then
    dceD.SetFocus
  else if not dceA.valid then
    dceA.SetFocus
  else
    ModalResult := mrOK;
end;

procedure TfmCopyDist.DAchange(Sender: TObject);
begin
  dceX.OnChange := nil;
  dceY.OnChange := nil;
  dceX.NumValue := Cos(dceA.NumValue) * dceD.NumValue;
  dceY.NumValue := Sin(dceA.NumValue) * dceD.NumValue;
  dceX.OnChange := XYchange;
  dceY.OnChange := XYchange;
end;

procedure TfmCopyDist.FormActivate(Sender: TObject);
begin
  if not init then case l^.getp.key of
    s1 : cbLayer.SetFocus;
    s4 : dceY.SetFocus;
    s5 : dceZ.SetFocus;
    s6 : dceD.SetFocus;
    s7 : dceA.SetFocus;
  end;

  init := true;
end;

procedure TfmCopyDist.FormCreate(Sender: TObject);
var
  lbl, msg : string;
  lyrname : shortstring;
begin
  SetFormPos (TForm(Sender));

  LabelCaption (317, lblDestLyr); // Destination Layer

  GetLbl(100, lbl, msg);
  (Sender as TfmCopyDist).Caption := lbl;
  (Sender as TfmCopyDist).left := 0;

  ButtonLblParams(101, [], btnOK);     //OK|Accept input
  ButtonLblParams(102, [], btnCancel); //Cancel|Cancel input
  ButtonLblParams(103, [], btnInvertX);//Invert|
  ButtonLblParams(103, [], btnInvertY);//Invert|
  ButtonLblParams(103, [], btnInvertZ);//Invert|
  ButtonLblParams(103, [], btnInvertA);//Invert|

  init := false;
  dceX.AllowNegative := true;
  dceY.AllowNegative := true;
  dceZ.AllowNegative := true;
  dceA.AllowNegative := true;

  dceX.OnChange := nil;
  dceY.OnChange := nil;
  dceD.OnChange := nil;
  dceA.OnChange := nil;

  dceX.NumValue := l^.Pnt2.x;
  dceY.NumValue := l^.Pnt2.y;
  dceZ.NumValue := l^.Pnt2.z;
  dceD.NumValue := Sqrt(sqr(l^.Pnt2.x) + sqr(l^.Pnt2.y));
  dceA.NumValue := arctan2 (l^.Pnt2.y, l^.Pnt2.x);

  PopulateLayerList (96, cbLayer);  //** Same Layer as Original **
  cbLayer.ItemIndex := 0;
  if not isnil(l^.Adr) then begin
    getLongLayerName (l^.Adr, lyrname);
    cbLayer.Text := string(lyrName);
  end;

  dceX.OnChange := XYchange;
  dceY.OnChange := XYchange;
  dceD.OnChange := DAchange;
  dceA.OnChange := DAchange;

end;

procedure TfmCopyDist.XYchange(Sender: TObject);
begin
  dceD.OnChange := nil;
  dceA.OnChange := nil;
  dceD.NumValue := Sqrt(sqr(dceX.NumValue) + sqr(dceY.NumValue));
  dceA.NumValue := arctan2 (dceY.NumValue, dceX.NumValue);
  dceD.OnChange := DAchange;
  dceA.OnChange := DAchange;
end;

end.
