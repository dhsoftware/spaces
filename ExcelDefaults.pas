unit ExcelDefaults;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Buttons, Vcl.StdCtrls, Vcl.Grids, Settings,
  CommonStuff, DcadNumericEdit, UInterfaces, UConstants, Vcl.Menus, Language, System.Types,
  ExcelHelper;

type
  TFmExcelDefaults = class(TForm)
    Label1: TLabel;
    edFileName: TEdit;
    SpeedButton2: TSpeedButton;
    Label2: TLabel;
    DcadNumericEdit1: TDcadNumericEdit;
    sgColMapping: TStringGrid;
    Label3: TLabel;
    Button1: TButton;
    Button2: TButton;
    PopupMenu1: TPopupMenu;
    procedure SpeedButton2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure sgColMappingMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure MenuClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    MenuCol : integer;
    { Public declarations }
  end;


implementation

{$R *.dfm}

procedure TFmExcelDefaults.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  SaveFormPos(TForm(Sender));
end;

procedure TFmExcelDefaults.FormCreate(Sender: TObject);
var
  i : integer;
begin
  SetFormPos (TForm(Sender));
  DcadNumericEdit1.IntValue := GetSvdInt ('SpdhXLDltRow', 0);
  edFileName.Text := string(GetSvdStr ('SpdhXLDeflt', ''));

  SetMapGridHeadings (sgColMapping, 'SpdhXLdftC', 0);
  BuildExportPopUpMenu (PopupMenu1);
  for i := 0 to PopupMenu1.Items.Count-1 do
    PopupMenu1.Items[i].OnClick := MenuClick;
end;

procedure TFmExcelDefaults.MenuClick(Sender: TObject);
var
  pos, col : integer;
  s : string;
begin
  if (TMenuItem(Sender)).Tag = -1 then begin
    sgColMapping.Cells[MenuCol, 0] := xlsColHeading(MenuCol+1);
    exit;
  end;
  s := (TMenuItem(Sender)).Caption;
  pos := ansipos ('}', s);
  if pos > 0 then
    delete (s, pos+1, 100);
  pos := ansipos ('&', s);
  if pos > 0 then
    delete (s, pos, 1);
  sgColMapping.Cells[MenuCol, 0] := s;
  for col := 0 to sgColMapping.ColCount-1 do
    if col <> MenuCol then begin
      if sgColMapping.Cells[col, 0] = s then begin
        sgColMapping.Cells[col, 0] := xlsColHeading(col+1);
      end;
    end;
end;

procedure TFmExcelDefaults.sgColMappingMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  MousePoint : TPoint;
  ARow : integer;
begin
  {get X, Y in screen coordinates}
  MousePoint := sgColMapping.ClientToScreen(System.Classes.Point(X, Y));
  {get the cell clicked in}
  sgColMapping.MouseToCell(X, Y, MenuCol, ARow);
  if (ARow = 0)  then begin
    PopUpMenu1.PopUp(MousePoint.X, MousePoint.Y);
  end;
end;

procedure TFmExcelDefaults.SpeedButton2Click(Sender: TObject);
var
  OpenDlg : TOpenDialog;
  s : string;
  ss : shortstring;
begin
  OpenDlg := TOpenDialog.Create(Application);
  try
    OpenDlg.Filter := 'Excel Files (*.xls, *.xlsx)|*.xls;*.xlsx';
    OpenDlg.FilterIndex := 1;
    GetPath (ss, pathdfd);
    s := string(GetSvdStr ('SpdhXLDeflt', ss));
    if ansipos ('|pathxls|', s) = 1 then begin  //ExcelPath has been replaced with a tag ... need to substitute with actual Excelpath
      delete (s, 1, 9);
      GetPath (ss, pathxls);
      insert (string(ss), s, 1);
    end;
    if ansipos ('|pathdfd|', s) = 1 then begin  //Dflt Dwg Path has been replaced with a tag ... need to substitute with actual Excelpath
      delete (s, 1, 9);
      insert (string(ss), s, 1);
    end;
    if ExtractFilePath (edFileName.Text) <> '' then
      OpenDlg.InitialDir := ExtractFilePath (edFileName.Text)
    else
      OpenDlg.InitialDir := ExtractFilePath (s);
    if ExtractFileName (edFileName.Text) <> '' then
      OpenDlg.FileName := ExtractFileName (edFileName.Text)
    else
      OpenDlg.FileName := ExtractFileName (s);
    OpenDlg.Options := [ofFileMustExist];
    if OpenDlg.Execute then
      edFileName.Text := OpenDlg.FileName;
  finally
    OpenDlg.Free;
  end;
end;

end.
