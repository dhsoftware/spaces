unit SpaceUtil;

interface
  uses CommonStuff, UConstants, UInterfaces, UInterfacesRecords, URecords, UDCLibrary,
  PlnUtil, StrUtils, System.Generics.Collections, Language, Math, DateUtils, SysUtils,
  FileUtil, SelectSpace, Forms;

  FUNCTION CreateNewTag : atrname;

  FUNCTION LabelAngle (LblList : TList<entity>; LblStartNdx : integer) : double;   overload;
  FUNCTION LabelAngle (var mode : mode_type) : double;  overload;
  FUNCTION LabelCenter (plnent : entity; spacecntr : point;
                        var LblList : tList<entity>; var lblAngle : double) : point;  overload;
  FUNCTION LabelCenter (plnent : entity; spacecntr : point) : point;    overload;
  FUNCTION LabelCenter (plnent : entity) : point;    overload;

  FUNCTION FindSpaceFromPoint (pnt : point; sideonly, vertexonly : boolean;
                               var ent : entity; var tag : atrname) : boolean;  overload;
  FUNCTION FindSpaceFromPoint (pnt : point; sideonly, vertexonly : boolean;
                               var ent : entity; showError: boolean) : boolean;  overload;

  FUNCTION FindAllSpaceEnts (plnent : entity; VAR tag : atrname; VAR EntList : tlist<entity>) : boolean; overload
  FUNCTION FindAllSpaceEnts (plnent : entity; VAR EntList : tlist<entity>) : boolean; overload;
  FUNCTION FindAllLblEnts (plnent : entity; VAR EntList : tlist<entity>) : boolean;
  FUNCTION SpaceEnts_to_hiliteSS (var plnent : entity; makecopy : byte; hilite, undraw : boolean;
                                  var CntrlineEnt : entity; LabelOnly : boolean) : boolean;   overload;
  FUNCTION SpaceEnts_to_hiliteSS (var plnent : entity; makecopy : byte; hilite, undraw : boolean; var CntrlineEnt : entity) : boolean; overload;
  FUNCTION SpaceEnts_to_hiliteSS (var plnent : entity; hilite, undraw : boolean; var CntrlineEnt : entity) : boolean;   overload;
  FUNCTION SpaceEnts_to_hiliteSS (var plnent : entity; hilite, undraw : boolean) : boolean;    overload;

  FUNCTION IsWallCenterEnt (plnent : entity; out WallWidth : double) : boolean;
  FUNCTION FindWallCenterEnt (plnent : entity; var CenterEnt : entity) : boolean;

  PROCEDURE SpaceRotate (var plnent : entity; pivotpnt : point; ang : double; draw : boolean);
  PROCEDURE UpdateCenterEnt (var ent : entity);  overload;
  PROCEDURE UpdateCenterEnt;  overload;

  FUNCTION FindByNumber (num : str9; ignore : entaddr; var found : entity) : boolean;

  PROCEDURE DisplayCenters (ShowThem : boolean);
  PROCEDURE DisplaySurfaces (ShowThem : boolean);

  CONST
    COPY_NONE = 0;
    COPY_ORIGLYR = 1;
    COPY_TOLYR = 2;
implementation


  CONST
    TAG_NUM_NAME = 'dhSp_TagNum';


  PROCEDURE UpdateCenterEnt (var ent : entity);
  VAR
    WallWidth : double;
    ent2 : entity;
  BEGIN
    // update Wall Centre polyline if it is a separate line
    if (not IsWallCenterEnt (ent, WallWidth)) and FindWallCenterEnt (ent, ent2) then begin
      ent_draw (ent2, drmode_black);
      OffsetPln (ent, ent2, true, false, WallWidth/2, false);
      ent_draw (ent2, drmode_white);
    end;
  END;

  PROCEDURE UpdateCenterEnt;
  BEGIN
    UpdateCenterEnt (l^.ent);
  END;


  FUNCTION CreateNewTag : atrname;
  VAR
    yr,m,d,h,mi,s,ms	: word;
    i : integer;
    mode : mode_type;
    ndx : integer;
    atr : attrib;
  BEGIN
    result := 'dh~      ';
    DecodeDateTime (Now, yr, m, d, h,mi, s, ms);
    yr := yr mod 100;
    result[4] := chars[ms mod 68 + 1];
    result[5] := chars[s mod 60 + 1];
    result[6] :=	chars[mi mod 60 + 1];
    ndx := h mod 24;
    if ms > 499 then
      ndx := ndx + 24;
    if (ms > 750) and (ndx < 44) then
      ndx := ndx + 24;
    result[7] :=	chars[ndx + 1];
    ndx := d + ((yr div 5) mod 2) * 31;
    result[8] :=	chars[ndx mod 68 + 1];
    ndx := m + (yr mod 5)*12;
    result[9] :=	chars[ndx mod 68 + 1];

    if atr_sysfind (TAG_NUM_NAME, atr) then begin  // append a 3 digit sequence number from the TAG_NUM_NAME attribute
      if (atr.atrtype <> atr_str) or (length (atr.str) <> 3)  or (not cvstint (atr.str, i)) then begin
        atr.atrtype := atr_str;
        atr.str := '000';
      end;
      strinc (atr.str);
      atr_update (atr);
    end
    else begin
      atr_init (atr, atr_str);
      atr.name := TAG_NUM_NAME;
      atr.str := '000';
      atr_add2sys (atr);
    end;
    result := result + atr.str;

    // check that the tag does not already exist
    mode_init (mode);
    mode_lyr (mode, lyr_all);
    mode_atr (mode, result);
    h := 0;
    while not isnil (ent_first (mode)) and (h < 1001) do begin
      strinc (result);
      mode_init (mode);
      mode_lyr (mode, lyr_all);
      mode_atr (mode, result);
      h := h+1;
    end;
    if h > 1000 then begin
      // very unlikely to get here - really just added the incrementing h test to avoid
      // extremely remote possibility of an infinite loop. Add some random stuff to the end of
      // the string and hope for the best
      Randomize;
      result[10] := chars[trunc(Random(68))+1];
      result[11] := chars[trunc(Random(68))+1];
      result[12] := chars[trunc(Random(68))+1];
    end;
  END;

  FUNCTION LabelAngle (LblList : TList<entity>; LblStartNdx : integer) : double;
  // get angle of existing text in label - if label comes from a symbol then it is possible
  // that there could be multiple angles, so get angle from the longest piece of text
  VAR
    i : integer;
    p1, p2, p3, p4 : point;
    d, txtlen : double;
  BEGIN
    result := 0;
    txtlen := 0;
    for i := lblstartndx to lbllist.Count-1 do begin
      if lbllist[i].enttype = enttxt then begin
        txtbox (lblList[i], p1, p2, p3, p4);
        d := distance (p1, p2);
        if d > txtlen then begin
          txtlen := d;
          result := lblList[i].txtang;
        end;
      end;
    end;
    angnormalize (result);
  END;

  FUNCTION LabelAngle (var mode : mode_type) : double;
  // get angle of existing text in label - if label comes from a symbol then it is possible
  // that there could be multiple angles, so get angle from the longest piece of text
  VAR
    adr : lgl_addr;
    ent : entity;
    p1, p2, p3, p4 : point;
    d, txtlen : double;
  BEGIN
    result := 0;
    txtlen := 0;
    adr := ent_first (mode);
    while ent_get (ent, adr) do begin
      adr := ent_next (ent, mode);
      if ent.enttype = enttxt then begin
        txtbox (ent, p1, p2, p3, p4);
        d := distance (p1, p2);
        if d > txtlen then begin
          txtlen := d;
          result := ent.txtang;
        end;
      end;
    end;
    angnormalize (result);
  END;

  FUNCTION LabelCenter (plnent : entity; spacecntr : point;
                        var LblList : tList<entity>; var lblAngle : double) : point;
  VAR
//    LblList : tList<entity>;
    SvLyr : lgl_addr;
    TempLyr : lgl_addr;
//    ang : double;
    ent, newent, tempent : entity;
    first : boolean;
    entmin, entmax, lblmin, lblmax : point;
    rmat : modmat;
  BEGIN
    result := spacecntr;
//    LblList := tList<entity>.Create;
    if not FindAllLblEnts (plnent, LblList) then
      exit;
    lblAngle := LabelAngle(LblList, 0);
    first := true;
    if RealEqual(lblAngle, 0) or RealEqual (lblAngle, halfpi) or RealEqual (lblAngle, pi) or
       RealEqual (lblAngle, 3*halfpi) or RealEqual (lblAngle, twopi) then
    begin
      for tempent in LblList do begin
        ent := tempent;
        if first then begin
           ent_extent (ent, lblmin, lblmax);
           first := false;
        end
        else begin
          ent_extent (ent, entmin, entmax);
          lblmin.x := min (lblmin.x, entmin.x);
          lblmin.y := min (lblmin.y, entmin.y);
          lblmax.x := max (lblmax.x, entmax.x);
          lblmax.y := max (lblmax.y, entmax.y);
        end;
      end;
      meanpnt (lblmin, lblmax, result);
    end
    else begin
      // need to rotate label to find this definition of 'Center' - copy it to
      // a temporary layer for this process (as otherwise some entity types are
      // updated in the rotated state
      SvLyr := getlyrcurr;
      lyr_init (templyr);
      lyr_set (templyr);
      try
        for tempent in LblList do if ent_get (ent, tempent.addr) then begin
          ent_copy (ent, newent, true, false);
          ent_rotate (newent, spacecntr, -lblAngle);
          if first then begin
             ent_extent (newent, lblmin, lblmax);
             first := false;
          end
          else begin
            ent_extent (newent, entmin, entmax);
            lblmin.x := min (lblmin.x, entmin.x);
            lblmin.y := min (lblmin.y, entmin.y);
            lblmax.x := max (lblmax.x, entmax.x);
            lblmax.y := max (lblmax.y, entmax.y);
          end;
        end;
        lyr_clear (templyr);
        lyr_term (templyr);
        meanpnt (lblmin, lblmax, result);
        // unrotate resulting center
        setrotrel (rmat, lblAngle, x, spacecntr);
        xformpt (result, rmat, result);
      finally
        lyr_set (SvLyr);
      end;
    end;
  END;

  FUNCTION LabelCenter (plnent : entity; spacecntr : point) : point;
  VAR
    LblList : tList<entity>;
    lblAngle : double;
  BEGIN
    LblList := tList<entity>.Create;
    try
      result := LabelCenter (plnent, spacecntr, LblList, lblAngle);
    finally
      LblList.Free;
    end;

  END;


  FUNCTION LabelCenter (plnent : entity) : point;
  VAR
    area : double;
    temppnt, lblCntr : point;
  BEGIN
    pline_centroid (plnent.plnfrst, plnent.plnlast, area, temppnt, lblcntr, true, true);
    result := LabelCenter (plnent, lblCntr);
  END;


  FUNCTION FindSpaceFromPoint (pnt : point; sideonly, vertexonly : boolean;
                               var ent : entity; var tag : atrname) : boolean;  overload;
  VAR
    mode : mode_type;
    atr : attrib;
    adr : lgl_addr;
    tempent : entity;
    b1, b2 : boolean;
    pStandardAtrStr : ^StandardAtrStr;
    Matches : TList<lgl_addr>;
    i : integer;
    pvadr : pvtaddr;
    pv : polyvert;
    missdist : double;
    temppnt : point;
  BEGIN
    mode_init1 (mode);
    mode_enttype (mode, entpln);
    mode_atr (mode, STANDARD_ATR_NAME);
    result := ent_near (ent, pnt.x, pnt.y, mode, false);
    if result then begin
      // check if more than one
      Matches := TList<lgl_addr>.create;
      try
        repeat
          mode_ignore (mode);
          Matches.Add(ent.addr);
          ent_setunused (ent, true);
        until not ent_near (ent, pnt.x, pnt.y, mode, false);
        // set all found entities to be used again
        for i := (Matches.Count-1) downto 0 do begin
          if ent_get (tempent, Matches[i]) then begin
            ent_setunused (tempent, false);
            if not ((tempent.enttype = entpln) and
                    atr_entfind (tempent, STANDARD_ATR_NAME, atr) and
                    (atr.atrtype = atr_str255)) then
              Matches.Delete(i)
            else if vertexonly then begin
              // check that the point selected is a vertex for each found polyline
              b1 := false;
              missdist := pgSaveVar^.missdis * pixsize;
              pvadr := tempent.plnfrst;
              while (not b1) and polyvert_get (pv, pvadr, tempent.plnfrst) do begin
                pvadr := pv.Next;
                if distance (pv.pnt, pnt) <= missdist then
                  b1 := true;
              end;
              if not b1 then
                Matches.Delete(i);
            end
            else if sideonly then begin
              // check that the point selected is on a side for each found polyline
              if not FindPvSideFromPln (tempent, pnt, temppnt, pv) then
                Matches.Delete(i);
            end;
          end;
        end;
        if Matches.Count = 0 then
          result := false
        else if Matches.Count = 1 then begin
          if ent_get (ent, Matches[0]) then
            result := true
          else
            result := false;
        end
        else begin
          // prompt user to select which space
          SpaceSelect := TSpaceSelect.Create(Application);
          SpaceSelect.SetOptions(Matches);
          SpaceSelect.ShowModal;
          result := ent_get (ent, Matches[SpaceSelect.Selected]);
          SpaceSelect.Free;
        end;
      finally
        Matches.Free;
      end;

    end
    else begin
      if sideonly or vertexonly then
        exit;
      mode_init1 (mode);
      result := ent_near (ent, pnt.x, pnt.y, mode, false);
    end;
    if not result then
      exit;
    if (ent.enttype = entpln) and atr_entfind (ent, STANDARD_ATR_NAME, atr) then begin
      if atr.atrtype = atr_str255 then begin
        pStandardAtrStr := Addr (atr.shstr);
        tag := pStandardAtrStr^.fields.tag;
      end;

      exit;
    end;

    tag := '';
      adr:= atr_entfirst (ent);
    while atr_get (atr, adr) and (tag = '') do begin
      adr := atr_next (atr);
      if AnsiStartsStr ('dh~', string(atr.name)) then
        tag := atr.name;
    end;

    if tag = ''  then begin
      result := false;
      exit;
    end;

    mode_init1 (mode);
    mode_enttype (mode, entpln);
    mode_atr (mode, tag);
    adr := ent_first (mode);
    setnil (ent.addr);
    while ent_get (tempent, adr) do begin
      adr := ent_next (tempent, mode);
      if atr_entfind (tempent, STANDARD_ATR_NAME, atr) then begin
        if isnil (ent.addr) then
          ent_get (ent, tempent.addr)
        else begin
          // user must have copied a space, try to find correct one based on distance from selected point
          b1 := PointInPln (pnt, tempent) >= 0;
          b2 := PointInPln (pnt, ent) >= 0;
          if b1 and not b2 then
            ent_get (ent, tempent.addr)
          else if not b2 then begin
            if DisFromPln (pnt, tempent) < DisFromPln (pnt, ent) then
              ent_get (ent, tempent.addr);
          end;
        end;
      end;
    end;
    result := not isnil (ent.addr);
  END;

  FUNCTION FindSpaceFromPoint (pnt : point; sideonly, vertexonly : boolean;
                               var ent : entity; showError: boolean) : boolean;  overload;
  VAR
    tag : AtrName;
  BEGIN
    result := FindSpaceFromPoint (pnt, sideonly, vertexonly, ent, tag);
    if not result then begin
      lwrterr (29, true); //Existing space not found within miss distance of point entered
    end;

  END;


  FUNCTION FindAllSpaceEnts (plnent : entity; VAR tag : atrname; VAR EntList : tlist<entity>) : boolean; overload;
  ///  plnent should be a space outline.  This function will add that entity and all
  ///  related entities to the passed list (which should have already been created by
  ///  the calling logic, but may or may not be empty).
  ///  This function will return true if any entities are added, else false
  TYPE
    entRec = Record
      ent : entity;
      alreadyfound : boolean;
    end;
  VAR
    atr : attrib;
    adr : lgl_addr;
    NumLblEnts, i, ndx : integer;
    area : double;
    frstmom : URecords.point;
    cntr : URecords.point;
    lblents : array of entrec;
    tempent : entity;
    mode : mode_type;
    pStandardAtr : ^StandardAtr;
    allfound : boolean;

  BEGIN
    result := false;

    if atr_entfind (plnent, STANDARD_ATR_NAME, atr) then begin
      pStandardAtr := System.Addr (atr.shstr);
      tag := pStandardAtr^.tag;
    end
    else
      exit;

    if atr_entfind (plnent, tag, atr) and (atr.atrtype = atr_int) then begin
      NumLblEnts := atr.int;
    end
    else
      exit;

    EntList.Add(plnent);
    result := true;

    SetLength(lblents, NumLblEnts+2);
    for i := 0 to NumLblEnts do begin
      setnil (lblents[i].ent.addr);
      lblents[i].alreadyfound := false;
    end;

    // find plnent centroid - may need it to find closest ents if multiples are found
    pline_centroid (plnent.plnFrst, plnent.plnlast, area, frstmom, cntr, true,true);

    // start by checking entities in the same group as plnent
    mode_init (mode);
    mode_group (mode, plnent);
    mode_atr (mode, tag);
    adr := ent_first (mode);
    while ent_get (tempent, adr) do begin
      adr := ent_next (tempent, mode);
      if (not Addr_Equal(tempent.addr, plnent.addr)) and
         atr_entfind (tempent, tag, atr) then
        if ((atr.atrtype = atr_int) and (atr.int >= 0) and (atr.int <= NumLblEnts)) or
           (atr.atrtype = atr_str) then begin
          if atr.atrtype = atr_str then
            ndx := NumLblEnts+1
          else
            ndx := atr.int;
          if isnil (lblents[ndx].ent.addr) then
            lblents[ndx].ent := tempent
          else begin
            // need to work out which ent is closest to center of entpln
            if distance (entCenter (tempent), cntr) <
                          distance (entCenter (lblents[ndx].ent), cntr) then
              lblents[ndx].ent := tempent;
          end;
        end
    end;

    allfound := true;
    for i := 0 to NumLblEnts+1 do
      if isnil (lblents[i].ent.addr) then
        allfound := false
      else
        lblents[i].alreadyfound := true;

    if not allfound then begin
      // check entities on same layer as plnent
      mode_init (mode);
      mode_1lyr (mode, plnent.lyr);
      mode_atr (mode, tag);
      adr := ent_first (mode);
      while ent_get (tempent, adr) do begin
        adr := ent_next (tempent, mode);
        if (not Addr_Equal(tempent.addr, plnent.addr)) and
           atr_entfind (tempent, tag, atr) then
          if ((atr.atrtype = atr_int) and (atr.int >= 0) and (atr.int <= NumLblEnts)) or
             (atr.atrtype = atr_str) then begin
            if atr.atrtype = atr_str then
              ndx := NumLblEnts+1
            else
              ndx := atr.int;
            if isnil (lblents[ndx].ent.addr) then
              lblents[ndx].ent := tempent
            else if not lblents[ndx].alreadyfound then begin
              // need to work out which ent is closest to center of entpln
              if distance (entCenter (tempent), cntr) <
                            distance (entCenter (lblents[ndx].ent), cntr) then
                lblents[ndx].ent := tempent;
            end;
          end;
      end;

      allfound := true;
      for i := 0 to NumLblEnts do
        if isnil (lblents[i].ent.addr) then
          allfound := false
        else
          lblents[i].alreadyfound := true;
    end;


    if not allfound then begin
      // widen search to all layers
      mode_init (mode);
      mode_lyr (mode, lyr_all);
      mode_atr (mode, tag);
      adr := ent_first (mode);
      while ent_get (tempent, adr) do begin
        adr := ent_next (tempent, mode);
        if (not Addr_Equal(tempent.addr, plnent.addr)) and
           atr_entfind (tempent, tag, atr) and (atr.atrtype = atr_int) then begin
          if (atr.int >= 1) and (atr.int <= NumLblEnts) and
                    not lblents[atr.int].alreadyfound then begin
            if isnil (lblents[atr.int].ent.addr) then
              lblents[atr.int].ent := tempent
            else begin
              // need to work out which ent is closest to center of entpln
              if distance (entCenter (tempent), cntr) <
                            distance (entCenter (lblents[atr.int].ent), cntr) then
                lblents[atr.int].ent := tempent;
            end;
          end;
        end;
      end;
    end;

    if not isnil (lblents[0].ent.addr) then
      EntList.Add(lblents[0].ent);  // this is wall centerline

    if not isnil (lblents[NumLblEnts+1].ent.addr) then
      EntList.Add(lblents[NumLblEnts+1].ent);  // this is knockout box. Needs to be added before text so that text is kept in front of it

    for i := 1 to NumLblEnts do
      if not isnil (lblents[i].ent.addr) then begin
        EntList.Add(lblents[i].ent);
        if lblents[i].ent.enttype = enttxt then begin
          // could be additional entities if stacked fractions, so check for them
          mode_init (mode);
          mode_group (mode, lblents[i].ent);
          mode_atr (mode, tag);
          adr := ent_first (mode);
          while ent_get (tempent, adr) do begin
            adr := ent_next (tempent, mode);
            if (tempent.enttype = enttxt) and atr_entfind (tempent, tag, atr) and
               (atr.atrtype = atr_int) and (atr.int = -i) then
              EntList.Add(tempent);
          end;
        end;
      end;
  END;

  FUNCTION FindAllSpaceEnts (plnent : entity; VAR EntList : tlist<entity>) : boolean;  overload;
  ///  plnent should be a space outline.  This function will add that entity and all
  ///  related entities to the passed list (which should have already been created by
  ///  the calling logic, but may or may not be empty).
  ///  This function will return true if any entities are added, else false
  VAR
    tag : atrname;
  BEGIN
    result := FindAllSpaceEnts (plnent, tag, EntList);
  END;

  FUNCTION FindWallCenterEnt (plnent : entity; var CenterEnt : entity) : boolean;
  VAR
    tag : atrName;
    tempent : entity;
    mode : mode_type;
    pStandardAtr : ^StandardAtr;
    atr : attrib;
    adr : lgl_addr;
    area : double;
    frstmom, cntr : URecords.point;
  BEGIN
    result := false;

    if atr_entfind (plnent, 'dhSpacesPln', atr) then begin
      pStandardAtr := System.Addr (atr.shstr);
      tag := pStandardAtr^.tag;
    end
    else
      exit;

    // find plnent centroid - may need it to find closest ents if multiples are found
    pline_centroid (plnent.plnFrst, plnent.plnlast, area, frstmom, cntr, true,true);
    setnil (CenterEnt.addr);

    // start by checking entities in the same group as plnent
    mode_init (mode);
    mode_group (mode, plnent);
    mode_atr (mode, tag);
    adr := ent_first (mode);
    while ent_get (tempent, adr) do begin
      adr := ent_next (tempent, mode);
      if (not Addr_Equal(tempent.addr, plnent.addr)) and
         atr_entfind (tempent, tag, atr) and (atr.atrtype = atr_int) and (atr.int = 0) then begin
          if isNil (CenterEnt.addr) then
            ent_get (CenterEnt, tempent.Addr)
          else begin
            // need to work out which ent is closest to center of entpln
            if distance (entCenter (tempent), cntr) <
                          distance (entCenter (CenterEnt), cntr) then
              ent_get (CenterEnt, tempent.Addr);
          end;
        end
    end;

    if not isnil (CenterEnt.addr) then begin
      result := true;
      exit;
    end;

    // check entities on same layer as plnent
    mode_init (mode);
    mode_1lyr (mode, plnent.lyr);
    mode_atr (mode, tag);
    adr := ent_first (mode);
    while ent_get (tempent, adr) do begin
      adr := ent_next (tempent, mode);
      if (not Addr_Equal(tempent.addr, plnent.addr)) and
         atr_entfind (tempent, tag, atr) and (atr.atrtype = atr_int) and (atr.int = 0) then begin
          if isNil (CenterEnt.addr) then
            ent_get (CenterEnt, tempent.Addr)
          else begin
            // need to work out which ent is closest to center of entpln
            if distance (entCenter (tempent), cntr) <
                          distance (entCenter (CenterEnt), cntr) then
              ent_get (CenterEnt, tempent.Addr);
          end;
        end;
    end;

    if not isnil (CenterEnt.addr) then begin
      result := true;
      exit;
    end;

    // widen search to all layers
    mode_init (mode);
    mode_lyr (mode, lyr_all);
    mode_atr (mode, tag);
    adr := ent_first (mode);
    while ent_get (tempent, adr) do begin
      adr := ent_next (tempent, mode);
      if (not Addr_Equal(tempent.addr, plnent.addr)) and
         atr_entfind (tempent, tag, atr) and (atr.atrtype = atr_int) and (atr.int = 0) then begin
          if isNil (CenterEnt.addr) then
            ent_get (CenterEnt, tempent.Addr)
          else begin
            // need to work out which ent is closest to center of entpln
            if distance (entCenter (tempent), cntr) <
                          distance (entCenter (CenterEnt), cntr) then
              ent_get (CenterEnt, tempent.Addr);
          end;
        end;
    end;

    result := not isnil (CenterEnt.addr);

  END;

  FUNCTION IsWallCenterEnt (plnent : entity; out WallWidth : double) : boolean;
  VAR
    atr : attrib;
    pStandardAtr : ^StandardAtr;
  BEGIN
    result := false;
    if not atr_entfind (plnent, STANDARD_ATR_NAME, atr) then
      exit;      // this function is designed to determine if the primary polyline is for the wall center, so it must have the standard attribute
    pStandardAtr := addr (atr.shstr);
    WallWidth := pStandardAtr^.WallThick;
    if Flag (pStandardAtr^.Flags, IS_SURFACE) then
      exit;
    result := true;
  END;


  FUNCTION FindAllLblEnts (plnent : entity; VAR EntList : tlist<entity>) : boolean;
  ///  plnent should be a space outline.  This function will add all related label
  ///  entities to the passed list (which should have already been created by the
  ///  calling logic, but may or may not be empty).
  ///  This function will return true if any entities are added, else false
  VAR
    InitialCount : integer;
    atr : attrib;
    pStandardAtr : ^StandardAtr;
    tag : atrName;
  BEGIN
    InitialCount := EntList.Count;
    FindAllSpaceEnts (plnent, EntList);
    if EntList.Count > InitialCount then begin
      if atr_entfind (EntList[InitialCount], STANDARD_ATR_NAME, atr) and
         (atr.atrtype = atr_str255) then begin
        pStandardAtr := addr (atr.shstr);
        tag := pStandardAtr^.tag;
      end
      else
        tag := '';
      EntList.Delete(InitialCount);    // delete the actual polyline ent
    end;
    if (EntList.Count > InitialCount) and (tag <> '') and
       (EntList[InitialCount].enttype = entpln) and
       atr_entfind (EntList[InitialCount], tag, atr) and
       (atr.atrtype = atr_int) and (atr.int = 0) then
      EntList.Delete(InitialCount);   // this is wall centerline polyline
    result := EntList.Count > InitialCount;
  END;


  FUNCTION SpaceEnts_to_hiliteSS (var plnent : entity; makecopy : byte; hilite, undraw : boolean;
                                  var CntrlineEnt : entity; LabelOnly : boolean) : boolean;
  ///  plnent should be a space outline.  This function will hilite that entity and all
  ///  related entities.
  ///  If makecopy = COPY_NONE then the actual entities will be added to hilite_ss. If COPY_ORIGLYR
  ///  then the copy will be on the original layer(s); if COPY_TOLYR then copy will be on the current layer.
  ///  This function will return true if any entities are added to the selset, else false
  ///  If there is an associate CntrLine polyline then that (or its copy) is returnd in CntrlineEnt
  VAR
    EntList : tlist<entity>;
    ent, myent : entity;
    tag, newtag : atrname;
    atr : attrib;
    pStandardAtr : ^StandardAtr;
    i, loopstart : integer;
    SaveFile : lglFile;
  BEGIN
    try
      EntList := tlist<entity>.Create;
      setnil (CntrlineEnt.addr);
      result := FindAllSpaceEnts (plnent, tag, EntList);
      if LabelOnly then begin
        loopstart := 1;
        if EntList.Count > 1 then begin
          ent := EntList[1];
          if (ent.enttype = entpln) and atr_entfind (ent, tag, atr) and
             (atr.atrtype = atr_int) and (atr.int = 0) then
            loopstart := 2;
          if (EntList.Count < loopstart+1) then
            result := false;
        end;
      end
      else
        loopstart := 0;

      if result then begin
        unhilite_all (true, true, true);

        if makecopy > COPY_NONE then begin
          OpenTempDwgFile (SaveFile, true);
          newtag := CreateNewTag;
          for i := loopstart to EntList.Count-1 do begin
            ent := EntList[i];
            write (SaveFile, ent.addr);       // if copying then save addresses of original entities in case needed for subsequent processing
            if hilite then begin
              hi_lite (true, ent, true, true);
            end;
            ent_copy (ent, myent, makecopy >= COPY_TOLYR, true);
            if atr_entfind (myent, STANDARD_ATR_NAME, atr) and (atr.atrtype = atr_str255) then begin
              pStandardAtr := addr (atr.shstr);
              pStandardAtr^.tag := newtag;
              atr_update (atr);
            end;
            if atr_entfind (myent, tag, atr) then begin
              atr.name := newtag;
              atr_update (atr);
            end;
            EntList[i] := myent;
          end;
          tag := newtag;
          ent_get (plnent, EntList[0].addr);
        end;

        if atr_entfind (EntList[1], tag, atr) and (atr.atrtype = atr_int) and (atr.int = 0) then
          CntrlineEnt := EntList[1];
        ssClear (hilite_ss);
        for i := LoopStart to Entlist.count-1 do begin
          ent_get (ent, EntList[i].addr);
          if hilite and (makeCopy = COPY_NONE) then
            hi_lite (true, ent, true, true)
          else begin
            if undraw then
              ent_draw_dl (ent, drmode_black, false);
            ssAdd (hilite_ss, ent);
          end;
        end;
      end;
    finally
      EntList.Free;
    end;
  END;

  FUNCTION SpaceEnts_to_hiliteSS (var plnent : entity; makecopy : byte; hilite, undraw : boolean; var CntrlineEnt : entity) : boolean;
  BEGIN
    result := SpaceEnts_to_hiliteSS (plnent, makecopy, hilite, undraw, CntrlineEnt, false);
  END;

  FUNCTION SpaceEnts_to_hiliteSS (var plnent : entity; hilite, undraw : boolean) : boolean;
  VAR
    TempEnt : entity;
  BEGIN
    result := SpaceEnts_to_hiliteSS (plnent, hilite, undraw, tempent);
  END;

  FUNCTION SpaceEnts_to_hiliteSS (var plnent : entity; hilite, undraw : boolean; var CntrlineEnt : entity) : boolean;
  BEGIN
    result := SpaceEnts_to_hiliteSS (plnent, COPY_NONE, hilite, undraw, CntrlineEnt);
  END;

  PROCEDURE SpaceRotate (var plnent : entity; pivotpnt : point; ang : double; draw : boolean);
  VAR
    EntList : tlist<entity>;
    ent : entity;
    atr : attrib;
    tag : atrname;
    lblstartndx, i : integer;
    p2, lblCntr, newlblCntr, lblmov, plncntr : point;
    mat : modmat;
    fits : boolean;
    d, txtang, rotang : double;
    pStandardAtr : ^StandardAtrStr;
  BEGIN
    EntList := tlist<entity>.Create;
    try
      FindAllSpaceEnts (plnent, tag, EntList);
      if draw then
        ent_draw (plnent, drmode_black);
      ent_rotate (plnent, pivotpnt, ang);
      ent_update (plnent);
      // update stored space center so that auto label centering will continue to work
      pline_centroid (plnent.plnfrst, plnent.plnlast, d, p2, plncntr, true, true);
      if atr_entfind (plnent, STANDARD_ATR_NAME, atr) and (atr.atrtype = atr_str255) then begin
        pStandardAtr := addr (atr.shstr);
        pStandardAtr^.fields.centroid_x := plncntr.x;
        pStandardAtr^.fields.centroid_y := plncntr.y;
        atr_update (atr);
      end;
      if draw then
        ent_draw (plnent, drmode_white);

      if entlist.Count < 2 then
        exit;

      ent := entlist[1];
      if (ent.enttype = entpln) and atr_entfind (ent, tag, atr) and
         (atr.atrtype = atr_int) and (atr.int = 0) then begin
        //this is wall centreline, rotate it same as plnent
        if draw then
          ent_draw (ent, drmode_black);
        ent_rotate (ent, pivotpnt, ang);
        ent_update (ent);
        if draw then
          ent_draw (ent, drmode_white);
        lblstartndx := 2
      end
      else
        lblstartndx := 1;

      if entlist.Count < (lblstartndx+1) then
        exit;

      lblCntr := LabelCenter (plnent);
      setrotrel (mat, ang, z, pivotpnt);
      xformpt (lblCntr, mat, newLblCntr);
      subpnt (newLblCntr, lblCntr, lblMov);

      //move label without rotating to start with
      fits := true;
      for i := lblstartndx to entlist.Count-1 do begin
        if ent_get (ent, entlist[i].addr) then begin
          if draw then
            ent_draw (ent, drmode_black);
          ent_move (ent, lblmov.x, lblmov.y, 0.0);
          ent_update (ent);
          entlist[i] := ent;
          if not EntFitsInPline(ent, plnent) then
            fits := false;
        end;
      end;

      //if unrotated label does not fit then try rotating it
      if not fits then begin
        // decide which angle to rotate based on existing angle
        txtang := LabelAngle (entlist, lblstartNdx);
        if (txtang > 0) and (txtang <= pi) then
          rotang := -halfpi
        else
          rotang := halfpi;

        //see if rotated label will fit
        fits := true;
        for i := lblstartndx to entlist.Count-1 do begin
          if not RotatedEntFitsInPline (EntList[i], plnent, newlblCntr, rotang) then
            fits := false;
        end;
        // if it does fit then  rotate it
        for i := lblstartndx to entlist.Count-1 do begin
          ent := EntList[i];
          if fits then begin
            ent_rotate (ent, newlblCntr, rotang);
            ent_update (ent);
          end;
        end;
      end;

      if draw then for i := 0 to entlist.Count-1 do begin
        ent := entlist[i];
        ent_draw (ent, drmode_white);
      end;

    finally
      EntList.Free;
    end;

  END;

  FUNCTION FindByNumber (num : str9; ignore : entaddr; var found : entity) : boolean;
  VAR
    mode : mode_type;
    adr : entaddr;
    atr : attrib;
    pStandardAtr : ^StandardAtr;
  BEGIN
    mode_init (mode);
    mode_lyr (mode, lyr_all);
    mode_atr (mode, STANDARD_ATR_NAME);
    result := false;
    adr  := ent_first (mode);
    while ent_get (found, adr) do begin
      adr := ent_next (found, mode);
      if not addr_equal (found.addr, ignore) then begin
        if atr_entfind (found, STANDARD_ATR_NAME, atr) then begin
          pStandardAtr := Addr (atr.shstr);
          if pStandardAtr^.Num = num then begin
            result := true;
            exit;
          end;
        end;
      end;
    end;
  END;


  PROCEDURE DisplaySurfaces (ShowThem : boolean);
  VAR
    ent : entity;
    mode : mode_type;
    adr : lgl_addr;
    atr : attrib;
    pStandardAtr : ^StandardAtr;
  BEGIN
    mode_init (mode);
    mode_lyr (mode, lyr_all);
    mode_enttype (mode, entpln);
    mode_atr (mode, STANDARD_ATR_NAME);
    adr := ent_first (mode);
    while ent_get (ent, adr) do begin
      adr := ent_next (ent, mode);
      if atr_entfind (ent, STANDARD_ATR_NAME, atr) and (atr.atrtype = atr_str255) then begin
        pStandardAtr := addr (atr.shstr);
        if Flag (pStandardAtr^.Flags, IS_SURFACE) then begin
          if lyr_ison (ent.lyr) then begin
            if not ShowThem then
              ent_draw (ent, drmode_black);
          end;
          HidePln (ent, not ShowThem);
          ent_draw (ent, drmode_white);
        end;
      end;
    end;
  END;

  PROCEDURE DisplayCenters (ShowThem : boolean);
  VAR
    ent : entity;
    mode : mode_type;
    adr, adr1 : lgl_addr;
    atr : attrib;
    pStandardAtr : ^StandardAtr;
    found : boolean;
  BEGIN
    mode_init (mode);
    mode_lyr (mode, lyr_all);
    mode_enttype (mode, entpln);
    adr := ent_first (mode);
    while ent_get (ent, adr) do begin
      adr := ent_next (ent, mode);
      if atr_entfind (ent, STANDARD_ATR_NAME, atr) then begin
        if (atr.atrtype = atr_str255) then begin
          pStandardAtr := addr (atr.shstr);
          if not Flag (pStandardAtr^.Flags, IS_SURFACE) then begin
            if lyr_ison (ent.lyr) then begin
              if not ShowThem then
                ent_draw (ent, drmode_black);
            end;
            HidePln (ent, not ShowThem);
            ent_draw (ent, drmode_white);
          end;
        end;
      end
      else begin
        adr1 := atr_entfirst (ent);
        found := false;
        while atr_get (atr, adr1) and not found do begin
          adr1 := atr_next (atr);
          if (atr.atrtype = atr_int) and  AnsiStartsStr('dh~', string(atr.name)) and
             (atr.int = 0) then begin
            found := true;
            if lyr_ison (ent.lyr) then begin
              if not ShowThem then
                ent_draw (ent, drmode_black);
            end;
            HidePln (ent, not ShowThem);
            ent_draw (ent, drmode_white);
          end;
        end;
      end;
    end;
  END;




end.

