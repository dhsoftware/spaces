unit Properties;

interface
uses UInterfaces, URecords, UConstants, Settings, StrUtil, Language, Constants,
      SysUtils;

FUNCTION doGetName (var name : string; existing : boolean) : boolean;

FUNCTION GetNumber (var number : str8; existing : boolean) : boolean;

PROCEDURE NextNumber (var number : str8);


implementation


PROCEDURE NextNumber (var number : str8);
VAR
	i, j : integer;
BEGIN
	number := GetSvdStr ('dhSPLastNum', '');
	if blankstring (number) then begin
		exit;
	end;
	if numeric (number, false) then begin
		j := length(number);
		if cvstint (number, i) then begin
			i := i + 1;
			cvintst (i, number);
		end;
		while length(number) < j do begin
			strins (number, '0', 1);
		end;
	end else begin
		strinc (number);
	end;
END; //NextNumber;

FUNCTION GetNumber (var number : str8; existing : boolean) : boolean;
VAR
	i, j, res, key : integer;
BEGIN
	wrtlvl ('');
	if (not existing) and (length(number) = 0) then begin
		NextNumber (number);
	end;
{	repeat
		lblsinit;
		if existing then begin
			llblset (20, 25);		//Cancel|Cancel input and retain previous value
		end else begin
			llblset (20, 188);	//Cancel|Cancel creation of this room
		end;
		lblson;
		lwrtmsg (118);	//Enter Room Number

		res := dgetstr (number, 8, key);
		if res = res_normal then begin
			savestr ('dhSPLastNum', number, false);
			result := true;
		end else if key = s0 then begin
			result := false;
		end else begin
			beep;
		end;
	until false;    }
END; //GetNumber;

FUNCTION doGetName (var name : string; existing : boolean ) : boolean;
VAR
	iniFileName	: str255;
	s	: str255;
	str : str255;
	key : integer;
	dfltndx : integer;
	i, j, k : integer;
	names : array [1..20] of str25;
	recent : array [1..20] of str25;
	res : integer;
	BlankConfirmed : boolean;
	defaultvalue : boolean;
	AllCaps	: boolean;
BEGIN
	BlankConfirmed := false;
	AllCaps := GetSvdBln ('dhSPNameCaps', false);
	lwrtlvl (248);	//Space Name|(level)
	repeat
		getpath (iniFileName, pathsup);
		 iniFileName := iniFileName + 'dhsoftware\\RoomNames.ini';
		lblsinit;
		//Add default names to function keys (and to names array)
		s := 'name00';
		dfltndx := 0;
		repeat
			strinc (s);
			dfltndx := dfltndx+1;
			str := readIniStr (iniFileName, 'Defaults', s, '');
			if BlankString (str) then begin
				names[dfltndx] := '';
			end else begin
        names[dfltndx] := str25(str);
				lblset (dfltndx, names[dfltndx]);
				lblmsg (dfltndx, names[dfltndx]);
			end;
		until BlankString (names[dfltndx]) or (dfltndx=10);
		if BlankString (names[dfltndx]) then begin
			dfltndx := dfltndx-1;
		end;
		//Add recent names to recent array
		s := 'name00';
		for j := 1 to 20 do begin
			strinc (s);
			str := readiniStr (iniFileName, 'Recent', s, '');
			if BlankString (str) then begin
				recent[j] := '';
			end
      else begin
        recent[j] := str25(str);
			end;
		end;
		//Add recent name to Names array (and to function keys)
		for j := (dfltndx+1) to 20 do begin
			 names[j] := recent[j-dfltndx];
			if (not BlankString (names[j])) and (j < 18) then begin
				lblset (j, names[j]);
				lblmsg (j, names[j]);
			end;
		end;

		llblsett (19, 220, AllCaps);
		if existing then begin
			llblset (20, 25);		//Cancel|Cancel input and retain previous value
		end else begin
			llblset (20, 188);	//Cancel|Cancel creation of this room
			name := GetSvdStr ('dhSP_NxtName', '');
		end;
		lblson;
		//get input

		lwrtmsg (87);
{		res := dgetstr (name, 25, key);
		if res = res_escape then begin		//process function keys, move recent name up list if appropriate
			j := fnKeyConv (key);
			if (j = -1) or (j=18) then begin
				beep;
			end else if j = 19 then begin
				AllCaps := not AllCaps;
				SaveBln ('dhSPNameCaps', AllCaps, true, true);
			end
      else if j = 20 then begin
				result := false;
        exit;
			end
      else if BlankString (names[j]) then begin
				name := '';
				beep;
			end
      else begin
				name := names[j];
				if j > (dfltndx+1) then begin //they selected a recent value, move it up to the most recent
					for k := 2 to(j-dfltndx) do begin
						 recent[(j-dfltndx)-k+2] := recent[(j-dfltndx-k+1]);
					end;
					recent[1] := str25(name);
				end;
			end;
		end
    else begin
			defaultvalue := false;
			if BlankString(name) then begin	//allow a blank string if user confirms that is what they want
				beep;
				BlankConfirmed := lMsg_Confirm (88);
			end
      else begin
				//check if input name matches one of the function keys
				if dfltndx > 0 then begin
					for k := 1 to dfltndx do begin
						if strcomp (names[k], name, -1) then begin
							defaultvalue := true;
						end;
					end;
				end;

				k  := 1;
				while k <= 17 do begin
					if strcomp (recent[k], name, -1) then begin
						// remove from recent array ... it will be added back to the start of the array in the next step
						if k < 17 then begin
							for i := k to 19 do begin
								 recent[i] := recent[i+1];
							end;
						end;
						recent[17] := '';
					end else begin
						k := k+1;
					end;
				end;
				//if not default value then add to start of recent array
				if not (defaultvalue or BlankConfirmed) then begin
					k := 17;
					while k > 1 do begin
						 recent[k] := recent[k-1];
						k := k-1;
					end;
          recent[1] := str25(name);
				end;
			end;
			// save recent array
		end;      }
	until BlankConfirmed or (not BlankString (Name));
	wrtmsg ('');
	//logstr ('Name entered');

	//update stored recent values
	if not BlankConfirmed then begin
		s := 'name00';
		for j := 1 to 20 do begin
			strinc (s);
			 str := recent[j];
			writeiniStr (iniFileName, 'Recent', s, str);
		end;
	end;
	//logstr ('Stored names updated');
	if AllCaps then begin
		name := UpperCase (Name);
	end;

	if (Name[length(Name)] >= '0') and (Name[length(Name)] <= '9') and (not existing) then begin
		 s := shortstring(Name);
		strinc (s);
		SaveStr ('dhSP_NxtName', s, false);
	end
  else begin
		SaveStr ('dhSP_NxtName', '', false);
	end;
	//logstr ('Name complete');
	{todo: check if exit required after this line} result := true;
END; //GetName;

end.

