object fmExportOptions: TfmExportOptions
  Left = 0
  Top = 0
  Caption = 'fmExportOptions'
  ClientHeight = 200
  ClientWidth = 735
  Color = clBtnFace
  Constraints.MinHeight = 239
  Constraints.MinWidth = 491
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  DesignSize = (
    735
    200)
  PixelsPerInch = 96
  TextHeight = 13
  object lblSheet: TLabel
    Left = 16
    Top = 16
    Width = 28
    Height = 13
    Caption = 'Sheet'
  end
  object lblRowsToLeave: TLabel
    Left = 264
    Top = 54
    Width = 136
    Height = 13
    Caption = 'Rows to leave above export'
  end
  object Label3: TLabel
    Left = 16
    Top = 72
    Width = 96
    Height = 13
    Caption = 'Column Mapping:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object cbSheets: TComboBox
    Left = 66
    Top = 13
    Width = 145
    Height = 21
    TabOrder = 0
    Text = 'cbSheets'
  end
  object chbReplace: TCheckBox
    Left = 264
    Top = 8
    Width = 233
    Height = 17
    Caption = 'Delete and replace with exported data'
    TabOrder = 1
  end
  object chbUpdateLinks: TCheckBox
    Left = 585
    Top = 138
    Width = 161
    Height = 17
    Anchors = [akRight, akBottom]
    Caption = 'Update Space/Excel Links'
    TabOrder = 2
  end
  object chbAddAfter: TCheckBox
    Left = 264
    Top = 31
    Width = 233
    Height = 17
    Caption = 'Add export after last row of sheet'
    TabOrder = 3
  end
  object dceRowsAbove: TDcadNumericEdit
    Left = 424
    Top = 54
    Width = 43
    Height = 21
    NumberType = IntegerNum
    AllowNegative = False
    TabOrder = 4
    Text = '0'
  end
  object sgColMapping: TStringGrid
    Left = 16
    Top = 90
    Width = 711
    Height = 41
    Anchors = [akLeft, akTop, akRight]
    ColCount = 420
    FixedCols = 0
    RowCount = 1
    FixedRows = 0
    TabOrder = 5
    OnMouseDown = sgColMappingMouseDown
  end
  object btnOK: TButton
    Left = 585
    Top = 167
    Width = 142
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 6
  end
  object btnCancel: TButton
    Left = 496
    Top = 167
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 7
  end
  object PopupMenu1: TPopupMenu
    Left = 144
    Top = 160
  end
end
