object RegistationPrompt: TRegistationPrompt
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Please register this macro'
  ClientHeight = 211
  ClientWidth = 457
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  Scaled = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 0
    Top = 8
    Width = 457
    Height = 164
    BorderStyle = bsNone
    Color = clBtnFace
    Lines.Strings = (
      
        'This macro is free - there is no obligation to pay for it (altho' +
        'ugh if you find it useful a '
      
        'contribution towards the cost of its development and distributio' +
        'n is expected and would '
      'be appreciated!).'
      ''
      
        'Registration is also free, and is recommended as it will allow m' +
        'e to advise you of any '
      'updated versions of the macro.'
      'Please click on the button below to go to the registration page.'
      ''
      'Thank you.')
    ReadOnly = True
    TabOrder = 0
  end
  object btnOK: TButton
    Left = 89
    Top = 178
    Width = 360
    Height = 25
    Caption = 'Go to Registration Page'
    Default = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = btnOKClick
  end
  object btnCancel: TButton
    Left = 8
    Top = 178
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    TabOrder = 2
    OnClick = btnCancelClick
  end
  object chbNotAgain: TCheckBox
    Left = 8
    Top = 160
    Width = 249
    Height = 17
    Caption = 'Do not show this message again'
    TabOrder = 3
  end
end
