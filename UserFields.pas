unit UserFields;

{-------------------------------------------------------------------------------
 User field definitions are saved in the drawing file as attributes.
 The attribute name will start with 'SpdhUF', and the final 6 characters of the
 name are the tag assigned to the field.
 The first character of the string value of the attribute indicates the type of
 field (I=integer, R=real, D=distance, A=angle, S=text), followed by a Str30
 containing the field description, followed by the default value (see
 UserFieldDef record defined in CommonStuff).
-------------------------------------------------------------------------------}

interface

uses CommonStuff, URecords, UInterfaces, UConstants, UInterfacesRecords,
     SysUtils, StrUtils, System.Generics.Collections, DcadEdit;

function DecodeUserFieldAtr (atr : attrib) : UserFieldDef;

function UserFieldAtrType (FieldType : UserFieldType) : integer;
function EditFieldTypeToAtrType (EditFieldType : FieldType) : integer;

procedure SaveUserFieldDefs;

implementation

function DecodeUserFieldAtr (atr : attrib) : UserFieldDef;
begin
  result.tag := copy (atr.name, 7, 6);
  case atr.str[1] of
    'I' : result.numeric := int;
    'R' : result.numeric := rl;
    'D' : result.numeric := dis;
    'A' : result.numeric := ang;
    'E' : result.numeric := UserFieldType.area;
    else result.numeric := str;
  end;
  move (atr.str[2], result.desc, sizeof (result.desc));
  move (atr.str[33], result.defaultstr, sizeof (result.defaultstr));
end;

function UserFieldAtrType (FieldType : UserFieldType) : integer;
begin
  case FieldType of
    int : result := atr_int;
    rl  : result := atr_rl;
    dis : result := atr_dis;
    ang : result := atr_ang;
    UserFieldType.area: result := atr_Area
    else  result := atr_str;
  end;
end;

function EditFieldTypeToAtrType (EditFieldType : FieldType) : integer;
begin
  case EditFieldType of
    Distance : result := atr_dis;
    DecDegrees, DegMinSec : result := atr_ang;
    IntegerNum : result := atr_int;
    DecimalNum : result := atr_rl;
    Area : result := atr_Area
    else result := atr_str;
  end;

end;

procedure SaveUserFieldDefs;
var
  i : integer;
  atr : attrib;
  ufd : UserFieldDef;

  procedure SetAtrText;
  begin
    ufd := l^.UserFields[i];
    setlength (atr.str, 80);
    case ufd.numeric of
      int : atr.str[1] := 'I';
      rl : atr.str[1] := 'R';
      dis : atr.str[1] := 'D';
      ang : atr.str[1] := 'A';
      UserFieldType.area : atr.str[1] := 'E';
      else atr.str[1] := 'S';
    end;
    move (ufd.desc, atr.str[2], sizeof (ufd.desc));
    move (ufd.defaultstr, atr.str[33], sizeof (ufd.defaultstr));
  end;

begin
  if l^.UserFields.Count > 0 then begin
    for i := (l^.UserFields.Count-1) downto 0 do begin
      if atr_sysfind ('SpdhUF'+l^.UserFields[i].tag, atr) then begin
        if l^.UserFields[i].deleted then begin
          atr_delsys (atr);
          l^.UserFields.Delete(i);
        end
        else begin
          SetAtrText;
          atr_update (atr);
        end;
      end
      else if not l^.UserFields[i].deleted then begin
        atr_init (atr, atr_str);
        atr.name := 'SpdhUF'+l^.UserFields[i].tag;
        SetAtrText;
        atr_add2sys (atr);
      end;
    end;
  end;
end;

end.
