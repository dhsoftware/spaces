unit rectangle;

interface

uses UInterfaces, URecords, UConstants, CommonStuff, PlnUtil, CreateSpaces, SpaceUtil;

FUNCTION IsRectangle (plnent : entity; var pnts : array of point;
                      var xaxisangle, xdim, ydim :double) : boolean;

PROCEDURE ResizeRectangle (var plnent : entity; Anchor : byte;
                           pnts : array of point; Xdis, Ydis : double);

FUNCTION CreateRectangle (p1, p2 : point) : entity;
FUNCTION Create3Pnt (p1, p2, p3 : point) : entity;
FUNCTION CreateDimRectangle (Pnt : point; Xdis, Ydis : double;
                             xAxisAngle: double; Anchor : byte) : entity;  overload;
FUNCTION CreateDimRectangle (Pnt : point; Xdis, Ydis : double;
                             xAxisAngle: double; Anchor : byte;
                             forVoid : boolean) : entity;   overload;
PROCEDURE CreateDimVoid (var ent : entity; Pnt : point; Xdis, Ydis : double;
                             xAxisAngle: double; Anchor : byte);

implementation

FUNCTION IsRectangle (plnent : entity; var pnts : array of point;
                      var xaxisangle, xdim, ydim :double) : boolean;
VAR
  pv : polyvert;
  adr : lgl_addr;
  i : integer;
  a : double;
BEGIN
  result := false;
  adr := plnent.plnfrst;
  i := 0;
  while polyvert_get (pv, adr, plnent.plnfrst) do begin
    adr := pv.Next;
    if (i > 3) or (pv.shape <> pv_vert) then
      exit;
    pnts[low(pnts)+i] := pv.pnt;

    i := i+1;
    if i = 1 then begin
      xaxisangle := angle (pv.pnt, pv.nextpnt);
      angnormalize (xaxisangle);
      xdim := distance (pv.pnt, pv.nextpnt);
    end
    else if i = 2 then begin
      ydim := distance (pv.pnt, pv.nextpnt);
      a := angle (pv.pnt, pv.nextpnt);
      angnormalize (a);
      if not (realequal (xaxisangle, a+halfpi) or realequal (xaxisangle, a-halfpi) or
              realequal (xaxisangle, a+3*halfpi) or realequal (xaxisangle, a-3*halfpi)) then
        exit;
    end
    else if i = 3 then begin
      if not realequal (distance (pv.pnt, pv.nextpnt), xdim) then
        exit;
    end
    else if i = 4 then begin
      if not realequal (distance (pv.pnt, pv.nextpnt), ydim) then
        exit;
      if PointsEqual (pv.pnt, pnts[1]) then
        exit;

    end;
  end;

  result := true;
END;

PROCEDURE ResizeRectangle (var plnent : entity; Anchor : byte;
                           pnts : array of point; Xdis, Ydis : double);
VAR
  xAngle, yAngle : double;
  RelativePnt : point;
  pv : polyvert;
  adr : lgl_addr;
  i : integer;
BEGIN
  Anchor := Anchor - 1; //Anchor is 1..4, but pnts array is zero based
  if Anchor in [0, 3] then begin
    xAngle := angle (pnts[0], pnts[1]);
    cylind_cart (Xdis, xAngle, 0, RelativePnt.x, RelativePnt.y, RelativePnt.z);
    AddPnt (pnts[0], RelativePnt, pnts[1]);
    AddPnt (pnts[3], RelativePnt, pnts[2]);
  end
  else begin
    xAngle := angle (pnts[1], pnts[0]);
    cylind_cart (Xdis, xAngle, 0, RelativePnt.x, RelativePnt.y, RelativePnt.z);
    AddPnt (pnts[1], RelativePnt, pnts[0]);
    AddPnt (pnts[2], RelativePnt, pnts[3]);
  end;
  if Anchor in [0, 1] then begin
    yAngle := angle (pnts[0], pnts[3]);
    cylind_cart (Ydis, yAngle, 0, RelativePnt.x, RelativePnt.y, RelativePnt.z);
    AddPnt (pnts[0], RelativePnt, pnts[3]);
    AddPnt (pnts[1], RelativePnt, pnts[2]);
  end
  else begin
    yAngle := angle (pnts[2], pnts[1]);
    cylind_cart (Ydis, yAngle, 0, RelativePnt.x, RelativePnt.y, RelativePnt.z);
    AddPnt (pnts[3], RelativePnt, pnts[0]);
    AddPnt (pnts[2], RelativePnt, pnts[1]);
  end;
  adr := plnent.plnfrst;
  i := 0;
  while polyvert_get (pv, adr, plnent.plnfrst) do begin
    adr := pv.Next;
    pv.pnt := pnts[i];
    polyvert_update (pv);
    i := i+1;
    if i > 3 then
      exit;
  end;
END;


FUNCTION CreateRectangle (p1, p2 : point) : entity;
VAR
  pv  : polyvert;
  i   : integer;
BEGIN
  ent_init (result, entpln);
  for i := 1 to 4 do begin
    polyvert_init (pv);
    pv.shape := pv_vert;
    if i=1 then
      pv.pnt := p1
    else if i = 2 then begin
      pv.pnt.x := p2.x;
      pv.pnt.y := p1.y;
    end else if i = 3 then
      pv.pnt := p2
    else begin
      pv.pnt.x := p1.x;
      pv.pnt.y := p2.y;
    end;
    pv.pnt.z := PGsavevar.basez;
    polyvert_add (pv, result.plnfrst, result.plnlast);
  end;
  result.plnclose := true;
END;

FUNCTION Create3Pnt (p1, p2, p3 : point) : entity;
VAR
  pv  : polyvert;
BEGIN
  ent_init (result, entpln);

  polyvert_init (pv);
  pv.shape := pv_vert;
  pv.pnt := p1;
  polyvert_add (pv, result.plnfrst, result.plnlast);

  polyvert_init (pv);
  pv.shape := pv_vert;
  pv.pnt := p2;
  polyvert_add (pv, result.plnfrst, result.plnlast);

  polyvert_init (pv);
  pv.shape := pv_vert;
  pv.pnt := p3;
  polyvert_add (pv, result.plnfrst, result.plnlast);

  polyvert_init (pv);
  pv.shape := pv_vert;
  subpnt (p1,p2, pv.pnt);
  addpnt (pv.pnt, p3, pv.pnt);
  polyvert_add (pv, result.plnfrst, result.plnlast);

  result.plnclose := true;
END;


FUNCTION CreateDimRectangle (Pnt : point; Xdis, Ydis : double;
                             xAxisAngle: double; Anchor : byte;
                             forVoid : boolean) : entity;
VAR
  pv  : polyvert;
BEGIN
{  if l^.DefineByCenter and (not forVoid) then begin
    // if entity is being defined by wall center we want the actual room size (from wall
    // surface to equal the passed dimensions, so add wall width ..
    Xdis := Xdis + l^.WallWidth;
    Ydis := Ydis + l^.WallWidth;
  end;  }

  ent_init (result, entpln);

  polyvert_init (pv);
  pv.shape := pv_vert;
  pv.pnt := Pnt;
  polyvert_add (pv, result.plnfrst, result.plnlast);

  polyvert_init (pv);
  pv.shape := pv_vert;
  pv.pnt := Pnt;
  case Anchor of
    1 : pv.pnt.y := Pnt.y - Ydis;
    2 : pv.pnt.x := Pnt.x - Xdis;
    3 : pv.pnt.y := Pnt.y + Ydis
    else  pv.pnt.x := Pnt.x + Xdis;
  end;
  polyvert_add (pv, result.plnfrst, result.plnlast);

  polyvert_init (pv);
  pv.shape := pv_vert;
  case Anchor of
    1 : begin
          pv.pnt.x := Pnt.x + Xdis;
          pv.pnt.y := Pnt.y - Ydis;
        end;
    2 : begin
          pv.pnt.x := Pnt.x - Xdis;
          pv.pnt.y := Pnt.y - Ydis;
        end;
    3 : begin
          pv.pnt.x := Pnt.x - Xdis;
          pv.pnt.y := Pnt.y + Ydis;
        end
    else begin
          pv.pnt.x := Pnt.x + Xdis;
          pv.pnt.y := Pnt.y + Ydis;
        end;
  end;
  pv.pnt.z := Pnt.z;
  polyvert_add (pv, result.plnfrst, result.plnlast);

  polyvert_init (pv);
  pv.shape := pv_vert;
  pv.pnt := Pnt;
  case Anchor of
    1 : pv.pnt.x := Pnt.x + Xdis;
    2 : pv.pnt.y := Pnt.y - Ydis;
    3 : pv.pnt.x := Pnt.x + Xdis
    else  pv.pnt.y := Pnt.y + Ydis;
  end;
  polyvert_add (pv, result.plnfrst, result.plnlast);

  result.plnclose := true;

  if xAxisAngle <> 0 then
    ent_rotate (result, Pnt, xAxisAngle);
END;


FUNCTION CreateDimRectangle (Pnt : point; Xdis, Ydis : double;
                             xAxisAngle: double; Anchor : byte) : entity;
BEGIN
  result := CreateDimRectangle (Pnt, Xdis, Ydis, xAxisAngle, Anchor, false);
END;



PROCEDURE CreateDimVoid (var ent : entity; Pnt : point; Xdis, Ydis : double;
                             xAxisAngle: double; Anchor : byte);
VAR
  TempEnt : entity;
  atr : attrib;
  atrstr : StandardAtrStr;
BEGIN
  TempEnt := CreateDimRectangle (Pnt, Xdis, Ydis, xAxisAngle, Anchor);
  if plnAddVoid (ent, tempent) then begin
    if atr_entfind (ent, STANDARD_ATR_NAME, atr) then
      if atr.atrtype = atr_str255 then begin
        move (atr.shstr, atrstr.str, sizeof(atr.shstr));
        RefreshLabel (ent, atrstr);
      end;
  end;
END;


end.

