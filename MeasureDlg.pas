unit MeasureDlg;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, CommonStuff, URecords, UInterfaces, plnutil,
  Vcl.ExtCtrls, StrUtil, UserFields, Language, UConstants, Settings;

type
  TMeasures = class(TForm)
    btnOK: TButton;
    lblArea: TLabel;
    EdArea: TEdit;
    lblPerim: TLabel;
    EdPerim: TEdit;
    plnPerimDetail: TPanel;
    EdPerimOutline: TEdit;
    lblVoids2: TLabel;
    EdPerimVoid: TEdit;
    lblVol: TLabel;
    EdVolume: TEdit;
    lblOutline1: TLabel;
    pnlAreaDetail: TPanel;
    lblVoids1: TLabel;
    lblGross1: TLabel;
    EdAreaGross: TEdit;
    EdAreaVoids: TEdit;
    pnlVolDetail: TPanel;
    lblVoids4: TLabel;
    lblGross2: TLabel;
    EdVolGross: TEdit;
    EdVolVoids: TEdit;
    lblNum: TLabel;
    EdNumber: TEdit;
    lblName: TLabel;
    EdName: TEdit;
    lblCat: TLabel;
    EdCategory: TEdit;
    lblPerimArea: TLabel;
    EdPerimArea: TEdit;
    pnlPerimAreaDetail: TPanel;
    lblVoids3: TLabel;
    lblOutline2: TLabel;
    EdPerimAreaOutline: TEdit;
    EdPerimAreaVoid: TEdit;
    lblX: TLabel;
    EdX: TEdit;
    lblY: TLabel;
    EdY: TEdit;
    lblAng: TLabel;
    EdAngle: TEdit;
    lblWallThick: TLabel;
    EdWallThick: TEdit;
    sbCustom: TScrollBox;
    lblCustom1: TLabel;
    EdCustom1: TEdit;
    sbExcel: TScrollBox;
    lblXL: TLabel;
    lblExcel: TLabel;
    lblFloorZ: TLabel;
    EdFloorZ: TEdit;
    lblCeilingHt: TLabel;
    EdClgHeight: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Measures: TMeasures;

implementation

{$R *.dfm}

procedure TMeasures.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  SaveFormPos (TForm(Sender));
end;

procedure TMeasures.FormCreate(Sender: TObject);
VAR
  pStrings : ^Strings;
  pAtr : ^Attrib;
  pAtrStr : ^StandardAtrStr;
  TempStr, TempStr1 : string;
  adr : lgl_addr;
  atr, atr1 : attrib;
  UserField : UserFieldDef;
  Customfieldnumber : integer;
  CustomFieldLabels : array of TLabel;
  CustomFieldValues : array of TEdit;
  s : shortstring;
  st : string;
  i : integer;
begin
  SetFormPos (TForm(Sender));
  pStrings := Addr(l^.ent2);
  pAtr := Addr (pStrings^[26]);
  pAtrStr := Addr (pAtr^.shstr);
  EdNumber.Text := string (pAtrStr^.fields.Num);
  EdName.Text := trimright (string (pAtrStr^.fields.Name));
  EdCategory.Text := string (pAtrStr^.fields.Category);

  LabelCaption (111, lblName);  // Name
  LabelCaption (112, lblNum);   // Number
  LabelCaption (113, lblCat);   // Category
  LabelCaption (146, lblArea);  // Area:
  LabelCaption (147, lblPerim);  //Perimeter:
  LabelCaption (148, lblPerimArea);  //Perim Area:
  LabelCaption (153, lblGross1);  // Gross:
  LabelCaption (153, lblGross2);  // Gross:
  LabelCaption (154, lblOutline1);  // Outline:
  lblOutline2.Caption := lblOutline1.Caption;
  LabelCaption (155, lblVoids1);  // Voids:
  lblVoids2.Caption := lblVoids1.Caption;
  lblVoids3.Caption := lblVoids1.Caption;
  lblVoids4.Caption := lblVoids1.Caption;
  LabelCaption (157, lblX); // X Size:
  LabelCaption (166, lblY); // Y Size:
  LabelCaption (167, lblAng); //Angle:
  LabelCaption (168, lblWallThick); //Wall thick:
  LabelCaption (123, lblXL);  //Excel Association:
  ButtonLblParams(108, [], btnOK);  //OK

  if pAtrStr^.fields.ExcelNdx = 0 then begin
    sbExcel.Visible := false;
    sbCustom.Height := btnOK.Top - sbCustom.Top - 1;
  end
  else begin
    atr1.name := 'SpdhXL' + shortstring(IntToStr(pAtrStr^.fields.ExcelNdx));
    if atr_sysfind (atr1.name, atr1) and (atr1.atrtype = atr_str255) then begin
      st := string(atr1.shstr);
      if st.StartsWith('|pathxls|') then begin
        delete (st, 1, 9);
        insert ('{Excel Path}', st, 1);
      end;
      i := ansipos ('|', st);
      if i > 0 then begin
        delete (st, i, 1);
        GetMsg (71, s);  //Sheet:
        insert (sLineBreak + string(s) + ' ', st, i);
        GetMsg (72, s); //Row:
        st := st + '     ' + string(s) + ' ' + inttostr (pAtrStr^.fields.ExcelRow);
      end;
    end
    else begin
      GetMsg(70, st);   //Error in Excel association
    end;
    lblExcel.Caption := st;
    //lblExcel.Width := sbExcel.Width - lblExcel.Left;

  end;

  edArea.Text := string(pStrings^[1]);
  pnlAreaDetail.Visible := (pStrings^[9] > '');
  if pnlAreaDetail.Visible then begin
    EdAreaGross.Text := string (pStrings^[9]);
    EdAreaVoids.Text := string (pStrings^[10]);
  end;
  edPerim.Text := string(pStrings^[2]);
  plnPerimDetail.Visible := (pStrings^[3] > '');
  if plnPerimDetail.Visible then begin
    EdPerimOutline.Text := string (pStrings^[3]);
    EdPerimVoid.Text := string (pStrings^[4]);
  end;
  edPerimArea.Text := string (pStrings^[6]);
  pnlPerimAreaDetail.Visible := (pStrings^[7] > '');
  if pnlPerimAreaDetail.Visible then begin
    EdPerimAreaOutline.Text := string (pStrings^[7]);
    EdPerimAreaVoid.Text := string (pStrings^[8]);
  end;
  EdVolume.Text := string (pStrings^[5]);
  pnlVolDetail.Visible := (pStrings^[11] > '');
  if pnlVolDetail.Visible then begin
    EdVolGross.Text := string (pStrings^[11]);
    EdVolVoids.Text := string (pStrings^[12]);
  end;
  DisString (l^.ent.plbase, UNITS_CLIP, TempStr, TempStr1);
  EdFloorZ.Text:= TempStr;
  DisString (l^.ent.plnhite - l^.ent.plbase, UNITS_CLIP, TempStr, TempStr1);
  EdClgHeight.Text:= TempStr;

  DisString(pAtrStr^.fields.hdim, UNITS_CLIP, TempStr, TempStr1);
  EdX.Text := Trim (TempStr + ' ' + TempStr1);
  DisString(pAtrStr^.fields.vdim, UNITS_CLIP, TempStr, TempStr1);
  EdY.Text := Trim (TempSTr + ' ' + TempStr1);
  CvAngSt (getxAxisAngle (l^.ent), pStrings^[13]);
  EdAngle.Text := string(pStrings^[13]);

  lblWallThick.Visible := pAtrStr^.fields.WallThick > 0;
  EdWallThick.Visible := lblWallThick.Visible;
  if lblWallThick.Visible then begin
    DisString (pAtrStr^.fields.WallThick, UNITS_CLIP, TempStr, TempStr1);
    EdWallThick.Text := Trim (TempStr + ' ' + TempStr1);
  end;

  // display any user defined attributes
  lblCustom1.Caption := '';
  adr := atr_sysfirst;
  Customfieldnumber := 0;
  while atr_get (atr, adr) do begin
    adr := atr_next (atr);
    if ((string(atr.name)).StartsWith('SpdhUF')) then begin
      Customfieldnumber := Customfieldnumber + 1;
      UserField := DecodeUserFieldAtr(atr);
      if Customfieldnumber = 1 then begin
        SetLength (CustomFieldLabels, 1);
        CustomFieldLabels[0] := lblCustom1;
        CustomFieldLabels[0].Caption := string(UserField.desc) + ':';

        SetLength (CustomFieldValues, 1);
        CustomFieldValues[0] := EdCustom1;
      end
      else begin
        SetLength (CustomFieldLabels, CustomFieldNumber);
        CustomFieldLabels[CustomFieldnumber-1] := TLabel.Create(lblCustom1.Owner);
        CustomFieldLabels[CustomFieldnumber-1].Parent := lblCustom1.Parent;
        CustomFieldLabels[CustomFieldnumber-1].Top := lblCustom1.Top + (CustomFieldNumber-1)*Round(lblCustom1.Height*1.5);
        CustomFieldLabels[CustomFieldnumber-1].Left := lblCustom1.Left;
        CustomFieldLabels[CustomFieldnumber-1].AutoSize := true;
        CustomFieldLabels[CustomFieldnumber-1].Height := lblCustom1.Height;
        CustomFieldLabels[CustomFieldnumber-1].Anchors := lblCustom1.Anchors;
        CustomFieldLabels[CustomFieldnumber-1].Font := lblCustom1.Font;
        CustomFieldLabels[CustomFieldnumber-1].Caption := string(UserField.desc) + ':';

        SetLength (CustomFieldValues, CustomFieldNumber);
        CustomFieldValues[CustomFieldnumber-1] := TEdit.Create(EdCustom1.Owner);
        CustomFieldValues[CustomFieldnumber-1].Parent := edCustom1.Parent;
        CustomFieldValues[CustomFieldnumber-1].Top := CustomFieldLabels[CustomFieldnumber-1].Top;
        CustomFieldValues[CustomFieldnumber-1].ReadOnly := true;
        CustomFieldValues[CustomFieldnumber-1].Height := EdCustom1.Height;
        CustomFieldValues[CustomFieldnumber-1].Anchors := EdCustom1.Anchors;
        CustomFieldValues[CustomFieldnumber-1].Font := EdCustom1.Font;
        CustomFieldValues[CustomFieldnumber-1].BevelKind := EdCustom1.BevelKind;
        CustomFieldValues[CustomFieldnumber-1].BorderStyle := EdCustom1.BorderStyle;
        CustomFieldValues[CustomFieldnumber-1].Color := EdCustom1.Color;
      end;
      CustomFieldValues[CustomFieldNumber-1].Left := CustomFieldLabels[CustomFieldNumber-1].Left +
                                                     CustomFieldLabels[CustomFieldNumber-1].Width +
                                                     CustomFieldLabels[CustomFieldNumber-1].Height;
      if atr_entfind (l^.ent, 'SpdhCF'+UserField.tag, atr1) then
        move (atr1.str, UserField.defaultstr, sizeof (UserField.defaultstr));
      case UserField.numeric of
        int: CustomFieldValues[CustomFieldnumber-1].Text := IntToStr(UserField.defaultint);
        rl : CustomFieldValues[CustomFieldnumber-1].Text := FloatToStr(UserField.defaultrl);
        dis: begin
                DisString (UserField.defaultdis, UNITS_CLIP, TempStr, TempStr1);
                CustomFieldValues[CustomFieldnumber-1].Text := Trim (string(Tempstr + ' ' + Tempstr1));
             end;
        ang: begin
               s := AngString (UserField.defaultang);
               CustomFieldValues[CustomFieldnumber-1].Text := string(s);
             end;
        UserFieldType.area:begin
               AreaString (UserField.defaultarea, CustArea, UNITS_CLIP, TempStr, TempStr1);
               CustomFieldValues[CustomFieldnumber-1].Text := Trim (string(Tempstr + ' ' + Tempstr1));
             end
        else CustomFieldValues[CustomFieldnumber-1].Text := string(UserField.defaultstr);
      end;

    end;

  end;


end;

end.
