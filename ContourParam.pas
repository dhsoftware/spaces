unit ContourParam;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, DcadEdit, CommonStuff, Settings, Constants;

type
  TfmContourParam = class(TForm)
    Label1: TLabel;
    dceThick: TDcadEdit;
    Label2: TLabel;
    dceOpening: TDcadEdit;
    procedure FormCreate(Sender: TObject);
    procedure dceOpeningExit(Sender: TObject);
    procedure dceThickExit(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmContourParam: TfmContourParam;

implementation

{$R *.dfm}

procedure TfmContourParam.dceOpeningExit(Sender: TObject);
begin
  if dceOpening.valid then begin
    l^.maxopening := dceOpening.NumValue;
    SaveRl (SpdhMaxOpeng, l^.maxopening, false);
  end;
end;

procedure TfmContourParam.dceThickExit(Sender: TObject);
begin
  if dceThick.valid then begin
    l^.WallThick := dceThick.NumValue;
    SaveRl (SpdhWllThick, l^.WallThick, false);
  end;
end;

procedure TfmContourParam.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  SaveFormPos(TForm(Sender));
end;

procedure TfmContourParam.FormCreate(Sender: TObject);
begin
  dceThick.NumValue := l^.WallThick;
  dceOpening.NumValue := l^.maxopening;
  SetFormPos(TForm(Sender));
end;

procedure TfmContourParam.FormHide(Sender: TObject);
begin
  SaveFormPos(TForm(Sender));
end;

end.
