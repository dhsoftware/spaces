object fmCopyDist: TfmCopyDist
  Left = 0
  Top = 0
  BorderIcons = []
  Caption = 'Copy Dist'
  ClientHeight = 231
  ClientWidth = 194
  Color = clBtnFace
  Constraints.MaxHeight = 270
  Constraints.MinHeight = 256
  Constraints.MinWidth = 164
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poOwnerFormCenter
  StyleElements = [seFont, seClient]
  OnActivate = FormActivate
  OnCreate = FormCreate
  DesignSize = (
    194
    231)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 9
    Top = 56
    Width = 10
    Height = 13
    Caption = 'x:'
  end
  object Label2: TLabel
    Left = 9
    Top = 83
    Width = 10
    Height = 13
    Caption = 'y:'
  end
  object Label3: TLabel
    Left = 9
    Top = 110
    Width = 9
    Height = 13
    Caption = 'z:'
  end
  object Label4: TLabel
    Left = 9
    Top = 137
    Width = 10
    Height = 13
    Caption = 'd:'
  end
  object Label5: TLabel
    Left = 9
    Top = 164
    Width = 10
    Height = 13
    Caption = 'a:'
  end
  object Shape1: TShape
    Left = 8
    Top = 46
    Width = 173
    Height = 1
  end
  object lblDestLyr: TLabel
    Left = 8
    Top = 5
    Width = 84
    Height = 13
    Caption = 'Destination Layer'
  end
  object dceX: TDcadEdit
    Left = 25
    Top = 53
    Width = 94
    Height = 21
    AllowNegative = False
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    OnChange = XYchange
  end
  object dceY: TDcadEdit
    Left = 25
    Top = 80
    Width = 94
    Height = 21
    AllowNegative = False
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 1
    OnChange = XYchange
  end
  object dceZ: TDcadEdit
    Left = 24
    Top = 107
    Width = 95
    Height = 21
    AllowNegative = False
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 2
  end
  object dceD: TDcadEdit
    Left = 25
    Top = 134
    Width = 94
    Height = 21
    AllowNegative = False
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 3
    OnExit = DAchange
  end
  object dceA: TDcadEdit
    Left = 25
    Top = 161
    Width = 94
    Height = 21
    NumberType = DegMinSec
    AllowNegative = False
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 4
    OnExit = DAchange
  end
  object btnOK: TButton
    Left = 71
    Top = 192
    Width = 111
    Height = 25
    Anchors = [akLeft, akTop, akRight]
    Caption = 'btnOK'
    Default = True
    TabOrder = 5
    OnClick = btnOKClick
  end
  object btnCancel: TButton
    Left = 8
    Top = 192
    Width = 57
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 6
    TabStop = False
  end
  object cbLayer: TComboBox
    Left = 8
    Top = 20
    Width = 173
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 7
    Text = 'cbLayer'
  end
  object btnInvertX: TButton
    Left = 125
    Top = 53
    Width = 56
    Height = 21
    Anchors = [akTop, akRight]
    Caption = 'Invert'
    TabOrder = 8
    OnClick = btnInvertXClick
  end
  object btnInvertY: TButton
    Left = 125
    Top = 80
    Width = 56
    Height = 21
    Anchors = [akTop, akRight]
    Caption = 'Invert'
    TabOrder = 9
    OnClick = btnInvertYClick
  end
  object btnInvertZ: TButton
    Left = 125
    Top = 107
    Width = 56
    Height = 21
    Anchors = [akTop, akRight]
    Caption = 'Invert'
    TabOrder = 10
    OnClick = btnInvertZClick
  end
  object btnInvertA: TButton
    Left = 125
    Top = 161
    Width = 56
    Height = 21
    Anchors = [akTop, akRight]
    Caption = 'Invert'
    TabOrder = 11
    OnClick = btnInvertAClick
  end
end
