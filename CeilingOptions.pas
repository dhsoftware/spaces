unit CeilingOptions;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Language,
  Vcl.Imaging.pngimage, DcadNumericEdit, Vcl.ComCtrls;

type
  TfCeilingOptions = class(TForm)
    cbCeilingType: TComboBox;
    pcCeiling: TPageControl;
    tsFlat: TTabSheet;
    LblCeilingHt: TLabel;
    dcCeilingHeight: TDcadNumericEdit;
    tsSingle: TTabSheet;
    Label74: TLabel;
    Label75: TLabel;
    dcCeilingHeightSingle: TDcadNumericEdit;
    PageControl2: TPageControl;
    TabSheet6: TTabSheet;
    Image1: TImage;
    TabSheet7: TTabSheet;
    Image2: TImage;
    dcCeilingSingleSlope: TDcadNumericEdit;
    tsDouble: TTabSheet;
    Shape2: TShape;
    Label76: TLabel;
    Label77: TLabel;
    Label78: TLabel;
    Label79: TLabel;
    Label80: TLabel;
    Shape3: TShape;
    Label81: TLabel;
    Label82: TLabel;
    Label83: TLabel;
    Label84: TLabel;
    Label85: TLabel;
    dcCeilingHeightDbl1: TDcadNumericEdit;
    dcCeilingHeightDbl2: TDcadNumericEdit;
    dcDoubleSlopeHeight: TDcadNumericEdit;
    dcDoubleSlope1: TDcadNumericEdit;
    dcDoubleSlope2: TDcadNumericEdit;
    rbSlopes: TRadioButton;
    rbTopHeight: TRadioButton;
    PageControl3: TPageControl;
    TabSheet8: TTabSheet;
    Image3: TImage;
    TabSheet9: TTabSheet;
    Image4: TImage;
    TabSheet10: TTabSheet;
    Image5: TImage;
    tsArch: TTabSheet;
    Label86: TLabel;
    Label87: TLabel;
    Label88: TLabel;
    Label89: TLabel;
    Label90: TLabel;
    Label91: TLabel;
    Label92: TLabel;
    dcnCeilingHeightArch1: TDcadNumericEdit;
    dcnCeilingHeightArch2: TDcadNumericEdit;
    rbArchSemiCircle: TRadioButton;
    rbArchRadius: TRadioButton;
    dcnArchRadius: TDcadNumericEdit;
    rbArchTopHeight: TRadioButton;
    dcnArchTopHeight: TDcadNumericEdit;
    rbArchHorizontal: TRadioButton;
    PageControl4: TPageControl;
    TabSheet2: TTabSheet;
    Image6: TImage;
    TabSheet11: TTabSheet;
    Image7: TImage;
    TabSheet12: TTabSheet;
    Image8: TImage;
    TabSheet13: TTabSheet;
    Image9: TImage;
    TabSheet14: TTabSheet;
    Image10: TImage;
    tsCustom: TTabSheet;
    lblCustCeiling: TLabel;
    lblCeilingTp: TLabel;
    chbUseZ: TCheckBox;
    lblIn12: TLabel;
    LblDecDegrees: TLabel;
    lblIn12_1: TLabel;
    lblIn12_2: TLabel;
    procedure cbCeilingTypeChange(Sender: TObject);
    procedure rbArchSemiCircleClick(Sender: TObject);
    procedure rbArchRadiusClick(Sender: TObject);
    procedure rbArchTopHeightClick(Sender: TObject);
    procedure rbArchHorizontalClick(Sender: TObject);
    procedure rbSlopesClick(Sender: TObject);
    procedure rbTopHeightClick(Sender: TObject);
    procedure chbUseZClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
     procedure Enable (value : boolean);
  end;

var
  fCeilingOptions: TfCeilingOptions;

implementation

{$R *.dfm}

procedure TfCeilingOptions.cbCeilingTypeChange(Sender: TObject);
begin
  pcCeiling.ActivePageIndex := cbCeilingType.ItemIndex;
end;

procedure TfCeilingOptions.rbArchHorizontalClick(Sender: TObject);
begin
  if PageControl4.ActivePageIndex < 3 then
    PageControl4.ActivePageIndex := 3;
end;

procedure TfCeilingOptions.rbArchRadiusClick(Sender: TObject);
begin
  PageControl4.ActivePageIndex := 1;
end;

procedure TfCeilingOptions.rbArchSemiCircleClick(Sender: TObject);
begin
  PageControl4.ActivePageIndex := 0;
end;

procedure TfCeilingOptions.rbArchTopHeightClick(Sender: TObject);
begin
  PageControl4.ActivePageIndex := 2;
end;

procedure TfCeilingOptions.rbSlopesClick(Sender: TObject);
begin
  PageControl3.ActivePageIndex := 0;
end;

procedure TfCeilingOptions.rbTopHeightClick(Sender: TObject);
begin
  if PageControl3.ActivePageIndex < 1 then
    PageControl3.ActivePageIndex := 1;
end;

procedure TfCeilingOptions.chbUseZClick(Sender: TObject);
begin
  dcCeilingHeight.enabled := not chbUseZ.Checked;
  LblCeilingHt.enabled := not chbUseZ.Checked;
end;

procedure TfCeilingOptions.Enable (value : boolean);
begin
  cbCeilingType.Enabled := value;
  pcCeiling.Enabled := value;
  tsFlat.Enabled := value;
  dcCeilingHeight.Enabled := value and (not chbUseZ.checked);
  LblCeilingHt.Enabled := dcCeilingHeight.Enabled;
  tsSingle.Enabled := value;
  dcCeilingHeightSingle.Enabled := value;
  PageControl2.Enabled := value;
  TabSheet6.Enabled := value;
  TabSheet7.Enabled := value;
  dcCeilingSingleSlope.Enabled := value;
  tsDouble.Enabled := value;
  dcCeilingHeightDbl1.Enabled := value;
  dcCeilingHeightDbl2.Enabled := value;
  dcDoubleSlopeHeight.Enabled := value;
  dcDoubleSlope1.Enabled := value;
  dcDoubleSlope2.Enabled := value;
  rbSlopes.Enabled := value;
  rbTopHeight.Enabled := value;
  PageControl3.Enabled := value;
  TabSheet8.Enabled := value;
  TabSheet9.Enabled := value;
  TabSheet10.Enabled := value;
  tsArch.Enabled := value;
  dcnCeilingHeightArch1.Enabled := value;
  dcnCeilingHeightArch2.Enabled := value;
  rbArchSemiCircle.Enabled := value;
  rbArchRadius.Enabled := value;
  dcnArchRadius.Enabled := value;
  rbArchTopHeight.Enabled := value;
  dcnArchTopHeight.Enabled := value;
  rbArchHorizontal.Enabled := value;
  PageControl4.Enabled := value;
  TabSheet2.Enabled := value;
  TabSheet11.Enabled := value;
  TabSheet12.Enabled := value;
  TabSheet13.Enabled := value;
  TabSheet14.Enabled := value;
  tsCustom.Enabled := value;
  chbUseZ.Enabled := value;
end;


procedure TfCeilingOptions.FormCreate(Sender: TObject);
var
  lbl, msg : string;
begin
  cbCeilingType.Items.Clear;
  GetLbl (124, lbl, msg);
  cbCeilingType.Items.Add(lbl);
  GetLbl (125, lbl, msg);
  cbCeilingType.Items.Add(lbl);
  lblCustCeiling.Caption := msg;
  cbCeilingType.ItemIndex := 0;

  LabelCaption (306, lblCeilingHt);    //Ceiling Height
  LabelCaption (307, lblCeilingTp);    //Ceiling Type
  CheckCaption (308, chbUseZ);         //Use Z Settings
end;

end.
