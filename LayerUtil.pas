unit LayerUtil;

interface

  uses
    URecords, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, UInterfaces, CommonStuff, Language,
    StrUtil, Settings;

  procedure PopulateLblLayers (var LyrList : TComboBox);
  procedure SaveLblLayerInfo (var LyrList : TComboBox; var Ix : tEdit);
  procedure LblLyrChange(const LyrList : TComboBox; var Ix : tEdit; var lblIx : TLabel);

implementation

  procedure LblLyrChange(const LyrList : TComboBox; var Ix : tEdit; var lblIx : TLabel);
  begin
    Ix.Visible := (LyrList.ItemIndex in [1, 2]);
    lblIx.Visible := Ix.Visible;
    if Ix.Visible then
      LabelCaption (350+LyrList.ItemIndex, lblIx); // 351= Label Lyr Prefix, 352= Label Lyr Suffix
  end;



  procedure SaveLblLayerInfo (var LyrList : TComboBox; var Ix : tEdit);

  begin
    if (LyrList.ItemIndex = 0) or BlankString (LyrList.Text) or
       ((LyrList.ItemIndex in [1, 2]) and BlankString(Ix.Text))
    then begin
      l^.lblLyrSpec := LSActive;
    end
    else if LyrList.ItemIndex = 1 then begin
      l^.lblLyrSpec := LSPrefix;
      l^.lblLyrIx := Ix.Text;
      SaveStr ('SpdhLblLyrIx', Ix.Text);
    end
    else if LyrList.ItemIndex = 2 then begin
      l^.lblLyrSpec := LSSuffix;
      l^.lblLyrIx := Ix.Text;
      SaveStr ('SpdhLblLyrIx', Ix.Text);
    end
    else begin
      l^.lblLyrSpec := LSSpecific;
      l^.lblLyrSpecific := LyrList.Text;
      SaveStr ('SpdhLblLyr', LyrList.Text, false);    // do not save specific layer to ini file as it may not be applicable to other drawings
    end;
    SaveInt ('SpdhLblLyrSp', ord(l^.lblLyrSpec));
  end;


  procedure PopulateLblLayers (var LyrList : TComboBox);
  var
    lyraddr : lgl_addr;
    rlyr : Rlayer;
    ss : shortstring;
    s : string;
    svText : string;
  begin
    svText := LyrList.Text;
    LyrList.Items.Clear;
    LyrList.Sorted := l^.Options.LyrSort;
    lyraddr := lyr_first;
    while lyr_get (lyraddr, rlyr) do begin
      lyraddr := lyr_next (lyraddr);
      getLongLayerName (rlyr.addr, ss);
      LyrList.Items.Add (string(ss));
    end;
    LyrList.Sorted := false;
    s := GetLbl (348);  //Same as outline (active lyr)
    LyrList.Items.Insert(0, s);
    s := GetLbl (350);  //Add prefix to outline lyr
    LyrList.Items.Insert(1, s);
    s := GetLbl (349);  //Add suffix to outline lyr
    LyrList.Items.Insert(2, s);

    if not BlankString(svText) then begin
      if LyrList.Items.IndexOf(svText) < 0 then
        LyrList.Text := SvText
      else
        LyrList.ItemIndex := LyrList.Items.IndexOf(SvText);
    end
    else if (l^.lblLyrSpec = LSActive) or
            ((l^.lblLyrSpec in [LSPrefix, LSSuffix]) and Blankstring(l^.lblLyrIx)) or
            ((l^.lblLyrSpec = LSSpecific) and Blankstring(l^.lblLyrSpecific))  then
      LyrList.ItemIndex := 0
    else if l^.lblLyrSpec = LSPrefix then
      LyrList.ItemIndex := 1
    else if l^.lblLyrSpec = LSSuffix then
      LyrList.ItemIndex := 2
    else begin
      if BlankString(l^.lblLyrSpecific) then
        LyrList.ItemIndex := 0
      else if LyrList.Items.IndexOf(l^.lblLyrSpecific) < 0 then
        LyrList.Text := l^.lblLyrSpecific
      else
        LyrList.ItemIndex := LyrList.Items.IndexOf(l^.lblLyrSpecific);
    end;
  end;


end.
