unit SummaryRpt;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Tabs, Language, CommonStuff, Vcl.Clipbrd,
  Settings, URecords, UInterfaces, UConstants, StrUtil;

type
  TfmSummary = class(TForm)
    tsLayers: TTabSet;
    lblCategory: TLabel;
    cbCategory: TComboBox;
    lblNumSpaces: TLabel;
    edNumSpaces: TEdit;
    lblArea: TLabel;
    edArea: TEdit;
    lblPerim: TLabel;
    edPerim: TEdit;
    lblVol: TLabel;
    edVol: TEdit;
    btnDone: TButton;
    btnClipboard: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ChangeParameters(Sender: TObject);
    procedure tsLayersChange(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);
    procedure btnClipboardClick(Sender: TObject);
  private
    { Private declarations }
    init : boolean;
    CurrentTab : integer;
  public
    { Public declarations }
  end;

var
  fmSummary: TfmSummary;

implementation

{$R *.dfm}

procedure TfmSummary.btnClipboardClick(Sender: TObject);
var
  ClipboardStr : string;
begin
  ClipboardStr := lblNumSpaces.Caption + #9 + edNumSpaces.Text + sLineBreak +
                  lblArea.Caption + #9 + edArea.Text + sLineBreak +
                  lblPerim.Caption + #9 + edPerim.Text + sLineBreak +
                  lblVol.Caption + #9 + edVol.Text;
  try
    Clipboard.AsText := ClipboardStr;
  except
    lmsg_ok (101); //Could not copy to clipboard
  end;
end;

procedure TfmSummary.ChangeParameters(Sender: TObject);
var
  mode : mode_type;
  pStandardAtr : ^StandardAtr;
  ent : entity;
  adr : lgl_addr;
  atr : attrib;
  count : integer;
  area, perim, vol : double;
  s1, s2 : string;
  Uncategorised : boolean;
begin
  count := 0;
  area := zero;
  perim := zero;
  vol := zero;
  mode_init (mode);
  Uncategorised := false;
  case CurrentTab of
    0 : mode_lyr (mode, lyr_all);
    1 : mode_lyr (mode, lyr_on)
    else mode_lyr (mode, lyr_curr);
  end;
  mode_atr (mode, STANDARD_ATR_NAME);
  mode_enttype (mode, entpln);
  adr := ent_first (mode);
  while ent_get (ent, adr) do begin
    if atr_entfind (ent, STANDARD_ATR_NAME, atr) and (atr.atrtype = atr_str255) then begin
      pStandardAtr := addr (atr.shstr);
      if (not init) and (cbCategory.Items.IndexOf(string(pStandardAtr^.Category)) < 0) then begin
        if BlankString(pStandardAtr^.Category) then
          Uncategorised := true
        else
          cbCategory.Items.Add(string(pStandardAtr^.Category));
      end;
      if (not init) or (UpperCase (cbCategory.Text) = UpperCase (string(pStandardAtr^.Category))) or
         (cbCategory.ItemIndex = 0) or ((cbCategory.ItemIndex = 1) and BlankString (pStandardAtr^.Category))
      then begin
        count := count + 1;
        area := area + pStandardAtr^.area;
        perim := perim + pStandardAtr^.OutlinePerim;
        vol := vol + pStandardAtr^.volume;
      end;
    end;
    adr := ent_next (ent, mode);
  end;

  if not init then begin
    cbCategory.Sorted := false;
    s1 := GetLvl (344);  //ALL CATEGORIES
    cbCategory.Items.Insert(0, s1);
    if Uncategorised then begin
      s1 := GetMsg(122);  //Uncategorised
      cbCategory.Items.Insert(1, s1);
    end;
    cbCategory.ItemIndex := 0;
    init := true;
  end;

  edNumSpaces.Text := inttostr (count);

  AreaString (area, Flr, UNITS_CLIP, s1, s2);
  if not BlankString(s2) THEN
    s1 := s1 + ' ' + s2;
  edArea.Text := s1;

  DisString (perim, UNITS_CLIP, s1, s2);
  if not BlankString(s2) THEN
    s1 := s1 + ' ' + s2;
  edPerim.Text := s1;

  VolString (vol, UNITS_CLIP, s1, s2);
  if not BlankString(s2) THEN
    s1 := s1 + ' ' + s2;
  edVol.Text := s1;
end;

procedure TfmSummary.tsLayersChange(Sender: TObject; NewTab: Integer;
  var AllowChange: Boolean);
begin
  CurrentTab := NewTab;
  ChangeParameters (Sender);
end;


procedure TfmSummary.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  SaveFormPos(TForm(Sender));
end;

procedure TfmSummary.FormCreate(Sender: TObject);
begin
  SetFormPos (TForm(Sender));
  LabelCaption (113, lblCategory);  //Category
  LabelCaption (339, lblNumSpaces); //Number of Spaces
  LabelCaption (340, lblArea);      //Total Area
  LabelCaption (341, lblPerim);     //Total Perimeter
  LabelCaption (342, lblVol);       //TotalVolume
  ButtonLblParams(20, [], btnDone); //Exit
  (Sender as TForm).Caption := GetLvl (343);  //Summary Report
  tsLayers.tabs[2] := GetLbl (74); //Active Layer
  tsLayers.tabs[1] := GetLbl (75); //On Layers|Report on all spaces on Layers that are currently ON
  tsLayers.tabs[0] := GetLbl (76); //All Layers
  ButtonLblParams(312, [], btnClipboard);   //to Clipboard

  cbCategory.Items.Clear;
  tsLayers.TabIndex := 0;
  CurrentTab := 0;

  ChangeParameters (self);

  tsLayers.OnChange := tsLayersChange;

end;


end.
