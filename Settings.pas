unit Settings;

interface

uses inifiles, SysUtils, Dialogs, Controls, System.IOUtils, URecords, UInterfaces,
     UConstants, System.Classes, System.UITypes, WinAPI.Windows, Vcl.Forms, Language,
     StrUtils, RegPrompt;

const
  MainIni = 'dhsoftware/dhsoftware.ini';
  REFRESHMACRO_NAME = 'SpaceRefresh';

PROCEDURE SaveIniInt (name : string;  value : integer);   overload;
PROCEDURE SaveIniInt (name : atrname;  value : integer);  overload;
PROCEDURE SaveInt (name : atrname;  value : integer; toini : boolean);  overload;
PROCEDURE SaveInt (name : atrname;  value : integer);  overload;
PROCEDURE SaveInt (name : string;  value : integer);  overload;
FUNCTION GetIniInt (name : atrname; default : integer) : integer;  overload;
FUNCTION GetIniInt (name : string; default : integer) : integer;  overload;
FUNCTION GetSvdInt (name : atrname; default : integer) : integer; overload;
FUNCTION GetSvdInt (name : string; default : integer) : integer; overload;
FUNCTION InitSettings (MacroName : string; RefreshOnly : boolean) : boolean; overload;
FUNCTION InitSettings (MacroName : string) : boolean; overload;
PROCEDURE SaveAng (name : atrname;  value : Double; toini : boolean);
FUNCTION GetIniRl (name : atrname; default : Double) : Double;
FUNCTION GetSvdAng (name : atrname; default : Double) : Double;
PROCEDURE SaveRl (name : atrname;  value : Double; toini : boolean);   overload;
PROCEDURE SaveRl (name : string;  value : Double; toini : boolean);  overload;
PROCEDURE SaveRl (name : string;  value : Double);  overload;
PROCEDURE SaveIniRl (name : atrname; value : Double);
FUNCTION GetSvdRl (name : atrname; default : Double) : Double;  overload;
FUNCTION GetSvdRl (name : string; default : Double) : Double;  overload;
PROCEDURE SaveBln (name : atrname;  value : boolean; toini : boolean; toAttr : boolean); overload;
PROCEDURE SaveBln (name : atrname;  value : boolean); overload;
FUNCTION GetIniBln (name : atrname; default : boolean) : boolean;
FUNCTION GetSvdBln (name : atrname; default : boolean) : boolean;
PROCEDURE SaveIniStr (name : atrname;  value : shortstring);  overload;
PROCEDURE SaveIniStr (name : atrname;  value : string);  overload;
PROCEDURE SaveStr (name : atrname;  value : shortstring; toini : boolean); overload;
PROCEDURE SaveStr (name : atrname;  value : string; toini : boolean); overload;
PROCEDURE SaveStr (name : atrname;  value : shortstring); overload;
PROCEDURE SaveStr (name : atrname;  value : string); overload;
PROCEDURE SaveStr (name : string;  value : string); overload;
FUNCTION GetIniStr (name : atrname; default : shortstring) : shortstring; overload;
FUNCTION GetIniStr (name : atrname; default : string) : string; overload;
FUNCTION GetSvdStr (name : atrname; default : shortstring) : shortstring; overload;
FUNCTION GetSvdStr (name : string; default : shortstring) : shortstring; overload;
PROCEDURE SaveLayer (name : atrname; lyr : layer);
FUNCTION GetSvdLyr (name : atrname; VAR lyr : layer) : boolean;
FUNCTION readIniStr (iniFileName : string; section : string; ident : atrname;
                    default : shortstring) : shortstring;
PROCEDURE writeIniStr (iniFileName : string; section : string; ident : string; value : string); overload;
PROCEDURE writeIniStr (iniFileName : string; section : string; ident : atrname; value : shortstring); overload;
FUNCTION PrevMacroPath : string;

PROCEDURE DeleteSetting (name : atrName);

PROCEDURE SaveStrToMainIni (const  Ident, val : string);
FUNCTION GetStrFromMainIni (const Ident, Default : string) : string;

PROCEDURE SetFormPos (var form : TForm);
PROCEDURE SaveFormPos (var form : TForm);

implementation


VAR
	iniFileName	: string;
	section			: string;
	atr					: attrib;
	initialised	: boolean;
  OldMacroPath : string;

FUNCTION NameTooLong (name : string) : boolean;
BEGIN
  result := length(name) > 12;
  if result then
    MessageDlg ('Atr Name (' + name + ') too long' + sLineBreak + 'Kindly report this error to dhSoftware.', mtError, [mbOK], 0);
END;

FUNCTION StrTooLongForAtr (value : string; name : atrname; OnlySaveOption : boolean) : boolean;
var
  atr : attrib;
BEGIN
  result := length(value) > 255;
  if OnlySaveOption then
    MessageDlg ('The following string is too long to be saved:'
                  + sLineBreak + sLineBreak + value + sLineBreak, mtError, [mbOK], 0)
  else begin
    // delete any existing atr from the drawing so that it will pick up the ini file setting;
    if atr_sysfind (name, atr) then
      atr_delsys (atr);
  end;
END;

PROCEDURE writeIniInt (iniFileName : string; section : string; ident : string; value : integer);  overload;
VAR
  iniFile: TIniFile;
BEGIN
  iniFile := TIniFIle.Create(iniFileName);
  try
    iniFile.WriteInteger(section, ident, value);
  finally
    iniFile.Free;
  end;
END;

PROCEDURE writeIniInt (iniFileName : string; section : string; ident : atrname; value : integer); overload;
BEGIN
  writeIniInt(iniFileName, section, string(ident), value);
END;

PROCEDURE writeIniStr (iniFileName : string; section : string; ident : string; value : string);
VAR
  iniFile: TIniFile;
BEGIN
  iniFile := TIniFIle.Create(iniFIleName);
  try
    iniFile.WriteString(section, ident, value);
  finally
    iniFile.Free;
  end;
END;

PROCEDURE writeIniStr (iniFileName : string; section : string; ident : atrname; value : shortstring);
BEGIN
  writeIniStr (iniFileName, section, string(ident), string(value));
END;

PROCEDURE writeIniReal (iniFileName : string; section : string; ident : atrname; value : Double);
VAR
  iniFile: TIniFile;
BEGIN
  iniFile := TIniFIle.Create(iniFIleName);
  try
    iniFile.WriteFloat(section, string(ident), value);
  finally
    iniFile.Free;
  end;
END;

FUNCTION readIniStr (iniFileName : string; section : string; ident : atrname;
                    default : shortstring) : shortstring;     overload;
VAR
  iniFile: TIniFile;
BEGIN
  iniFile := TIniFIle.Create(iniFIleName);
  try
    result := shortstring(iniFile.ReadString(section, string(ident), string(default)));
  finally
    iniFile.Free;
  end;
END;

FUNCTION readIniStr (iniFileName : string; section : string; ident : atrname;
                    default : string) : string;     overload;
VAR
  iniFile: TIniFile;
BEGIN
  iniFile := TIniFIle.Create(iniFIleName);
  try
    result := iniFile.ReadString(section, string(ident), default);
  finally
    iniFile.Free;
  end;
END;

FUNCTION readIniInt (iniFileName : string; section : string; ident : string;
                    default : integer) : integer;
VAR
  iniFile: TIniFile;
BEGIN
  iniFile := TIniFIle.Create(iniFIleName);
  try
    result := iniFile.ReadInteger(section, string(ident), default);
  finally
    iniFile.Free;
  end;
END;

FUNCTION readIniReal (iniFileName : string; section : string; ident : atrname;
                    default : Double) : Double;
VAR
  iniFile: TIniFile;
BEGIN
  iniFile := TIniFIle.Create(iniFIleName);
  try
    result := iniFile.ReadFloat(section, string(ident), default);
  finally
    iniFile.Free;
  end;
END;

FUNCTION InitSettings (MacroName : string; RefreshOnly : boolean) : boolean; overload;
VAR
	MacroPath, SupPath : shortstring;
  ssPrompt : shortstring;
  MacroPath1, prompt, OldIniFile, TempStr : string;
  reg : string;
  Name: Array [0 .. 256] of char;
  ComputerName, UserName : string;
  size : dword;
  Registered : boolean;
  regno : integer;
  RegistrationPrompt: TRegistationPrompt;

BEGIN
  result := true;

	section := string(macroName);
	UInterfaces.getpath (SupPath, pathsup);
	iniFileName := string(SupPath) + MainIni;

  if not (RefreshOnly or (MacroName=REFRESHMACRO_NAME)) then begin
    reg := readIniStr (iniFileName, section, 'Registration', '0');

    Size := 256;
    GetComputerName(Name, Size);
    ComputerName := string (Name);

    Size := 256;
    GetUserName (Name, Size);
    UserName := string(Name);
    Registered := ContainsText (reg, ComputerName) and ContainsText (reg, UserName);
    if not Registered then begin
      try
        regno := strtoint (reg.SubString(0, 1));
      except
        regno := 0;
        reg := '0' + reg;
      end;
      regno := regno + 1;
      if regno > 5 then begin
        RegistrationPrompt := TRegistationPrompt.Create(Application);
        try
          RegistrationPrompt.ShowModal;
          if RegistrationPrompt.ModalResult = mrCancel then
            regno := 0
          else begin
            if not ContainsText (reg, ComputerName) then
              reg := Reg + ComputerName;
            if not ContainsText (reg, UserName) then
              reg := Reg + UserName;
          end;
          if RegistrationPrompt.ModalResult = mrClose then
            lmsg_ok (130);
        finally
          RegistrationPrompt.Free;
        end;
      end;
      reg := inttostr(regno) + reg.SubString(1);
      writeIniStr (inifileName, section, 'Registration', reg);
    end;

    WriteIniStr (iniFIleName, section, 'Registration', reg);
  end;

	UInterfaces.getpath (MacroPath, pathmcr);
	if not  fileexists (inifilename) then
		writeIniStr (iniFileName, section, 'PathMCR', string(MacroPath));

	OldMacroPath := readIniStr (iniFileName, Section, 'PathMCR', '');
	MacroPath1 := OldMacroPath;
	writeIniStr (iniFileName, section, 'PathMCR', string(MacroPath));

	if length (MacroPath1) = 0 then begin
		writeIniStr (iniFileName, section, 'PathMCR', string(MacroPath));
		MacroPath1 := string(MacroPath);
	end;

  if RefreshOnly then  // if refreshing only then always use the last settings used by the Spaces macro
    iniFileName := string(OldMacroPath) + macroName + '.ini'
  else begin
	  iniFileName := string(MacroPath) + macroName + '.ini';

    if CompareText (string(MacroPath), MacroPath1) <> 0 then begin
      OldIniFile := MacroPath1 + macroName + '.ini';
      if FileExists (OldIniFile) and FileExists (iniFileName) and (macroName <> REFRESHMACRO_NAME) then begin
        GetMsgParams (32, [shortstring(macroname)], ssPrompt);
        prompt := string(ssPrompt) + sLineBreak + sLineBreak;     //Settings for this macro are stored in a file in your macro path.
        prompt := prompt + GetMsg(33) + sLineBreak + MacroPath1 + sLineBreak + sLineBreak;  //Last time you used the macro this path was:
        prompt := prompt + GetMsg(34) + sLineBreak + string(MacroPath) + sLineBreak + sLineBreak;  //It is now:
        prompt := prompt + GetMsg(35);  //Would you like to copy settings from the previous path to the current one?

        if messagedlg (prompt, mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
          TempStr := iniFileName + '-old';
          if FileExists (TempStr) then
            SysUtils.DeleteFile (TempStr);
          RenameFile (iniFileName, TempStr);
          TFile.Copy (string(OldIniFile), iniFileName);
        end
        else if FileExists (OldIniFile) and not FileExists (iniFileName) then // just copy old file if new file does not exist
          TFile.Copy (string(OldIniFile), iniFileName);
      end;
    end;
  end;
	initialised := true;
END;


FUNCTION InitSettings (macroName : string) :boolean;
BEGIN
  result := InitSettings (macroName, false);
END;

FUNCTION PrevMacroPath : String;
BEGIN
  result := OldMacroPath;
END;

PROCEDURE SaveIniInt (name : string;  value : integer);
BEGIN
		writeIniInt (iniFileName, section, name, value);
END;

PROCEDURE SaveIniInt (name : atrname;  value : integer);
BEGIN
		writeIniInt (iniFileName, section, name, value);
END;

PROCEDURE SaveInt (name : atrname;  value : integer; toini : boolean);
BEGIN
	if atr_sysfind (name, atr) then begin
    atr.atrtype := atr_int;
		atr.int := value;
		atr_update (atr);
  end
	else begin
		atr_init (atr, atr_int);
		atr.name := name;
		atr.int := value;
		atr_add2sys (atr);
	end;
	if toini then
		SaveIniInt(name, value);
END;

PROCEDURE SaveInt (name : atrname;  value : integer);
BEGIN
  SaveInt (name, value, true);
END;

PROCEDURE SaveInt (name : string;  value : integer);
BEGIN
  if not NameTooLong(name) then
  SaveInt (atrname(name), value, true);
END;

FUNCTION GetIniInt (name : atrname; default : integer) : integer;
BEGIN
		result := readIniInt (iniFileName, section, string(name), default);
END;

FUNCTION GetIniInt (name : string; default : integer) : integer;
BEGIN
		result := readIniInt (iniFileName, section, name, default);
END;

FUNCTION GetSvdInt (name : atrname; default : integer) : integer;
BEGIN
	if atr_sysfind (name, atr) then
		result := atr.int
	else
		result := GetIniInt (name, default);
END;

FUNCTION GetSvdInt (name : string; default : integer) : integer;
BEGIN
	if NameTooLong(name) then
    result := 0
  else
		result := GetSvdInt (atrname(name), default);
END;


PROCEDURE SaveAng (name : atrname;  value : Double; toini : boolean);
BEGIN
	if atr_sysfind (name, atr) then begin
    atr.atrtype := atr_ang;
		atr.ang := value;
		atr_update (atr);
  end
	else  begin
		atr_init (atr, atr_ang);
		atr.name := name;
		atr.ang := value;
		atr_add2sys (atr);
	end;
	if toini then
		writeIniReal (iniFileName, section, name, value);
END;

FUNCTION GetIniRl (name : atrname; default : Double) : Double;
BEGIN
	result := readIniReal (iniFileName, section, name, default);
END;


FUNCTION GetSvdAng (name : atrname; default : Double) : Double;
BEGIN
	if atr_sysfind (name, atr) then
		result := atr.ang
	else
		result := GetIniRl (name, default);
END;

PROCEDURE SaveIniRl (name : atrname; value : Double);
BEGIN
		writeIniReal (iniFileName, section, name, value);
END;


PROCEDURE SaveRl (name : atrname;  value : Double; toini : boolean);
BEGIN
	if atr_sysfind (name, atr) then begin
    atr.atrtype := atr_rl;
		atr.rl := value;
		atr_update (atr);
  end
	else begin
		atr_init (atr, atr_rl);
		atr.name := name;
		atr.rl := value;
		atr_add2sys (atr);
	end;
	if toini then
		SaveIniRl (name, value);
END;

PROCEDURE SaveRl (name : string;  value : Double; toini : boolean);
BEGIN
	if not NameTooLong(name) then
		SaveRl (atrname(name), value, toini);
END;

PROCEDURE SaveRl (name : string;  value : Double);
BEGIN
  SaveRl (name, value, true);
END;

FUNCTION GetSvdRl (name : atrname; default : Double) : Double;
BEGIN
	if atr_sysfind (name, atr) then
		result := atr.rl
	else
		result := GetIniRl (name, default);
END;

FUNCTION GetSvdRl (name : string; default : Double) : Double;
BEGIN
	if NameTooLong (name) then
    result := 0.0
	else
		result := GetSvdRl (atrname(name), default);
END;




PROCEDURE SaveBln (name : atrname;  value : boolean; toini : boolean; toAttr : boolean);
VAr
	ival : integer;
	sval : str255;
BEGIN
	if value then begin
		ival := 1;
		sval := 'true';
  end
	else begin
		sval := 'false';
		ival := 0;
	end;
	if toAttr then begin
		if atr_sysfind (name, atr) then begin
      atr.atrtype := atr_int;
			atr.int := ival;
			atr_update (atr);
    end
		else begin
			atr_init (atr, atr_int);
			atr.name := name;
			atr.int := ival;
			atr_add2sys (atr);
		end;
	end;
	if toini then
		writeIniStr (iniFileName, section, name, shortstring(sval));
END;

PROCEDURE SaveBln (name : atrname;  value : boolean);
BEGIN
  SaveBln (name, value, true, true);
END;


FUNCTION GetIniBln (name : atrname; default : boolean) : boolean;
VAR
	s : string;
	d	: string;
BEGIN
	if default then
		d := 'true'
	else
		d := 'false';

	s := readIniStr (iniFileName, section, name, d);
	result := (pos ('t', s, 1) = 1) or (pos ('T', s, 1) = 1);
END;

FUNCTION GetSvdBln (name : atrname; default : boolean) : boolean;
BEGIN
	if atr_sysfind (name, atr) then
    result := atr.int <> 0
	else
		result := GetIniBln (name, default);
END;

PROCEDURE SaveIniStr (name : atrname;  value : shortstring);
BEGIN
	writeIniStr (iniFileName, section, name, value);
END;

PROCEDURE SaveIniStr (name : atrname;  value : string);
BEGIN
	writeIniStr (iniFileName, section, string(name), value);
END;

PROCEDURE SaveStr (name : atrname;  value : shortstring; toini : boolean);
BEGIN
  if atr_sysfind (name, atr) then begin
    atr.atrtype := atr_str255;
    atr.shstr := str255(value);
    atr_update (atr);
  end
  else begin
    atr_init (atr, atr_str255);
    atr.name := name;
    atr.shstr := str255(value);
    atr_add2sys (atr);
  end;

	if toini then
		SaveIniStr (name, value);
END;

PROCEDURE SaveStr (name : atrname;  value : string; toini : boolean);
BEGIN
  SaveStr (name, shortstring(value), toini);
END;


PROCEDURE SaveStr (name : atrname;  value : shortstring);
BEGIN
  SaveStr (name, value, true);
END;

PROCEDURE SaveStr (name : atrname;  value : string); overload;
BEGIN
  if StrTooLongForAtr (value, name, false) then
    SaveIniStr (name, value)
  else
    SaveStr (name, shortstring(value), true);
END;

PROCEDURE SaveStr (name : string;  value : string); overload;
BEGIN
  if not NameTooLong(name) then
    SaveStr (atrname(name), value);
END;

FUNCTION GetIniStr (name : atrname; default : shortstring) : shortstring;
BEGIN
	result := readIniStr (iniFileName, section, name, default);
END;

FUNCTION GetIniStr (name : atrname; default : string) : string;
BEGIN
	result := readIniStr (iniFileName, section, name, default);
END;

FUNCTION GetSvdStr (name : atrname; default : shortstring) : shortstring;
BEGIN
	if atr_sysfind (name, atr) then begin
    if atr.atrtype = atr_str then
		  result := shortstring(atr.str)
    else if atr.atrtype = atr_str255 then begin
      result := shortstring(atr.shstr)
    end
    else
      result := GetIniStr (name, default);
  end
	else
		result := GetIniStr (name, default);
END;

FUNCTION GetSvdStr (name : string; default : shortstring) : shortstring;
BEGIN
	if NameTooLong (name) then
		result := ''
	else
		result := GetSvdStr (atrname(name), default);
END;

PROCEDURE SaveLayer (name : atrname; lyr : layer);
VAR
	templyr : layer;
BEGIN
	templyr := lyr_first;
	while not lyr_nil (templyr) do begin
		if atr_lyrfind (templyr, name, atr) then begin
			if (templyr.page <> lyr.page) or (templyr.ofs <> lyr.ofs) then
				atr_dellyr (templyr, atr)
    end
		else if (templyr.page = lyr.page) and (templyr.ofs = lyr.ofs) then begin
			atr_init (atr, atr_str);
			atr.name := name;
			atr.str := name;
			atr_add2lyr (lyr, atr);
		end;
		templyr := lyr_next(templyr);
	end;
END;

FUNCTION GetSvdLyr (name : atrname; VAR lyr : layer) : boolean;
VAR
	TempLyr : layer;
BEGIN
	templyr := lyr_first;
	while not lyr_nil (templyr) do begin
		if atr_lyrfind (templyr, name, atr) then begin
			lyr := templyr;
			result := true;
      exit;
    end;
		templyr := lyr_next(templyr);
	end;
	result := false;
END;

PROCEDURE DeleteSetting (name : atrName);
VAR
  atr : attrib;
  Ini : TIniFile;
BEGIN
  if atr_sysfind (name, atr) then
    atr_delsys (atr);
  ini := TIniFile.Create(iniFileName);
  try
    ini.DeleteKey(section, string(name));
  except
  end;
  ini.Free;
END;

PROCEDURE SetFormPos (var form : TForm);
VAR
  SupPath : shortstring;
  iniFileName : string;
  iniFile: TIniFile;
BEGIN
	UInterfaces.getpath (SupPath, pathsup);
  iniFileName := string(SupPath) + MainIni;
	if fileexists (inifilename) then begin
    iniFile := TIniFIle.Create(iniFileName);
    try
      form.left := iniFile.ReadInteger(section, form.name + '-left', form.left);
      form.width := iniFile.ReadInteger(section, form.name + '-width', form.width);
      form.top := iniFile.ReadInteger(section, form.name + '-top', form.top);
      form.height := iniFile.ReadInteger(section, form.name + '-height', form.Height);
    finally
      iniFile.Free;
    end;
  end;
END;

PROCEDURE SaveFormPos (var form : TForm);
VAR
  SupPath : shortstring;
  iniFileName : string;
  iniFile: TIniFile;
BEGIN
	UInterfaces.getpath (SupPath, pathsup);
  iniFileName := string(SupPath) + MainIni;
	if fileexists (inifilename) then begin
    iniFile := TIniFIle.Create(iniFileName);
    try
      iniFile.WriteInteger(section, form.name + '-left', form.left);
      iniFile.WriteInteger(section, form.name + '-width', form.width);
      iniFile.WriteInteger(section, form.name + '-top', form.top);
      iniFile.WriteInteger(section, form.name + '-height', form.height);
    finally
      iniFile.Free;
    end;
  end;
END;


PROCEDURE SaveStrToMainIni (const  Ident, val : string);
VAR
  SupPath : shortstring;
  iniFileName : string;
  iniFile: TIniFile;
BEGIN
  UInterfaces.getpath (SupPath, pathsup);
    iniFileName := string(SupPath) + MainIni;
  if fileexists (inifilename) then begin
    iniFile := TIniFIle.Create(iniFileName);
    try
      iniFile.WriteString(section, ident, val);
    finally
      iniFile.Free;
    end;
  end;
END;

FUNCTION GetStrFromMainIni (const Ident, Default : string) : string;
VAR
  SupPath : shortstring;
  iniFileName : string;
  iniFile: TIniFile;
BEGIN
  try
    UInterfaces.getpath (SupPath, pathsup);
    iniFileName := string(SupPath) + MainIni;
    if fileexists (inifilename) then begin
      iniFile := TIniFIle.Create(iniFileName);
      try
        try
          result := iniFile.ReadString (section, ident, default);
        except
          result := default;
        end;
      finally
        iniFile.Free;
      end;
    end
    else
      result := default;
  except
    result := default;
  end;
END;




END.

