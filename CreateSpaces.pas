unit CreateSpaces;

{$B-}

interface

uses  SysUtils, URecords,  UInterfaces, UConstants, StrUtil, CommonStuff, Version,
      Settings, vcl.dialogs, System.UITypes, Math, System.Generics.Collections,
      SpaceDetails, DCADEdit, Calcs, PlnUtil, SpaceUtil, UDCLibrary, UserFields,
      StrUtils, Language, Constants;

PROCEDURE CreateSpace (const fmSpaceProperties : TfmSpaceProperties);  overload;
PROCEDURE CreateSpace (CeilingTypeNdx : integer;
                       FloorZ : double;
                       CeilingHeight : double;
                       Name, Number, Category : string;
                       UserFieldValues : TList<UserFieldValue>;
                       xlsSource, xlsRow : smallint);      overload;
PROCEDURE PlaceLabel (pntfrom, pntto : point; Autoplacing : boolean);
PROCEDURE DrawLabel;
PROCEDURE GroupLabel;
PROCEDURE RotateLabel (ang : double);

PROCEDURE RefreshLabel (plnent : entity; mas : StandardAtrStr);  overload;
PROCEDURE RefreshLabel (plnent : entity);  overload;
PROCEDURE RefreshLabels (checkIfChanged : boolean);

PROCEDURE UpdateLabel (plnent : entity);

PROCEDURE ProcessOffsetLine (NeedToAddEntity, DefineByWallSurface,
                             CreateWallSurface, CreateWallCenter : boolean;
                             WallWidth : double);
PROCEDURE ProcessVisibility (DefineByWallSurface, CreateWallSurface, CreateWallCenter : boolean );

FUNCTION SubstituteTags (var str : shortstring; CONST StAtr : StandardAtr; Destination : integer;
                         DoCustomTags, UpdateParam : boolean) : string;   overload;
FUNCTION SubstituteTags (var str : shortstring; CONST StAtr : StandardAtr; LyrName : string;
                         floorz, ceilingz, RptTotArea, DwgTotArea : double; count : word;
                         Destination : integer; DoCustomTags, UpdateParam : boolean) : string;   overload;

implementation


FUNCTION SubstituteTags (var str : shortstring; CONST StAtr : StandardAtr; LyrName : string;
                         floorz, ceilingz, RptTotArea, DwgTotArea : double; count : word;
                         Destination : integer; DoCustomTags, UpdateParam : boolean) : string;
VAR
  tempstr, datastr : string;
  i, j, pos, pos1, pos2 : integer;
  d : double;
  ConvertUpper : boolean;
  atr : attrib;
  custstr, tempshortstring : shortstring;
  hdim_disstr, vdim_disstr, perim_disstr, operim_disstr, vperim_disstr, d_units : string;
  hdim_calc, vdim_calc, perim_calc, operim_calc, vperim_calc : boolean;
  area_str, oarea_str, varea_str, parea_str, oparea_str, vparea_str, area_units, parea_units : string;
  area_calc, oarea_calc, varea_calc, parea_calc, oparea_calc, vparea_calc : boolean;
  vol_str, vol_units : string;
  vol_calc : boolean;
  standard : StandardAtrStr;
  UserField : UserFieldDef;
  carea_units : string;

BEGIN
  result := string(str);

  // use booleans below to ensure dim, area, vol strings are only calculated once
  hdim_calc := false;
  vdim_calc := false;
  perim_calc := false;
  vperim_calc := false;
  operim_calc := false;
  area_calc := false;
  oarea_calc := false;
  varea_calc := false;
  parea_calc := false;
  oparea_calc := false;
  vparea_calc := false;
  vol_calc := false;

  // check for standard tags
  for i := 1 to NumTags do begin
    repeat // repeat is only necessary in case user has same tag more than once - can't see why they would, but might as well allow for it
      tempstr := result.ToUpper;
      pos := ansipos (Tags[i, 1].ToUpper, tempstr);
      if pos > 0 then begin
        ConvertUpper := true;
        for j := pos to pos + length(Tags[i,1]) do
          if CharInSet(result[j], ['a'..'z']) then     // if the tag is ALL upper case then the data associated with it is to be converted to upper case
            ConvertUpper := False;
        delete (result, pos, length(Tags[i, 1]));
        case i of
          tag_nbr : datastr := string(StAtr.Num);    //{nbr}
          tag_name : datastr := TrimRight(string(StAtr.Name));   //{name}
          tag_hdim : begin     //{hdim}
                if not hdim_calc then begin
                  DisString (StAtr.hdim, Destination, hdim_disstr, d_units);
                  hdim_calc := true;
                end;
                datastr := hdim_disstr;
              end;
          tag_vdim : begin     //{vdim}
                if not vdim_calc then begin
                  DisString (StAtr.vdim, Destination, vdim_disstr, d_units);
                  vdim_calc := true;
                end;
                datastr := vdim_disstr
              end;
          tag_sdim : begin     //{sdim}
                if StAtr.vdim < StAtr.hdim then begin
                  if not vdim_calc then begin
                    DisString (StAtr.vdim, Destination, vdim_disstr, d_units);
                    vdim_calc := true;
                  end;
                  datastr := vdim_disstr
                end
                else begin
                  if not hdim_calc then begin
                    DisString (StAtr.hdim, Destination, hdim_disstr, d_units);
                    hdim_calc := true;
                  end;
                  datastr := hdim_disstr;
                end;
              end;
           tag_ldim : begin     //{ldim}
                if StAtr.vdim > StAtr.hdim then begin
                  if not vdim_calc then begin
                    DisString (StAtr.vdim, Destination, vdim_disstr, d_units);
                    vdim_calc := true;
                  end;
                  datastr := vdim_disstr;
                end
                else begin
                  if not hdim_calc then begin
                    DisString (StAtr.hdim, Destination, hdim_disstr, d_units);
                    hdim_calc := true;
                  end;
                  datastr := hdim_disstr;
                end;
              end;
          tag_perim : begin     //{perim}
                if not perim_calc then begin
                  DisString (StAtr.OutlinePerim + StAtr.VoidPerim,
                             Destination, perim_disstr, d_units);
                  perim_calc := true;
                end;
                datastr := perim_disstr;
              end;

          tag_operim : begin     //{operim}
                if not operim_calc then begin
                  DisString (StAtr.OutlinePerim, Destination, operim_disstr, d_units);
                  operim_calc := true;
                end;
                datastr := operim_disstr;
              end;

          tag_vperim : begin     //{vperim}
                if not vperim_calc then begin
                  DisString (StAtr.VoidPerim, Destination, vperim_disstr, d_units);
                  vperim_calc := true;
                end;
                datastr := vperim_disstr;
              end;

          tag_dunit : begin   //{dunit}
                if not (perim_calc or operim_calc or vperim_calc or hdim_calc or vdim_calc) then begin
                  DisString (StAtr.OutlinePerim + StAtr.VoidPerim,
                             Destination, perim_disstr, d_units);
                  perim_calc := true;
                end;
                datastr := d_units;
              end;
          tag_area : begin     //{area}
                if not area_calc then begin
                  AreaString (StAtr.area, Flr, Destination, area_str, area_units);
                  area_calc := true;
                end;
                datastr := area_str;
              end;

          tag_oarea : begin     //{oarea}
                if not oarea_calc then begin
                  AreaString (StAtr.GrossArea, Flr, Destination, oarea_str, area_units);
                  oarea_calc := true;
                end;
                datastr := oarea_str;
              end;

          tag_varea : begin     //{varea}
                if not varea_calc then begin
                  AreaString ((StAtr.GrossArea - StAtr.area), Flr, Destination, varea_str, area_units);
                  varea_calc := true;
                end;
                datastr := varea_str;
              end;

          tag_aunit : begin     //{aunit}
                if not area_calc then begin
                  AreaString (StAtr.area, Flr, Destination, area_str, area_units);
                  area_calc := true;
                end;
                datastr := area_units;
              end;

          tag_parea : begin     //{parea}
                if not parea_calc then begin
                  AreaString (StAtr.OutlinePerimArea + StAtr.VoidPerimArea,
                              Wall, Destination, parea_str, parea_units);
                  parea_calc := true;
                end;
                datastr := parea_str;
              end;


          tag_oparea: begin     //{oparea}
                if not oparea_calc then begin
                  AreaString (StAtr.OutlinePerimArea,
                              Wall, Destination, oparea_str, parea_units);
                  oparea_calc := true;
                end;
                datastr := oparea_str;
              end;

          tag_vparea: begin     //{vparea}
                if not vparea_calc then begin
                  AreaString (StAtr.VoidPerimArea,
                              Wall, Destination, vparea_str, parea_units);
                  vparea_calc := true;
                end;
                datastr := vparea_str;
              end;

          tag_aaunit: begin     //{aaunit}
                datastr := string(l^.AreaSettings[AltAreaRec, UNITS_LBL].CustUnitDisplay);
              end;

          tag_vol: begin     //{vol}
                if not vol_calc then begin
                  VolString (StAtr.volume, Destination, vol_str, vol_units);
                  vol_calc := true;
                end;
                datastr := vol_str;
              end;
          tag_vunit: begin     //{vunit}
                if not vol_calc then begin
                  VolString (StAtr.volume, Destination, vol_str, vol_units);
                  vol_calc := true;
                end;
                datastr := vol_units;
              end;
          tag_rareapct:  //'{rarea%}
              try
                datastr := floattostrf (StAtr.area/RptTotArea*100, ffFixed, 3, 2);
              except
                datastr := '%%%';
              end;
          tag_dareapct:  //'{darea%}
              try
                datastr := floattostrf (StAtr.area/DwgTotArea*100, ffFixed, 3, 2);
              except
                datastr := '%%%';
              end;
          tag_lyr: begin     //{lyr}
                datastr := LyrName;
              end;
          tag_cat: begin     //{cat}
                datastr := string(StAtr.Category);
              end;
          tag_grp: begin      //{grp}
                if blankstring(StAtr.Category) then
                  datastr := LyrName
                else
                  datastr := string(StAtr.Category);
              end;
          tag_flr: begin      //{fllvl}
                  DisString (floorz, Destination, datastr, d_units);
              end;
          tag_ceiling: begin  //{ceilng}
                  DisString (ceilingz - floorz, Destination, datastr, d_units);
              end;
          tag_count: begin
                 datastr := inttostr (count);
              end

          else
              datastr := '???';
        end;
        if ConvertUpper then
          DataStr := DataStr.ToUpper;
        if pos > length(result) then
          result := result + datastr
        else
          insert (datastr, result, pos);
      end;
    until pos < 1;
  end;

  if DoCustomTags then begin
    //check for custom tags
    pos := result.IndexOf ('{');
    while pos >= 0 do begin
      pos1 := result.IndexOf('}', pos+1);
      if pos1 > 0 then begin
        tempstr := result.Substring(pos+1, pos1-pos-1);
        delete (result, pos+1, pos1-pos+1);
        if atr_entfind (l^.ent, 'SpdhCF'+shortstring(tempstr), atr) then begin
          datastr := '';
          case atr.atrtype of
            atr_str : custstr := atr.str;
            atr_dis : begin //cvdisst (atr.dis, custstr);
                        DisString (atr.dis, Destination, datastr, d_units);
                        custstr := shortstring (datastr);
                      end;
            atr_int : begin
                        datastr := atr.int.ToString;
                        custstr := shortstring (datastr);
                      end;
            atr_rl  : begin
                        datastr := atr.rl.ToString;
                        custstr := shortstring (datastr);
                      end;
            atr_ang : custstr := AngString (atr.ang);
            atr_Area: begin
                        AreaString (atr.rl, CustArea, Destination, datastr, carea_units);
                        custstr := shortstring (datastr);
                      end;
          end;
          if length(result) < pos then
            result := result + string(custstr)
          else
            insert (string(custstr), result, pos+1);
        end
        else if atr_sysfind ('SpdhUF'+atrname(tempstr), atr) then begin
              UserField := DecodeUserFieldAtr (atr);
              case UserField.numeric of
                int : begin
                        datastr := UserField.defaultint.ToString;
                        custstr := shortstring (datastr);
                      end;
                rl  : begin
                        datastr := UserField.defaultrl.ToString;
                        custstr := shortstring (datastr);
                      end;
                dis : begin
                        DisString (UserField.defaultdis, Destination, datastr, d_units);
                        custstr := shortstring (datastr);
                      end;
                ang : begin
                        custstr := AngString (UserField.defaultang);
                      end;
                UserFieldType.area : begin
                        AreaString (UserField.defaultarea, CustArea, Destination, datastr, d_units);
                        custstr := shortstring (datastr);
                      end
                else custstr := UserField.defaultstr;
              end;
              if length(result) < pos then
                result := result + string(custstr)
              else
                insert (string(custstr), result, pos+1);
        end;
      end;
      pos := result.IndexOf ('{', pos1);
    end;
  end;  // if DoCustomTags

  if UpdateParam then
    str := shortstring (result);
END;


FUNCTION SubstituteTags (var str : shortstring; CONST StAtr : StandardAtr; Destination : integer;
                         DoCustomTags, UpdateParam : boolean) : string;
VAR
  LyrName : shortstring;
BEGIN
  if l^.ent.enttype <> entpln then begin
    lpMsg_Error (126, []);
    exit;
  end;
  getLongLayerName (l^.ent.lyr, LyrName);
  result := SubstituteTags (str, StAtr, string(LyrName), l^.ent.plbase, l^.ent.plnhite, 0, 0, 0,
                            Destination, DoCustomTags, UpdateParam);
END;



PROCEDURE DrawLabel;
VAR
  adr : lgl_addr;
  tempent : entity;
BEGIN
  adr := ent_first(l^.lblmode);
  while ent_get (tempent, adr) do begin
    adr := ent_next (tempent, l^.lblmode);
    drawIfOn (tempent, drmode_white);
  end;
END;

PROCEDURE GroupLabel;
// groups polylines into same group as label entities
VAR
  pln_ent, lbl_ent : entity;
BEGIN
  if ent_get (lbl_ent, ent_first(l^.lblmode)) and ent_get (pln_ent, l^.ent.addr) and
     addr_equal (lbl_ent.lyr, pln_ent.lyr)  //do not regroup if on different layers
  then begin
    ent_relink (lbl_ent, pln_ent);
    ent_relink (pln_ent, lbl_ent);  //this is necessary to make sure the label is in front of the polyline (important if the label has knockout turned on)
  end;
  if ent_get (lbl_ent, ent_first(l^.lblmode)) and ent_get (pln_ent, l^.ent2.addr) and
     addr_equal (lbl_ent.lyr, pln_ent.lyr)  //do not regroup if on different layers
  then begin
    ent_relink (lbl_ent, pln_ent);
    ent_relink (pln_ent, lbl_ent);
  end;
END;

PROCEDURE RotateLabel (ang : double);
VAR
  adr, adr1 : lgl_addr;
  tempent : entity;
  pv : polyvert;
  mat : modmat;
BEGIN
  adr := ent_first(l^.lblmode);
  while ent_get (tempent, adr) do begin
    adr := ent_next (tempent, l^.lblmode);
    if tempent.enttype = entpln then begin
      adr1 := tempent.plnfrst;
      setrotrel (mat, ang, z, l^.lblcntr);
      while polyvert_get (pv, adr1, tempent.plnfrst) do begin
        adr1 := pv.Next;
        xformpt (pv.pnt, mat, pv.pnt);
        polyvert_update (pv);
      end;
    end
    else
      ent_rotate (tempent, l^.lblcntr, ang);
    ent_update (tempent);
  end;
END;

PROCEDURE PlaceLabel (pntfrom, pntto : point; Autoplacing : boolean);
VAR
  dis2move : point;
  adr : lgl_addr;
  tempent : entity;
  LblFits : boolean;
BEGIN
  subpnt (pntto, pntfrom, dis2move);
  adr := ent_first(l^.lblmode);
  LblFits := true;
  while ent_get (tempent, adr) do begin
    adr := ent_next (tempent, l^.lblmode);
    ent_move (tempent, dis2move.x, dis2move.y, 0.0);
    if AutoPlacing and LblFits then
      LblFits := EntFitsInPline(tempent, l^.ent);
    ent_update (tempent);
  end;

  if AutoPlacing and not LblFits then begin
    LblFits := true;
    adr := ent_first(l^.lblmode);
    while ent_get (tempent, adr) do begin
      adr := ent_next (tempent, l^.lblmode);
      if LblFits then
        LblFits := RotatedEntFitsInPline (tempent, l^.ent, pntto, halfpi);
    end;
    if LblFits then begin
      adr := ent_first(l^.lblmode);
      while ent_get (tempent, adr) do begin
        adr := ent_next (tempent, l^.lblmode);
        ent_rotate (tempent, pntto, halfpi);
        ent_update (tempent);
      end;
    end
    else begin
      adr := ent_first(l^.lblmode);
      while ent_get (tempent, adr) do begin
        adr := ent_next (tempent, l^.lblmode);
      end;
    end;
  end;
END;


PROCEDURE CreateText (var lblent : entity; existing : boolean;
                      var OldFractionEnts, NewFractionEnts : TList<Entity>;
                      mas : StandardAtrStr;
                      entityNum : integer;
                      out botleft, topright : point);     Overload;
CONST
  attribname = atrName('SpdhLblText');
  AttribVersion = 1;

TYPE
  str236 = string[236];
  atrstrdef = record
    case byte of
       0: (wholestr: str255);
       1: ({000} Len : byte;
           {002} Alignment: char;  // 'C'=centred, 'R'=right, anything else=left
           {004} LblWidth : double;  // necessary for labels containing fractions that are not
                                     // left aligned. Stores the overall width of all text entities
                                     // that make up the string so that a refresh can realign it.
           {012} pattern : str150;
           {164} txtsiz : double;
           {172} spaceBelow : double;
           {180} filler : string[73];
           {254} version : word);   // in case I change this definition in a future release
  end;
VAR
  s : string;
  i, svClr, svWidth : integer;
  nextent, prevent : entity;
  p1, p2, p3, p4 : point;
  lblents : TList<Entity>;
  textmovdis, textentwidth : double;
  thisAlignment : AlignmentType;
  atr : attrib;
  pAtrstr : ^atrstrdef;
  txtang : double;
  origtxtpnt : point;
  mytxtsiz : double;
  mat : modmat;

BEGIN
  lblents:= TList<Entity>.Create;

  if existing then begin
    if not (ent_get (lblent, lblent.addr) and
            atr_entfind (lblent, attribname, atr) and
            (atr.atrtype = atr_str255))
    then begin
      beep;
      exit;
    end;
    pAtrStr := addr(atr.shstr);
    case pAtrStr^.version of
      1 : mytxtsiz := pAtrStr^.txtsiz;  // don't get text size from entity as the label line may contain only stacked fractions
      else mytxtsiz := lblEnt.txtsiz;   // there was a beta version that did not include txtsiz in the attribute, so just in case we encounter a label created with that version
    end;

    txtAng := lblent.txtang;
    origtxtpnt := lblent.txtpnt;
    if txtAng <> 0 then
      ent_rotate (lblent, origtxtpnt, -txtAng);      // rotate label temporarily so that my calcs on postion etc only need to cater for horizontal text

    case pAtrStr^.Alignment of
      'C' : begin
              thisAlignment := center;
              if lblent.txttype.justification = TEXT_JUST_LEFT then begin
                lblent.txtpnt.x := lblent.txtpnt.x + pAtrStr^.LblWidth/2; // set txtpnt to center of current lbl entities
              end;
            end;
      'R' : begin
              thisAlignment := right;
              if lblent.txttype.justification = TEXT_JUST_LEFT then begin
                lblent.txtpnt.x := lblent.txtpnt.x + pAtrStr^.LblWidth; // set txtpnt to right of current lbl entities
              end;
            end
      else thisAlignment := left;
    end;

    lblent.txtstr := pAtrStr^.pattern;

    s := SubstituteTags (lblent.txtstr, mas.fields, UNITS_LBL, true, true);
    i := ansipos (char(27), s);
    if i > 0 then    // escape character signifying stacked fraction - need to create multiple text entitiestxtstr, i-1);
      setlength (lblent.txtstr, i-1);
    if length(lblent.txtstr) = 0 then
      lblent.txtstr := ' ';
    ent_update (lblent);
    lblents.Add (lblent);
  end
  else begin
    txtAng := 0;
    mytxtsiz := LblEnt.txtsiz;
    if l^.LabelFromSymbol then
      case lblent.txttype.justification of
        TEXT_JUST_CENTER : thisAlignment := center;
        TEXT_JUST_RIGHT : thisAlignment := right
        else thisAlignment := left;
      end
    else
      thisAlignment := l^.LabelTextAlignment;

    atr_init (atr, atr_str255);
    pAtrStr := addr(atr.shstr);
    case thisAlignment of
      center : pAtrStr^.Alignment := 'C';
      right  : pAtrStr^.Alignment := 'R'
      else pAtrStr^.Alignment := 'L';
    end;
    pAtrStr^.pattern := str150(lblent.txtstr);  // lblent currently contains text pattern
    pAtrStr^.txtsiz := mytxtsiz;
    pAtrStr^.version := AttribVersion;
    pAtrStr^.Len := 255;
    atr.name := attribname;

    s := SubstituteTags (lblent.txtstr, mas.fields, UNITS_LBL, true, true);
    i := ansipos (char(27), s);
    if i > 0 then    // escape character signifying stacked fraction - need to create multiple text entitiestxtstr, i-1);
      setlength (lblent.txtstr, i-1);
    if length(lblent.txtstr) = 0 then
      lblent.txtstr := ' ';
    svClr := lblent.color;
    svWidth := lblent.Width;
    ent_add (lblent);
    lblent.color := SvClr;  // this (and following ent_update) are necessary as ent_add changes it to current layer colour
    lblent.Width := svWidth;
    ent_update (lblent);
    lblents.Add (lblent);
    atr_add2ent (lblent, atr);    // add atr with alignment and pattern so that we can easily update text if space is changed

    atr_init (atr, atr_int);       // add tag attribute to all associated text
    atr.name := mas.fields.tag;    // entities.
    atr.int := entityNum;
    atr_add2ent (lblent, atr);

  end;


  // create multiple entities as required for stacked fractions
  while i > 0 do begin
    delete (s, 1, i);
    while (length (s) > 0) and (CharInSet (s[1], [char(27), ' '])) do delete (s, 1, 1);
    i := ansipos ('/', s);
    if i <= 1 then exit;   //should never happen
    if not (existing and (OldFractionEnts.Count > 0) and ent_get (nextent, OldFractionEnts[0].addr)) then
      ent_copy (lblent, nextent, false, false);
    nextent.txtsiz := mytxtsiz *0.6;
    nextent.txtang := 0;
    nextent.txtstr := shortstring (copy(s, 1, i-1));
    delete (s, 1, i);
    txtbox (lblent, p1, p2, p3, p4);
    nextent.txtpnt.x := p2.x + (p4.x-p2.x)*0.467;
    nextent.txtpnt.y := p2.y + (p4.y-p2.y)*0.467;
    if isnil (nextent.addr) then
      ent_add (nextent)
    else
      ent_update (nextEnt);
    if existing and (OldFractionEnts.Count > 0) then begin
      OldFractionEnts.Delete(0);
    end
    else begin
      atr_init (atr, atr_int);       // add tag attribute to all associated text
      atr.name := mas.fields.tag;    // entities.
      atr.int := -entityNum;         // attr value is negative for all but first entity in each pattern
      atr_add2ent (nextent, atr);
    end;
    lblents.Add (nextent);
    if existing then begin
      NewFractionEnts.Add(nextent);
      ent_relink (lblent, nextent);
    end;

    prevent := nextent;

    if not (existing and (OldFractionEnts.Count > 0) and ent_get (nextent, OldFractionEnts[0].addr)) then
      ent_copy (lblent, nextent, false, false);
    nextent.txtang := 0;
    nextent.txtsiz := mytxtsiz;
    nextent.txtstr := '/';
    txtbox (prevent, p1,p2,p3,p4);
    nextent.txtpnt.y := lblent.txtpnt.y;
    nextent.txtpnt.x := prevent.txtpnt.x;
    if (p2.x-p1.x) > 0.4*lblent.txtsiz then
      nextent.txtpnt.x := nextent.txtpnt.x + (p2.x-p1.x) - 0.4*lblent.txtsiz;
    if isnil (nextent.addr) then
      ent_add (nextent)
    else
      ent_update (nextEnt);
    if existing and (OldFractionEnts.Count > 0) then begin
      OldFractionEnts.Delete(0);
    end
    else begin
      atr_init (atr, atr_int);       // add tag attribute to all associated text
      atr.name := mas.fields.tag;    // entities.
      atr.int := -entityNum;         // attr value is negative for all but first entity in each pattern
      atr_add2ent (nextent, atr);
    end;
    lblents.Add(nextent);
    if existing then begin
      NewFractionEnts.Add(nextent);
      ent_relink (lblent, nextent);
    end;

    prevent := nextent;

    if not (existing and (OldFractionEnts.Count > 0) and ent_get (nextent, OldFractionEnts[0].addr)) then
      ent_copy (lblent, nextent, false, false);
    nextent.txtsiz := mytxtsiz*0.6;
    nextent.txtang := 0;
    nextent.txtstr := '';
    while (length(s) > 0) and CharInSet (s[1], ['0'..'9']) do begin
      nextent.txtstr := nextent.txtstr + shortstring(s[1]);
      delete (s, 1, 1);
    end;
    nextent.txtpnt.y := lblent.txtpnt.y - 0.223*lblent.txtsiz;
    nextent.txtpnt.x := prevent.txtpnt.x + 0.4174*lblent.txtsiz;
    if isnil (nextent.addr) then
      ent_add (nextent)
    else
      ent_update (nextEnt);
    if existing and (OldFractionEnts.Count > 0) then begin
      OldFractionEnts.Delete(0);
    end
    else begin
      atr_init (atr, atr_int);       // add tag attribute to all associated text
      atr.name := mas.fields.tag;    // entities.
      atr.int := -entityNum;         // attr value is negative for all but first entity in each pattern
      atr_add2ent (nextent, atr);
    end;
    lblents.Add(nextent);
    if existing then begin
      NewFractionEnts.Add(nextent);
      ent_relink (lblent, nextent);
    end;

    prevent := nextent;

    if length(s) > 0 then begin
      if not (existing and (OldFractionEnts.Count > 0) and ent_get (nextent, OldFractionEnts[0].addr)) then
        ent_copy (lblent, nextent, false, false);
      nextent.txtang := 0;
      nextent.txtsiz := mytxtsiz;
      nextent.txtstr := shortstring(s);
      i := ansipos (char(27), s);
      if i > 0 then begin
        SetLength(nextent.txtstr, i-1);
      end;
      nextent.txtpnt.y := lblent.txtpnt.y;
      txtbox (prevent, p1, p2, p3, p4);
      nextent.txtpnt.x := p2.x;
      if isnil (nextent.addr) then
        ent_add (nextent)
      else
        ent_update (nextEnt);
      if existing and (OldFractionEnts.Count > 0) then begin
        OldFractionEnts.Delete(0);
      end
      else begin
        atr_init (atr, atr_int);       // add tag attribute to all associated text
        atr.name := mas.fields.tag;    // entities.
        atr.int := -entityNum;         // attr value is negative for all but first entity in each pattern
        atr_add2ent (nextent, atr);
      end;
      lblents.Add(nextent);
      if existing then begin
        NewFractionEnts.Add(nextent);
        ent_relink (lblent, nextent);
      end;

      lblent := nextent;
    end;
  end;

  if lblents.Count = 1 then begin
    ent_get (lblent, lblents[0].addr);

    if not l^.LabelFromSymbol then begin
      lblent.txttype.justification := ord (thisAlignment);
      ent_update (lblent);
    end;
    ent_extent (lblent, botleft, topright);
  end
  else if lblents.count > 1 then begin
    // alignment gets tricky with stacked fractions.
    // left align each entity, and then move the whole group to the required aligned position
    for i := 0 to lblents.Count-1 do begin
      lblent := lblents[i];
      ent_extent (lblent, p1, p2);
      if i = 0 then begin
        p3 := p1;
        p4 := p2;
      end
      else begin
        p3.x := min (p3.x, p1.x);
        p4.x := max (p4.x, p2.x);
      end;
    end;
    textentwidth := abs (p4.x - p3.x);
    if thisAlignment = center then
      textmovdis := textentwidth/2
    else if thisAlignment = right then
      textmovdis := textentwidth
    else
      textmovdis := 0;
    botleft := p3;
    topright := p4;
    botleft.x := botleft.x - textmovdis;
    topright.x := topright.x - textmovdis;

    for i := 0 to lblents.Count-1 do begin
      ent_get (lblent, lblents[i].addr);
      lblent.txtpnt.x := lblent.txtpnt.x - textmovdis;
      ent_update (lblent);
    end;
  end;

  if ent_get (lblent, lblents[0].addr) and atr_entfind (lblent, attribname, atr) then begin
    pAtrstr := addr (atr.shstr);
    pAtrstr^.LblWidth := abs(topright.x - botleft.x);
    atr_update (atr);
  end;

  if txtAng <> 0 then begin
    for i := 0 to lblents.Count-1 do begin
      ent_get (lblent, lblents[i].addr);
      ent_rotate (lblent, origtxtpnt, txtAng);
      ent_update (lblent);
    end;
    setrotrel (mat, txtAng, z, origtxtpnt);
    xformpt (topright, mat, topright);
    xformpt (botleft, mat, botleft);
  end;

  lblents.Free;
END;  //CreateText

PROCEDURE CreateText (var lblent : entity;
                      mas : StandardAtrStr;
                      entityNum : integer;
                      var botleft, topright : point);     Overload;
VAR
  Temp, Temp2 : TList<Entity>;
BEGIN
  Temp := TList<Entity>.Create;
  try
    Temp2 := TList<Entity>.Create;
    try
      CreateText (lblent, false, Temp, Temp, mas, entityNum, botleft, topright);
    finally
      Temp2.Free;
    end;
  finally
    Temp.Free;
  end;
END;

FUNCTION CreateLabel (var ent : entity; mas : StandardAtrStr) : integer;
// returns number of label entities created
TYPE
  DblArr = array[1..4] of double;
VAR
  i : integer;
  s : string;
  sym : sym_type;
  lblent, borderent : entity;
  pv : polyvert;
  dfltclr : integer;
  ttf : boolean;
  pos, thispos : double;
  atr : attrib;
  lowleft, upright, lowleft1, upright1 : point;
  area : double;
  temppnt, lblcntr : point;
  entityNum : integer;
  adr : lgl_addr;
  txtstr : str255;
  symname : symstr;
  pDblArr : ^DblArr;
  FileDateTime : TDateTime;
  dflttxtsize : double;
BEGIN
  stopgroup;
  entityNum := 0;
  if l^.LabelFromSymbol then begin      // label comes from symbol
    s := string (GetSvdStr ('SpdhLbSymbol', ''));
    if not FileAge (s, FileDateTime, true) then
      FileDateTime := Now;
    symname := '';
    AppendDateTimeStamp (symname, FileDateTime, true, true, false, false, false);
    if length (s) > 4 then begin
      delete (s, length(s)-3, 4);   // remove .dsf extension
      symname := symstr(s) + symname;
      if symread (symstr(s), symname, sym) = fl_OK then begin
        adr := sym.frstent;
        while ent_get (lblent, adr) do begin
          if addr_equal(adr, sym.lastent) then
            setnil (adr)  // ensure loop stops at last entity in symbol
          else
            adr := lblent.Next;

          entityNum := entityNum+1;

          if lblent.enttype = enttxt then begin
            if entityNum = 1 then
              CreateText(lblent, mas, entityNum, lowleft, upright)
            else begin
              CreateText(lblent, mas, entityNum, lowleft1, upright1)
            end;
          end
          else begin
            i := lblent.color;
            ent_add(lblent);
            lblent.color := i;
            ent_update(lblent);
            if entityNum = 1 then
              ent_extent (lblent, lowleft, upright)
            else
              ent_extent (lblent, lowleft1, upright1);
            atr_init (atr, atr_int);
            atr.name := mas.fields.tag;
            atr.int := entityNum;
            atr_add2ent (lblent, atr);
          end;
          if entityNum > 1 then begin
            lowleft.x := min (lowleft.x, lowleft1.x);
            lowleft.y := min (lowleft.y, lowleft1.y);
            upright.x := max (upright.x, upright1.x);
            upright.y := max (upright.y, upright1.y);
          end;
        end;
      end
      else begin
        MessageDlg ('Error opening symbol file' + sLineBreak + sLineBreak +
                    'Go to settings and check symbol file name is correct',
                    mtError, [mbOK], 0);
      end;
    end;
  end
  else begin
    pos := 0;
    CheckTxtScale;
    dflttxtsize := DefaultTxtSize;

    for i := 1 to 5 do begin
      thispos := pos;
      if i < 5 then begin  // set pos to the required y value for the next line
        pos :=  pos - (1 + GetSvdRl ('SpdhSpBlw'+inttostr(i), 0.25)) *
                ActualTextSize (GetSvdRl('SpdhSl' + inttostr(i+1) + 'Siz', dflttxtsize));
      end;
      txtstr := GetSvdStr ('SpdhLine' + inttostr(i) + 'Ptn', '!@#$%');
      if txtstr = '!@#$%' then    // try looking for an old Space Planner line format
        txtstr := GetSvdStr ('Line' + inttostr(i) + 'Ptn', defaultPtn[i]);
      if length (txtstr) > 0 then begin
        ent_init (lblent, enttxt);
        if not l^.LabelFromSymbol then begin
          if l^.Knockout = KOentity then
            lblent.KnockOut := 1
          else if l^.Knockout = KOentityBorder then
            lblent.KnockOut := 2
          else
            lblent.KnockOut := 0;
        end;
        lblent.txtstr := txtstr;
        lblent.txtpnt.x := 0.0;
        lblent.txtpnt.y := thispos;
        lblent.txtsiz := ActualTextSize(GetSvdRl ('SpdhSl' + inttostr(i) + 'Siz', dflttxtsize));
        lblent.txtaspect := GetSvdRl ('SpdhSl' + inttostr(i) + 'Asp', 0.0);
        if RealEqual(lblent.txtaspect, 0.0) then  // try looking for a Space Planner setting
          lblent.txtaspect := GetSvdRl ('dhSPStl' + inttostr(i) + 'Asp', 1.0);
        lblent.txtslant := getsvdRl ('SpdhSl' + inttostr(i) + 'Sln', 999.0);
        if lblent.txtslant = 999.0 then   // try looking for a Space Planner setting'
          lblent.txtslant := getsvdRl ('dhSPStl' + inttostr(i) + 'Sln', 0.0);
        lblent.Width := byte (getsvdInt ('SpdhSl' + inttostr(i) + 'Wgt', 0));
        if lblent.Width = 0 then   // try looking for a Space Planner setting
          lblent.Width := byte (getsvdInt ('dhSPStl' + inttostr(i) + 'Wgt', 1));
        dfltclr := lblent.color; // should have been set to current colour by ent_init
        lblent.color := GetSvdInt ('SpdhSl' + inttostr(i) + 'Clr', 0);
        if lblent.color = 0 then  // try looking for a Space Planner setting
          lblent.color := GetSvdInt ('dhSPStl' + inttostr(i) + 'Clr', dfltclr);
        s := string(GetSvdStr ('SpdhSl' + inttostr(i) + 'Font', ''));
        ttf := s.EndsWith('(TTF)');
        if ttf then begin
          SetLength(s, Length(s)-6);
          lblent.txtdisplayttf.dooutline := true;
          lblent.txtdisplayttf.dofill := true;
          lblent.txtdisplayttf.OutlineClr := lblent.color;
          lblent.txtdisplayttf.FillClr := lblent.color;
          lblent.TxtWinfont := font_ttf;
        end
        else begin
          lblent.TxtWinfont := font_shx
        end;
        lblent.txtfon := fontstr(s);
        entityNum := entityNum+1;
        if entityNum = 1 then
          CreateText(lblent, mas, entityNum, lowleft, upright)
        else begin
          CreateText(lblent, mas, entityNum, lowleft1, upright1);
          lowleft.x := min (lowleft.x, lowleft1.x);
          upright.x := max (upright.x, upright1.x);
          lowleft.y := min (lowleft.y, lowleft1.y);
          upright.y := max (upright.y, upright1.y);
        end;
        if (not l^.LabelFromSymbol) and (l^.Knockout = KOentity) and (l^.KnockoutEnlX <> 0) or (l^.KnockoutEnlX <> 0) then begin
          atr_init (atr, atr_pnt);
          atr.name := 'KnockOut_ENL';
          atr.pnt.x := l^.KnockoutEnlX;
          atr.pnt.y := l^.KnockoutEnlY;
          atr.pnt.z := 1.0;
          atr_add2ent (lblent, atr);
        end;
      end;
    end;
  end;

  if (l^.Knockout in [KObox, KOboxBorder]) and not l^.LabelFromSymbol then begin
    ent_init (borderent, entpln);
    borderent.plnclose := true;
    polyvert_init (pv);
    pv.pnt.x := lowleft.x - l^.KnockoutMargin;
    pv.pnt.y := lowleft.y - l^.KnockoutMargin;
    pv.pnt.z := lblent.txtpnt.z;
    polyvert_add (pv, borderent.plnfrst, borderent.plnlast);
    polyvert_init (pv);
    pv.pnt.x := upright.x + l^.KnockoutMargin;
    pv.pnt.y := lowleft.y - l^.KnockoutMargin;
    pv.pnt.z := lblent.txtpnt.z;
    polyvert_add (pv, borderent.plnfrst, borderent.plnlast);
    polyvert_init (pv);
    pv.pnt.x := upright.x + l^.KnockoutMargin;
    pv.pnt.y := upright.y + l^.KnockoutMargin;
    pv.pnt.z := lblent.txtpnt.z;
    polyvert_add (pv, borderent.plnfrst, borderent.plnlast);
    polyvert_init (pv);
    pv.pnt.x := lowleft.x - l^.KnockoutMargin;
    pv.pnt.y := upright.y + l^.KnockoutMargin;
    pv.pnt.z := lblent.txtpnt.z;
    polyvert_add (pv, borderent.plnfrst, borderent.plnlast);

    ent_add (borderent);
    borderent.color := l^.KnockoutClr;
    if l^.Knockout = KOboxBorder then
      borderent.KnockOut := 2
    else
      borderent.KnockOut := 1;
    ent_update (borderent);

    atr_init (atr, atr_str);
    atr.name := mas.fields.tag;
    atr.str[0] := char(80);
    pDblArr := addr (atr.str[1]);
    pDblArr^[1] := l^.KnockoutMargin;
    pDblArr^[2] := l^.KnockoutMargin;
    pDblArr^[3] := l^.KnockoutMargin;
    pDblArr^[4] := l^.KnockoutMargin;
    atr_add2ent (borderent, atr);
    // place label text in front of this border
    mode_init (l^.lblmode);
    mode_group (l^.lblmode, lblent);
    adr := ent_first (l^.lblmode);
    while ent_get (lblent, adr) do begin
      adr := ent_next (lblent, l^.lblmode);
      if not addr_equal(lblent.addr, borderent.addr) then begin
        ent_get (borderent, borderent.addr);
        ent_relink (borderent, lblent);
      end;
    end;
  end;
  stopgroup;

  if (entityNum > 0 )  and (not isnil (l^.ent.addr)) then begin
    pline_centroid (l^.ent.plnfrst, l^.ent.plnlast, area, temppnt, l^.lblcntr, true, true);
    meanpnt (upright, lowleft, lblcntr);


    // move label entities to centroid of space as a starting point.
    mode_init (l^.lblmode);
    mode_group (l^.lblmode, lblent);
    PlaceLabel (lblcntr, l^.lblcntr, l^.AutoPlace);

    l^.lblvertical := false;
    l^.lblrotate := false;
  end;

  result := entityNum;
END;   // CreateLabel

PROCEDURE RefreshLabel (plnent : entity; mas : StandardAtrStr);
type
  FoundLabels = record
    StepFound : integer;  // 1=group, 2=layer, 3=anywhere (any higher value=not found)
    Entities : TList<entity>;
    OldFractionEnts : TList<entity>; // Need to track fraction entities as there may be a
    NewFractionEnts : TList<entity>; // different number of enttities if dimensions change
  end;
var
  tag : atrName;
  newent : entity;
  atr, tempatr : attrib;
  adr :lgl_addr;
  found : boolean;
  mode : mode_type;
  ent : entity;
  lyrmatches, i, j : integer;
  area : double;
  frstmom, plnctr, oldplnctr : point;
  mink, maxk, meank,
  minj, maxj, meanj,
  minLbl, maxLbl : point;
  lblCntr, NewLblCntr : point;
  ReCenterDis : point;
  sym : symbol;
  symname : str255;
  NumLblEnts : integer;
  lblEnt : entity;
  LblEntities : array of FoundLabels;
  AllLblsFound : boolean;
  BorderEnt : entity;
  BorderStepFound : integer;
  BorderMargins : array[1..4] of double;
  disj,disk : double;
  groupFound : boolean;
  first : boolean;
  min_ent, max_ent, min_lbl, max_lbl : point;
  tempent : entity;
  doReCenter : boolean;
  pv : polyvert;
  borderAng : double;
  mat : modmat;
  leftshift, rightshift, topshift, bottomshift : double;
  debugClr : integer;
BEGIN
  debugClr := 1;
  tag := mas.fields.tag;   // atr with this name is used to link all the space entities ...
  atr_entfind (plnent, tag, atr);   // this is an integer atr storing the number of label entities
  if (atr.atrtype <> atr_int) or (atr.int = 0) then begin
    beep;
    exit;
  end;

  NumLblEnts := atr.int;

  // find plnent centroid - used in various bits of logic below
  pline_centroid (plnent.plnFrst, plnent.plnlast, area, frstmom, plnctr, true,true);
  oldplnctr.x := mas.fields.centroid_x;
  oldplnctr.y := mas.fields.centroid_y;
  oldplnctr.z := plnent.plbase;

  SetLength (LblEntities, NumLblEnts);
  for i := 0 to NumLblEnts-1 do begin
    LblEntities[i].StepFound := 9;  // high value = not found
    LblEntities[i].Entities := TList<Entity>.Create;
    LblEntities[i].OldFractionEnts := TList<Entity>.Create;
    LblEntities[i].NewFractionEnts := TList<Entity>.Create;
  end;
  BorderStepFound := 9;  // high value = not found
  setnil (BorderEnt.addr);

  //first look for label ents in same group as plnent
  mode_init (mode);
  mode_group (mode, plnent);
  mode_atr(mode, tag);
  adr := ent_first (mode);
  while ent_get (lblent, adr) do begin
    adr := ent_next (lblent, mode);
    if atr_entfind (lblent, tag, atr) and not atr_entfind (lblent, STANDARD_ATR_NAME, tempatr) then begin
      if atr.atrtype = atr_int then begin
        if (atr.int < 0) and (atr.int >= -NumLblEnts) then
          LblEntities[-atr.int-1].OldFractionEnts.add(lblEnt)  // stacked fraction entity
        else if (atr.int > 0) and (atr.int <= NumLblEnts) then begin
          LblEntities[atr.int-1].Entities.Add(lblEnt);
          LblEntities[atr.int-1].StepFound := 1;
        end;
      end
      else if (lblent.enttype = entpln) and (atr.atrtype = atr_str) and not Flag (mas.fields.Flags, FROM_SYMBOL) then begin
        if isnil (BorderEnt.addr) then begin
          ent_get (BorderEnt, lblent.addr);
          move (atr.str[1], BorderMargins, sizeof(BorderMargins));
          BorderStepFound := 1;
        end
        else begin
          // more than 1 matching entity - choose one that is closest to pln cntr
          ent_extent (BorderEnt, minj, maxj);
          meanpnt (minj, maxj, meanj);
          ent_extent (lblent, mink, maxk);
          meanpnt (mink, maxk, meank);
          if UInterfaces.distance (plnctr, meank) < UInterfaces.distance (plnctr, meanj) then begin
            ent_get (BorderEnt, lblent.addr);
            move (atr.str[1], BorderMargins, sizeof (BorderMargins));
          end;
        end;
      end;
    end;
  end;
  AllLblsFound := true;
  for i := 0 to NumLblEnts-1 do
    if LblEntities[i].Entities.Count < 1 then AllLblsFound := false;
  if AllLblsFound and Flag (mas.fields.Flags, HAS_KNOCKOUT_PLN) and isnil (borderent.addr) then
    AllLblsFound := false;

  if not AllLblsFound then begin    // widen search to current layer
    mode_init (mode);
    mode_1lyr (mode, plnent.lyr);
    mode_atr(mode, tag);
    adr := ent_first (mode);
    while ent_get (lblent, adr) do begin
      adr := ent_next (lblent, mode);
      if atr_entfind (lblent, tag, atr) and not atr_entfind (lblent, STANDARD_ATR_NAME, tempatr) then begin
        if atr.atrtype = atr_int then begin
          if (atr.int < 0) and (atr.int >= -NumLblEnts) and (LblEntities[-atr.int-1].StepFound > 1) then
            LblEntities[-atr.int-1].OldFractionEnts.add(lblEnt)  // stacked fraction entity
          else if (atr.int > 0) and (atr.int <= NumLblEnts) and (LblEntities[atr.int-1].StepFound > 1) then begin
            LblEntities[atr.int-1].Entities.Add(lblEnt);
            LblEntities[atr.int-1].StepFound := 2;
          end;
        end
        else if (BorderStepFound > 1) and (lblent.enttype = entpln) and (atr.atrtype = atr_str) then begin
          if isnil (BorderEnt.addr) then begin
            ent_get (BorderEnt, lblent.addr);
            move (atr.str[1], BorderMargins, sizeof(BorderMargins));
            BorderStepFound := 2;
          end
          else begin
            // more than 1 matching entity - choose one that is closest to pln cntr
            ent_extent (BorderEnt, minj, maxj);
            meanpnt (minj, maxj, meanj);
            ent_extent (lblent, mink, maxk);
            meanpnt (mink, maxk, meank);
            if UInterfaces.distance (plnctr, meank) < UInterfaces.distance (plnctr, meanj) then begin
              ent_get (BorderEnt, lblent.addr);
              move (atr.str[1], BorderMargins, sizeof(BorderMargins));
            end;
          end;
        end;
      end;
    end;
    AllLblsFound := true;
    for i := 0 to NumLblEnts-1 do
      if LblEntities[i].Entities.Count < 1 then AllLblsFound := false;
    if AllLblsFound and Flag (mas.fields.Flags, HAS_KNOCKOUT_PLN) and isnil (borderent.addr) then
      AllLblsFound := false;
  end;

  if not AllLblsFound then begin    // widen search to all layers
    mode_init (mode);
    mode_lyr (mode, lyr_all);
    mode_atr(mode, tag);
    adr := ent_first (mode);
    while ent_get (lblent, adr) do begin
      adr := ent_next (lblent, mode);
      if atr_entfind (lblent, tag, atr) and not atr_entfind (lblent, STANDARD_ATR_NAME, tempatr) then begin
        if atr.atrtype = atr_int then begin
          if (atr.int < 0) and (atr.int >= -NumLblEnts)  and (LblEntities[-atr.int-1].StepFound > 2)then
            LblEntities[-atr.int-1].OldFractionEnts.add(lblEnt)  // stacked fraction entity
          else //if atr_entfind (lblent, tag, atr) then
            if (atr.int > 0) and (atr.int <= NumLblEnts) and (LblEntities[atr.int-1].StepFound > 2) then begin
              LblEntities[atr.int-1].Entities.Add(lblEnt);
              LblEntities[atr.int-1].StepFound := 3;
            end;
        end
        else if (BorderStepFound > 2) and (lblent.enttype = entpln) and (atr.atrtype = atr_str) then begin
          if isnil (BorderEnt.addr) then begin
            ent_get (BorderEnt, lblent.addr);
            move (atr.str[1], BorderMargins, sizeof(BorderMargins));
            BorderStepFound := 3;
          end
          else begin
            // more than 1 matching entity - choose one that is closest to pln cntr
            ent_extent (BorderEnt, minj, maxj);
            meanpnt (minj, maxj, meanj);
            ent_extent (lblent, mink, maxk);
            meanpnt (mink, maxk, meank);
            if UInterfaces.distance (plnctr, meank) < UInterfaces.distance (plnctr, meanj) then begin
              ent_get (BorderEnt, lblent.addr);
              move (atr.str[1], BorderMargins, sizeof(BorderMargins));
            end;
          end;
        end;
      end;
    end;
  end;

  // make sure we only have 1 of each label entity
  // also get label extents in the same loop if required for re-centering logic
  first := true;
  disj := 0;     // initialise disj, disk to stop compiler warning
  disk := 0;
  for i := 0 to NumLblEnts-1 do with LblEntities[i] do
    if Entities.Count > 1 then begin  // multiple ents found, remove those that are further from plnent
      for j := Entities.Count-2 downto 0 do begin
        lblEnt := Entities[j];
        ent_extent (lblEnt, minj, maxj);
        meanpnt (minj, maxj, meanj);
        lblEnt := Entities[j+1];
        ent_extent (lblEnt, mink, maxk);
        meanpnt (mink, maxk, meank);
        disj := UInterfaces.distance (plnctr, meanj);
        disk := UInterfaces.distance (plnctr, meank);
        if disk < disj then
          Entities.Delete(j)
        else
          Entities.Delete(j+1);
      end;
      if (Entities[0].enttype <> entpln) and
         (l^.ReCenterLabel or Flag (mas.fields.Flags, HAS_KNOCKOUT_PLN)) then begin     // update label extents
        if (disk < disj) then begin
          if first then begin
            MinLbl := mink;
            MaxLbl := maxk;
          end
          else begin
            MinLbl.x := min (mink.x, MinLbl.x);
            MinLbl.y := min (mink.y, MinLbl.y);
            MinLbl.z := min (mink.z, MinLbl.z);
            MaxLbl.x := max (maxk.x, MaxLbl.x);
            MaxLbl.y := max (maxk.y, MaxLbl.y);
            MaxLbl.z := max (maxk.z, MaxLbl.z);
          end;
        end
        else begin
          if first then begin
            MinLbl := minj;
            MaxLbl := maxj;
          end
          else begin
            MinLbl.x := min (minj.x, MinLbl.x);
            MinLbl.y := min (minj.y, MinLbl.y);
            MinLbl.z := min (minj.z, MinLbl.z);
            MaxLbl.x := max (maxj.x, MaxLbl.x);
            MaxLbl.y := max (maxj.y, MaxLbl.y);
            MaxLbl.z := max (maxj.z, MaxLbl.z);
          end;
        end;
        first := false;
      end;
    end
    else if (Entities.Count = 1) and (l^.ReCenterLabel or Flag (mas.fields.Flags, HAS_KNOCKOUT_PLN)) then begin
      lblEnt := Entities[0];
      if lblEnt.enttype <> entpln then begin
        ent_extent (lblEnt, minj, maxj);
        if first then begin
          MinLbl := minj;
          MaxLbl := maxj;
        end
        else begin
          MinLbl.x := min (minj.x, MinLbl.x);
          MinLbl.y := min (minj.y, MinLbl.y);
          MinLbl.z := min (minj.z, MinLbl.z);
          MaxLbl.x := max (maxj.x, MaxLbl.x);
          MaxLbl.y := max (maxj.y, MaxLbl.y);
          MaxLbl.z := max (maxj.z, MaxLbl.z);
        end;
        first := false;
      end;
    end;

  // check that any 'Spare Entities' found are in same group as associated text. Remove any that are not
  for i := 0 to NumLblEnts-1 do with LblEntities[i] do
    if OldFractionEnts.Count > 0 then
      for j := OldFractionEnts.Count-1 downto 0 do begin
        if Entities.Count = 0 then
          OldFractionEnts.Delete(j)
        else begin
          mode_init (mode);
          mode_group (mode, OldFractionEnts[j]);
          groupFound := false;
          adr := ent_first (mode);
          while ent_get (lblent, adr) and not groupFound do begin
            if addr_equal(adr, Entities[0].addr) then
              groupFound := true
            else
              adr := ent_next (lblent, mode);
          end;
          if not groupFound then
            OldFractionEnts.Delete(j)
          else if (OldFractionEnts[j].enttype <> entpln) and
                  (l^.ReCenterLabel or Flag (mas.fields.Flags, HAS_KNOCKOUT_PLN)) then begin   // include this entity in Lbl Extents
            if ent_get (tempent, OldFractionEnts[j].addr) then begin
              ent_extent (tempent, minj, maxj);
              if minj.x < minLbl.x then minLbl.x := minj.x;
              if minj.y < minLbl.y then minLbl.y := minj.y;
              if maxj.x > maxLbl.x then maxLbl.x := maxj.x;
              if maxj.y > maxLbl.y then maxLbl.y := maxj.y;
            end;
          end;
        end;
      end;

  if l^.ReCenterLabel then begin
    meanpnt (minLbl, maxLbl, lblCntr);
    doReCenter := PointsEqual (oldplnctr, lblCntr);
  end
  else
    doReCenter := false;


  // update mas record with new dimensions
  DoCalcs (plnent, mas);
  // update entity atrib with new data
  if atr_entfind (plnent, STANDARD_ATR_NAME, atr) then
    if atr.atrtype = atr_str255 then begin
      move (mas.str, atr.shstr, sizeof(atr.shstr));
      atr_update (atr);
    end;

  // undraw existing label entities
  for i := 0 to NumLblEnts-1 do with LblEntities[i] do begin
    if Entities.Count > 0 then begin
      TempEnt := Entities[0];
      drawIfOn (TempEnt, drmode_black);
      if OldFractionEnts.Count > 0 then
        for j := 0 to OldFractionEnts.Count-1 do begin
          tempent := OldFractionEnts[j];
          drawIfOn (tempent, drmode_black);
        end;
      end;
  end;

  // update text and label extents for each found entity
  first := true;
  for i := 0 to NumLblEnts-1 do with LblEntities[i] do
    if Entities.Count = 1 then begin
      if ent_get (ent, Entities[0].addr) then begin
        CreateText (ent, true, OldFractionEnts, NewFractionEnts, mas, i+1, min_ent, max_ent);
        ent_update (ent);
        Entities[0] := ent;

        if first then begin
          max_lbl.x := max (max_ent.x, min_ent.x);
          max_lbl.y := max (max_ent.y, min_ent.y);
          max_lbl.z := max_ent.z;
          min_lbl.x := min (max_ent.x, min_ent.x);
          min_lbl.y := min (max_ent.y, min_ent.y);
          min_lbl.z := min_ent.z;
        end
        else begin
          min_lbl.x := min (min_ent.x, min(min_lbl.x, max_ent.x));
          min_lbl.y := min (min_ent.y, min(min_lbl.y, max_ent.y));
          max_lbl.x := max (min_ent.x, max(max_lbl.x, max_ent.x));
          max_lbl.y := max (min_ent.y, max(max_lbl.y, max_ent.y));
        end;
        first := false;
      end;
      while OldFractionEnts.Count > 0 do begin
        if ent_get (ent, OldFractionEnts[0].addr) then
          ent_del (ent);
        OldFractionEnts.Delete(0);
      end;
    end;

  if doReCenter then begin
    meanpnt (min_lbl, max_lbl, NewLblCntr);
    subpnt (plnctr, NewLblCntr, ReCenterDis);
  end;

  if not isnil (BorderEnt.addr) then begin
    drawIfOn (BorderEnt, drmode_black);
    // in case whole label has been rotated, get angle of first straight side of BorderEnt
    adr := BorderEnt.plnfrst;
    repeat
      polyvert_get (pv, adr, BorderEnt.plnfrst);
      adr := pv.Next;
    until (pv.shape = pv_vert) or isnil (adr);
    if isnil (adr) then
      borderAng := 0
    else
      borderAng := angle (pv.pnt, pv.nextpnt);

    while BorderAng > pi do
      BorderAng := BorderAng - Pi;
    while BorderAng < 0 do
      BorderAng := BorderAng + Pi;

    if not (RealEqual (BorderAng, Pi) or RealEqual (BorderAng, 0) or
            RealEqual (BorderAng, halfpi)) then begin
      setrotate (mat, -BorderAng, z);
      xformpt (min_lbl, mat, min_lbl);
      xformpt (max_lbl, mat, max_lbl);
      adr := BorderEnt.plnfrst;
      while polyvert_get (pv, adr, BorderEnt.plnfrst) do begin
        xformpt (pv.pnt, mat, pv.pnt);
        polyvert_update (pv);
        adr := pv.Next;
      end;
    end;

    ent_extent (BorderEnt, mink, maxk);
    leftshift :=  min_lbl.x - (mink.x + BorderMargins[1]);
    rightshift := max_lbl.x - (maxk.x - BorderMargins[2]);
    topshift := max_lbl.y - (maxk.y - BorderMargins[3]);
    bottomshift := min_lbl.y - (mink.y + BorderMargins[4]);
    meanpnt (mink, maxk, meank);
    adr := BorderEnt.plnfrst;
    while polyvert_get (pv, adr, BorderEnt.plnfrst) do begin
      if pv.pnt.x < meank.x then
        pv.pnt.x := pv.pnt.x + leftshift;
      if pv.pnt.x > meank.x then
        pv.pnt.x := pv.pnt.x + rightshift;
      if pv.pnt.y < meank.y then
        pv.pnt.y := pv.pnt.y + bottomshift;
      if pv.pnt.y > meank.y then
        pv.pnt.y := pv.pnt.y + topshift;
      polyvert_update (pv);
      adr := pv.Next;
    end;

    if not (RealEqual (BorderAng, Pi) or RealEqual (BorderAng, 0) or
            RealEqual (BorderAng, halfpi)) then begin
      setrotate (mat, BorderAng, z);
      adr := BorderEnt.plnfrst;
      while polyvert_get (pv, adr, BorderEnt.plnfrst) do begin
        xformpt (pv.pnt, mat, pv.pnt);
        polyvert_update (pv);
        adr := pv.Next;
      end;
    end;

    if doReCenter then begin
      ent_move (BorderEnt, ReCenterDis.x, ReCenterDis.y, 0);
      ent_update (BorderEnt);
    end;
    drawIfOn (BorderEnt, drmode_white);
  end;

  for i := 0 to NumLblEnts-1 do with LblEntities[i] do begin
    // Recenter entities if required, draw them to screen and free lists
    if Entities.Count > 0 then begin
      ent_get (tempent, Entities[0].addr);
      if doReCenter then begin
        ent_move (tempent, ReCenterDis.x, ReCenterDis.y, 0);
        ent_update (tempent);
      end;
      drawIfOn (tempent, drmode_white);
    end;
    Entities.Free;

    OldFractionEnts.Free;

    if NewFractionEnts.Count> 0 then
      for j := 0 to NewFractionEnts.Count-1 do begin
        ent_get (tempent, NewFractionEnts[j].addr);
        if doReCenter then begin
          ent_move (tempent, ReCenterDis.x, ReCenterDis.y, 0);
          ent_update (tempent);
        end;
        drawIfOn (tempent, drmode_white);
      end;
    NewFractionEnts.Free;;
  end;
  Finalize (LblEntities);
END;  // RefreshLabel

PROCEDURE RefreshLabel (plnent : entity);
VAR
  pStandardAtrStr : ^StandardAtrStr;
  atr : attrib;
BEGIN
  if atr_entfind (plnent, STANDARD_ATR_NAME, atr) and (atr.atrtype = atr_str255) then begin
    pStandardAtrStr := addr(atr.shstr);
    RefreshLabel (plnent, pStandardAtrStr^);
  end;
END;



PROCEDURE RefreshLabels (checkIfChanged : boolean);
VAR
  mode : mode_type;
  adr : lgl_addr;
//  ent : entity;
  atrstr : StandardAtrStr;
  major, minor, patch, build : integer;
BEGIN
  mode_init (mode);
  mode_lyr (mode, lyr_all);
  mode_atr (mode, STANDARD_ATR_NAME);
  mode_enttype (mode, entpln);
  adr := ent_first (mode);

  while ent_get (l^.ent, adr) do begin
    adr := ent_next (l^.ent, mode);
    if (not CheckIfChanged) or SpaceHasChanged (l^.ent, atrstr) then begin

      if (l^.DCADver = 0) and DataCADVersion (major, minor, patch, build) then begin
        if DataCADVersionEqOrGtr (21, 0, 1, 0) then
          l^.DCADver := 3
        else if DataCADVersionEqOrGtr (17, 0, 0, 0) then
          l^.DCADver := 2
        else l^.DCADver := 1;
      end;

      if l^.DCADver >= 3 then
        undoStartTransaction;

      if (not CheckIfChanged) then
        RefreshLabel (l^.ent)
      else
        RefreshLabel (l^.ent, atrstr);
      UpdateCenterEnt (l^.ent);

      if l^.DCADver >= 3 then
        undoEndTransaction;

    end;
  end;
END;




PROCEDURE CreateTagAtr (var ent : entity;
                        var tag : atrname); //;
                       // CONST form : TfmSpaceProperties);
// Create a unique tag for the association logic
VAR
  atr : attrib;
BEGIN
  // attribute created is used to link the various entities (polyline, label) together
  // and is created with a unique name for this purpose.
  tag := CreateNewTag;
	atr_init (atr, atr_int);
  atr.name := tag;
	atr.int := 0;     // will update value with number of associated entities later
	atr_add2ent (ent, atr);
END; //CreateTagAtr;

PROCEDURE InitAtrStr (VAR atrstr : StandardAtrStr;
                      CONST tag : AtrName;
                      CONST Name, Number, Category : string;
                      CONST CeilingTypeNdx : integer;
                      CONST CeilingHeight : double;
                      CONST xlsSource, xlsRow : smallint);
BEGIN
  // create an attribute to contain name, number, measures etc.)
  atrstr.fields.len := 255;
  atrstr.fields.Flags := 0;
  atrstr.fields.tag := tag;
  atrstr.fields.WallThick := l^.wallwidth;
  SetFlag (atrstr.fields.Flags, IS_SURFACE, l^.CreateAtSurface);
  atrstr.fields.Name := str25 (Name);

  atrstr.fields.Num := shortstring (Number);
  atrstr.fields.Category := shortstring (Category);
  atrstr.fields.ExcelNdx := xlsSource;
  atrstr.fields.ExcelRow := xlsRow;

  case CeilingTypeNdx of
    0 : atrstr.fields.Height1 := CeilingHeight;
    1 : SetFlag (atrstr.fields.Flags, CEILING_FROM_MODEL, true);
  end;

  if l^.Knockout in [KObox, KOboxBorder] then
    SetFlag (atrstr.fields.Flags, HAS_KNOCKOUT_PLN, true);
  if l^.LabelFromSymbol then
    SetFlag (atrstr.fields.Flags, FROM_SYMBOL, true);

END;

PROCEDURE CreateCustomFieldAtrs (ent : entity;
                                 UserFieldValues : TList<UserFieldValue>);
VAR
  i : integer;
  atr : attrib;
BEGIN
  if UserFieldValues.Count = 0 then
    exit;
  for i := 0 to UserFieldValues.Count-1 do begin
    case UserFieldValues[i].numeric of
      str : begin
        atr_init (atr, atr_str);
        atr.str := UserFieldValues[i].str;
      end;
      int: begin
        atr_init (atr, atr_int);
        atr.int := UserFieldValues[i].int;
      end;
      rl: begin
        atr_init (atr, atr_rl);
        atr.rl := UserFieldValues[i].rl;
      end;
      dis : begin
        atr_init (atr, atr_dis);
        atr.dis := UserFieldValues[i].dis;
      end;
      ang : begin
        atr_init (atr, atr_ang);
        atr.ang := UserFieldValues[i].ang;
      end;
      UserFieldType.area : begin
        atr_init (atr, atr_area);
        atr.rl := UserFieldValues[i].area;
      end
      else
        atr_init (atr, atr_pnt);  // should never happen, but initialise to a type we don't use so that we don't process it
    end;

    if atr.atrtype <> atr_pnt then begin
      atr.name := 'SpdhCF' + UserFieldValues[i].tag;
      atr_add2ent (ent, atr);
    end;
  end;

END;

PROCEDURE ProcessOffsetLine (NeedToAddEntity, DefineByWallSurface,
                             CreateWallSurface, CreateWallCenter : boolean;
                             WallWidth : double);
VAR
  Adr : lgl_addr;
BEGIN
  setnil (l^.ent2.addr);
  PlnFix (l^.ent, false);
  if NeedToAddEntity then begin
    if DefineByWallSurface and (not CreateWallSurface) and (WallWidth > 0) then begin
      offsetPln (l^.ent, l^.ent2, false, false, WallWidth/2);
      if ent_get (l^.ent, l^.ent2.addr) then
        setnil (l^.ent2.addr);
    end
    else if DefineByWallSurface or CreateWallCenter then begin
      ent_add (l^.ent);
      if (not DefineByWallSurface) and
         CreateWallSurface and
         (WallWidth > 0) then
        offsetPln (l^.ent, l^.ent2, false, true, WallWidth/2)
      else if DefineByWallSurface and CreateWallSurface and CreateWallCenter and
         (WallWidth > 0) then
        offsetPln (l^.ent, l^.ent2, false, false, WallWidth/2)
    end
    else begin
      offsetPln (l^.ent, l^.ent2, false, true, WallWidth/2);
      l^.ent := l^.ent2;
      setnil (l^.ent2.addr);
    end;
  end
  else if (not DefineByWallSurface) and
          CreateWallSurface and
          (WallWidth > 0) then begin
    offsetPln (l^.ent, l^.ent2, false, true, WallWidth/2);
  end;

  if (not DefineByWallSurface) and
     CreateWallSurface and
     CreateWallCenter then begin
    // swap entities around - we always want wall surface to the ent1 if possible
    Adr := l^.ent2.addr;
    if not ent_get (l^.ent2, l^.ent.addr) then beep;
    if not ent_get (l^.ent, Adr) then beep;
  end
  else
    ent_get (l^.ent, l^.ent.addr);
END;


PROCEDURE ProcessVisibility (DefineByWallSurface, CreateWallSurface, CreateWallCenter : boolean );
BEGIN
{  if (DefineByWallSurface or CreateWallSurface) and
     (l^.ShowPln in [PLshowNone, PLshowCntr]) then begin
        HidePln (l^.ent, true);
  end;

  if (not DefineByWallSurface) and (not CreateWallSurface) and
     (l^.ShowPln in [PLshowNone, PLshowSurf]) then begin
        HidePln (l^.ent, true);
  end;

  if DefineByWallSurface and CreateWallSurface and
     CreateWallCenter and (l^.ShowPln in [PLshowNone, PLshowSurf]) then begin
        HidePln (l^.ent2, true);
  end; }

  if CreateWallSurface and (l^.ShowPln in [PLshowNone, PLshowCntr]) then
    HidePln (l^.ent, true);

  if CreateWallCenter and (not CreateWallSurface) and (l^.ShowPln in [PLshowNone, PLshowSurf]) then
    HidePln (l^.ent, true);

  if CreateWallCenter and CreateWallSurface and (l^.ShowPln in [PLShowNone, PLshowSurf]) then
    HidePln (l^.ent2, true);

END;


PROCEDURE CreateSpace (const fmSpaceProperties : TfmSpaceProperties);
VAR
  UserFieldValues  : TList<UserFieldValue>;
  UserFieldVal  : UserFieldValue;
  i : integer;
BEGIN
  UserFieldValues := TList<UserFieldValue>.Create;
  try
    for i := 0 to length (fmSpaceProperties.CatCustFields)-1 do begin
      with fmSpaceProperties.CatCustFields[i].Input do begin
        UserFieldVal.tag := fmSpaceProperties.CatCustFields[i].tag;
        case NumberType of
          DCADEdit.TextStr : begin
            UserFieldVal.numeric := str;
            UserFieldVal.str := shortstring(Text);
            UserFieldValues.Add(UserFieldVal);
          end;
          DCADEdit.IntegerNum: begin
            UserFieldVal.numeric := int;
            UserFieldVal.int := IntValue;
            UserFieldValues.Add(UserFieldVal);
          end;
          DCADEdit.DecimalNum: begin
            UserFieldVal.numeric := rl;
            UserFieldVal.rl := NumValue;
            UserFieldValues.Add(UserFieldVal);
          end;
          DCADEdit.Distance : begin
            UserFieldVal.numeric := dis;
            UserFieldVal.dis := NumValue;
            UserFieldValues.Add(UserFieldVal);
          end;
          DCADEdit.DecDegrees, DCADEdit.DegMinSec : begin
            UserFieldVal.numeric := ang;
            UserFieldVal.ang := NumValue;
            UserFieldValues.Add(UserFieldVal);
          end;
          DCADEdit.Area : begin
            UserFieldVal.numeric := UserFieldType.area;
            UserFieldVal.area := NumValue;
            UserFieldValues.Add(UserFieldVal);
          end;
        end;
      end;
    end;


    CreateSpace (fmSpaceProperties.fCeilingOptions.cbCeilingType.ItemIndex,
                 fmSpaceProperties.dcFloorZ.NumValue,
                 fmSpaceProperties.fCeilingOptions.dcCeilingHeight.NumValue,
                 fmSpaceProperties.cbName.Text,
                 fmSpaceProperties.edNumber.Text,
                 fmSpaceProperties.cbCategory.Text,
                 UserFieldValues, 0, 0);

    SaveStr (SpdhSpaceNum, l^.PrevNum, false);

  finally
    UserFieldValues.Free;
  end;
END;

PROCEDURE CreateSpace (CeilingTypeNdx : integer;
                       FloorZ : double;
                       CeilingHeight : double;
                       Name, Number, Category : string;
                       UserFieldValues : TList<UserFieldValue>;
                       xlsSource, xlsRow : smallint);

VAR  tag : atrname;
  lblent : entity;
  atr : attrib;
  atrstr : StandardAtrStr;
  NumLabelEnts : integer;
  s : shortstring;
  svLyr, textlyr : lgl_addr;
BEGIN
  if l^.doFill then begin
    if l^.FillColours[1] = 0 then
      atr := InitSolidFillAttribute (LineColor, 0, 0)
    else
      atr := InitSolidFillAttribute (l^.FillColours[1], 0, 0);
    atr.SPBvisible := l^.doFillOutline;
    atr.xdata.Data[12] := l^.FillOpacity;
    atr_add2ent (l^.ent, atr)
  end;
  l^.ent.plbase := FloorZ;
  case CeilingTypeNdx of
    0,1 : l^.ent.plnhite := l^.ent.plbase + CeilingHeight;
    2 : l^.ent.plnhite := l^.ent.plbase + CeilingHeight;
    3 : l^.ent.plnhite := l^.ent.plbase + CeilingHeight;
    4 : l^.ent.plnhite := l^.ent.plbase + CeilingHeight;
  end;
  ent_update (l^.ent);
  ent_draw (l^.ent, drmode_white);
  if not isnil (l^.ent2.addr) then begin
    ent_update (l^.ent2);
    ent_draw (l^.ent2, drmode_white);
  end;

  CreateTagAtr (l^.ent, tag); //, fmSpaceProperties);
  if not isnil (l^.ent2.addr) then begin
    ent_get (l^.ent2, l^.ent2.addr);
    atr_init (atr, atr_int);
    atr.name := tag;
    atr.int := 0;
    atr_add2ent (l^.ent2, atr);
    ent_get (l^.ent, l^.ent.addr);
  end;

  InitAtrStr (atrstr, tag, Name, Number, Category, CeilingTypeNdx, CeilingHeight, xlsSource, xlsRow);
  DoCalcs (atrstr);
  atr_init (atr, atr_str255);
  atr.name := STANDARD_ATR_NAME;
  atr.shstr := atrstr.str;
  atr_add2ent (l^.ent, atr);

  CreateCustomFieldAtrs (l^.ent, UserFieldValues);

  if l^.lblLyrSpec <> LSActive then begin
    svLyr := getlyrcurr;
    if l^.lblLyrSpec = LSSpecific then
      s := shortstring(l^.lblLyrSpecific)
    else begin
      getlyrname (svLyr, s);
      if l^.lblLyrSpec = LSPrefix then
        s := shortstring(l^.lblLyrIx) + s
      else
        s := s + shortstring(l^.lblLyrIx);
    end;
    if lyr_find (s, textlyr) then
      lyr_set (textlyr)
    else begin
      lyr_create (s, textlyr);
      if lyr_find (s, textlyr) then
        lyr_set (textlyr)
    end;
  end;

  NumLabelEnts := CreateLabel (lblent, atrstr);
  if ent_get (l^.ent, l^.ent.addr) and atr_entFind (l^.ent, tag, atr) and (atr.atrtype = atr_int) then begin
    atr.int := NumLabelEnts;
    atr_update (atr);
  end;

  if l^.lblLyrSpec <> LSActive then
    lyr_set (svLyr);

  ent_get (l^.ent, l^.ent.addr);

END;


PROCEDURE UpdateLabel (plnent : entity);
VAR
  LblCntr, NewLblCntr, dis2move : point;
  LblAng : double;
  area : double;
  temppnt, plnCntr : point;
  LblList : tList<entity>;
  ent : entity;
  i, NumLabelEnts : integer;
  atr : attrib;
  pAtrStr : ^StandardAtrStr;
  svAutoPlace : boolean;
  SvLyr : lgl_addr;
BEGIN
  if plnent.enttype <> entpln then
    exit;
  if (not atr_entfind (plnent, STANDARD_ATR_NAME, atr)) or (atr.atrtype <> atr_str255) then
    exit;
  pAtrStr := Addr (atr.shstr);
  DoCalcs (plnent, pAtrStr^);
  pline_centroid (plnent.plnfrst, plnent.plnlast, area, temppnt, plncntr, true, true);
  LblList := tList<entity>.Create;
  try
    LblCntr := LabelCenter (plnent, plncntr, LblList, lblAng);
    for i := 0 to LblList.Count-1 do begin
      if ent_get (ent, LblList[i].addr) then begin
        ent_draw (ent, drmode_black);
        ent_del (ent);
      end;
    end;
    LblList.Clear;

    svAutoPlace := l^.AutoPlace;
    l^.AutoPlace := true;
    SvLyr := getlyrcurr;
    lyr_set (plnent.lyr);   // want to create label on same layer as outline
    NumLabelEnts := CreateLabel (ent, pAtrStr^);
    lyr_set (SvLyr);
    stopgroup;
    if (not RealEqual (lblAng, 0)) and FindAllLblEnts (plnent, LblList) then begin
      for i := 0 to LblList.Count-1 do begin
        if ent_get (ent, LblList[i].addr) then begin
          ent_rotate (ent, LblCntr, lblAng);
          ent_update (ent);
        end;
      end;
      LblList.Clear;
    end;
    NewLblCntr := LabelCenter (plnent, plncntr, LblList, lblAng);
    subpnt (NewLblCntr, LblCntr, dis2move);
    if LblList.Count > 0 then for i := 0 to LblList.Count-1 do begin
      if ent_get (ent, LblList[i].addr) then begin
        ent_move (ent, dis2move.x, dis2move.y, dis2move.z);
        ent_update (ent);
        ent_draw (ent, drmode_white);
      end;
    end;
    l^.AutoPlace := svAutoPlace;
    if ent_get (plnent, plnent.addr) and atr_entFind (plnent, pAtrStr.fields.tag, atr) and (atr.atrtype = atr_int) then begin
      atr.int := NumLabelEnts;
      atr_update (atr);
    end;

  finally
    LblList.free;
  end;
END;


end.

