object fmLicence: TfmLicence
  Left = 0
  Top = 0
  Caption = 'First Use of Macro'
  ClientHeight = 338
  ClientWidth = 447
  Color = clBtnFace
  Constraints.MinHeight = 300
  Constraints.MinWidth = 450
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  DesignSize = (
    447
    338)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 6
    Width = 268
    Height = 13
    Caption = 'Please accept the Licence Agreement to use this macro:'
  end
  object btnExit: TButton
    Left = 8
    Top = 305
    Width = 105
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Exit Macro'
    ModalResult = 2
    TabOrder = 0
    ExplicitTop = 168
  end
  object Button1: TButton
    Left = 290
    Top = 305
    Width = 151
    Height = 25
    Anchors = [akRight]
    Cancel = True
    Caption = 'Accept'
    ModalResult = 1
    TabOrder = 1
  end
  object memLicence: TMemo
    Left = 8
    Top = 24
    Width = 433
    Height = 273
    Anchors = [akLeft, akTop, akRight, akBottom]
    Lines.Strings = (
      '')
    ScrollBars = ssVertical
    TabOrder = 2
  end
end
