unit Licence;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Constants;

type
  TfmLicence = class(TForm)
    btnExit: TButton;
    Button1: TButton;
    memLicence: TMemo;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmLicence: TfmLicence;

implementation

{$R *.dfm}

procedure TfmLicence.FormCreate(Sender: TObject);
begin
  MemLicence.Lines.Clear;
  MemLicence.Lines.Add(LicParagraph1);
  MemLicence.Lines.Add('');
  MemLicence.Lines.Add(LicParagraph2);
  MemLicence.Lines.Add('');
  MemLicence.Lines.Add(LicParagraph3);
  MemLicence.Lines.Add('');
  MemLicence.Lines.Add(LicParagraph4);
  MemLicence.Lines.Add('');
  MemLicence.Lines.Add(LicParagraph5);
end;

end.
