unit CommonStuff;

interface

uses URecords, UInterfaces, UConstants, UInterfacesRecords, Graphics, Settings,
     SysUtils, System.Generics.Collections, System.classes, Math, Variants, Vcl.StdCtrls,
     Language, Vcl.ExtCtrls, WinAPI.Windows, ClrUtil;

FUNCTION CompareCategoryDef (def1, def2 : Pointer) : integer;
FUNCTION GridAngle : double;
PROCEDURE SetGridAngle (newang : double);

PROCEDURE SetLineColor (clr : integer);

PROCEDURE NextState (state : asInt);

PROCEDURE PrevState;

FUNCTION RealEqual (r1, r2, delta : double) : boolean;  overload;
FUNCTION RealEqual (r1, r2 : double) : boolean;  overload;

PROCEDURE SaveOptions;
PROCEDURE SaveFlags;
PROCEDURE LoadFlags;
PROCEDURE SaveDimSettings;
PROCEDURE LoadDimSettings;
PROCEDURE SaveAreaSettings;
PROCEDURE LoadAreaSettings;
PROCEDURE SaveVolSettings;
PROCEDURE LoadVolSettings;
PROCEDURE DrawAnchor (UndrawOnly : boolean);

FUNCTION EntCenter (ent : entity) : URecords.point;


PROCEDURE RequiredFields (VAR NameRqd, NumRqd : boolean);

FUNCTION addr_equal (addr1, addr2 : lgl_addr) : boolean;
FUNCTION PointsEqual (p1, p2 : URecords.point) : boolean;

FUNCTION Flag (Flags : word; FlagNum : integer) : boolean;
PROCEDURE SetFlag (VAR Flags : word;
                   FlagNum : integer;
                   Value : boolean);

FUNCTION Pv_ToVoid (pv : polyvert) : boolean;
FUNCTION Pv_FromVoid (pv : polyvert) : boolean;
FUNCTION Pv_VoidLine (pv : polyvert) : boolean;

FUNCTION DimUnitSize (Destination : integer) : double;
FUNCTION ActualTextSize (siz : double) : double;

FUNCTION DefaultTxtSize : double;
PROCEDURE CheckTxtScale;

PROCEDURE PopulateLayerList (MsgNdx : integer; var ComboList : TComboBox);

PROCEDURE CleanUp;

FUNCTION ColourName (clr : longint) : shortstring;

PROCEDURE FillClrPnlCaption (pnl : TPanel; clr : longint);

PROCEDURE DrawIfOn (ent : entity; drMode : integer);


CONST
  UNITS_LBL = 0;
  UNITS_RPT = 1;
  UNITS_CLIP = 2;

  SQFTCONV = 384.0*384.0;
  SQCONV = 384.0*384.0*100;
  ACRECONV = 384.0*384.0*43560;
  SQMCONV = 384.0*384.0*10.7639104167097223083335055559;
  HECTARECONV = 384.0*384.0*107639.104167097223083335055559;
	MCONV = 1259.8425196850393700787401574803;
  LITRCONV = 1999626.0464961874805639375058278;
	M3CONV = 1999626046.4961874805639375058278;
	FT3CONV = 56623104;
	YD3CONV = 1528823808;

  RPT_NONE = 0;
  RPT_ACTIVE = 1;
  RPT_ON = 2;
  RPT_ALL = 3;
  RPT_AREA = 4;
  RPT_AREA_ADD = 5;
  RPT_AREA_DEL = 6;


  xlByColumns = 2;
  xlByRows = 1;
  xlPrevious = 2;
  xlNormal = -4143;
  xlMaximized = -4137;
  xlMinimized = -4140;

  atr_Area = 8;  //note: 8 is the number for atr_str255, but used for area attributes in user field definitions (which do not otherwise use str255 attributes)

TYPE
  str25 = string[25];
  str35 = string[35];
  str30 = string[30];
  str50 = string[50];
  str60 = string[60];
  str8 = string[8];
  str9 = string[9];
  str2 = string[2];
  str6 = string[6];
  str16 = string[16];
  str10 = string[10];
  str150 = string[150];

  Strings = Array[1..26] of Str80;

  lglFile = File of lgl_addr;

  UserFieldType = (str, int, rl, dis, ang, area);
  CeilingTypes  = (flat, {SingleSlope, DoubleSlope1, DoubleSlope2, Arch1, Arch2,
                   Arch3, Arch4,} Custom);
  AlignmentType = (left, center, right);

  UserFieldDef = record
    tag : str6;
    desc : str30;
    numeric : UserFieldType;
    deleted : boolean;
    case UserFieldType of
      str : (defaultstr : str25);
      int : (defaultint : integer);
      rl  : (defaultrl : double);
      dis : (defaultdis : double);
      ang : (defaultang : double);
      area: (defaultarea : double);
  end;

  UserFieldValue = record
    tag : string[6];
    numeric : UserFieldType;
    case UserFieldType of
      str : (str : str25);
      int : (int : integer);
      rl  : (rl : double);
      dis : (dis : double);
      ang : (ang : double);
      area: (area : double);
  end;

  CatStandardData = packed record
    strlen  : byte;
    version : byte;
    UseZHt  : boolean;
    DoFill : boolean;
    FillColor : longint;  {zero if not fill}
    Height1 : double;
    Height2 : double;
    Height3 : double;
    Slope1  : double;
    Slope2  : double;
    name    : str25;
    FillOpacity : byte;
    CeilingType : CeilingTypes;
  end;
  CategoryStandardFields = record
    case byte of
      1 :(str : str80);
      2 :(filler : byte;
          name : str25;
          DoFill : boolean;
          FillColor : longint;  {zero if not fill}
          CeilingType : CeilingTypes;
          Height1 : double;
          Height2 : double;
          Height3 : double;
          Slope1  : double;
          Slope2  : double;
          UseZHt  : boolean);
      3 :(Data : CatStandardData);
  end;

  CategoryDef = record
    num : str2;
    Standard : CategoryStandardFields;
    Custom  : TList<UserFieldValue>;
    updated : boolean;
  end;


  OptionsType = record
    CustomClr : boolean;
//    pitchEntry : boolean;
    NameAllCaps : boolean;
    NumAllCaps : boolean;
    LyrSort : boolean;
  end;

  FractionType = (Fr_Decimal, Fr_2, Fr_4, Fr_8, Fr_16, Fr_32, Fr_64, Fr_128);

  DimensionType = (Current, FeetInch, DecFt, Mtr, Cm, Mm, CustomDim, SameAsLbl, SameAsRpt);
  RoundingType = (RoundDown, RoundNear, RoundUp);
  DimensionsRec = RECORD
    DimTyp : DimensionType;
    Rounding : RoundingType;
    DecPlaces : byte;
    RemTrailingZero : boolean;
    CustUnitSize : double;
    CustUnitDisplay : str9;
    DecUnitDisplay : str9;
    Fractions : FractionType;
  END;

  AreaSourceTyp = (Flr, Wall, CustArea, Ceiling, Window, Door);
  AreaType = (SqFt, Squares, Acres, SqMtr, Hectares, CustomArea, AreaSameLbl, AreaSameRpt, AltSameAsMain);
  AreaRecords = (MainAreaRec, AltAreaRec);
  AreasRec = RECORD
    AreaTyp : AreaType;
    Rounding : RoundingType;
    DecPlaces : byte;
    RemTrailingZero : boolean;
    CustUnitSize : double;
    CustUnitDisplay : str9;
    DecUnitDisplay : str9;
    Fractions : FractionType;
  END;

  VolumeType = (CuFt, CuYard, Litre, CuMtr, CustomVol, VolSameLbl, VolSameRpt);
  VolumesRec = RECORD
    VolTyp : VolumeType;
    Rounding : RoundingType;
    DecPlaces : byte;
    RemTrailingZero : boolean;
    CustUnitSize : double;
    CustUnitDisplay : str9;
    DecUnitDisplay : str9;
    Fractions : FractionType;
  END;

//  AnchorType = (TopLeft, Cnter, TopRight, BottomLeft, BottomRight);

  LeaderTp = (Ldr_Pln, Ldr_Fillet, Ldr_BSp);
  ArrowRec = RECORD
    Style : integer;
    Filled : boolean;
    Knockout : boolean;
    Size : double;
    Wgt : integer;
    Aspect : double;
    Clr : integer;
    Ortho1 : boolean;
    LeaderType : LeaderTp;
  END;

  KnockoutType = (KOnone, KOentity, KObox, KOentityBorder, KOboxBorder);
  ShowPlnType = (PLshowNone, PLshowSurf, PLshowCntr, PLshowAll);
  LyrSpecType = (LSActive, LSSuffix, LSPrefix, LSSpecific);


  SpacesL = record
    state   :  asint;
    states : array [1..6] of asint;
    DCADver : byte; // 0 = not set, 1 = prior to DC17, 2 = DC17 to 21.00.00, 3 = DC21.00.01 onwards
    inputmode : integer;
    void_inputmode : integer;
    DimensionSettings : array [UNITS_LBL .. UNITS_CLIP] of DimensionsRec;
    AreaSettings : array [AreaRecords, UNITS_LBL .. UNITS_CLIP] of AreasRec;
    VolSettings : array [UNITS_LBL .. UNITS_CLIP] of VolumesRec;
    LabelTextAlignment : AlignmentType;
    LabelFromSymbol : boolean;
    PrevNum : str9;
    Increment : integer;
    PrevName : str25;
    NameIncrement : integer;
    AutoPlace : boolean;
    ShowPln : ShowPlnType;
    Pnt1, Pnt2: URecords.point;
    ent, ent2 : entity;       // note : ent2 is overlaid with Atr (& StandardAtrStr) for Measures functionality
    lblmode : mode_type;
    lblcntr : URecords.point;
    lblvertical : boolean;
    lblrotate : boolean;
    closd : boolean;
    plninit : boolean;
    doCover : boolean;
    doRect : boolean;
    wallthick, maxopening : double;    // these are used for Room Contour
//    txtscale : boolean;
    doFill : boolean;
    doFillOutline : boolean;
    FillOpacity : byte;
    Options : OptionsType;
    FillColours : array [1..23] of longint;
    svGridAng : double;
    UserFields : TList<UserFieldDef>;
    Categories : TList<CategoryDef>;
    CatNdx : word;
    NameRqd : boolean;
    NbrRqd : boolean;
    RoomNameFile : string;
    AltWallArea, AltCustomArea, AltCeilingArea, AltWindArea, AltDoorArea : boolean;
    RecalcLabelsAtStart, RecalcRptAtStart, RecalcReport : boolean;
    WallWidth : double;     // used for defining spaces by wall center
    DefineByCenter, CreateAtSurface, CreateAtCenter, KeyDimCenter : boolean;
    xAxisAngle : double;
    Xdis, Ydis : double;
    gridDis : double;
    Anchor : byte;
    AnchorBox : array[1..4] of URecords.point;
    ReCenterLabel : boolean;
//    ReportAnchor : AnchorType;
    RptTxtAlign : string[12];
    DoRefresh : boolean;
    ArrowSettings : ArrowRec;
    SvOrtho : boolean;
    VoidSpecifyZ :  boolean;
    flagFirst : boolean;
    ShowMeasuresDlg : boolean;
    Knockout : KnockoutType;
    KnockoutClr : integer;
    KnockoutEnlX, KnockoutEnlY : double;
    KnockoutMargin : double;
    EditSelection : word;
    Adr : lgl_addr;
    MovSideStretch : boolean;
    ReportSelection : integer;     //
    RptMode : mode_type;
    RptSubtotBy : integer;
    d : double;
    RptLineClr : integer;
    CopySelect : byte;
    lblLyrSpec : LyrSpecType;
    lblLyrSpecific : string;
    lblLyrIx : string;
    case byte of
       0: (getp: getpointarg);
       1: (getd: getdisarg);
       2: (dgetd: DgetdisArg);
       3: (globesc : GlobEscArg);
       4: (getpln : getPlinArg);
       5: (geti: getintarg);
       6: (geta: getangarg);
       7: (getr: dgetrealArg);
       8: (getclr: getclrArg);
       9: (getstr: dgetstrArg);
  end;

  PSpacesL = ^SpacesL;

VAR
  l :      PSpacesL;
  RoomNames : tstringlist;  // list of previously used room names




CONST
  STANDARD_ATR_NAME = 'dhSpacesPln';

  //Flag Constants
  IS_SURFACE = 0;
  CEILING_FROM_MODEL = 1;
  HAS_KNOCKOUT_PLN = 2;
  FROM_SYMBOL = 3;

  //Flag Constants - Bulge
  TO_VOID = 0;
  FROM_VOID = 1;

  //Tag Constants
  NumTags = 28;
  Tags : array [1..NumTags, 1..3] of string =(('{nbr}', 'Room Number', 'R'),
                            {1=tag}           ('{name}', 'Room Name', 'R'),
                            {2=desc}          ('{hdim}', 'Horizontal dimension of room', 'R'),
                            {3=Excel Flag}    ('{vdim}','Vertical dimension of room', 'R'),
                                              ('{sdim}', 'the Smaller of the 2 room dimensions (either horizontal or vertical)', 'W'),
                                              ('{ldim}', 'the Larger of the 2 room dimensions (either horizontal or vertical)', 'W'),
                                              ('{perim}',	'Total Room Perimeter (includes perim of any voids)', 'W'),
                                              ('{operim}', 'Perimeter of room outline (not including any voids)', 'W'),
                                              ('{vperim}', 'Perimeter of any voids', 'W'),
                                              ('{dunit}',	'Dimension Units', 'W'),
                                              ('{area}', 'Room area', 'W'),
                                              ('{oarea}', 'Overall Room area (excluding voids)', 'W'),
                                              ('{varea}', 'Room Void Area', 'W'),
                                              ('{aunit}',	'Room area units', 'W'),
                                              ('{parea}',	'Total Area of perimeter (including area of void perimeters)', 'W'),
                                              ('{oparea}', 'Area of Outline Perimeter (NOT including any void perimeters)', 'W'),
                                              ('{vparea}',	'Area of Void Perimeters', 'W'),
                                              ('{aaunit}', 'Alternate Area units', 'W'),
                                              ('{vol}',	'Volume of the room', 'W'),
                                              ('{vunit}',	'Volume Units', 'W'),
                                              ('{rarea%}', 'Percentage of Report Total Area', 'W'),
                                              ('{darea%}', 'Percentage of Drawing Total Area', 'W'),
                                              ('{lyr}', 'layer', 'W'),
                                              ('{cat}', 'category', 'R'),
                                              ('{grp}', 'field that is used for sub-total grouping (e.g. lyr or cat)', ' '),
                                              ('{fllvl}', 'Floor Level', 'R'),
                                              ('{ceilng}', 'Ceiling Height', 'R'),
                                              ('{count}', 'Count of Spaces', 'W'));


tag_nbr = 1;
tag_name = 2;
tag_hdim =3;
tag_vdim = 4;
tag_sdim = 5;
tag_ldim = 6;
tag_perim = 7;
tag_operim = 8;
tag_vperim = 9;
tag_dunit = 10;
tag_area = 11;
tag_oarea = 12;
tag_varea = 13;
tag_aunit = 14;
tag_parea = 15;
tag_oparea = 16;
tag_vparea = 17;
tag_aaunit = 18;
tag_vol = 19;
tag_vunit = 20;
tag_rareapct =21;
tag_dareapct =22;
tag_lyr = 23;
tag_cat = 24;
tag_grp = 25;
tag_flr = 26;
tag_ceiling = 27;
tag_count = 28;

// default label definition
defaultPtn : array [1..5] of shortstring = ('{nbr} {name}', '{hdim} x {vdim}', '{area} {aunit}', '', '');


// state constants
State_Exit = 0;
State_Main = 1;

State_Rectangle = 2;
State_Pln = 3;
State_3Pt = 4;
State_3PtA = 5;     // important: value must be State_3Pt + 1
State_Contour = 6;
State_Exist = 7;
State_Void = 8;
State_GetName = 9;
State_KeySizeX = 10;
State_KeySizeY = 11;  // important: value must be State_KeySizeX + 1
State_KeyAngle = 12;

// important: there is logic in SpaceDetails module that tests for states < Ztate_Esc
//            Any state that is used to create new spaces MUST be less than State_Esc.

State_Esc = 20;
State_EscRefresh = 21;
State_Snap = 22;
State_Edit = 23;
State_Increment = 24;
State_NameIncrement = 25;
State_Report = 26;
State_ReportArea = 27;
State_GridOrigin = 28;
State_GridSize = 29;

State_LblMove = 33;
State_LblRotate = 34;
State_Measure = 35;
State_Arrow1 = 36;
State_Arrow2 = 37;
State_ArrowSz = 38;
State_ArrowAspect = 39;
State_ArrowWeight = 40;
State_ArrowClr = 41;
State_Void2 = 42;
State_VoidRectangle = 43;
State_VoidPln = 44;
State_Void3pt = 45;
State_Void3ptA = 46;      // important: value must be State_Void3pt + 1
State_VoidKeySizeX = 47;
State_VoidKeySizeY = 48;  // important: value must be State_VoidKeySizeX + 1
State_tangents = 49;
State_ShowMeasures = 50;
State_UpdateRefresh = 51;
State_UpdateArea = 52;
State_UpdateArea2 = 53;
State_UpdateSome = 54;
State_CopyDisLyr = 55;
State_CopyDisLyr2 = 56;
State_CopyArea = 57;
State_ReportAreaDel = 58;
State_ReportPos = 59;
State_Excel = 60;

State_DoRefresh = 85;

State_Move = 100;
State_Copy = 101;
State_Rotate = 103;
State_KeyRotAngle = 104;
State_MoveLbl = 105;
State_KeyRotLblAngle = 106;
State_KeyLblAngle = 107;
State_GetNewX = 108;
State_GetNewY = 109;
State_ResizeSpace = 110;
State_Fold = 111;
State_MoveCorner = 112;
State_MoveSide = 113;
State_AddCorner = 114;
State_RenumberFirst = 115;
State_Renumber = 116;
State_SkipBack = 999;

// constant containing characters that are valid and unique in attribute names (only includes upper case letters as some functions do not distinguish upper and lower case as unique)
Chars = shortstring('!"#$%&''()*+,-./:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`{|}0123456789 ');


TYPE
  TextStyle = RECORD
		TxClr : integer;
		TxSiz : aFloat;
		TxSln : aFloat;
		TxAsp : aFloat;
		TxWgt : integer;
		Font  : fontstr;
		Space : aFloat;
		Space1: aFloat;
		Caps  : boolean;
		TxScale : boolean;
  END;

  FillDef = record
    Version : smallint; // Should be 1
    Fill_color : integer; // Entity color if < 0
    Fill_pattern : integer; // 0-6
    Pattern_color : integer; // Entity color if < 0
  END;


  StandardAtr = RECORD
    len : byte;
    tag : atrname;
    WallThick : double;
    Num : str9;
    Flags : Word;
    ExcelNdx : SmallInt;
    filler : lgl_addr;  // putting data here appears to cause the undo system to crash ?????
    Name : str25;
    Category : str25;
    ExcelRow : SmallInt;
    Height1 : double;
    area : double;
    OutlinePerim : double;  // perimeter of outline ( does not include voids )
    VoidPerim : double;  // total perimeter of any voids
    OutlinePerimArea : double;
    VoidPerimArea : double;
    volume : double;
    vdim : double;
    hdim : double;
    CeilingArea : double;
    this_area : double;   // area of this pln - used for quick check if something has changed
                          // (if it is at wall centre then it will not match area calculated for the space)
    centroid_x, centroid_y : double;
    this_DCPerim : double;  // perimeter of this pln as calculated by standard Datacad routine -
                            // used for quick check is something has changed (see above comment
    GrossArea : double; // area not taking into account any voids
    GrossVolume : double; // volume not taking into account any voids
    // current length = 232
  END;

  StandardAtrStr = RECORD
    case byte of
       0: (str: str255);
       1: (fields: StandardAtr);
  END;

  PolyvertBulgeRec = RECORD      // I use the bulge field to store other information for straight sides
    Flags : word;
    Material : word;
  END;

  FUNCTION AreaUnitSize (Destination : integer; AreaRec : AreaRecords) : double;
  FUNCTION AreaUnitDisplay (Destination : integer; AreaRec : AreaRecords) : string;
  FUNCTION VolUnitSize (Destination : integer) : double;
  FUNCTION VolUnitDisplay (Destination : integer) : string;
  FUNCTION DisUnitDisplay (Destination : integer) : string;

implementation


FUNCTION Flag (Flags : word; FlagNum : integer) : boolean;
VAR
  CompareValue : word;
BEGIN
  CompareValue := 1 shl FlagNum;
  result := (Flags and CompareValue) = CompareValue;
END;

PROCEDURE SetFlag (var Flags : word;
                   FlagNum : integer;
                   Value : boolean);
VAR
  CompareValue : word;
BEGIN
  if Value then begin
    CompareValue := 1 shl FlagNum;
    Flags := Flags or CompareValue;
  end
  else begin
    CompareValue := $FFFF - round(Power (2, FlagNum));
    Flags := Flags and CompareValue;
  end;
END;


FUNCTION CompareCategoryDef (def1, def2 : Pointer) : integer;
begin
  result := CompareText (string((CategoryDef(def1^)).standard.Data.name),
                         string((CategoryDef(def2^)).standard.Data.name))
end;

FUNCTION GridAngle : double;
VAR
  rlyr : RLayer;
BEGIN
  if lyr_get(getlyrcurr, rlyr) then
    result := rlyr.gridang
  else
    result := 0.0;
END;

PROCEDURE SetGridAngle (newang : double);
VAR
  rlyr : RLayer;
BEGIN
  if lyr_get(getlyrcurr, rlyr) then begin
    rlyr.gridang := newang;
    lyr_put (rlyr);
  end;
END;


PROCEDURE SetLineColor (clr : integer);
VAR
  rlyr : RLayer;
BEGIN
  if lyr_get(getlyrcurr, rlyr) then begin
    rlyr.Color := clr;
    lyr_put (rlyr);
  end;
END;

PROCEDURE NextState (state : asInt);
VAR
  i : integer;
BEGIN
  for i := 6 downto 2 do
    l.states[i] := l.states[i-1];
  l.states[1] := state;
  l.state := l.states[1];
END;

PROCEDURE PrevState;
VAR
  i : integer;
BEGIN
  for i := 1 to 5 do
    l.states[i] := l.states[i+1];
  l.states[6] := state_Main;
  l.state := l.states[1];
END;

FUNCTION RealEqual (r1, r2, delta : double) : boolean;
BEGIN
  result := abs(r1-r2) < delta;
END;

FUNCTION RealEqual (r1, r2 : double) : boolean;
BEGIN
  result := abs(r1-r2) < 0.00001;
END;

PROCEDURE SaveOptions;
VAR
  s : str80;
BEGIN
  s := GetSvdStr ('dhSpaceFlags', '                                              ');
  if l^.Options.CustomClr then s[1] := 'X'
    else s[1] := ' ';
//  if l^.Options.pitchEntry then s[2] := 'X';
  if l^.Options.NameAllCaps then s[3] := 'X'
    else s[3] := ' ';
  if l^.Options.NumAllCaps then s[4] := 'X'
    else s[5] := ' ';
  if l^.Options.LyrSort then s[26] := 'S'
    else s[26] := ' ';

  SaveStr ('dhSpaceFlags', s, true);
END;

PROCEDURE SaveFlags;
VAR
  s : str80;
BEGIN
  s := shortstring(StringOfChar ('-', 80));
  if l^.Options.CustomClr then s[1] := 'X';
//  if l^.Options.pitchEntry then s[2] := 'X';
  if l^.Options.NameAllCaps then s[3] := 'X';
  if l^.Options.NumAllCaps then s[4] := 'X';
  if l^.AltWallArea then s[16] := 'X';
  if l^.AltCeilingArea then s[17] := 'X';
  if l^.AltWindArea then s[18] := 'X';
  if l^.AltDoorArea then s[19] := 'X';
  if l^.RecalcLabelsAtStart then s[20] := 'X';
  if l^.RecalcRptAtStart then s[21] := 'X';
  if l^.RecalcReport  then s[22] := 'X';
  if l^.LabelTextAlignment = center then
    s[23] := 'C'
  else if l^.LabelTextAlignment = right then
    s[23] := 'R'
  else
    s[23] := 'L';
  if l^.LabelFromSymbol then s[24] := 'Y';
  if l^.ReCenterLabel then s[25] := 'C';
  if l^.Options.LyrSort then s[26] := 'S';
  if l^.VoidSpecifyZ then s[27] := 'X';      // note: not implemented in this version
  case l^.Knockout of
    KOentity : s[28] := 'E';
    KObox : s[28] := 'B';
    KOentityBorder : s[28] := 'F';
    KOboxBorder : s[28] := 'C';
  end;
  if l^.MovSideStretch then s[29] := 'S';
  if l^.AltCustomArea then s[30] := 'X';
  if l^.KeyDimCenter then s[31] := 'X';

  SaveStr ('dhSpaceFlags', s, true);
END;

PROCEDURE LoadFlags;
VAR
  s : str80;
BEGIN
  s := GetSvdStr ('dhSpaceFlags', '-------------------X----C---S-----------------');
  l^.Options.CustomClr := (s[1] = 'X');
//  l^.Options.pitchEntry := (s[2] = 'X');
  l^.Options.NameAllCaps := (s[3] = 'X');
  l^.Options.NumAllCaps := (s[4] = 'X');
  l^.AltWallArea := (s[16] = 'X');
  l^.AltCeilingArea := (s[17] = 'X');
  l^.AltWindArea := (s[18] = 'X');
  l^.AltDoorArea := (s[19] = 'X');
  l^.RecalcLabelsAtStart := (s[20] = 'X');
  l^.RecalcRptAtStart := (s[21] = 'X');
  l^.RecalcReport := (s[22] = 'X');
  CASE s[23] of
    'C' : l^.LabelTextAlignment := center;
    'R' : l^.LabelTextAlignment := right
    else l^.LabelTextAlignment := left;
  END;
  l^.LabelFromSymbol := (s[24] = 'Y');
  l^.ReCenterLabel := (s[25] = 'C');
  l^.Options.LyrSort := (s[26] = 'S');
  l^.VoidSpecifyZ := (s[27] = 'X');
  case s[28] of
    'E' : l^.Knockout := KOentity;
    'B' : l^.Knockout := KObox;
    'F' : l^.Knockout := KOentityBorder;
    'C' : l^.Knockout := KOboxBorder;
    else  l^.Knockout := KOnone;
  end;
  l^.MovSideStretch := (s[29] = 'S');
  l^.AltCustomArea := (s[30] = 'X');
  l^.KeyDimCenter := (s[31] = 'X');
END;


CONST
  UNITS_ATR_PREFIXES : array [UNITS_LBL .. UNITS_CLIP] of string = ('SpdhDmL',
                                                                    'SpdhDmR',
                                                                    'SpdhDmC');
PROCEDURE SaveDimSettings;
VAR
  i : integer;
BEGIN
  for i := UNITS_LBL to UNITS_CLIP do with l^.DimensionSettings[i] do begin
    SaveInt (UNITS_ATR_PREFIXES[i] + 'Type', + Ord (DimTyp));
    SaveInt (UNITS_ATR_PREFIXES[i] + 'Round', Ord(Rounding));
    SaveInt (UNITS_ATR_PREFIXES[i] + 'DecPl', DecPlaces);
    SaveBln (AtrName(UNITS_ATR_PREFIXES[i] + 'Rem0'), RemTrailingZero);
    SaveRl (UNITS_ATR_PREFIXES[i] + 'CusSz', CustUnitSize);
    SaveStr (UNITS_ATR_PREFIXES[i] + 'CusDp', string(CustUnitDisplay));
    SaveStr (UNITS_ATR_PREFIXES[i] + 'UntDp', string(DecUnitDisplay));
    SaveInt (UNITS_ATR_PREFIXES[i] + 'Fract', Ord(Fractions));
  end;
END;

PROCEDURE LoadDimSettings;
VAR
  i, j : integer;
BEGIN
  for i := UNITS_LBL to UNITS_CLIP do
    with l^.DimensionSettings[i] do begin
      //DimTyp := DimensionType (GetSvdInt (UNITS_ATR_PREFIXES[i] + 'Type', 0));
      j := GetSvdInt (UNITS_ATR_PREFIXES[i] + 'Type', -1);
      if j = -1 then begin
        case i of
          UNITS_LBL : DimTyp := Current;
          UNITS_RPT : DimTyp := SameAsLbl
          else        DimTyp := SameAsRpt;
        end;
      end
      else
        DimTyp := DimensionType (j);

      if DimTyp = SameAsLbl then begin
        Rounding := l^.DimensionSettings[UNITS_LBL].Rounding;
        DecPlaces := l^.DimensionSettings[UNITS_LBL].DecPlaces;
        RemTrailingZero := l^.DimensionSettings[UNITS_LBL].RemTrailingZero;
        CustUnitSize := l^.DimensionSettings[UNITS_LBL].CustUnitSize;
        CustUnitDisplay := l^.DimensionSettings[UNITS_LBL].CustUnitDisplay;
        DecUnitDisplay := l^.DimensionSettings[UNITS_LBL].DecUnitDisplay;
        Fractions := l^.DimensionSettings[UNITS_LBL].Fractions;
      end
      else if DimTyp = SameAsRpt then begin
        Rounding := l^.DimensionSettings[UNITS_RPT].Rounding;
        DecPlaces := l^.DimensionSettings[UNITS_RPT].DecPlaces;
        RemTrailingZero := l^.DimensionSettings[UNITS_RPT].RemTrailingZero;
        CustUnitSize := l^.DimensionSettings[UNITS_RPT].CustUnitSize;
        CustUnitDisplay := l^.DimensionSettings[UNITS_RPT].CustUnitDisplay;
        DecUnitDisplay := l^.DimensionSettings[UNITS_RPT].DecUnitDisplay;
        Fractions := l^.DimensionSettings[UNITS_RPT].Fractions;
      end
      else begin
        Rounding := RoundingType (GetSvdInt (UNITS_ATR_PREFIXES[i] + 'Round', 1));
        DecPlaces := byte (GetSvdInt(UNITS_ATR_PREFIXES[i] +  'DecPl', 3));
        RemTrailingZero := GetSvdBln (AtrName(UNITS_ATR_PREFIXES[i] + 'Rem0'), true);
        CustUnitSize := GetSvdRl(UNITS_ATR_PREFIXES[i] + 'CusSz', 1152);
        CustUnitDisplay := GetSvdStr (UNITS_ATR_PREFIXES[i] + 'CusDp', 'Custom');
        DecUnitDisplay := GetSvdStr (UNITS_ATR_PREFIXES[i] + 'UntDp', '');
        Fractions := FractionType (GetSvdInt (UNITS_ATR_PREFIXES[i] + 'Fract', 0));
      end;
    end;
END;


CONST
  AREA_ATR_PREFIXES : array [UNITS_LBL .. 3+UNITS_CLIP] of string = ('SpdhArL',
                                                                    'SpdhArR',
                                                                    'SpdhArC',
                                                                    'SpdhLAr',
                                                                    'SpdhRAr',
                                                                    'SpdhCAr');
PROCEDURE SaveAreaSettings;
VAR
  i : integer;
BEGIN
  for i := UNITS_LBL to UNITS_CLIP do
    with l^.AreaSettings[MainAreaRec, i] do begin
      SaveInt (AREA_ATR_PREFIXES[i] + 'Type', + Ord (AreaTyp));
      SaveInt (AREA_ATR_PREFIXES[i] + 'Round', Ord(Rounding));
      SaveInt (AREA_ATR_PREFIXES[i] + 'DecPl', DecPlaces);
      SaveBln (AtrName(AREA_ATR_PREFIXES[i] + 'Rem0'), RemTrailingZero);
      SaveRl (AREA_ATR_PREFIXES[i] + 'CusSz', CustUnitSize);
      SaveStr (AREA_ATR_PREFIXES[i] + 'CusDp', string(CustUnitDisplay));
      SaveStr (AREA_ATR_PREFIXES[i] + 'UntDp', string(DecUnitDisplay));
      SaveInt (AREA_ATR_PREFIXES[i] + 'Fract', Ord(Fractions));
    end;
  for i := UNITS_LBL to UNITS_CLIP do
    with l^.AreaSettings[AltAreaRec, i] do begin
      SaveInt (AREA_ATR_PREFIXES[i+3] + 'Type', + Ord (AreaTyp));
      SaveInt (AREA_ATR_PREFIXES[i+3] + 'Round', Ord(Rounding));
      SaveInt (AREA_ATR_PREFIXES[i+3] + 'DecPl', DecPlaces);
      SaveBln (AtrName(AREA_ATR_PREFIXES[i+3] + 'Rem0'), RemTrailingZero);
      SaveRl (AREA_ATR_PREFIXES[i+3] + 'CusSz', CustUnitSize);
      SaveStr (AREA_ATR_PREFIXES[i+3] + 'CusDp', string(CustUnitDisplay));
      SaveStr (AREA_ATR_PREFIXES[i+3] + 'UntDp', string(DecUnitDisplay));
      SaveInt (AREA_ATR_PREFIXES[i+3] + 'Fract', Ord(Fractions));
    end;
END;

PROCEDURE LoadAreaSettings;
VAR
  i, j : integer;
  ss : shortstring;
  DfltAreaTyp : AreaType;
BEGIN
  if PGSaveVar^.scaletype in [0{Architectural}, 1{Engineering}, 2{Decimal},
                              4{Inches Fraction}, 5{Inches Decimal}] then
    DfltAreaTyp := SqFt
  else {3=Meters, 6=cm, 7=mm, 8=Metric, 9=DIN, 10=AS 1100}
    DfltAreaTyp := SqMtr;

  for i := UNITS_LBL to UNITS_CLIP do
    with l^.AreaSettings[MainAreaRec, i] do begin
      //AreaTyp := AreaType (GetSvdInt (AREA_ATR_PREFIXES[i] + 'Type', ord(DfltAreaTyp)));
      j := GetSvdInt (AREA_ATR_PREFIXES[i] + 'Type', -1);
      if j = -1 then begin
        case i of
          UNITS_LBL : AreaTyp := DfltAreaTyp;
          UNITS_RPT : AreaTyp := AreaSameLbl
          else AreaTyp := AreaSameRpt;
        end;
      end
      else
        AreaTyp := AreaType(j);

      if AreaTyp = AreaSameLbl then begin
        Rounding := l^.AreaSettings[MainAreaRec, UNITS_LBL].Rounding;
        DecPlaces := l^.AreaSettings[MainAreaRec, UNITS_LBL].DecPlaces;
        RemTrailingZero := l^.AreaSettings[MainAreaRec, UNITS_LBL].RemTrailingZero;
        CustUnitSize := l^.AreaSettings[MainAreaRec, UNITS_LBL].CustUnitSize;
        CustUnitDisplay := l^.AreaSettings[MainAreaRec, UNITS_LBL].CustUnitDisplay;
        Fractions := l^.AreaSettings[MainAreaRec, UNITS_LBL].Fractions;
        DecUnitDisplay := l^.AreaSettings[MainAreaRec, UNITS_LBL].DecUnitDisplay;
      end
      else if AreaTyp = AreaSameRpt then begin
        Rounding := l^.AreaSettings[MainAreaRec, UNITS_RPT].Rounding;
        DecPlaces := l^.AreaSettings[MainAreaRec, UNITS_RPT].DecPlaces;
        RemTrailingZero := l^.AreaSettings[MainAreaRec, UNITS_RPT].RemTrailingZero;
        CustUnitSize := l^.AreaSettings[MainAreaRec, UNITS_RPT].CustUnitSize;
        CustUnitDisplay := l^.AreaSettings[MainAreaRec, UNITS_RPT].CustUnitDisplay;
        Fractions := l^.AreaSettings[MainAreaRec, UNITS_RPT].Fractions;
        DecUnitDisplay := l^.AreaSettings[MainAreaRec, UNITS_RPT].DecUnitDisplay;
      end
      else begin
        Rounding := RoundingType (GetSvdInt (AREA_ATR_PREFIXES[i] + 'Round', 1));
        DecPlaces := byte (GetSvdInt(AREA_ATR_PREFIXES[i] +  'DecPl', 3));
        RemTrailingZero := GetSvdBln (AtrName(AREA_ATR_PREFIXES[i] + 'Rem0'), true);
        CustUnitSize := GetSvdRl(AREA_ATR_PREFIXES[i] + 'CusSz', 1327104);
        CustUnitDisplay := GetSvdStr (AREA_ATR_PREFIXES[i] + 'CusDp', 'Custom');
        Fractions := FractionType (GetSvdInt (AREA_ATR_PREFIXES[i] + 'Fract', 0));

        // set default unit display
        case AreaTyp of
          SqFt        : ss := 'S.F.';
          Squares     : ss := 'Sqr';
          Acres       : ss := 'Acre';
          SqMtr       : ss := 'm2';
          Hectares    : ss := 'ha';
          CustomArea  : ss := '';
          AreaSameLbl : ss := l^.AreaSettings[MainAreaRec, UNITS_LBL].DecUnitDisplay;
          AreaSameRpt : ss := l^.AreaSettings[MainAreaRec, UNITS_RPT].DecUnitDisplay
          else ss := '';
        end;
        DecUnitDisplay := GetSvdStr (AREA_ATR_PREFIXES[i] + 'UntDp', ss);
      end;
    end;

  for i := UNITS_LBL to UNITS_CLIP do
    with l^.AreaSettings[AltAreaRec, i] do begin
      //AreaTyp := AreaType (GetSvdInt (AREA_ATR_PREFIXES[i+3] + 'Type', ord(DfltAreaTyp)));
      j := GetSvdInt (AREA_ATR_PREFIXES[i+3] + 'Type', -1);
      if j = -1 then begin
        case i of
          UNITS_LBL : AreaTyp := DfltAreaTyp;
          UNITS_RPT : AreaTyp := AreaSameLbl
          else AreaTyp := AreaSameRpt;
        end;
      end
      else
        AreaTyp := AreaType(j);

      if AreaTyp = AreaSameLbl then begin
        Rounding := l^.AreaSettings[AltAreaRec, UNITS_LBL].Rounding;
        DecPlaces := l^.AreaSettings[AltAreaRec, UNITS_LBL].DecPlaces;
        RemTrailingZero := l^.AreaSettings[AltAreaRec, UNITS_LBL].RemTrailingZero;
        CustUnitSize := l^.AreaSettings[AltAreaRec, UNITS_LBL].CustUnitSize;
        CustUnitDisplay := l^.AreaSettings[AltAreaRec, UNITS_LBL].CustUnitDisplay;
        Fractions := l^.AreaSettings[AltAreaRec, UNITS_LBL].Fractions;
        DecUnitDisplay := l^.AreaSettings[AltAreaRec, UNITS_LBL].DecUnitDisplay;
      end
      else if AreaTyp = AreaSameRpt then begin
        Rounding := l^.AreaSettings[AltAreaRec, UNITS_RPT].Rounding;
        DecPlaces := l^.AreaSettings[AltAreaRec, UNITS_RPT].DecPlaces;
        RemTrailingZero := l^.AreaSettings[AltAreaRec, UNITS_RPT].RemTrailingZero;
        CustUnitSize := l^.AreaSettings[AltAreaRec, UNITS_RPT].CustUnitSize;
        CustUnitDisplay := l^.AreaSettings[AltAreaRec, UNITS_RPT].CustUnitDisplay;
        Fractions := l^.AreaSettings[AltAreaRec, UNITS_RPT].Fractions;
        DecUnitDisplay := l^.AreaSettings[AltAreaRec, UNITS_RPT].DecUnitDisplay;
      end
      else begin
        Rounding := RoundingType (GetSvdInt (AREA_ATR_PREFIXES[i+3] + 'Round', 1));
        DecPlaces := byte (GetSvdInt(AREA_ATR_PREFIXES[i+3] +  'DecPl', 3));
        RemTrailingZero := GetSvdBln (AtrName(AREA_ATR_PREFIXES[i+3] + 'Rem0'), true);
        CustUnitSize := GetSvdRl(AREA_ATR_PREFIXES[i+3] + 'CusSz', 1327104);
        CustUnitDisplay := GetSvdStr (AREA_ATR_PREFIXES[i+3] + 'CusDp', 'Custom');
        Fractions := FractionType (GetSvdInt (AREA_ATR_PREFIXES[i+3] + 'Fract', 0));

        // set default unit display
        case AreaTyp of
          SqFt        : ss := 'S.F.';
          Squares     : ss := 'Sqr';
          Acres       : ss := 'Acre';
          SqMtr       : ss := 'm2';
          Hectares    : ss := 'ha';
          CustomArea  : ss := '';
          else ss := '';
        end;
        DecUnitDisplay := GetSvdStr (AREA_ATR_PREFIXES[i+3] + 'UntDp', ss);
      end;
    end;
END;

CONST
  VOL_ATR_PREFIXES : array [UNITS_LBL .. UNITS_CLIP] of string = ('SpdhVoL',
                                                                    'SpdhVoR',
                                                                    'SpdhVoC');
PROCEDURE SaveVolSettings;
VAR
  i : integer;
BEGIN
  for i := UNITS_LBL to UNITS_CLIP do with l^.VolSettings[i] do begin
    SaveInt (VOL_ATR_PREFIXES[i] + 'Type', + Ord (VolTyp));
    SaveInt (VOL_ATR_PREFIXES[i] + 'Round', Ord(Rounding));
    SaveInt (VOL_ATR_PREFIXES[i] + 'DecPl', DecPlaces);
    SaveBln (AtrName(VOL_ATR_PREFIXES[i] + 'Rem0'), RemTrailingZero);
    SaveRl (VOL_ATR_PREFIXES[i] + 'CusSz', CustUnitSize);
    SaveStr (VOL_ATR_PREFIXES[i] + 'CusDp', string(CustUnitDisplay));
    SaveStr (VOL_ATR_PREFIXES[i] + 'UntDp', string(DecUnitDisplay));
    SaveInt (VOL_ATR_PREFIXES[i] + 'Fract', Ord(Fractions));
  end;
END;

PROCEDURE LoadVolSettings;
VAR
  i, j : integer;
  DfltVolTyp : VolumeType;
BEGIN
  if PGSaveVar^.scaletype in [0{Architectural}, 1{Engineering}, 2{Decimal},
                              4{Inches Fraction}, 5{Inches Decimal}] then
    DfltVolTyp := CuFt
  else {3=Meters, 6=cm, 7=mm, 8=Metric, 9=DIN, 10=AS 1100}
    DfltVolTyp := Litre;
  for i := UNITS_LBL to UNITS_CLIP do
    with l^.VolSettings[i] do begin
      //VolTyp := VolumeType (GetSvdInt (VOL_ATR_PREFIXES[i] + 'Type', ord(DfltVolTyp)));
      j := GetSvdInt (VOL_ATR_PREFIXES[i] + 'Type', -1);
      if j = -1 then begin
        case i of
          UNITS_LBL : VolTyp := DfltVolTyp;
          UNITS_RPT : VolTyp := VolSameLbl
          else        VolTyp := VolSameRpt;
        end;
      end;

      if VolTyp = VolSameLbl then begin
        Rounding := l^.VolSettings[UNITS_LBL].Rounding;
        DecPlaces := l^.VolSettings[UNITS_LBL].DecPlaces;
        RemTrailingZero := l^.VolSettings[UNITS_LBL].RemTrailingZero;
        CustUnitSize := l^.VolSettings[UNITS_LBL].CustUnitSize;
        CustUnitDisplay := l^.VolSettings[UNITS_LBL].CustUnitDisplay;
        DecUnitDisplay := l^.VolSettings[UNITS_LBL].DecUnitDisplay;
        Fractions := l^.VolSettings[UNITS_LBL].Fractions;
      end
      else if VolTyp = VolSameRpt then begin
        Rounding := l^.VolSettings[UNITS_RPT].Rounding;
        DecPlaces := l^.VolSettings[UNITS_RPT].DecPlaces;
        RemTrailingZero := l^.VolSettings[UNITS_RPT].RemTrailingZero;
        CustUnitSize := l^.VolSettings[UNITS_RPT].CustUnitSize;
        CustUnitDisplay := l^.VolSettings[UNITS_RPT].CustUnitDisplay;
        DecUnitDisplay := l^.VolSettings[UNITS_RPT].DecUnitDisplay;
        Fractions := l^.VolSettings[UNITS_RPT].Fractions;
      end
      else begin
        Rounding := RoundingType (GetSvdInt (VOL_ATR_PREFIXES[i] + 'Round', 1));
        DecPlaces := byte (GetSvdInt(VOL_ATR_PREFIXES[i] +  'DecPl', 3));
        RemTrailingZero := GetSvdBln (AtrName(VOL_ATR_PREFIXES[i] + 'Rem0'), true);
        CustUnitSize := GetSvdRl(VOL_ATR_PREFIXES[i] + 'CusSz', 1152);
        CustUnitDisplay := GetSvdStr (VOL_ATR_PREFIXES[i] + 'CusDp', 'Custom');
        DecUnitDisplay := GetSvdStr (VOL_ATR_PREFIXES[i] + 'UntDp', '');
        Fractions := FractionType (GetSvdInt (VOL_ATR_PREFIXES[i] + 'Fract', 0));
      end;
    end;
END;

PROCEDURE DrawAnchor (UndrawOnly : boolean);
VAR
  SideLen : real;
  i : integer;
  TempEnt, TempEnt1 : entity;
BEGIN
  if (l^.state = State_GetNewX) or (l^.state = State_GetNewY) then begin
    ent_init (tempEnt, entcrc);
    tempEnt.color := clrltred;
    tempEnt.crcRad := pixsize * 5.0;
    for i := 1 to 4 do begin
      tempEnt.crcCent := l^.AnchorBox[i];
      if (i = l^.Anchor) and not UndrawOnly then
        ent_draw (tempent, drmode_white)
      else
        ent_draw (tempent, drmode_black);
    end;
    exit;
  end;
  ent_init (tempEnt1, entply);
  tempEnt1.color := clrRed;
  tempEnt1.plynpnt := 4;
  if not PointsEqual (l^.AnchorBox[1], l^.AnchorBox[2]) then begin   // undraw previous box
    move (l^.AnchorBox, TempEnt1.plypnt, sizeof(l^.AnchorBox));
    ent_draw (tempEnt1, drmode_black);
  end;

  ent_init (tempEnt, entcrc);
  tempEnt.color := clrltred;
  tempEnt.crcCent := l^.Pnt1;
  tempEnt.crcRad := pixsize * 5.0;

  if UndrawOnly then begin
    ent_draw (tempEnt, drmode_black);
    for i := 1 to 4 do
      SetPoint (l^.AnchorBox[i], 0.0);
    exit;
  end
  else
    ent_draw (tempEnt, drmode_white);

  for i := 1 to 4 do
    tempEnt1.plypnt[i] := l^.Pnt1;
  SideLen := 30.0 * pixsize;
  case l^.Anchor of
  1 : begin
        tempEnt1.plypnt[2].x := l.Pnt1.x + SideLen;
        tempEnt1.plypnt[2].y := l.Pnt1.y;
        tempEnt1.plypnt[3].x := tempEnt1.plypnt[2].x;
        tempEnt1.plypnt[3].y := l.Pnt1.y  - SideLen;
        tempEnt1.plypnt[4].x := l.Pnt1.x;
        tempEnt1.plypnt[4].y := tempEnt1.plypnt[3].y;
      end;
  2 : begin
        tempEnt1.plypnt[2].x := l.Pnt1.x - SideLen;
        tempEnt1.plypnt[2].y := l.Pnt1.y;
        tempEnt1.plypnt[3].x := tempEnt1.plypnt[2].x;
        tempEnt1.plypnt[3].y := l.Pnt1.y  - SideLen;
        tempEnt1.plypnt[4].x := l.Pnt1.x;
        tempEnt1.plypnt[4].y := tempEnt1.plypnt[3].y;
      end;
  3 : begin
        tempEnt1.plypnt[2].x := l.Pnt1.x - SideLen;
        tempEnt1.plypnt[2].y := l.Pnt1.y;
        tempEnt1.plypnt[3].x := tempEnt1.plypnt[2].x;
        tempEnt1.plypnt[3].y := l.Pnt1.y  + SideLen;
        tempEnt1.plypnt[4].x := l.Pnt1.x;
        tempEnt1.plypnt[4].y := tempEnt1.plypnt[3].y;
      end;
  4 : begin
        tempEnt1.plypnt[2].x := l.Pnt1.x + SideLen;
        tempEnt1.plypnt[2].y := l.Pnt1.y;
        tempEnt1.plypnt[3].x := tempEnt1.plypnt[2].x;
        tempEnt1.plypnt[3].y := l.Pnt1.y  + SideLen;
        tempEnt1.plypnt[4].x := l.Pnt1.x;
        tempEnt1.plypnt[4].y := tempEnt1.plypnt[3].y;
      end
  else
    for i := 2 to 4 do
      tempEnt1.plypnt[i] := tempEnt1.plypnt[1];
  end;
  if l^.xAxisAngle <> 0.0 then
    ent_rotate (tempEnt1, tempEnt1.plypnt[1], l^.xAxisAngle);
  ent_draw (tempEnt1, drmode_white);
  move (TempEnt1.plypnt, l^.AnchorBox, sizeof(l^.AnchorBox));
END; // DrawAnchor


FUNCTION addr_equal (addr1, addr2 : lgl_addr) : boolean;
BEGIN
  result := (addr1.page = addr2.page) and (addr1.ofs = addr2.ofs);
END;

PROCEDURE RequiredFields (VAR NameRqd, NumRqd : boolean);
VAR
  i : integer;
  s : string;
  sym : sym_type;
  ent : entity;
BEGIN
  NameRqd := false;
  NumRqd := false;

  // check if required for labels
  if l^.LabelFromSymbol then begin
    s := string (GetSvdStr ('SpdhLbSymbol', ''));
    if length (s) > 4 then begin
      delete (s, length(s)-3, 4);
      if symread  (symstr(s), 'tempsym', sym) = fl_OK then begin
        i := 0;
        repeat
          if i=0 then begin
            if not ent_get (ent, sym.frstent) then
              i := 1000;
          end
          else if not ent_get (ent, ent.Next) then
            i := 1000;
          if i < 1000 then begin
            if ent.enttype = enttxt then begin
              if ansipos ('{name}', string(ent.txtstr)) > 0 then begin
                NameRqd := true;
              end;
              if ansipos ('{nbr}', string(ent.txtstr)) > 0 then begin
                NumRqd := true;
              end;
            end;
          end;
          i := i+1;
        until  (i > 1000) or (NameRqd and NumRqd) or addr_equal(ent.addr, sym.lastent);
      end;
    end;
  end
  else begin
    i := 0;
    repeat
      i := i+1;
      s := string (GetSvdStr ('SpdhLine' + inttostr(i) + 'Ptn', ' '));
      if ansipos ('{name}', s) > 0 then begin
        NameRqd := true;
      end;
      if ansipos ('{nbr}', s) > 0 then begin
        NumRqd := true;
      end;
    until (i>4) or (NameRqd and NumRqd);
  end;

  //check if required for report
  for i := 1 to 10 do begin
    s := string (GetSvdStr ('SpdhRptHed' + inttostr(i), ' '));
    if ansipos ('{name}', s) > 0 then begin
      NameRqd := true;
      if NumRqd then
        exit;
    end;
    if ansipos ('{nbr}', s) > 0 then begin
      NumRqd := true;
      if NameRqd then
        exit;
    end;
  end;
END;



FUNCTION PointsEqual (p1, p2 : URecords.point) : boolean;
BEGIN
  result := RealEqual(p1.x , p2.x) and RealEqual(p1.y, p2.y);
END;


PROCEDURE Cleanup;
VAR
  CatDef : CategoryDef;
BEGIN
  unhilite_all (false, true, true);
  SetGridAngle (l^.svGridAng);
  if assigned (l^.Categories) and (l^.Categories.Count > 0) then begin
    for CatDef in l^.Categories do
      if assigned (CatDef.Custom) and (CatDef.Custom.Count > 0) then
        CatDef.Custom.Free;
    l^.Categories.Free;
  end;
  if assigned(l^.UserFields) and (l^.UserFields.Count > 0) then
    l^.UserFields.Free;
END;

FUNCTION EntCenter (ent : entity) : URecords.point;
VAR
  BotLeft, TopRight : URecords.point;
BEGIN
  if ent.enttype = entpln then begin
    pline_centroid (ent.plnFrst, ent.plnlast, BotLeft.x, TopRight, result, true,true)
  end
  else begin
    ent_extent (ent, BotLeft, TopRight);
    meanpnt (BotLeft, TopRight, result);
  end;
END;

FUNCTION Pv_ToVoid (pv : polyvert) : boolean;
VAR
  pPvBulgeRec : ^PolyvertBulgeRec;
BEGIN
  if pv.shape = pv_bulge then
    result := false
  else begin
    pPvBulgeRec := addr (pv.bulge);
    result := Flag (pPvBulgeRec^.Flags, TO_VOID);
  end;
END;


FUNCTION Pv_FromVoid (pv : polyvert) : boolean;
VAR
  pPvBulgeRec : ^PolyvertBulgeRec;
BEGIN
  if pv.shape = pv_bulge then
    result := false
  else begin
    pPvBulgeRec := addr (pv.bulge);
    result := Flag (pPvBulgeRec^.Flags, FROM_VOID);
  end;
END;

FUNCTION Pv_VoidLine (pv : polyvert) : boolean;
BEGIN
  result := Pv_ToVoid(pv) or Pv_FromVoid(pv);
END;

FUNCTION ConvertDimImport (dim : double) : double;
VAR
	dimRec : DimensionsRec;
BEGIN
	result := dim;
	dimRec := l^.DimensionSettings[UNITS_CLIP];
	if dimRec.DimTyp = SameAsRpt then
		dimRec := l^.DimensionSettings[UNITS_RPT];
	if dimRec.DimTyp = SameAsLbl then
		dimRec := l^.DimensionSettings[UNITS_LBL];
	case dimRec.DimTyp of
		Current: begin
				case PGSaveVar^.scaletype of
					0,1,2	: result := dim * onefoot;	//feet
					3,8,9,10	: result := dim * MCONV;		//meters
					4,5	: result := dim * thirtytwo;	//inches
					6:	result := dim * MCONV / 100;	//cm
					7: result := dim * MCONV / 1000;	//mm
				end;
			end;
		FeetInch: result := dim * thirtytwo;	//inches
		DecFt: result := dim * onefoot;	//feet
		Mtr:  result := dim * MCONV;
		Cm: 	result := dim * MCONV / 100;	//cm
		Mm:	result := dim * MCONV / 1000;	//mm
		CustomDim: result := dim * dimRec.CustUnitSize;
	end;

END;

FUNCTION AreaUnitSize (Destination : integer; AreaRec : AreaRecords) : double;
BEGIN
  if l^.AreaSettings[AreaRec, Destination].AreaTyp = AltSameAsMain then
    AreaRec :=  MainAreaRec;
  if l^.AreaSettings[AreaRec, Destination].AreaTyp = AreaSameRpt then
    Destination := UNITS_RPT;
  if l^.AreaSettings[AreaRec, Destination].AreaTyp = AreaSameLbl then
    Destination := UNITS_LBL;

  case l^.AreaSettings[AreaRec, Destination].AreaTyp of
    SqFt: result := SQFTCONV;
    Squares: result := ACRECONV;
    Acres: result := ACRECONV;
    SqMtr: result := SQMCONV;
    Hectares: result := HECTARECONV;
    CustomArea: result := l^.AreaSettings[AreaRec, Destination].CustUnitSize
    else result := 1;
  end;
END;

FUNCTION AreaUnitDisplay (Destination : integer; AreaRec : AreaRecords) : string;
BEGIN
  if l^.AreaSettings[AreaRec, Destination].AreaTyp = AltSameAsMain then
    AreaRec :=  MainAreaRec;
  if l^.AreaSettings[AreaRec, Destination].AreaTyp = AreaSameRpt then
    Destination := UNITS_RPT;
  if l^.AreaSettings[AreaRec, Destination].AreaTyp = AreaSameLbl then
    Destination := UNITS_LBL;

  if l^.AreaSettings[AreaRec, Destination].AreaTyp = CustomArea then
    result := string(l^.AreaSettings[AreaRec, Destination].CustUnitDisplay)
  else
    result := string (l^.AreaSettings[AreaRec, Destination].DecUnitDisplay);
END;


FUNCTION VolUnitSize (Destination : integer) : double;
BEGIN
  if l^.VolSettings[Destination].VolTyp = VolSameRpt then
    Destination := UNITS_RPT;
  if l^.VolSettings[Destination].VolTyp = VolSameLbl then
    Destination := UNITS_LBL;

  case l^.VolSettings[Destination].VolTyp of
    CuFt: result := FT3CONV;
    CuYard: result := YD3CONV;
    Litre: result := LITRCONV;
    CuMtr: result := M3CONV;
    CustomVol: result := l^.VolSettings[Destination].CustUnitSize;
    else result := 1;
  end;
END;

FUNCTION DimUnitSize (Destination : integer) : double;
BEGIN
  result := 1;
  if l^.DimensionSettings[Destination].DimTyp = SameAsRpt then
    Destination := UNITS_RPT;
  if l^.DimensionSettings[Destination].DimTyp = SameAsLbl then
    Destination := UNITS_LBL;

  case l^.DimensionSettings[Destination].DimTyp of
    Current:
      case PGSaveVar^.scaletype of
        0,1,2	: result := onefoot;	//feet
        3,8,9,10	: result :=  MCONV;		//meters
        4,5	: result :=  thirtytwo;	//inches
        6:	result := MCONV / 100;	//cm
        7: result := MCONV / 1000;	//mm
      end;
    FeetInch, DecFt : result := onefoot;
    Mtr : result := MCONV;
    Cm : result := MCONV/100;
    Mm : result := MCONV/1000;
    CustomDim: result := l^.DimensionSettings[Destination].CustUnitSize
    else result := 1;
  end;
END;

FUNCTION VolUnitDisplay (Destination : integer) : string;
BEGIN
  if l^.VolSettings[Destination].VolTyp = VolSameRpt then
    Destination := UNITS_RPT;
  if l^.VolSettings[Destination].VolTyp = VolSameLbl then
    Destination := UNITS_LBL;

  if l^.VolSettings[Destination].VolTyp = CustomVol then
    result := string(l^.VolSettings[Destination].CustUnitDisplay)
  else
    result := string (l^.VolSettings[Destination].DecUnitDisplay);
END;

FUNCTION DisUnitDisplay (Destination : integer) : string;
BEGIN
  if l^.DimensionSettings[Destination].DimTyp = SameAsRpt then
    Destination := UNITS_RPT;
  if l^.DimensionSettings[Destination].DimTyp = SameAsLbl then
    Destination := UNITS_LBL;

  if l^.DimensionSettings[Destination].DimTyp = CustomDim then
    result := string(l^.DimensionSettings[Destination].CustUnitDisplay)
  else
    result := string (l^.DimensionSettings[Destination].DecUnitDisplay);
END;

FUNCTION ActualTextSize (siz : double) : double;
VAR
  s : shortstring;
  scale : double;
BEGIN
  if PGSavevar.txtuseplt then begin
    scale_get (PGSavevar.plt.scalei , scale, s);
    result := siz / scale;
  end
  else
    result := siz;
END;

FUNCTION DefaultTxtSize : double;
VAR
  scale : double;
  s : shortstring;
BEGIN
  result := pgSaveVar^.txtsiz;
  if PGSavevar.txtuseplt then begin
    scale_get (PGSavevar.plt.scalei , scale, s);
    result := result*scale;
  end;
END;

PROCEDURE CheckTxtScale;
// it is possible for the user to change the txtscale setting without the macro being
// aware of it (by pressing Alt/T to go to the Text Menu, then exiting back to the macro)
// so this procedure should always be called before functionality that creates text.
VAR
  tempstr : shortstring;
  scale, height : double;
  settingname : string;
  i : integer;
  TxtScale : boolean;
BEGIN
  TxtScale := GetSvdBln ('SpdhTxtScale', false);
  if TxtScale xor PGSavevar.txtuseplt then begin
    scale_get (PGSavevar.plt.scalei, scale, tempstr);
    for i := 1 to 11 do begin
      settingname := 'SpdhSl' + inttostr(i) + 'Siz';
      height := GetSvdRl (settingname, 0);
      if height <> 0 then begin
        if PGSavevar.txtuseplt then
          height := height * scale
        else
          height := height / scale;
        SaveRl(settingname, height);
      end;
    end;
    SaveBln ('SpdhTxtScale', PGSavevar.txtuseplt);
  end;
END;



PROCEDURE PopulateLayerList (MsgNdx : integer; var ComboList : TComboBox);
VAR
  s : string;
  ss : shortstring;
  templyr : lyraddr;
  rlyr : RLayer;
BEGIN
  ComboList.Items.Clear;
  if MsgNdx > 0 then begin
    GetMsg(MsgNdx, s);
    ComboList.Items.Add(s);
  end;
  tempLyr := lyr_first;
  while not isnil (templyr) do begin
    lyr_get (templyr, rlyr);
    if rlyr.group = -1 then begin // layer is not locked
      getLongLayerName(tempLyr, ss);
      ComboList.Items.Add(string(ss));
    end;
    tempLyr := lyr_next(tempLyr);
  end;
END;



FUNCTION ColourName (clr : longint) : shortstring;
VAR
  RBGClr : TColor;
BEGIN
  if (clr > 0) and (clr < 256) then
    clrGetName (clr, result)
  else if clr = 0 then
    result := ''
  else begin
    RBGClr := DCADRGBtoRGB(clr);
    //result := shortstring (ColorToString (DCADRGBtoRGB(clr)));
    result := shortstring (IntToStr(GetRValue(RBGClr)) + '/' +
                           IntToStr(GetGValue(RBGClr)) + '/' +
                           IntToStr(GetBValue(RBGClr)));
  end;
END;


PROCEDURE FillClrPnlCaption (pnl : TPanel; clr : longint);
VAR
  s : shortstring;
BEGIN
  if clr > 0 then begin
    s := ColourName (clr);
    pnl.Color := DCADRGBtoRGB (clr);
  end
  else begin
    s := shortstring(GetLbl(314)); //(not set)
    pnl.Color := DCADRGBtoRGB (CurrentState^.color);
  end;
  PanelLblParams (106, [s], pnl);  //Fill Clr: $|Select Fill Colour
  pnl.Font.Color := PnlFontClr (pnl.Color);
END;


PROCEDURE DrawIfOn (ent : entity; drMode : integer);
BEGIN
  if lyr_ison (ent.lyr) then
    ent_draw (ent, drMode);
END;

end.

