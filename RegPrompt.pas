unit RegPrompt;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Language, ShellAPI;

type
  TRegistationPrompt = class(TForm)
    Memo1: TMemo;
    btnOK: TButton;
    btnCancel: TButton;
    chbNotAgain: TCheckBox;
    procedure btnOKClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;


implementation

{$R *.dfm}

procedure TRegistationPrompt.btnCancelClick(Sender: TObject);
begin
  if chbNotAgain.Checked then
    ModalResult := mrClose
  else
    ModalResult := mrCancel;
end;

procedure TRegistationPrompt.btnOKClick(Sender: TObject);
begin
  ShellExecute(self.WindowHandle,'open','https://www.dhsoftware.com.au/contactreg.htm',nil,nil, SW_SHOWNORMAL);
  ModalResult := mrOK;
end;

procedure TRegistationPrompt.FormCreate(Sender: TObject);
begin
  CheckCaption (359, chbNotAgain);
  ButtonCaption(15, btnCancel);
  ButtonCaption (360, btnOK);
  (Sender as TForm).Caption := GetLbl (361);
end;

end.
