unit Catgries;


{-------------------------------------------------------------------------------
 Category Definitions are stored in the drawing file as attributes.
 There is a standard string attribute where the string ontains the fields
 defined below:
 ORIGINAL VERSION OF THIS MACRO (may still be legacy records):
  Name : 'SpdhCatXX'  (where XX is encoded category number 0 .. 4488)
  Fields :    Category Name : string[25]
              DoFill        : boolean
              Fill Color    : longint
              CeilingType   : CeilingTypes (enumerated - defined in CommonStuff)
              Height1       : double
              Height2       : double
              Height3       : double
              Slope1        : double
              Slope2        : double
              UseZHt        : boolean
              FillOpacity   : byte

  CURRENT VERSION OF THIS MACRO:
  Name : 'SdhCatXX'  (where XX is encoded category number 0 .. 4488)
  Fields :    version     : byte;
              UseZHt      : boolean;
              DoFill      : boolean;
              FillOpacity : byte;
              FillColor   : longint;
              Height1     : double;
              Height2     : double;
              Height3     : double;
              Slope1      : double;
              Slope2      : double;
              name        : str25;
              CeilingType : CeilingTypes;


 In addition, there is a separate attribute for each non-default user defined
 field:
  Name : 'SdhCXXTTTTTT' where XX is encoded category number
                              TTTTTT is the tag name for the field
  atrtype : specifies the type of user field, and the value is in the
            appropriate field for the type

-------------------------------------------------------------------------------}

interface

uses SysUtils, StrUtils, CommonStuff, Vcl.StdCtrls, URecords, UInterfaces,
     System.Generics.Collections, System.Generics.Defaults, UConstants,
     Vcl.Graphics, CeilingOptions, UDCLibrary, NewCatgry, Vcl.Forms, Controls;

procedure createCategories ();
function EncodeCatNum (num : integer): str2;
//function DecodeCatNum (numstr : str2): integer;
function GetCatIndex (const Num : str2; var ndx : integer) : boolean;
procedure PopulateCategories (var cbCat : TComboBox);
procedure AddCategory (name : string; doFill : boolean;
                       FillClr : TColor; FillOpacity : byte;
                       UseZforCeiling : boolean; CeilingHeight : double;
                       isCustomCeiling : boolean; SaveAttrib : boolean);   overload;
procedure AddCategory (name : string; doFill : boolean; FillClr : TColor;
                       FillOpacity : byte; fCeilingOptions : TfCeilingOptions);   overload;
procedure SaveCategory  (ndx : integer);
procedure SaveAllCategory;

procedure PopulateCeilingOptions (ndx : integer;
                                  var fCeilingOptions : TfCeilingOptions);
procedure UpdateCatInfo (ndx : integer;
                         doFill : boolean;
                         //FillClr : TColor;
                         fCeilingOptions : TfCeilingOptions);

function AddCategoryViaForm (CatName : string;
                             UserFieldValues  : TList<UserFieldValue>;
                             var catndx : integer) : boolean;

function FindCategory (CatName : string) : integer;

implementation


const
  CatVersion = 1;




procedure createCategories ();
var
  Comparison: TComparison<CategoryDef>;
begin
  Comparison :=
        function(const Left, Right: CategoryDef): Integer
        begin
          Result := CompareText (string(Left.standard.Data.name), string(Right.standard.Data.name));
        end;

  l^.Categories := TList<CategoryDef>.Create(TComparer<CategoryDef>.Construct(Comparison));
end;

function EncodeCatNum (num : integer): str2;
var i : integer;
begin
  result := '  ';
  if (num < 0) or (num > 4488) then begin
    exit;
  end
  else begin
    i := num div 67;
    result := str2(chars[i]);
    i := num mod 67;
    result := result + str2(chars[i]);
  end;
end;

procedure PopulateCategories (var cbCat : TComboBox);
var
  i : integer;
  s : string;
begin
  s := cbCat.Text;
  cbCat.Items.Clear;
  if l^.Categories.Count > 0 then begin
    for i := 0 to l^.Categories.Count-1 do
      cbCat.Items.Add (string(l^.Categories[i].Standard.Data.name));
//    cbCat.ItemIndex := 0;
//  cbCat.Text := s;
    for i := 0 to cbCat.Items.Count-1 do begin
      if cbCat.Items[i] = s then begin
        cbCat.ItemIndex := i;
        exit;
      end;
    end;
  end;
end;

function GetCatIndex (const Num : str2; var ndx : integer) : boolean;
var
  i : integer;
begin
  result := false;
  ndx := -1;
  if l^.Categories.Count = 0 then
    exit;
  { simple linear search - Categories is not sorted on num, and not
    expecting there to be a large number of categories }
  for i := 0 to l^.Categories.Count-1 do begin
    if l^.Categories.Items[i].num = Num then begin
      ndx := i;
      result := true;
      exit;
    end;
  end;
end;

function NextUnusedNumber : str2;
var
  i, j : integer;
  atr : attrib;
  unused : boolean;
begin
  for i := 0 to 4488 do begin
    result := EncodeCatNum(i);
    if not atr_sysfind ('SdhCat' + result, atr) then begin
      unused := true;
      for j := 0 to l^.Categories.Count-1 do
        if l^.Categories[j].num = result then
          unused := false;
      if unused then exit;
    end;
  end;
  result := '';
end;

function FindCategory (CatName : string) : integer;
var
  i : integer;
begin
  if length(CatName) > 0 then for i := 0 to l^.Categories.Count-1 do begin
    if UpperCase (string(l^.Categories.Items[i].Standard.data.name)) = UpperCase (CatName) then begin
      result := i;
      exit;
    end;
  end;
  result := -1;
end;

function AddCategoryViaForm (CatName : string;
                             UserFieldValues  : TList<UserFieldValue>;
                             var catndx : integer) : boolean;
var
  i, j : integer;
  fNewCategory: TfNewCategory;
  NewCatDef : CategoryDef;
  atr : attrib;
  UserFieldVal : UserFieldValue;
  aName : atrname;
begin
  result := true;
  catndx := FindCategory (CatName);
  if catndx >= 0 then
    exit;

  AddCategory (CatName, false, DCADRGBtoRGB (1), 100, true, 0, false, false);
  for i := 0 to l^.Categories.Count-1 do begin
    if UpperCase (string(l^.Categories.Items[i].Standard.Data.name)) = UpperCase (CatName) then begin
      catndx := i;
      fNewCategory := TfNewCategory.Create(Application);
      try
        fNewCategory.lblCatName.Caption := CatName;
        fNewCategory.fCeilingOptions.chbUseZ.Checked := l^.Categories[i].Standard.Data.UseZHt;
        fNewCategory.fCeilingOptions.dcCeilingHeight.NumValue := l^.Categories[i].Standard.Data.Height1;
        fNewCategory.fCeilingOptions.cbCeilingType.ItemIndex := 0;
        fNewCategory.CreateCatCustFields (UserFieldValues);


        if fNewCategory.showmodal = mrOK then begin
          NewCatDef := l^.Categories.Items[catndx];
          NewCatDef.Standard.Data.DoFill := fNewCategory.chbCatSolidFill.Checked;
          NewCatDef.Standard.Data.FillColor := RGBtoDCADRGB (fNewCategory.pCatColour.Color);

          NewCatDef.Standard.Data.UseZHt := fNewCategory.fCeilingOptions.chbUseZ.Checked;
          NewCatDef.Standard.Data.Height1 := fNewCategory.fCeilingOptions.dcCeilingHeight.NumValue;

          if length(fNewCategory.CatCustFields) > 0 then begin
            NewCatDef.Custom := TList<UserFieldValue>.Create;
            for j := 0 to length(fNewCategory.CatCustFields)-1 do begin
              UserFieldVal.tag := l^.userfields[j].tag;
              UserFieldVal.numeric := l^.UserFields[j].numeric;
              aName := 'SdhC' + NewCatDef.num + UserFieldVal.tag;
              case UserFieldVal.numeric of
                str: begin
                       UserFieldVal.str := str25(fNewCategory.CatCustFields[j].Input.text);
                       atr_init (atr, atr_str);
                       atr.name := aName;
                       atr.str := UserFieldVal.str;
                     end;
                int: begin
                       UserFieldVal.int := fNewCategory.CatCustFields[j].Input.IntValue;
                       atr_init (atr, atr_int);
                       atr.name := aName;
                       atr.int := UserFieldVal.int;
                     end;
                rl:  begin
                       UserFieldVal.rl := fNewCategory.CatCustFields[j].Input.NumValue;
                       atr_init (atr, atr_rl);
                       atr.name := aName;
                       atr.rl := UserFieldVal.rl;
                     end;
                dis: begin
                       UserFieldVal.dis := fNewCategory.CatCustFields[j].Input.NumValue;
                       atr_init (atr, atr_dis);
                       atr.name := aName;
                       atr.dis := UserFieldVal.dis;
                     end;
                ang: begin
                       UserFieldVal.ang := fNewCategory.CatCustFields[j].Input.NumValue;
                       atr_init (atr, atr_ang);
                       atr.name := aName;
                       atr.ang := UserFieldVal.ang;
                     end;
                UserFieldType.area: begin
                       UserFieldVal.area := fNewCategory.CatCustFields[j].Input.NumValue;
                       atr_init (atr, atr_Area);
                       atr.name := aName;
                       atr.rl := UserFieldVal.area;
                     end;
              end;
              NewCatDef.Custom.Add(UserFieldVal);

              // save attribute for non-default values
              if not fNewCategory.CatCustFields[j].Dft.Checked then begin
                atr_add2sys (atr);
              end;
            end;
          end;

          l^.Categories.Items[catndx] := NewCatDef;
          atr_init (atr, atr_str);
          atr.name := 'SdhCat' + NewCatDef.num;
          atr.str := NewCatDef.Standard.str;
          atr_add2sys (atr);

        end;
      finally
        fNewCategory.Free;
      end;

      exit;
    end;
  end;
  catndx := -1;
  result := false;
end;




procedure AddCategory (name : string; doFill : boolean;
                       FillClr : TColor; FillOpacity : byte;
                       UseZforCeiling : boolean; CeilingHeight : double;
                       isCustomCeiling : boolean; SaveAttrib : boolean);
var
  cat : CategoryDef;
  atr : attrib;
begin
  Cat.num := NextUnusedNumber;

  if Cat.num='' then begin
    beep;
    exit;
  end;

  SetLength (Cat.Standard.str, 80);
  Cat.standard.Data.version := CatVersion;
  Cat.standard.Data.name := str25(name);
  Cat.standard.Data.doFill := doFill;
  cat.Standard.Data.FillColor := FillClr;
  cat.Standard.Data.FillOpacity := FillOpacity;

  if isCustomCeiling then
    cat.Standard.Data.CeilingType := Custom
  else
    cat.Standard.Data.CeilingType := flat;
  cat.Standard.Data.Height1 := CeilingHeight;
  cat.Standard.Data.UseZHt := UseZforCeiling;

  cat.Custom := TList<UserFieldValue>.Create();

  if SaveAttrib then begin
    atr_init (atr, atr_str);
    atr.name := 'SdhCat' + cat.num;
    atr.str := cat.Standard.str;
    atr_add2sys (atr);
    cat.Updated := false;
  end
  else
    cat.updated := true;

  l^.Categories.Add(cat);
  l^.Categories.Sort;

end;


procedure AddCategory (name : string; doFill : boolean; FillClr : TColor;
                       FillOpacity : byte; fCeilingOptions : TfCeilingOptions);
/// note : this procedure does NOT add an attribute
///        (just updates memory list and sets Updated flag to true)
var
  cat : CategoryDef;
begin
  Cat.num := NextUnusedNumber;

  if Cat.num='' then begin
    beep;
    exit;
  end;

  SetLength (Cat.Standard.str, 80);
  Cat.standard.Data.version := CatVersion;
  Cat.standard.Data.name := str25(name);
  Cat.standard.Data.doFill := doFill;
  cat.Standard.Data.FillColor := FillClr;
  cat.Standard.Data.FillOpacity := FillOpacity;
  if fCeilingOptions = nil then begin
    cat.Standard.Data.CeilingType := flat;
    cat.Standard.Data.UseZHt := true;
  end
  else begin
    case fCeilingOptions.cbCeilingType.ItemIndex of
      0 : begin
            cat.Standard.Data.CeilingType := flat;
            cat.Standard.Data.Height1 := fCeilingOptions.dcCeilingHeight.NumValue;
          end;
      1 : begin
            cat.Standard.Data.CeilingType := Custom;
          end;
    end;

    cat.Standard.Data.UseZHt := RealEqual (fCeilingOptions.dcCeilingHeight.NumValue, zhite-zbase);
  end;

  cat.Custom := TList<UserFieldValue>.Create();
  cat.Updated := true;

  l^.Categories.Add(cat);
  l^.Categories.Sort;
end;

procedure UpdateCatInfo (ndx : integer;
                         doFill : boolean;
                         fCeilingOptions : TfCeilingOptions);
var
  cat : CategoryDef;
begin
  cat := l^.Categories[ndx];
  SetLength (Cat.Standard.str, 80);
  Cat.standard.Data.version := CatVersion;
  cat.Standard.Data.Name := l^.Categories[ndx].Standard.Data.Name;
  cat.Standard.Data.DoFill := doFill;

  case fCeilingOptions.cbCeilingType.ItemIndex of
    0 : begin
          cat.Standard.Data.CeilingType := flat;
          cat.Standard.Data.Height1 := fCeilingOptions.dcCeilingHeight.NumValue;
          cat.Standard.Data.UseZHt := fCeilingOptions.chbUseZ.Checked;
        end;

    1 : begin
          cat.Standard.Data.CeilingType := Custom;
        end;
  end;

  cat.updated := true;
  l^.Categories[ndx] := cat;
end;

procedure SaveCategory (ndx : integer);
var
  atr_name : atrname;
  atr : attrib;
  cat : CategoryDef;
begin
  Cat := l^.Categories[ndx];
  atr_name := 'SdhCat'+ Cat.num;
  if atr_sysfind (atr_Name, atr) then begin
    atr.str := Cat.Standard.Str;
    atr_update (atr);
  end
  else begin
    atr_init (atr, atr_str);
    atr.name := 'SdhCat'+ Cat.num;
    atr.str := Cat.Standard.Str;
    atr_add2sys (atr);
  end;
  Cat.updated := false;
  l^.Categories[ndx] := Cat;
end;

procedure SaveAllCategory;
var
  i : integer;
begin
  for i := 0 to l^.Categories.Count-1 do begin
  if l^.Categories[i].updated then begin
    SaveCategory (i);
  end;
end;
end;

procedure PopulateCeilingOptions (ndx : integer;
                                  var fCeilingOptions : TfCeilingOptions);
var
  defaultHeight : double;
begin
  defaultHeight := zhite - zbase;
  with  fCeilingOptions do begin
    // set each tab in pcCeiling to default values
    dcCeilingHeight.NumValue := defaultHeight;
    dcCeilingHeightSingle.NumValue := defaultHeight;
    dcCeilingHeightDbl1.NumValue := defaultHeight;
    dcCeilingHeightDbl2.NumValue := defaultHeight;
    rbSlopes.Checked := true;
    dcDoubleSlopeHeight.text := '';
    dcnCeilingHeightArch1.NumValue := defaultHeight;
    dcnCeilingHeightArch2.NumValue := defaultHeight;
    rbArchSemiCircle.Checked := true;
    dcnArchRadius.Text := '';
    dcnArchTopHeight.Text := '';

    if (ndx < 0) or (ndx > l^.Categories.Count-1) then begin
      cbCeilingType.ItemIndex := 0;
      pcCeiling.ActivePage := tsFlat;
      exit; // no category currently set
    end;

    // set the correct active tab, and set its values to those for the selected category
    with l^.Categories[ndx].Standard.Data do begin
      case CeilingType of
        flat:
          begin
            cbCeilingType.ItemIndex := 0;
            pcCeiling.ActivePage := tsFlat;
            dcCeilingHeight.NumValue := Height1;
          end;

        Custom:
          begin
            cbCeilingType.ItemIndex := 1;
            pcCeiling.ActivePage := tsCustom;
          end;
      end;

      chbUseZ.Checked := UseZHt;
    end;
  end;
end;


end.
