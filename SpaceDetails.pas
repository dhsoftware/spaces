unit SpaceDetails;

interface

uses
  URecords, Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
  Vcl.ExtCtrls, Vcl.Menus, DcadNumericEdit,CommonStuff, UInterfaces, Settings,
  Vcl.ComCtrls, Math, Vcl.Imaging.pngimage, DcadEdit, Catgries, ClrUtil, CeilingOptions,
  StrUtil, System.UITypes, Language, LayerUtil, UConstants, Constants,
  Vcl.Samples.Spin, Version;

type
  CatCustField = record
    Lbl : TLabel;
    Input : TDcadEdit;
    tag : str6;
  end;

  TfmSpaceProperties = class(TForm)
    lblName: TLabel;
    cbName: TComboBox;
    lblNumber: TLabel;
    edNumber: TEdit;
    lblCategory: TLabel;
    cbCategory: TComboBox;
    chbSolidFill: TCheckBox;
    MainMenu1: TMainMenu;
    Options1: TMenuItem;
    CustomFillColours1: TMenuItem;
    lblFloorZ: TLabel;
    dcFloorZ: TDcadNumericEdit;
    btnOK: TButton;
    btnCancel: TButton;
    Colour1: TPanel;
    Colour2: TPanel;
    Colour3: TPanel;
    Colour4: TPanel;
    Colour5: TPanel;
    Colour6: TPanel;
    Colour7: TPanel;
    Colour8: TPanel;
    Colour9: TPanel;
    Colour10: TPanel;
    Colour11: TPanel;
    Colour12: TPanel;
    Colour13: TPanel;
    Colour14: TPanel;
    Colour15: TPanel;
    Colour16: TPanel;
    Colour17: TPanel;
    Colour18: TPanel;
    Colour19: TPanel;
    Colour20: TPanel;
    Colour21: TPanel;
    Colour22: TPanel;
    sbCatUserFields: TScrollBox;
    lblUserFieldName1: TLabel;
    lblUserFieldName2: TLabel;
    dceUserFieldValue1: TDcadEdit;
    dceUserFieldValue2: TDcadEdit;
    pCatCeiling: TPanel;
    NameALLCAPS1: TMenuItem;
    NumberALLCAPS1: TMenuItem;
    chbOutline: TCheckBox;
    Panel1: TPanel;
    rbWallSurface: TRadioButton;
    rbWallCenter: TRadioButton;
    pnlCenter: TPanel;
    chbWallSurface: TCheckBox;
    chbWallCenter: TCheckBox;
    lblWallThick: TLabel;
    dceWallWidth: TDcadEdit;
    btnSaveWallSettings: TButton;
    chbCatDflt: TCheckBox;
    btnFillClr: TButton;
    lblLblLyr: TLabel;
    cbLblLyr: TComboBox;
    lblLyrIx: TLabel;
    edLyrIx: TEdit;
    LyrSort: TMenuItem;
    lblOpacity: TLabel;
    speFillOpacity: TSpinEdit;
    lblPct: TLabel;
    pnlClrFrame: TPanel;
    procedure CustomFillColours1Click(Sender: TObject);
    procedure NewColourClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure chbSolidFillClick(Sender: TObject);
    procedure cbCategoryChange(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure NameALLCAPS1Click(Sender: TObject);
    procedure cbNameKeyPress(Sender: TObject; var Key: Char);
    procedure NumberALLCAPS1Click(Sender: TObject);
    procedure chbOutlineClick(Sender: TObject);
    procedure btnSaveWallSettingsClick(Sender: TObject);
    procedure AddDisplayColours (newClr : longint);
    procedure edNumberKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cbLblLyrChange(Sender: TObject);
    procedure LyrSortClick(Sender: TObject);
    procedure Colour1MouseEnter(Sender: TObject);
    procedure Colour1MouseLeave(Sender: TObject);
  private
    Colours : array [1..22] of TPanel;
    ShowOpacity : boolean;
//    procedure PopulateLblLayers;
  public
    CatCustFields : array of CatCustField;
    fCeilingOptions : TfCeilingOptions;
  end;

var
  fmSpaceProperties: TfmSpaceProperties;
  initialised : boolean;

implementation

{$R *.dfm}
{
procedure TfmSpaceProperties.PopulateLblLayers;
var
  lyraddr : lgl_addr;
  rlyr : Rlayer;
  ss : shortstring;
  s : string;
begin
  cbLblLyr.Items.Clear;
  cbLblLyr.Sorted := l^.LyrSort;
  lyraddr := lyr_first;
  while lyr_get (lyraddr, rlyr) do begin
    lyraddr := lyr_next (lyraddr);
    getLongLayerName (rlyr.addr, ss);
    cbLblLyr.Items.Add (string(ss));
  end;
  cbLblLyr.Sorted := false;
  s := GetLbl (348);  //Same as outline (active lyr)
  cbLblLyr.Items.Insert(0, s);
  s := GetLbl (350);  //Add prefix to outline lyr
  cbLblLyr.Items.Insert(1, s);
  s := GetLbl (349);  //Add suffix to outline lyr
  cbLblLyr.Items.Insert(2, s);

  if l^.lblLyrSpec = LSActive then
    cbLblLyr.ItemIndex := 0
  else if l^.lblLyrSpec = LSPrefix then
    cbLblLyr.ItemIndex := 1
  else if l^.lblLyrSpec = LSSuffix then
    cbLblLyr.ItemIndex := 2
  else begin
    if cbLblLyr.Items.IndexOf(l^.lblLyrSpecific) < 0 then
      cbLblLyr.Text := l^.lblLyrSpecific
    else
      cbLblLyr.ItemIndex := cbLblLyr.Items.IndexOf(l^.lblLyrSpecific);
  end;
end;
}

procedure TfmSpaceProperties.AddDisplayColours (newClr : longint);
// add new colour to main button and shuffle existing colours along other buttons
var
  i : integer;
begin
  if newClr > 0 then begin
    move (l.FillColours[1], l.FillColours[2], 84);
    l.FillColours[1] := newClr;
    for i := 2 to 22 do begin    // check that main colour is not duplicated on one of the other buttons
      if l.FillColours[i] = NewClr then begin
        move (l.FillColours[i+1], l.FillColours[i], 84-4*i);
        l.FillColours[23] := 0;
      end;
    end;
    Colour1.Visible := true;
    btnFillClr.Visible := true;
  end
  else begin
    Colour1.Visible := false;
    btnFillClr.Visible := false;
  end;

  for i := 1 to 22 do begin
    if l.FillColours[i] = 0 then
      Colours[i].Visible := (i=1)   // always display main button
    else begin
      Colours[i].Visible := true;
      Colours[i].Color := DCADRGBtoRGB (l.FillColours[i]);
    end;
  end;
  btnFillClr.Visible := Colours[1].Visible;
end;

procedure TfmSpaceProperties.NameALLCAPS1Click(Sender: TObject);
begin
  l^.Options.NameAllCaps := not l^.Options.NameAllCaps;
  SaveOptions;
  NameALLCAPS1.Checked := l^.Options.NameAllCaps;
end;

procedure TfmSpaceProperties.NewColourClick(Sender: TObject);
var
  ClrD : TColorDialog;
  TempClr  : longint;
  strnum : string;
  ndx, i : integer;
begin
  strnum := string ((TPanel(Sender)).Name);
  delete (strnum, 1, 6);
  try
    ndx := strtoint (strnum)
  except
    ndx := 1;
  end;

  l.doFill := true;
  initialised := false;
  chbSolidFill.checked := true;
  initialised := true;
  if ndx = 1 then begin  // clicked main button - display colour picker dialog
    if l.Options.CustomClr then begin
       ClrD := TColorDialog.Create(Application);
        ClrD.Color := DCADRGBtoRGB (l.FillColours[1]);
        ClrD.Options := [cdFullOpen];
        if ClrD.Execute then
          TempClr := RGBtoDCADRGB(ClrD.Color)
        else
          exit;
    end
    else begin
      TempClr := l.FillColours[1];
      GetColorIndex(TempClr);
    end;
  end
  else begin  // clicked one of the previous colour buttons
    TempClr := l.FillColours[ndx];
  end;

  AddDisplayColours (TempClr);

  FillClrPnlCaption (Colour1, l.FillColours[1]);

  for i := 1 to 22 do
    SaveIniInt ('SpdhFilClr' + inttostr(i), l.FillColours[i]);
end;

procedure TfmSpaceProperties.NumberALLCAPS1Click(Sender: TObject);
begin
  l^.Options.NumAllCaps := not l^.Options.NumAllCaps;
  SaveOptions;
  NumberALLCAPS1.Checked := l^.Options.NumAllCaps;
end;



procedure SaveRoomNameToList (Name : string);
var
  i : integer;
begin
  if Blankstring (name) then
    exit;
  i := RoomNames.IndexOf(Name);
  if i = 0 then begin  // name already at top of list
    RoomNames[i] := Name;   // save new name over existing one as could be case difference
    exit;
  end;
  if i > 0 then
    RoomNames.Delete(i);   // remove existing matching entry
  RoomNames.Insert(0, Name);
  try
    RoomNames.SaveToFile(l^.RoomNameFile);
  except

  end;
end;

procedure TfmSpaceProperties.btnSaveWallSettingsClick(Sender: TObject);
begin
  l^.DefineByCenter := rbWallCenter.Checked;
  SaveBln (SpdhDefCntr, l^.DefineByCenter);
  l^.WallWidth := dceWallWidth.NumValue;
  SaveRl (SpdhWalWidth, l^.WallWidth, false);
  l^.CreateAtSurface := chbWallSurface.Checked;
  SaveBln (SpdhCrtSurf, l^.CreateAtSurface);
  l^.CreateAtCenter := chbWallCenter.Checked;
  SaveBln (SpdhCrtCent, l^.CreateAtCenter);
end;

procedure TfmSpaceProperties.btnOKClick(Sender: TObject);
begin
  if l^.NameRqd and BlankString(cbName.Text) then begin
    if l^.NbrRqd and BlankString(edNumber.Text) then begin
      if MessageDlg('Room Name and Number are required for room labels or reports'+sLineBreak+sLineBreak+
                    'Do you wish to continue with a blank Room Name and Number?', mtConfirmation,
                    [mbYes,mbNO], 0) = mrNo then exit;
    end
    else
      if MessageDlg('Room Name is required for room labels or reports'+sLineBreak+sLineBreak+
                    'Do you wish to continue with a blank Room Name?', mtConfirmation,
                    [mbYes,mbNO], 0) = mrNo then exit;
  end
  else if l^.NbrRqd and BlankString(edNumber.Text) then begin
    if MessageDlg('Room Number is required for room labels or reports'+sLineBreak+sLineBreak+
                  'Do you wish to continue with a blank Room Number?', mtConfirmation,
                  [mbYes,mbNO], 0) = mrNo then exit;
  end;

  SaveRoomNameToList (cbName.Text);   // save name for future population of drop-down list

  SaveLblLayerInfo (cbLblLyr, edLyrIx);

  l^.FillOpacity := round(speFillOpacity.Value*2.55);
  SaveInt ('SpdhOpacity', l^.FillOpacity, true);

  l^.PrevNum := str9(edNumber.Text);
  l^.PrevName := str25 (cbName.Text);

  if (not Blankstring (cbCategory.Text)) and (cbCategory.Items.IndexOf(cbCategory.Text) < 0) then begin
    AddCategory (cbCategory.Text,  chbSolidFill.Checked, l^.FillColours[1],
                 speFillOpacity.value, nil);
    SaveAllCategory;
  end;




  ModalResult := mrOK;
end;

procedure TfmSpaceProperties.cbCategoryChange(Sender: TObject);
var
  i, j : integer;
begin
  if not chbCatDflt.Checked then
    exit;
  if cbCategory.ItemIndex >= 0 then with l^.Categories.Items[cbCategory.ItemIndex] do begin
    chbSolidFill.Checked := Standard.Data.DoFill;
    if chbSolidFill.Checked then begin
      AddDisplayColours (Standard.Data.FillColor);
      FillClrPnlCaption (Colour1, Standard.Data.FillColor);
      Colour1.Font.Color := PnlFontClr (Colour1.Color);
      for i := 1 to 22 do
        SaveIniInt ('SpdhFilClr' + inttostr(i), l.FillColours[i]);
      speFillOpacity.Value := round(Standard.Data.FillOpacity / 2.55);
    end;
    case Standard.Data.CeilingType of
      flat: begin
              fCeilingOptions.cbCeilingType.ItemIndex := 0;
              if Standard.Data.UseZHt then
                fCeilingOptions.dcCeilingHeight.numvalue := abs (PGsavevar^.hitez - PGsavevar^.basez)
              else
                fCeilingOptions.dcCeilingHeight.numvalue := Standard.Data.Height1;
            end;
      CeilingTypes.Custom: fCeilingOptions.cbCeilingType.ItemIndex := 4;
    end;

    fCeilingOptions.pcCeiling.ActivePageIndex := fCeilingOptions.cbCeilingType.ItemIndex;

    if (Custom.Count > 0 ) then for i := 0 to Custom.Count-1 do
      if (Length (CatCustFields) > 0) then for j := 0 to Length(CatCustFields)-1 do
        if CatCustFields[j].tag = Custom[i].tag then begin
          case Custom[i].numeric of
            str : CatCustFields[j].Input.Text := string(Custom[i].str);
            int : CatCustFields[j].Input.IntValue := Custom[i].int;
            rl  : CatCustFields[j].Input.NumValue := Custom[i].rl;
            dis : CatCustFields[j].Input.NumValue := Custom[i].dis;
            ang : CatCustFields[j].Input.NumValue := Custom[i].ang;
            UserFieldType.area: CatCustFields[j].Input.NumValue := Custom[i].area;
          end;
        end;
  end;
end;

procedure TfmSpaceProperties.cbLblLyrChange(Sender: TObject);
begin
  LblLyrChange(cbLblLyr, edLyrIx, lblLyrIx);
end;

procedure TfmSpaceProperties.cbNameKeyPress(Sender: TObject; var Key: Char);
begin
  if l^.Options.NameAllCaps then begin
    if CharInSet (Key, ['a' .. 'z']) then
      Key := upcase(Key);
  end;
end;

procedure TfmSpaceProperties.chbOutlineClick(Sender: TObject);
begin
  if initialised then begin
    l^.doFillOutline := chbOutline.Checked;
    SaveBln (SpdhFilOutLn, l.doFillOutline);
  end;
end;

procedure TfmSpaceProperties.chbSolidFillClick(Sender: TObject);
var
  i : integer;
begin
  if initialised then begin
    l^.doFill := chbSolidFill.Checked;
    SaveBln (SpdhPlnFill, l.doFill);
    AddDisplayColours(0);
    Colours[1].Visible := chbSolidFill.Checked;
    btnFillClr.Visible := Colours[1].Visible;
    for i := 2 to 22 do
      Colours[i].Visible := chbSolidFill.Checked and (l.FillColours[i] <> 0);
    chbOutline.Visible := chbSolidFill.Checked;

    if ShowOpacity then begin
      lblOpacity.Visible := l^.doFill;
      speFillOpacity.visible := l^.doFill;
      lblPct.visible := l^.doFill;
    end;
  end;
end;

procedure TfmSpaceProperties.Colour1MouseEnter(Sender: TObject);
begin
  pnlClrFrame.Visible := true;
end;

procedure TfmSpaceProperties.Colour1MouseLeave(Sender: TObject);
begin
  pnlClrFrame.visible := false;
end;

procedure TfmSpaceProperties.CustomFillColours1Click(Sender: TObject);
begin
  l.Options.CustomClr := not l.Options.CustomClr;
  CustomFillColours1.checked := l.Options.CustomClr;
  SaveOptions;
end;

procedure TfmSpaceProperties.edNumberKeyPress(Sender: TObject; var Key: Char);
begin
  if l^.Options.NumAllCaps then begin
    if CharInSet (Key, ['a' .. 'z']) then
      Key := upcase(Key);
  end;

end;

procedure TfmSpaceProperties.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  SaveFormPos(TForm(Sender));
end;

procedure TfmSpaceProperties.FormCreate(Sender: TObject);
var
  i : integer;
  height : double;
  atr,atrCustom : attrib;
  pStandardAtr : ^StandardAtr;
  pFillDef : ^FillDef;
begin
  SetFormPos(TForm(Sender));
  initialised := false;

  LabelCaption (111, lblName); //Name
  LabelCaption (112, lblNumber); //Number
  LabelCaption (113, lblCategory); //Category
  CheckCaption (114, chbSolidFill); //Use Solid Fill
  CheckCaption (115, chbOutline); //Show Outline
  CheckCaption (116, chbCatDflt); //Use Category Defaults
  LabelCaption (117, lblFloorZ); //Floor Z
  RadioCaption (118, rbWallSurface); //Define by Wall Surface
  RadioCaption (119, rbWallCenter); //Define by Wall Center
  LabelCaption (120, lblWallThick); //Wall Thickness
  CheckCaption (121, chbWallSurface); // Create polyline at Wall Surface
  CheckCaption (122, chbWallCenter); // Create polyline at Wall Center
  FillClrPnlCaption (Colour1, l.FillColours[1]);
  LabelCaption (346, lblLblLyr);  //Label Layer
  PopulateLblLayers (cbLblLyr);
  cbLblLyrChange(Sender);
  edLyrIx.Text := l^.lblLyrIx;
  ButtonLblParams (107, [], btnSaveWallSettings); //Save Wall Settings as Default
  ButtonCaption (108, btnOK);
  ButtonCaption (109, btnCancel);
  rbWallSurface.TabStop := false;
  rbWallCenter.TabStop := false;
  MenuItemCaption (110, Options1); //Options
  MenuItemCaption (126, CustomFillColours1);  //Custom Fill Colours
  MenuItemCaption (127, NameALLCAPS1);  //Name ALL CAPS
  MenuItemCaption (128, NumberALLCAPS1);  //Number ALL CAPS

  cbName.Text := IncrementNumber (l^.PrevName, l^.NameIncrement);
  edNumber.Text := IncrementNumber (l^.PrevNum, l^.Increment);

  Colours[1] := Colour1;
  Colours[2] := Colour2;
  Colours[3] := Colour3;
  Colours[4] := Colour4;
  Colours[5] := Colour5;
  Colours[6] := Colour6;
  Colours[7] := Colour7;
  Colours[8] := Colour8;
  Colours[9] := Colour9;
  Colours[10] := Colour10;
  Colours[11] := Colour11;
  Colours[12] := Colour12;
  Colours[13] := Colour13;
  Colours[14] := Colour14;
  Colours[15] := Colour15;
  Colours[16] := Colour16;
  Colours[17] := Colour17;
  Colours[18] := Colour18;
  Colours[19] := Colour19;
  Colours[20] := Colour20;
  Colours[21] := Colour21;
  Colours[22] := Colour22;
  CustomFillColours1.checked := l.Options.CustomClr;

  ShowOpacity := DataCADVersionEqOrGtr (22,2,1,0);
  if not ShowOpacity then begin
    speFillOpacity.Value := 255;
    lblOpacity.Visible := false;
    speFillOpacity.visible := false;
    lblPct.visible := false;
  end;

  if (l^.state < State_Esc) then begin
    // set up defaults
    for i := 1 to 22 do begin
      if l.FillColours[i] = 0 then begin
        if i = 1 then begin
          Colours[i].Color := DCADRGBtoRGB (CurrentState^.color);
          Colours[i].Visible := l.doFill;
        end
        else
          Colours[i].Visible := false;
      end
      else begin
        Colours[i].Visible := l.doFill;
        Colours[i].Color := DCADRGBtoRGB (l.FillColours[i]);
      end;

    end;

    chbSolidFill.Checked := l^.doFill;
    chbOutline.Checked := l^.doFillOutline;
    chbOutline.Visible := l^.doFill;
    Colour1.Visible := l^.doFill;
    btnFillClr.Visible := l^.doFill;

    if ShowOpacity then begin
      speFillOpacity.Value := round(l^.FillOpacity / 2.55);
      lblOpacity.Visible := l^.doFill;
      speFillOpacity.visible := l^.doFill;
      lblPct.visible := l^.doFill;
    end;

  end
  else begin
    //  set up values from existing selected space
    if atr_entfind (l^.ent, 'DH_FILL', atr) or atr_entfind (l^.ent, 'DC_FILL', atr) then begin
      chbSolidFill.Checked := true;
      pFillDef := addr (atr.str[1]);
      AddDisplayColours (pFillDef^.Fill_color);
      FillClrPnlCaption (Colour1, pFillDef^.Fill_color);

      chbOutline.Checked := atr.SPBvisible;
      chbOutline.Visible := true;
      if ShowOpacity then begin
        speFillOpacity.Value := round(atr.xdata.Data[12]/2.55);
        lblOpacity.Visible := l^.doFill;
        speFillOpacity.visible := l^.doFill;
        lblPct.visible := l^.doFill;
      end;
    end
    else begin
      chbSolidFill.Checked := false;
      chbOutline.Checked := l^.doFillOutline;
      chbOutline.Visible := false;
      for i := 1 to 22 do
        Colours[i].Visible := false;
      btnFillClr.Visible := false;
      speFillOpacity.Value := round(l^.FillOpacity / 2.55);
      lblOpacity.Visible := false;
      speFillOpacity.visible := false;
      lblPct.visible := false;
    end;

    pnlCenter.Enabled := false;
    btnSaveWallSettings.Enabled := false;
    lblWallThick.Enabled := false;
    dceWallWidth.Enabled := false;
    rbWallSurface.Enabled := false;
    rbWallCenter.Enabled := false;
    chbWallSurface.Enabled := false;
    chbWallCenter.Enabled := false;

    edLyrIx.Visible := false;
    lblLyrIx.Visible := false;
    cbLblLyr.Visible := false;
    lblLblLyr.Visible := false;

  end;


  Colour1.Font.Color := PnlFontClr (Colour1.Color);

  initialised := true;

  SetLength (CatCustFields, l^.UserFields.Count);
  if l^.UserFields.Count >= 1 then begin
    for i := 0 to l^.UserFields.Count-1 do begin
      if i = 0 then begin
        CatCustFields[0].Lbl := lblUserFieldName1;
        CatCustFields[0].Input := dceUserFieldValue1;
      end
      else if i = 1 then begin
        CatCustFields[1].Lbl := lblUserFieldName2;
        CatCustFields[1].Input := dceUserFieldValue2;
      end
      else begin
        CatCustFields[i].Lbl :=  TLabel.Create(lblUserFieldName1.Owner);
        CatCustFields[i].Lbl.Parent := lblUserFieldName1.Parent;
        CatCustFields[i].Lbl.Top := lblUserFieldName2.Top + (i-1)*(lblUserFieldName2.Top - lblUserFieldName1.Top);
        CatCustFields[i].Lbl.Left := lblUserFieldName1.Left;
        CatCustFields[i].Lbl.AutoSize := false;
        CatCustFields[i].Lbl.Width := lblUserFieldName1.Width;
        CatCustFields[i].Lbl.Height := lblUserFieldName1.Height;
        CatCustFields[i].Lbl.Anchors := lblUserFieldName1.Anchors;
        CatCustFields[i].Lbl.Name := 'lblUserFieldName' + IntToStr(i+1);

        CatCustFields[i].Input := TDcadEdit.Create(dceUserFieldValue1.Owner);
        CatCustFields[i].Input.Parent := dceUserFieldValue1.Parent;
        CatCustFields[i].Input.Top := CatCustFields[i].Lbl.Top - 2;
        CatCustFields[i].Input.Left := dceUserFieldValue1.Left;
        CatCustFields[i].Input.Width := dceUserFieldValue1.Width;
        CatCustFields[i].Input.Anchors := dceUserFieldValue1.Anchors;
        CatCustFields[i].Input.Height := dceUserFieldValue1.Height;
        CatCustFields[i].Input.Anchors := dceUserFieldValue1.Anchors;
        CatCustFields[i].Input.Name := 'dceUserFieldValue' + IntToStr(i+1);
      end;

      CatCustFields[i].tag :=  l^.UserFields[i].tag;

      case l^.UserFields[i].numeric of
      str : begin
              CatCustFields[i].Input.NumberType := DCADEdit.TextStr;
              if atr_entfind (l^.ent, 'SpdhCF'+CatCustFields[i].tag, atrCustom) and
                 (atrCustom.atrtype = atr_str) then
                CatCustFields[i].Input.Text := string(atrCustom.str)
              else
                CatCustFields[i].Input.Text := string (l^.UserFields[i].defaultstr);
            end;
      int : begin
              CatCustFields[i].Input.NumberType := DCADEdit.IntegerNum;
              if atr_entfind (l^.ent, 'SpdhCF'+CatCustFields[i].tag, atrCustom) and
                 (atrCustom.atrtype = atr_int) then
                CatCustFields[i].Input.IntValue := atrCustom.int
              else
                CatCustFields[i].Input.IntValue := l^.UserFields[i].defaultint;
            end;
      rl  : begin
              CatCustFields[i].Input.NumberType := DCADEdit.DecimalNum;
              if atr_entfind (l^.ent, 'SpdhCF'+CatCustFields[i].tag, atrCustom) and
                 (atrCustom.atrtype = atr_rl) then
                CatCustFields[i].Input.NumValue := atrCustom.rl
              else
                CatCustFields[i].Input.NumValue := l^.UserFields[i].defaultrl;
            end;
      dis : begin
              CatCustFields[i].Input.NumberType := DCADEdit.Distance;
              if atr_entfind (l^.ent, 'SpdhCF'+CatCustFields[i].tag, atrCustom) and
                 (atrCustom.atrtype = atr_dis) then
                CatCustFields[i].Input.NumValue := atrCustom.dis
              else
                CatCustFields[i].Input.NumValue := l^.UserFields[i].defaultdis;
            end;
      ang : begin
              if pgSaveVar^.angstyl = 3 then
                CatCustFields[i].Input.NumberType := DCADEdit.DecDegrees
              else
                CatCustFields[i].Input.NumberType := DCADEdit.DegMinSec;
              if atr_entfind (l^.ent, 'SpdhCF'+CatCustFields[i].tag, atrCustom) and
                 (atrCustom.atrtype = atr_ang) then
                CatCustFields[i].Input.NumValue := atrCustom.ang
              else
                CatCustFields[i].Input.NumValue := l^.UserFields[i].defaultang;
            end;
      UserFieldType.area: begin
              CatCustFields[i].Input.NumberType := DCADEdit.area;
              if atr_entfind (l^.ent, 'SpdhCF'+CatCustFields[i].tag, atrCustom) and
                 (atrCustom.atrtype = atr_rl) then
                CatCustFields[i].Input.NumValue := atrCustom.rl
              else
                CatCustFields[i].Input.NumValue := l^.UserFields[i].defaultarea;
            end;
      end;

      CatCustFields[i].Lbl.Caption := string(l^.UserFields[i].desc);

    end;
  end
  else begin
    lblUserFieldName1.Visible := false;
    dceUserFieldValue1.Visible := false;
  end;
  if l^.UserFields.Count <= 1 then begin
    lblUserFieldName2.Visible := false;
    dceUserFieldValue2.Visible := false;
  end;

  PopulateCategories (cbCategory);

  fCeilingOptions := TfCeilingOptions.Create(self);
  fCeilingOptions.Parent := pCatCeiling;
  fCeilingOptions.Top := 0;
  fCeilingOptions.Left := 0;
  fCeilingOptions.Color := pCatCeiling.Color;
  fCeilingOptions.Visible := true;

  if (l^.state < State_Esc) then begin
    dcFloorZ.NumValue := min (PGsavevar^.basez, PGsavevar^.hitez);
    height := abs (PGsavevar^.hitez - PGsavevar^.basez);
    fCeilingOptions.cbCeilingType.ItemIndex := 0;
  end
  else begin
    dcFloorZ.NumValue := l^.ent.plbase;
    height := l^.ent.plnhite - l^.ent.plbase;
  end;

  fCeilingOptions.chbUseZ.checked := false;
  fCeilingOptions.chbUseZ.Visible := false;
  fCeilingOptions.dcCeilingHeight.NumValue := height;
  fCeilingOptions.dcCeilingHeightSingle.NumValue := height;
  fCeilingOptions.dcCeilingHeightDbl1.NumValue := height;
  fCeilingOptions.dcCeilingHeightDbl2.NumValue := height;
  fCeilingOptions.dcnCeilingHeightArch1.NumValue := height;
  fCeilingOptions.dcnCeilingHeightArch2.NumValue := height;

  NameALLCAPS1.Checked := l^.Options.NameAllCaps;
  NumberALLCAPS1.Checked := l^.Options.NumAllCaps;

  cbName.Items := RoomNames;

  if (l^.state < State_Esc) then begin
    dceWallWidth.NumValue := l^.WallWidth;
    rbWallSurface.Checked := not l^.DefineByCenter;
    rbWallCenter.Checked := l^.DefineByCenter;
    chbWallSurface.Checked := l^.CreateAtSurface;
    chbWallCenter.Checked := l^.CreateAtCenter;
  end
  else begin
    if atr_entfind (l^.ent, STANDARD_ATR_NAME, atr) then begin
      pStandardAtr := addr(atr.shstr);
      cbName.Text := string(pStandardAtr^.Name);
      edNumber.Text := string(pStandardAtr^.Num);
      cbCategory.Text := string(pStandardAtr^.Category);

      if Flag (pStandardAtr^.Flags, CEILING_FROM_MODEL) then
        fCeilingOptions.cbCeilingType.ItemIndex := 1
      else
        fCeilingOptions.cbCeilingType.ItemIndex := 0;

      dceWallWidth.NumValue := pStandardAtr^.WallThick;
      rbWallSurface.Checked := Flag (pStandardAtr^.Flags, IS_SURFACE);
    end;

    rbWallCenter.Checked := not rbWallSurface.Checked;
    rbWallSurface.Enabled := false;
    rbWallCenter.Enabled := false;
    Colour1.TabStop := false;
    btnSaveWallSettings.Visible := false;
  end;

end;

procedure TfmSpaceProperties.LyrSortClick(Sender: TObject);
begin
  l^.Options.LyrSort := not l^.Options.LyrSort;
  SaveOptions;
  LyrSort.Checked := l^.Options.LyrSort;
  PopulateLblLayers (cbLblLyr);
  cbLblLyr.OnChange(Sender);
end;

end.
