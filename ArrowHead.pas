unit ArrowHead;

interface

  uses UInterfaces, URecords, UConstants, CommonStuff, Settings, System.math,
  System.SysUtils;

	PROCEDURE DoArrowHead (pnt1, pnt2 : point; OUT newpnt : point; out HeadEnt : entity);

implementation

  FUNCTION MaxLabelTextSize : double;
  VAR
    i : integer;
    s : shortstring;
    addr : lgl_addr;
    sym : symbol;
    lblent : entity;
  BEGIN
    result := 0;
    if l^.LabelFromSymbol then begin
      s := GetSvdStr ('SpdhLbSymbol', '');
      if length (s) > 4 then begin
        delete (s, length(s)-3, 4);   // remove .dsf extension
        if symread  (symstr(s), 'tempsym', sym) = fl_OK then begin
          addr := sym.frstent;
          while ent_get (lblent, addr) do begin
            if addr_equal(addr, sym.lastent) then
              setnil (addr)  // ensure loop stops at last entity in symbol
            else
              addr := lblent.Next;
              if lblent.enttype = enttxt then
                result := max (result, lblent.txtsiz);
          end;
        end;
      end;

    end
    else begin
      for i := 1 to 5 do begin
        if length(GetSvdStr ('Line' + inttostr(i) + 'Ptn', defaultPtn[i])) > 0 then
          result := max (result, GetSvdRl ('SpdhSl' + inttostr(i) + 'Siz', pgSaveVar^.txtsiz));
      end;
    end;
  END;

	PROCEDURE DoArrowHead (pnt1, pnt2 : point; OUT newpnt : point; out HeadEnt : entity);
	VAR
    ent : entity;
		ang : real;
		pv, pv1 : polyvert;
    TextSize : double;
    atr : attrib;
	BEGIN
		ang := angle (pnt2,pnt1);
		ent_init (ent, entpln);
    TextSize := MaxLabelTextSize;
		if l^.ArrowSettings.Style < 2 then begin
			ent.plnclose := (l^.ArrowSettings.Style=1);
			polyvert_init (pv);
			pv.shape := pv_vert;
			pv.pnt.x := pnt2.x + TextSize * l^.ArrowSettings.Size * l^.ArrowSettings.Aspect/2;
			pv.pnt.y := pnt2.y + TextSize * l^.ArrowSettings.Size * 0.5;
			pv.pnt.z := zbase;
			polyvert_add (pv, ent.plnFrst, ent.plnLast);
			ent_add (ent);
			polyvert_init (pv);
			pv.shape := pv_vert;
			pv.pnt := pnt2;
			pv.pnt.z := zbase;
			polyvert_add (pv, ent.plnFrst, ent.plnLast);
			ent_update (ent);
			polyvert_init (pv);
			pv.shape := pv_vert;
			pv.pnt.x := pnt2.x + TextSize * l^.ArrowSettings.Size * l^.ArrowSettings.Aspect/2;
			pv.pnt.y := pnt2.y - TextSize * l^.ArrowSettings.Size * 0.5;
			pv.pnt.z := zbase;
			polyvert_add (pv, ent.plnFrst, ent.plnLast);
			ent_update (ent);
    end
		else if l^.ArrowSettings.Style < 4 then begin
			ent.plnclose := (l^.ArrowSettings.Style = 3);
			polyvert_init (pv);
			pv.shape := pv_bulge;
			pv.bulge := 1.0;
			pv.pnt.x := pnt2.x - TextSize * l^.ArrowSettings.Size * 0.5;
			pv.pnt.y := pnt2.y;
			pv.pnt.z := zbase;
			polyvert_add (pv, ent.plnFrst, ent.plnLast);
			ent_add (ent);
			polyvert_init (pv);
			pv.shape := pv_bulge;
			pv.bulge := 1.0;
			pv.pnt.x := pnt2.x + TextSize * l^.ArrowSettings.Size * 0.5;
			pv.pnt.y := pnt2.y;
			pv.pnt.z := zbase;
			polyvert_add (pv, ent.plnFrst, ent.plnLast);
			ent_update (ent);
    end
		else if l^.ArrowSettings.Style = 4 then begin
			ent.plnclose := false;
			polyvert_init (pv);
			pv.pnt := pnt2;
			pv.pnt.z := zbase;
			polyvert_add (pv, ent.plnFrst, ent.plnLast);
			ent_add (ent);
			polyvert_init (pv);
			pv.pnt.x := pnt2.x + TextSize * l^.ArrowSettings.Size;
			pv.pnt.y := pnt2.y;
			pv.pnt.z := zbase;
			polyvert_add (pv, ent.plnFrst, ent.plnLast);
			ent_update (ent);
		end;
		ent_rotate (ent, pnt2, ang);
		if l^.ArrowSettings.Clr > 0 then
			ent.color := l^.ArrowSettings.Clr;
		if l^.ArrowSettings.Wgt > 0 then
			ent.width := l^.ArrowSettings.Wgt;
		ent.ltype := ltype_solid;
		if polyvert_get (pv, ent.plnfrst, ent.plnFrst) then begin
      if (l^.ArrowSettings.Style <= 1) and polyvert_get (pv1, ent.plnlast, ent.plnfrst) then
        meanpnt (pv.pnt, pv1.pnt, newpnt)
      else
			  newpnt := pv.nextPnt
    end
    else
      newpnt := pnt2;

    if (l^.ArrowSettings.Style in [1, 3]) and l^.ArrowSettings.Filled then begin
      atr := InitSolidFillAttribute (l^.ArrowSettings.Clr, 0, 0);
      atr_add2ent (ent, atr);
    end;

    if l^.ArrowSettings.Knockout and (l^.ArrowSettings.Style in [0, 1, 3]) then
      ent.KnockOut := 2;

		ent_update (ent);
		HeadEnt := ent;
	END;



end.
