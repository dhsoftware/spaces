unit RoomOutline;

interface
uses URecords, UVariables, UInterfaces, UConstants;

FUNCTION ProcessRoom  (var mode : mode_type;
                       var ent : entity;
                       wall_thick, max_opening : double) : boolean;

implementation

CONST
  debug = false;

FUNCTION RealEqual (r1, r2 : double) : boolean;
BEGIN
  result := abs(r1-r2) < 0.00001;
END;

FUNCTION PointsEqual (p1, p2 : point) : boolean;
BEGIN
  result := (abs(p1.x - p2.x) < 0.0001) and (abs(p1.y - p2.y) < 0.0001);
END;

PROCEDURE SwapPoints (var p1, p2 : point);
VAR
  p3 : point;
BEGIN
  p3 := p1;
  p1 := p2;
  p2 := p3;
END;

FUNCTION AngBetween0andPi (ang : double) : double;
BEGIN
  result := ang;
  while result > pi do
    result := result - pi;
  while result < 0.0 do
    result := result + pi;
END;

FUNCTION LineAngleSame (ang1, ang2 : real) : boolean;
BEGIN
  result := realequal (ang1, ang2);
  if not result then
    result := RealEqual (abs (ang1-ang2), Pi);
END;

FUNCTION ProcessRoom  (var mode : mode_type;
                       var ent : entity;
                       wall_thick, max_opening : double) : boolean;
VAR
  pv1, pv2 : polyvert;
  lastent, nextent, possiblecandidate : entity;
  sv_MissDis : integer;
  acceptable, giveup, keeptrying : boolean;
  anglast, ang1, ang2, ang3: double;
  p1, searchpnt : point;
  searchdis : double;
  UnusedList : Array of entity;
  i, UnusedCount : integer;
  debugent,
  result_ent : entity;


  PROCEDURE SetUnused (ent : entity);
  BEGIN
    ent_setunused (ent, true);
    UnusedCount := UnusedCount + 1;
    if high(UnusedList) < (UnusedCount-1) then
      SetLength (UnusedList, UnusedCount + 30);
    UnusedList[UnusedCount-1] := ent;
  END;

BEGIN
  UnusedCount := 0;
  sv_MissDis := PGSaveVar^.missdis;
  PGSaveVar^.missdis := 1;
  mode_ignore (mode);
  ent_init (result_ent, entpln);
  result_ent.plnclose := true;
  polyvert_init (pv1);
  pv1.shape := pv_vert;
  pv1.pnt := ent.linpt1;
  polyvert_add (pv1, result_ent.plnfrst, result_ent.plnlast);

  try
    SetUnused (ent);

    repeat
      polyvert_init (pv2);
      pv2.shape := pv_vert;
      pv2.pnt := ent.linpt2;
      polyvert_add (pv2, result_ent.plnfrst, result_ent.plnlast);
      lastent := ent;
      giveup := false;
      repeat
        acceptable := false;
        if ent_near (ent, lastent.linpt2.x, lastent.linpt2.y, mode, false) then begin
          possiblecandidate := ent;
          if debug then begin
            ent.color := clryellow;
            ent_draw (ent, drmode_white);
          end;
          SetUnused (ent);
          acceptable := pointsequal (ent.linpt1, lastent.linpt2);     // found line starts at end point of previous line
          if not acceptable then begin
            swappoints (ent.linpt1, ent.linpt2);                  // try turning the found line around
            acceptable := pointsequal (ent.linpt1, lastent.linpt2);
          end;
          if acceptable then begin
            if (distance (ent.linpt1, ent.linpt2) < (wall_thick + 0.0001)) and  // if length of found line <= wall thickness then check if it is a wall cap
                not pointsequal (ent.linpt2, pv1.pnt)                 // (unless it closes out to starting point)
            then begin
              if debug then begin
                ent.color := clrred;
                ent_draw (ent, drmode_white);
              end;
              angLast := angle (lastent.linpt1, lastent.linpt2);
              ang1 := AngBetween0andPi(angLast);
              ang2 := AngBetween0andPi(angle (ent.linpt1, ent.linpt2));
              if realequal (abs(ang1 - ang2), halfpi) then begin      // only consider it as a wall cap if it is at 90 degrees to previous line
                if ent_near (nextent, ent.linpt2.x, ent.linpt2.y, mode, false) then begin
                  if pointsequal (ent.linpt2, nextent.linpt1) then begin
                    ang3 := angle (nextent.linpt2, nextent.linpt1);
                  end
                  else if pointsequal (ent.linpt2, nextent.linpt2) then begin
                    ang3 := angle (nextent.linpt1, nextent.linpt2)
                  end
                  else begin
                    ang3 := angLast + 1.0;  // line is not a candidate - set any arbitary angle that will not match angLast
                    setnil (possiblecandidate.addr);
                  end;
                end;
              end;
              if realequal (anglast, ang3) then
                acceptable := false;        // line appears to be other side of wall for lastent
            end;
          end;
        end
        else
          giveup := true;
      until acceptable or giveup;

      if giveup then begin  // search for line on other side of an opening
        PGSaveVar^.missdis := (round (wall_thick / pixsize) div 2) + 1;   // set missdis to just over half a wall width
        searchdis := 0.0;
        repeat
          searchdis := searchdis + wall_thick/2;
          cylind_cart (searchdis, angLast, 0.0, p1.x, p1.y, p1.z);
          addpnt (lastent.linpt2, p1, searchpnt);
          if debug then begin
             ent_init (debugent, entcrc);
             debugent.crcCent := searchpnt;
             debugent.crcrad := 40.0;
             debugent.color := clrcyan;
             ent_draw (debugent, drmode_white);
          end;
          if (distance (searchpnt, pv1.pnt) < (wall_thick + 0.0001)) and
              (dis_from_line (lastent.linpt1, lastent.linpt2, pv1.pnt) < 0.001)
          then begin
            ent.linpt1 := lastent.linpt2;
            ent.linpt2 := pv1.pnt;
            giveup := false;
          end
          else begin
            keeptrying := true;
            repeat
              if ent_near (nextent, searchpnt.x, searchpnt.y, mode, false) then begin
                SetUnused (nextent);
                if debug then begin
                  nextent.color := clrgrn;
                  ent_draw (nextent, drmode_white);
                end;
                ang3 := AngBetween0andPi(angle (nextent.linpt1, nextent.linpt2));
                if LineAngleSame (ang1, ang3) then begin
                  ent := nextent;
                  if distance (lastent.linpt2, ent.linpt1) > distance (lastent.linpt2, ent.linpt2) then
                    swappoints (ent.linpt1, ent.linpt2);
                  giveup := false;
                  keeptrying := false;
                end;
              end
              else begin
                keeptrying := false;
              end;
            until not keeptrying;
          end;
        until (not giveup) or (searchdis >= max_opening);

        if giveup then begin
          if (dis_from_line (lastent.linpt1, lastent.linpt2, pv1.pnt) < 0.001) and
            (between (lastent.linpt1, lastent.linpt2, pv1.pnt) = -2)
          then begin
            ent.linpt1 := lastent.linpt2;
            ent.linpt2 := pv1.pnt;
            giveup := false;
          end;
        end;

        PGSaveVar^.missdis := 1;
      end;

      if giveup then begin
        if not isnil (possiblecandidate.addr) then begin
          ent := possiblecandidate;
          acceptable := pointsequal (ent.linpt1, lastent.linpt2);     // found line starts at end point of previous line
          if not acceptable then begin
            swappoints (ent.linpt1, ent.linpt2);                  // try turning the found line around
            acceptable := pointsequal (ent.linpt1, lastent.linpt2);
          end;
          giveup := not acceptable;
        end;
      end;

    until pointsequal (ent.linpt2, pv1.pnt) or giveup;
  finally
    for i := 0 to (UnusedCount-1) do
      ent_setunused (UnusedList[i], false);

    PGSaveVar^.missdis := sv_MissDis;

    if not giveup then
      ent_add (result_ent)
    else
      setnil (result_ent.addr);
    ent := result_ent;
    result := not giveup;
  end;
END;

end.
