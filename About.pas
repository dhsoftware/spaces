unit About;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, ShellApi, Version, Language,
  UInterfaces, UConstants, StrUtil, CommonStuff, IdHTTP, Settings, Constants;

type
  TfmAbout = class(TForm)
    Panel1: TPanel;
    lblwww: TLabel;
    lblCopyright: TLabel;
    lblSpacePlanner: TLabel;
    lblVersion: TLabel;
    lblContribute: TLabel;
    lblErr: TLabel;
    mCopyright: TMemo;
    btnDismiss: TButton;
    btnManual: TButton;
    memVersion: TMemo;
    btnRegister: TButton;
    procedure btnDismissClick(Sender: TObject);
    procedure lblwwwClick(Sender: TObject);
    procedure lblContributeClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnManualClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnRegisterClick(Sender: TObject);
  private
    { Private declarations }
    dcver : string;

  public
    { Public declarations }
  end;

var
  fmAbout: TfmAbout;

implementation

{$R *.dfm}

procedure TfmAbout.btnDismissClick(Sender: TObject);
begin
  Close;
end;

procedure TfmAbout.btnManualClick(Sender: TObject);
var
  tempstr : shortstring;
  helpfilename : PWideChar;
begin
  UInterfaces.GetPath(tempstr, pathsup);
  helpfilename := PWideChar(string(tempstr) + 'dhsoftware\Spaces.pdf');
  ShellExecute(0, 'open', helpfilename,nil,nil, SW_SHOWNORMAL) ;
end;

procedure TfmAbout.btnRegisterClick(Sender: TObject);
begin
  ShellExecute(self.WindowHandle,'open','https://www.dhsoftware.com.au/contactreg.htm',nil,nil, SW_SHOWNORMAL);
end;

function EncodeForUrl (s : string) : string;
var
  i : integer;
begin
  result := '';
  for i := 1 to length(s) do
    if CharInSet (s[i], ['A'..'Z','a'..'z','0','1'..'9','-','_','.']) then
      result := result + s[i]
    else
      result := result + '%' + inttohex(ord(s[i]), 2);
end;


procedure TfmAbout.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  SaveFormPos (TForm(Sender));
end;

procedure TfmAbout.FormCreate(Sender: TObject);
var
  major, minor, patch, build : integer;
  s, s1 : string;
begin
  ButtonLblParams (104, [], btnManual);  //Show Instruction Manual|Open the instruction manual with your default Acrobat file viewer
  ButtonLblParams (105, [], btnDismiss); //Dismiss
  lblversion.Caption := 'v' + FileVersion;
  ButtonLblParams(362, [], btnRegister);

  GetMsg (107, s); //This macro is Compatible with DataCAD version 17 onwards.  It may work with earlier versions but has not been tested.
  memVersion.Lines[0] := s;

  if (l^.DCADver = 0) and DataCADVersion (major, minor, patch, build) then begin
    if DataCADVersionEqOrGtr (21, 0, 1, 0) then  begin
      l^.DCADver := 3;
    end
    else if DataCADVersionEqOrGtr (17, 0, 0, 0) then
      l^.DCADver := 2
    else
      l^.DCADver := 1;
  end;

  if DataCADVersion (major, minor, patch, build) then begin
    dcver := IntToZeroFilledStr (major, 2) + '.' + IntToZeroFilledStr (minor, 2) + '.' +
             IntToZeroFilledStr (patch, 2) + '.' + IntToZeroFilledStr (build, 2);
    GetMsg (108, s1);   //You are currently running DataCAD $.
    s1 := StringReplace (s1, '$', dcver, []);
    memVersion.Lines.Add(s1);
    if major < 17 then
      memVersion.Font.Color :=  clMaroon;
  end;

  mCopyright.Lines.Clear;
  mCopyright.Lines.Add(LicParagraph1);
  mCopyright.Lines.Add('');
  mCopyright.Lines.Add(LicParagraph2);
  mCopyright.Lines.Add('');
  mCopyright.Lines.Add(LicParagraph3);
  mCopyright.Lines.Add('');
  mCopyright.Lines.Add(LicParagraph4);
  mCopyright.Lines.Add('');
  mCopyright.Lines.Add(LicParagraph5);


  SetFormPos (TForm(Sender));
end;

procedure TfmAbout.lblContributeClick(Sender: TObject);
begin
  ShellExecute(self.WindowHandle,'open','www.dhsoftware.com.au/contribute.htm',nil,nil, SW_SHOWNORMAL);
end;

procedure TfmAbout.lblwwwClick(Sender: TObject);
begin
  ShellExecute(self.WindowHandle,'open','www.dhsoftware.com.au',nil,nil, SW_SHOWNORMAL);
end;

end.
