unit ImportExcel;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, System.Types,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Buttons, Settings, ComObj,
  CommonStuff, Language, Vcl.Grids, Vcl.Menus, URecords, UInterfaces, UConstants, Math,
  DcadEdit, ExcelHelper, LayerUtil;

type
  TImportXL = class(TForm)
    pSheet: TPanel;
    lblSheetName: TLabel;
    cbSheets: TComboBox;
    StringGrid1: TStringGrid;
    btnOK: TButton;
    btnCancel: TButton;
    lblSheetDims: TLabel;
    rbFeet: TRadioButton;
    rbInches: TRadioButton;
    rbMeters: TRadioButton;
    rbMillimeters: TRadioButton;
    rbCentimeters: TRadioButton;
    rbCustom: TRadioButton;
    dceCustomUnits: TDcadEdit;
    lblUnitSz: TLabel;
    PopupMenu1: TPopupMenu;
    lblLblLyr: TLabel;
    cbLblLyr: TComboBox;
    lblLyrIx: TLabel;
    edLyrIx: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure PopulateGrid;
    procedure cbSheetsChange(Sender: TObject);
    procedure StringGrid1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure StringGrid1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure MenuClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure rbFeetClick(Sender: TObject);
    procedure rbInchesClick(Sender: TObject);
    procedure rbMetersClick(Sender: TObject);
    procedure rbMillimetersClick(Sender: TObject);
    procedure rbCentimetersClick(Sender: TObject);
    procedure rbCustomClick(Sender: TObject);
    procedure dceCustomUnitsExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cbLblLyrChange(Sender: TObject);
  private
    { Private declarations }
    ExcelSheet : variant;
    Selection : TGridRect;
    ACol : integer;
    FlShtAtrName : atrName;
    procedure GetDimUnits;
    procedure SaveDimUnits;
  public
    { Public declarations }
    DfltDimension : double;
    XLatrndx : integer;
    ExcelApplication, wb : oleVariant;
    function ExcelNdx : integer;
  end;

var
  ImportXL: TImportXL;

implementation

{$R *.dfm}

procedure TImportXL.SaveDimUnits;
var
  atr : attrib;
begin
  atr.name := FlShtAtrName + 'UD';    // Unit Dimension attrib for this sheet
  if atr_sysfind (atr.name, atr) and (atr.atrtype = atr_dis) then begin
    atr.dis := dceCustomUnits.NumValue;
    atr_update (atr);
  end
  else begin
    atr_init (atr, atr_dis);
    atr.name := FlShtAtrName + 'UD';
    atr.dis := dceCustomUnits.NumValue;
    atr_add2sys (atr);
  end;
end;

procedure TImportXL.GetDimUnits;
var
  atr : attrib;
begin
  atr.name := FlShtAtrName + 'UD';    // Unit Dimension attrib for this sheet
  if not (atr_sysfind (atr.name, atr) and (atr.atrtype = atr_dis)) then begin
    atr_init (atr, atr_dis);
    atr.name := FlShtAtrName + 'UD';
    atr.dis := DfltDimension;
    atr_add2sys (atr);
  end;
  dceCustomUnits.NumValue := atr.dis;
  dceCustomUnits.Enabled := false;
  if realequal( atr.dis, 384, abszero) then
    rbFeet.Checked := true
  else if atr.dis = 32 then
    rbInches.Checked := true
  else if realequal (atr.dis, MCONV, abszero) then
    rbMeters.Checked := true
  else if realequal (atr.dis, MCONV/1000, abszero) then
    rbMillimeters.Checked := true
  else if realequal (atr.dis, MCONV/100, abszero) then
    rbCentimeters.Checked := true
  else begin
    rbCustom.Checked := true;
    dceCustomUnits.Enabled := true;
  end;
end;


function TImportXL.ExcelNdx : integer;
/// finds or creates an attribute specifying the filename/sheetname
///  (see attribut comments at the top of Excel unit)
var
  atr : attrib;
  ss : shortstring;
  done : boolean;
begin
  result := 1;
  done := false;
  ss := l^.ent2.txtstr + '|' + shortstring(cbSheets.Text);
  repeat
    FlShtAtrName := 'SpdhXL' + shortstring (IntToStr(result));
    if atr_sysfind (FlShtAtrName, atr) then begin
      if (atr.atrtype = atr_str255) and (atr.shstr = ss) then
        done := true
      else
        result := result+1;
    end
    else begin
      atr_init (atr, atr_str255);
      atr.name := FlShtAtrName;
      atr.shstr := ss;
      atr_add2sys (atr);
      done := true;
    end;
  until done or (result > 9999);

  GetDimUnits;

end;



procedure TImportXL.PopulateGrid;
var
  i, j, row, col, xlLastRow, xlLastCol : integer;
  s, s1 : string;
  atr : attrib;
  xlsData : variant;
  Range : OLEVariant;
begin
  ExcelSheet := wb.WorkSheets[cbSheets.Text];

  xlLastRow := ExcelSheet.Cells.Find('*',EmptyParam,EmptyParam,EmptyParam,xlByRows,xlPrevious,EmptyParam,EmptyParam).Row;
  xlLastCol := ExcelSheet.Cells.Find('*',EmptyParam,EmptyParam,EmptyParam,xlByColumns,xlPrevious,EmptyParam,EmptyParam).Column;

  StringGrid1.ColCount := xlLastCol+51;
  StringGrid1.RowCount := xlLastRow+1;


  XLatrndx := ExcelNdx;


  for j := 1 to xlLastCol+50 do begin
    if (j mod 42) = 1 then begin
      atr.name := 'SpdhXL' + shortstring(inttostr(XLatrndx)) + 'C' + shortstring(inttostr(j div 42));
      if atr_sysfind (atr.name, atr) and (atr.atrtype = atr_str255) then
        s := string (atr.shstr)
      else
        s := stringofchar (' ', 255);
    end;
    s1 := Trim(s.Substring(((j mod 42)-1)*6, 6));
    if length(s1) = 0 then
      StringGrid1.Cells[j,0]:= xlsColHeading(j)
    else
      StringGrid1.Cells[j,0]:= '{' + s1 + '}';     // use existing column mapping

  end;

  for i := 1 to StringGrid1.RowCount do
    StringGrid1.Cells[0,i] := inttostr (i);

  i := 1;
  xlsData := VarArrayCreate([1, xlLastCol, 1, 10], varVariant);
  while i <= xlLastRow do begin
    Range := ExcelSheet.Range[ExcelSheet.Cells[i, 1],
                    ExcelSheet.Cells[min (i+9, xlLastRow), xlLastCol]];
    xlsData := Range.Value;
    for col := 1 to xlLastCol do
      for row := i to min (i+9, xlLastRow) do
        try
          StringGrid1.Cells[col, row] := xlsData[(row-1) mod 10 + 1, col];
        except
          on e : exception do begin
            lpMsg_OK(62, [shortstring(e.Message)]);
            lpMsg_OK(62, [shortstring('ROW:' + inttostr(row) + '  COL:' + inttostr(col))]);
          end;
        end;
    i := i + 10;
  end;

  // ensure there is no initial selection
  Selection.Left := -1;
  Selection.Right := -1;
  Selection.Top := -1;
  Selection.Bottom := -1;
  StringGrid1.Selection := Selection;
end;

procedure TImportXL.rbCentimetersClick(Sender: TObject);
begin
  dceCustomUnits.NumValue := MCONV/100;
  dceCustomUnits.Enabled := false;
  SaveDimUnits;
end;

procedure TImportXL.rbCustomClick(Sender: TObject);
begin
  dceCustomUnits.Enabled := true;
end;

procedure TImportXL.rbFeetClick(Sender: TObject);
begin
  dceCustomUnits.NumValue := 384;
  dceCustomUnits.Enabled := false;
  SaveDimUnits;
end;

procedure TImportXL.rbInchesClick(Sender: TObject);
begin
  dceCustomUnits.NumValue := 32;
  dceCustomUnits.Enabled := false;
  SaveDimUnits;
end;

procedure TImportXL.rbMetersClick(Sender: TObject);
begin
  dceCustomUnits.NumValue := MCONV;
  dceCustomUnits.Enabled := false;
  SaveDimUnits;
end;

procedure TImportXL.rbMillimetersClick(Sender: TObject);
begin
  dceCustomUnits.NumValue := MCONV/1000;
  dceCustomUnits.Enabled := false;
  SaveDimUnits;
end;

procedure TImportXL.StringGrid1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  MousePoint : TPoint;
  ARow : integer;
begin
  {get X, Y in screen coordinates}
  MousePoint := StringGrid1.ClientToScreen(System.Classes.Point(X, Y));
  {get the cell clicked in}
  StringGrid1.MouseToCell(X, Y, ACol, ARow);
  Selection := StringGrid1.Selection;
  if (ARow = 0) and (ACol > 0) then begin
    PopUpMenu1.PopUp(MousePoint.X, MousePoint.Y);
  end;

end;

procedure TImportXL.StringGrid1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  MousePoint : TPoint;
  ACol, ARow : integer;
begin
  {get X, Y in screen coordinates}
  MousePoint := StringGrid1.ClientToScreen(System.Classes.Point(X, Y));
  {get the cell clicked in}
  StringGrid1.MouseToCell(X, Y, ACol, ARow);
  if ARow = 0 then begin
    StringGrid1.Selection := Selection;
  end
  else begin
    if (ssShift in Shift) then begin
      if ARow < Selection.Top then
        Selection.Top := ARow
      else if ARow > Selection.Bottom then
        Selection.Bottom := ARow;
    end
    else begin
      Selection.Top := ARow;
      Selection.Bottom := ARow;
    end;
    Selection.Left := 0;
    Selection.Right := StringGrid1.ColCount-1;
  end;
  StringGrid1.Selection := Selection;
end;

procedure TImportXL.btnOKClick(Sender: TObject);

  function ColFound (tag : string) : boolean;
  var
    i : integer;
  begin
    result := false;
    for i := 1 to StringGrid1.ColCount-1 do begin
      if StringGrid1.Cells[i,0] = tag then begin
        result := true;
        exit;
      end;
    end;
  end;

begin
  SaveLblLayerInfo (cbLblLyr, edLyrIx);

  if StringGrid1.Selection.Left = -1 then
    lMsg_OK(64)  //You must select rows to be imported
  else if not ColFound (Tags[tag_hdim, 1]) then
    lMsg_OK(65)  //You must select a column for Horizontal Dimension
  else if not ColFound (Tags[tag_vdim, 1]) then
    lMsg_OK(66)  //You must select a column for Vertical Dimension
  else if not (ColFound (Tags[tag_nbr, 1]) or ColFound (Tags[tag_name, 1])) then
    lMsg_OK(67)  //You must select a column for at least 1 of name or number
  else
    ModalResult := mrOk;
end;

procedure TImportXL.cbLblLyrChange(Sender: TObject);
begin
  LblLyrChange(cbLblLyr, edLyrIx, lblLyrIx);
end;

procedure TImportXL.cbSheetsChange(Sender: TObject);
var
  ExistingExcel : boolean;
begin
    screen.cursor:= crHourGlass;
    StringGrid1.ColCount := 1;
    StringGrid1.RowCount := 1;
    try
      try
        ExcelApplication := GetActiveOleObject('Excel.Application');
        ExistingExcel := true;
      except
        ExcelApplication := CreateOleObject('Excel.Application');
        ExistingExcel := false;
      end;

      //ExcelApplication := CreateOleObject('Excel.Application');
      if not ExistingExcel then
        ExcelApplication.Visible := false;
      wb := ExcelApplication.Workbooks.Open(string(l^.ent2.txtstr));
      PopulateGrid;
    except
      lmsg_ok (62); //Problem processing Excel file|(check file is valid and that Excel is installed on this computer)
      try
        if not ExcelApplication.Visible then  //ExistingExcel then
          ExcelApplication.Workbooks.Close;
      except
      end;
      {try
        if not ExistingExcel then
          ExcelApplication.Quit;
      except
      end;}
    end;
    screen.cursor:= crDefault;
end;

procedure TImportXL.dceCustomUnitsExit(Sender: TObject);
begin
  SaveDimUnits;
end;

procedure TImportXL.MenuClick(Sender: TObject);
var
  pos, col : integer;
  s : string;
begin
  if (TMenuItem(Sender)).Tag = -1 then begin
    StringGrid1.Cells[ACol, 0] := xlsColHeading(ACol);
    exit;
  end;
  s := (TMenuItem(Sender)).Caption;
  pos := ansipos ('}', s);
  if pos > 0 then
    delete (s, pos+1, 100);
  pos := ansipos ('&', s);
  if pos > 0 then
    delete (s, pos, 1);
  StringGrid1.Cells[ACol, 0] := s;
  for col := 1 to StringGrid1.ColCount-1 do
    if col <> ACol then begin
      if StringGrid1.Cells[col, 0] = s then begin
        StringGrid1.Cells[col, 0] := xlsColHeading(col);
      end;
    end;
end;


procedure TImportXL.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  SaveFormPos (TForm(Sender));
end;

procedure TImportXL.FormCreate(Sender: TObject);
var
  i : integer;
  menuItem, MenuWriteItem : TMenuItem;
  UserField : UserFieldDef;
  s : string;
begin
  SetFormPos (TForm(Sender));
  LabelCaption(319, lblSheetName);    //Sheet Name
  LabelCaption(320, lblSheetDims);    //Dimensions on this sheet are in:
  RadioCaption (321, rbFeet);   //Feet
  RadioCaption (322, rbInches);   //Inches
  RadioCaption (255, rbMeters);  //Meters
  RadioCaption (323, rbMillimeters); //Millimeters
  RadioCaption (324, rbCentimeters); //Centimeters
  RadioCaption (258, rbCustom);   //Custom
  LabelCaption (271, lblUnitSz);  //Unit

  LabelCaption (346, lblLblLyr);  //Label Layer


  PopulateLblLayers (cbLblLyr);
  cbLblLyrChange(Sender);
  edLyrIx.Text := l^.lblLyrIx;


  ButtonLblParams (101, [], btnOK);      // OK|Accept input
  ButtonLblParams (102, [], btnCancel);  // Cancel|Cancel input


  StringGrid1.ColWidths[0] := StringGrid1.ColWidths[0] div 2;

  for i := 1 to NumTags do if Tags[i, 3] = 'R' then begin  //Tags[i,3] is flag indicating how Excel can use the tag ('R' = can read/write, 'W' = write only, other = do not use)
    menuItem := TMenuItem.Create(PopupMenu1);
    menuItem.Caption := Tags[i, 1] + '  ' + Tags[i, 2];
    menuItem.OnClick := MenuClick;
    //assign it a custom integer value..
    menuItem.Tag := i;
    PopupMenu1.Items.Add(menuItem);
  end;

  i := 100;
  for UserField in l^.UserFields do begin
    menuItem := TMenuItem.Create(PopupMenu1);
    menuItem.Caption := string ('{' + UserField.tag + '}  ' + UserField.desc);
    menuItem.OnClick := MenuClick;
    i := i+1;
    menuItem.Tag := i;
    PopupMenu1.Items.Add(menuItem);
  end;

  MenuWriteItem := TMenuItem.Create(PopupMenu1);
  GetMsg (63, s); //Write Only
  menuWriteItem.Caption := s;
  menuWriteItem.Tag := 1000;
  PopupMenu1.Items.Add(menuWriteItem);

  for i := 1 to NumTags do if Tags[i, 3] = 'W' then begin  //Tags[i,3] is flag indicating how Excel can use the tag ('R' = can read/write, 'W' = write only, other = do not use)
    menuItem := TMenuItem.Create(PopupMenu1);
    menuItem.Caption := Tags[i, 1] + '  ' + Tags[i, 2];
    menuItem.OnClick := MenuClick;
    //assign it a custom integer value..
    menuItem.Tag := i;
    menuWriteItem.Add(menuItem);
  end;

  menuItem := TMenuItem.Create(PopupMenu1);
  GetMsg (68, s); //Clear
  menuItem.Caption := s;
  menuItem.OnClick := MenuClick;
  menuItem.Tag := -1;
  PopupMenu1.Items.Add(menuItem);

end;



end.
