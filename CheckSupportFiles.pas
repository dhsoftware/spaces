unit CheckSupportFiles;

interface
  uses FileUtil, UConstants, UInterfaces, SysUtils, System.IOUtils, Classes, WinApi.Windows;

  procedure CheckSupFiles;

implementation

const
  HelpFileName = 'Spaces.pdf';
  LblFileName = 'Spaces.lbl';
  MsgFileName = 'Spaces.msg';
  RefreshMsgFileName = 'SpaceRefresh.msg';
  UpdatedMsgLines : array [0..2] of integer = (97, 108, 132);
  UpdatedLblLines : array [0..1] of integer = (19, 366);

procedure CheckSupFiles;
var
	MacroPath, SupPath, DateTimeStr : string;
  s : shortstring;
  ExistFlContent, NewFlContent : TStringList;

  function GetSupPath : string;
  begin
    if SupPath = '' then begin
      UInterfaces.getpath (s, pathsup);
      SupPath := string(s) + 'dhSoftware\';
      if not DirectoryExists (SupPath) then
        CreateDir (SupPath);
    end;
    result := string (SupPath);
  end;

  function GetDateTimeStr : string;
  begin
    if DateTimeStr = '' then
       DateTimeStr := FormatDateTime ('yymmddhhnn', Now);
    result := DateTimeStr;
  end;

  procedure UpdateFile (const ndxs : array of integer; const filename : string);
  var
    i, j : integer;
    s, t, u : string;
  begin
    for i := 0 to High(ndxs) do begin
      if NewFlContent.Count >= ndxs[i] then begin
        while ExistFlContent.Count < ndxs[i] do
          ExistFlContent.Add('');
        s := ExistFlContent[ndxs[i]-1];
        j := pos ('//', s);
        if j > 0 then begin
          t := trimright (s.Substring(0, j-1));
          j := pos ('//', NewFlContent[ndxs[i]-1]);
          if j > 0 then
            u := NewFlContent[ndxs[i]-1].Substring(0, j-1)
          else
            u := NewFlContent[ndxs[i]-1];
          if trimright(u) = t then
            // do nothing
          else begin
            ExistFlContent[ndxs[i]-1] := NewFlContent[ndxs[i]-1];
            if s > '' then
              ExistFlContent[ndxs[i]-1] := ExistFlContent[ndxs[i]-1] + ' // ' + s;
          end;
        end
        else begin
          ExistFlContent[ndxs[i]-1] := NewFlContent[ndxs[i]-1];
          if s > '' then
            ExistFlContent[ndxs[i]-1] := ExistFlContent[ndxs[i]-1] + ' // ' + s;
        end;
      end;
    end;
    ExistFlContent.SaveToFile(filename);
  end;

begin
  SupPath := '';
  DateTimeStr := '';
  UInterfaces.getpath (s, pathmcr);
  MacroPath := string(s);

  if FileExists(MacroPath+HelpFileName) then begin
    if FileExists(GetSupPath+HelpFileName) then
      SysUtils.DeleteFile (GetSupPath+HelpFileName);        // simply overwrite and replace any existing help file
    TFile.Move(MacroPath+HelpFileName, GetSupPath+HelpFileName);
  end;

  if FileExists(MacroPath+LblFileName) then begin
    if FileExists(GetSupPath+LblFileName) then begin
      if FilesTheSame (MacroPath+LblFileName, GetSupPath+LblFileName)  then
        SysUtils.DeleteFile (MacroPath+LblFileName)
      else begin
        ExistFlContent := TStringList.Create;
        try
          ExistFlContent.LoadFromFile(GetSupPath+LblFileName);
          NewFlContent := TStringList.Create;
          try
            NewFlContent.LoadFromFile(MacroPath+LblFileName);
            UpdateFile (UpdatedLblLines, GetSupPath+LblFileName);
            SysUtils.DeleteFile (MacroPath+LblFileName)
          finally
            NewFlContent.Free;
          end;
        finally
          ExistFlContent.Free;
        end;
      end;
    end
    else
      TFile.Move(MacroPath+LblFileName, GetSupPath+LblFileName);
  end;

  if FileExists(MacroPath+MsgFileName) then begin
    if FileExists(GetSupPath+MsgFileName) then begin
      if FilesTheSame (MacroPath+MsgFileName, GetSupPath+MsgFileName)then
        SysUtils.DeleteFile (MacroPath+MsgFileName)
      else begin
        ExistFlContent := TStringList.Create;
        try
          ExistFlContent.LoadFromFile(GetSupPath+MsgFileName);
          NewFlContent := TStringList.Create;
          try
            NewFlContent.LoadFromFile(MacroPath+MsgFileName);
            UpdateFile (UpdatedMsgLines, GetSupPath+MsgFileName);
            SysUtils.DeleteFile (MacroPath+MsgFileName)
          finally
            NewFlContent.Free;
          end;
        finally
          ExistFlContent.Free;
        end;
      end
    end
    else
      TFile.Move(MacroPath+MsgFileName, GetSupPath+MsgFileName);
  end;


  if FileExists(MacroPath+RefreshMsgFileName) then begin
    if FileExists(GetSupPath+RefreshMsgFileName) then begin
      if FilesTheSame (MacroPath+RefreshMsgFileName, GetSupPath+RefreshMsgFileName) then
        SysUtils.DeleteFile (MacroPath+RefreshMsgFileName)
      else begin
        SysUtils.DeleteFile (MacroPath+RefreshMsgFileName);  // there is no update to the original file in the release, so just get rid of the new file if there is an existing one.
     end;
    end
    else
      TFile.Move(MacroPath+RefreshMsgFileName, GetSupPath+RefreshMsgFileName);
  end;
end;

end.
