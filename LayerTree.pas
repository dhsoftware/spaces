unit LayerTree;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UInterfaces, URecords, Vcl.ComCtrls, CommCtrl, CommonStuff,
  System.Generics.Collections, Vcl.StdCtrls, Settings, Language;

type
  TfmLayerTree = class(TForm)
    tvLayers: TTreeView;
    btnOK: TButton;
    btnCancel: TButton;
    procedure FormCreate(Sender: TObject);
    procedure tvLayersClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    onitems, offitems : TList<lgl_addr>;
    NodeOn, NodeOff : TTreeNode;
    procedure SetTreeViewNodeChecked(Node: TTreeNode; checked : boolean; Recursive: Boolean);
  public
    { Public declarations }
    SelectedLayers : TList<lgl_addr>;
    function IsChecked (node : TTreeNode) : boolean;
  end;

//var
//  fmLayerTree: TfmLayerTree;

implementation

{$R *.dfm}

const
  NODE_CHECKED = $2000;
  NODE_UNCHECKED = $1000;


procedure TfmLayerTree.FormDestroy(Sender: TObject);
begin
  OnItems.Free;
  OffItems.Free;
  SelectedLayers.Free;
end;

function TfmLayerTree.IsChecked (node : TTreeNode) : boolean;
var
  tvItem : TTVItem;
begin
  tvItem.mask := TVIF_STATE;
  tvItem.hItem := Node.ItemId;
  TreeView_GetItem(Node.TreeView.Handle, tvItem);
  result := (tvItem.state and NODE_CHECKED) > 0;
end;

procedure TfmLayerTree.SetTreeViewNodeChecked(Node: TTreeNode; checked : boolean; Recursive: Boolean);
var
  i : integer;
  tvItem : TTVItem;
begin
  tvItem.mask := TVIF_STATE;
  tvItem.stateMask := NODE_CHECKED or NODE_UNCHECKED;
  tvItem.hItem := Node.ItemId;
  TreeView_GetItem(Node.TreeView.Handle, tvItem);
  if checked then
    tvItem.state := NODE_CHECKED
  else
    tvItem.state := NODE_UNCHECKED;
  TreeView_SetItem(Node.TreeView.Handle, tvItem);
  if (not Recursive) then
    Exit;
  for i := 0 to Node.Count-1 do
    SetTreeViewNodeChecked(Node.Item[I], checked, True);
end;

procedure TfmLayerTree.tvLayersClick(Sender: TObject);
var
  P: TPoint;
  ClickedNode : TTreeNode;
begin
  GetCursorPos(P);
  P := tvLayers.ScreenToClient(P);
  if (htOnStateIcon in tvLayers.GetHitTestInfoAt(P.X, P.Y)) then begin
    ClickedNode := tvLayers.GetNodeAt(P.X, P.Y);
    SetTreeViewNodeChecked (ClickedNode, isChecked(ClickedNode), true);
  end;
end;

procedure TfmLayerTree.btnOKClick(Sender: TObject);
var
  i : integer;
begin
  for i := 0 to nodeOn.Count-1 do
    if isChecked (nodeOn.Item[i]) then
      SelectedLayers.Add(OnItems[i]);
  if nodeOff <> nil then for i := 0 to nodeOff.Count-1 do
    if isChecked (nodeOff.Item[i]) then
      SelectedLayers.Add(OffItems[i]);

  ModalResult := mrOK;
end;

procedure TfmLayerTree.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  SaveFormPos(TForm(Sender));
end;

procedure TfmLayerTree.FormCreate(Sender: TObject);
var
  thisNode : TTreeNode;
  lyraddr : lgl_addr;
  rlyr : rlayer;
  s : shortstring;
  tvItem : TTVItem;

begin
  SetFormPos (TForm(Sender));
  ButtonLblParams (101, [], btnOK);      // OK|Accept input
  ButtonLblParams (102, [], btnCancel);  // Cancel|Cancel input
  (Sender as TForm).Caption := GetLbl(318); //Select Layers

  SetWindowLong(tvLayers.Handle, GWL_STYLE,
                GetWindowLong(tvLayers.Handle, GWL_STYLE) or TVS_CHECKBOXES);

  OnItems := TList<lgl_addr>.Create;
  OffItems := TList<lgl_addr>.Create;
  SelectedLayers := TList<lgl_addr>.Create;
  with tvLayers do begin
    Items.BeginUpdate;
    try
      NodeOn := items.AddChild (items.item[0], 'ON layers');
      NodeOff := nil;
      lyraddr := lyr_first;
      while lyr_get (lyraddr, rlyr) do begin
        lyraddr := lyr_next (lyraddr);
        getLongLayerName (rlyr.addr, s);
        if rlyr.&on then begin
          thisNode := items.AddChild(NodeOn, trim(string(s)));
          OnItems.Add(rlyr.addr);
          if addr_equal(getlyrcurr, rlyr.addr) then begin
            tvItem.mask := TVIF_STATE;
            tvItem.stateMask := TVIS_BOLD;
            tvItem.hItem := thisNode.ItemId;
            TreeView_GetItem(thisNode.TreeView.Handle, tvItem);
            tvItem.state := TVIS_BOLD;
            TreeView_SetItem(thisNode.TreeView.Handle, tvItem);
          end;
        end
        else begin
          OffItems.Add(rlyr.addr);
          if NodeOff = nil then
            NodeOff := items.AddChild (items.item[0], 'OFF layers');
          items.AddChild(NodeOff, trim(string(s)))
        end;
      end;
      tvLayers.Items[0].Expand(true);
    finally
      Items.EndUpdate;
    end;
  end;

end;

end.
