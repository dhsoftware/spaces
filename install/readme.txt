IMPORTANT INSTALLATION INFORMATION FOLLOWS THE BELOW LICENCE AGREEMENT.

 This software is distributed free of charge and on an "AS IS" basis. To the extent 
 permitted by law, I disclaim all warranties of any kind, either express or implied,
 including but not limited to, warranties of merchantability, fitness for a particular purpose,
 and non-infringement of third-party rights.
 
 I will not be liable for any direct, indirect, actual, exemplary or any other damages
 arising from the use or inability to use this software, even if I have been advised of the
 possibility of such damages.
 
 Whilst I do solicit contributions toward the cost of developing and distributing my
 software, such contributions are entirely at your discretion and in no way constitute a
 payment for the software.
 
 You may distribute this software to others provided that you distribute the complete
 unaltered zip file provided by me at the dhsoftware.com.au web site, and that you do
 so free of charge. This includes not charging for any other software, service or 
 product that you associate with this software in such a way as to imply that a purchase
 is required in order to obtain this software (without limitation, examples of unacceptable
 charges would be charging for distribution media or for any accompanying software that is
 on the same media or contained in the same download or distribution file).  If you wish
 to make any charge at all you need to obtain specific permission from me.
 
 Whilst it is free (or because of this), I would like and expect that if you can think
 of any improvements or spot any bugs (or even spelling or formatting errors in the
 documentation) that you would let me know. Your feedback may help with future
 development of the macro.';

IF YOU AGREE WITH THE ABOVE AGREEMENT WITH THE SOFTWARE AUTHOR THEN PLEASE USE THE FOLLOWING INSTRUCTIONS TO INSTALL THE SOFTWARE:

When you extract SpacePlanner.DCX file from the zip file you will be prompted for a password.
The password is "accept".  Entry of the password will indicate acceptance of the above agreement.

This zip file contains a folder structure that reflects the structure of a typical DataCAD 
installation.
If you have a typical installation (where the Macros and Support Files folders
are located directly within your main DataCAD folder) then simply extract the Macros and
Support Files folders from the zip file into your main DataCAD folder (note that your main 
DataCAD folder normally includes the version number - e.g. 'DataCAD 22'). The result should 
be that the SpacePlanner.DCX file is extracted to your DataCAD/Macros folder, and that the remaining
files are extacted to a new 'dhSoftware' folder within your DataCA/Support Files folder.

If you do not have a typical DataCAD installation, then ensure to extract the SpacePlanner.DCX
file to your Macros folder, and to extract the dhSoftware folder from the zip file into
your Support Files folder.