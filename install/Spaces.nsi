
; The name of the installer
Name "Spaces Macro"

; The file to write
OutFile "SpacesInstall.exe"

var DCSupDir
var DCExcelDir

!define LANG_ENGLISH 3081
!define TEMP1 $R0 ;Temp variable
VIProductVersion "1.0.6.0"
VIAddVersionKey /LANG=${LANG_ENGLISH} "ProductName" "Spaces"
VIAddVersionKey /LANG=${LANG_ENGLISH} "Comments" "For use with DataCAD"
VIAddVersionKey /LANG=${LANG_ENGLISH} "CompanyName" "dhSoftware"
;VIAddVersionKey /LANG=${LANG_ENGLISH} "LegalTrademarks" "Test Application is a trademark of Fake company"
VIAddVersionKey /LANG=${LANG_ENGLISH} "LegalCopyright" "David Henderson 2020"
VIAddVersionKey /LANG=${LANG_ENGLISH} "FileDescription" "Spaces Macro for DataCAD"
VIAddVersionKey /LANG=${LANG_ENGLISH} "FileVersion" "1.0.6.0"

!include LogicLib.nsh


; StrContains
; This function does a case sensitive searches for an occurrence of a substring in a string. 
; It returns the substring if it is found. 
; Otherwise it returns null(""). 
; Written by kenglish_hi
; Adapted from StrReplace written by dandaman32
 
 
Var STR_HAYSTACK
Var STR_NEEDLE
Var STR_CONTAINS_VAR_1
Var STR_CONTAINS_VAR_2
Var STR_CONTAINS_VAR_3
Var STR_CONTAINS_VAR_4
Var STR_RETURN_VAR
 
Function StrContains
  Exch $STR_NEEDLE
  Exch 1
  Exch $STR_HAYSTACK
  ; Uncomment to debug
  ;MessageBox MB_OK 'STR_NEEDLE = $STR_NEEDLE STR_HAYSTACK = $STR_HAYSTACK '
    StrCpy $STR_RETURN_VAR ""
    StrCpy $STR_CONTAINS_VAR_1 -1
    StrLen $STR_CONTAINS_VAR_2 $STR_NEEDLE
    StrLen $STR_CONTAINS_VAR_4 $STR_HAYSTACK
    loop:
      IntOp $STR_CONTAINS_VAR_1 $STR_CONTAINS_VAR_1 + 1
      StrCpy $STR_CONTAINS_VAR_3 $STR_HAYSTACK $STR_CONTAINS_VAR_2 $STR_CONTAINS_VAR_1
      StrCmp $STR_CONTAINS_VAR_3 $STR_NEEDLE found
      StrCmp $STR_CONTAINS_VAR_1 $STR_CONTAINS_VAR_4 done
      Goto loop
    found:
      StrCpy $STR_RETURN_VAR $STR_NEEDLE
      Goto done
    done:
   Pop $STR_NEEDLE ;Prevent "invalid opcode" errors and keep the
   Exch $STR_RETURN_VAR  
FunctionEnd
 
!macro _StrContainsConstructor OUT NEEDLE HAYSTACK
  Push `${HAYSTACK}`
  Push `${NEEDLE}`
  Call StrContains
  Pop `${OUT}`
!macroend
 
!define StrContains '!insertmacro "_StrContainsConstructor"'

Function .onInit

ReadRegStr $0 HKCU "Software\Microsoft\Windows\CurrentVersion\App Paths\DCADWIN.EXE" Path
${If} $0 != ""
	StrCpy $1 "DCADWIN.ini"
	${If} ${FileExists} $0$1
		ReadINIStr $2 $0$1 "Paths" "PATH_SUPPORT"
		StrCpy $InstDir $2
		${If} ${FileExists} $InstDir
			${StrContains} $3 ":" $InstDir
			StrCmp "" $3 notfound
				Goto done
			notfound:
				StrCpy $InstDir $0$2
			done:
		${Else}
			StrCpy $InstDir $0$2
		${EndIf}
		StrCpy $DCSupDir $InstDir
		
		ReadINIStr $2 $0$1 "Paths" "PATH_EXCELFILES"
		StrCpy $InstDir $2
		${If} ${FileExists} $InstDir
		${Else}
			StrCpy $InstDir $0$2
		${EndIf}
		StrCpy $DCExcelDir $InstDir
		
		ReadINIStr $2 $0$1 "Paths" "PATH_MACROS"
		StrCpy $InstDir $2
		${If} ${FileExists} $InstDir
			${StrContains} $3 ":" $InstDir
			StrCmp "" $3 notfound1
				Goto done1
			notfound1:
				StrCpy $InstDir $0$2
			done1:
		${Else}
			StrCpy $InstDir $0$2
		${EndIf}
	${EndIf}
	${If} ${FileExists} $InstDir
	${Else}
		StrCpy $1 "DCX\"
		StrCpy $InstDir $0$1
		StrCpy $1 "Support Files"
		StrCpy $DCSupDir $0$1
		StrCpy $1 "Excel"
		StrCpy $DCExcelDir $0$1
		${If} ${FileExists} $InstDir
		${Else} 
			StrCpy $InstDir ""
		${EndIf}
		${If} $InstDir == ""
			StrCpy $1 "\DCX\"
			StrCpy $InstDir $0$1
			StrCpy $1 "\Support Files"
			StrCpy $DCSupDir $0$1
			StrCpy $1 "\Excel"
			StrCpy $DCExcelDir $0$1
			${If} ${FileExists} $InstDir
			${Else}
				StrCpy $InstDir ""
			${EndIf}
		${EndIf}
		${If} $InstDir == ""
			StrCpy $1 "Macros\"
			StrCpy $InstDir $0$1
			StrCpy $1 "Support Files"
			StrCpy $DCSupDir $0$1
			StrCpy $1 "Excel"
			StrCpy $DCExcelDir $0$1
			${If} ${FileExists} $InstDir
			${Else}
				StrCpy $InstDir ""
			${EndIf}
		${EndIf}
		${If} $InstDir == ""
			StrCpy $1 "\Macros\"
			StrCpy $InstDir $0$1
			StrCpy $1 "\Support Files"
			StrCpy $DCSupDir $0$1
			StrCpy $1 "\Excel"
			StrCpy $DCExcelDir $0$1
			${If} ${FileExists} $InstDir
			${Else}
				StrCpy $InstDir ""
			${EndIf}
		${EndIf}
	${EndIf}
${Else}
	ReadRegStr $0 HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\DCADWIN.exe" Path
	${If} $0 != ""
		StrCpy $1 "DCADWIN.ini"
		${If} ${FileExists} $0$1
			ReadINIStr $2 $0$1 "Paths" "PATH_SUPPORT"
			StrCpy $InstDir $2
			${If} ${FileExists} $InstDir
			${Else}
				StrCpy $InstDir $0$2
			${EndIf}
			StrCpy $DCSupDir $InstDir
			
			ReadINIStr $2 $0$1 "Paths" "PATH_MACROS"
			StrCpy $InstDir $2
			${If} ${FileExists} $InstDir
			${Else}
				StrCpy $InstDir $0$2
			${EndIf}
		${EndIf}
		${If} ${FileExists} $InstDir
		${Else}
			StrCpy $1 "DCX\"
			StrCpy $InstDir $0$1
			StrCpy $1 "Support Files"
			StrCpy $DCSupDir $0$1
			StrCpy $1 "Excel"
			StrCpy $DCExcelDir $0$1
			${If} ${FileExists} $InstDir
			${Else} 
				StrCpy $InstDir ""
			${EndIf}
			${If} $InstDir == ""
				StrCpy $1 "\DCX\"
				StrCpy $InstDir $0$1
				StrCpy $1 "\Support Files"
				StrCpy $DCSupDir $0$1
				StrCpy $1 "\Excel"
				StrCpy $DCExcelDir $0$1
				${If} ${FileExists} $InstDir
				${Else}
					StrCpy $InstDir ""
				${EndIf}
			${EndIf}
			${If} $InstDir == ""
				StrCpy $1 "Macros\"
				StrCpy $InstDir $0$1
				StrCpy $1 "Support Files"
				StrCpy $DCSupDir $0$1
				StrCpy $1 "Excel"
				StrCpy $DCExcelDir $0$1
				${If} ${FileExists} $InstDir
				${Else}
					StrCpy $InstDir ""
				${EndIf}
			${EndIf}
			${If} $InstDir == ""
				StrCpy $1 "\Macros\"
				StrCpy $InstDir $0$1
				StrCpy $1 "\Support Files"
				StrCpy $DCSupDir $0$1
				StrCpy $1 "\Excel"
				StrCpy $DCExcelDir $0$1
				${If} ${FileExists} $InstDir
				${Else}
					StrCpy $InstDir ""
				${EndIf}
			${EndIf}
		${EndIf}
	${ElseIf} ${FileExists} "C:\DataCAD 24\macros"
		StrCpy $InstDir "C:\DataCAD 24\macros\"
		StrCpy $DCSupDir "C:\DataCAD 24\Support Files"
		StrCpy $DCExcelDir "C:\DataCAD 24\Excel"
	${ElseIf} ${FileExists} "C:\DataCAD 23\macros"
		StrCpy $InstDir "C:\DataCAD 23\macros\"
		StrCpy $DCSupDir "C:\DataCAD 23\Support Files"
		StrCpy $DCExcelDir "C:\DataCAD 23\Excel"
	${ElseIf} ${FileExists} "C:\DataCAD 22\macros"
		StrCpy $InstDir "C:\DataCAD 22\macros\"
		StrCpy $DCSupDir "C:\DataCAD 22\Support Files"
		StrCpy $DCExcelDir "C:\DataCAD 22\Excel"
${ElseIf} ${FileExists} "C:\DataCAD 21\macros"
		StrCpy $InstDir "C:\DataCAD 21\macros\"
		StrCpy $DCSupDir "C:\DataCAD 21\Support Files"
		StrCpy $DCExcelDir "C:\DataCAD 21\Excel"
	${ElseIf} ${FileExists} "C:\DataCAD 20\macros"
		StrCpy $InstDir "C:\DataCAD 20\macros\"
		StrCpy $DCSupDir "C:\DataCAD 20\Support Files"
		StrCpy $DCExcelDir "C:\DataCAD 20\Excel"
	${ElseIf} ${FileExists} "C:\DataCAD 19\macros"
		StrCpy $InstDir "C:\DataCAD 19\macros\"
		StrCpy $DCSupDir "C:\DataCAD 19\Support Files"
		StrCpy $DCExcelDir "C:\DataCAD 19\Excel"
	${ElseIf} ${FileExists} "C:\DataCAD 18\macros"
		StrCpy $InstDir "C:\DataCAD 18\macros\"
		StrCpy $DCSupDir "C:\DataCAD 18\Support Files"
		StrCpy $DCExcelDir "C:\DataCAD 18\Excel"
	${ElseIf} ${FileExists} "C:\DataCAD 17\macros"
		StrCpy $InstDir "C:\DataCAD 17\macros\"
		StrCpy $DCSupDir "C:\DataCAD 17\Support Files"
		StrCpy $DCExcelDir "C:\DataCAD 17\Excel"
${ElseIf} ${FileExists} "C:\DataCAD 16\macros"
		StrCpy $InstDir "C:\DataCAD 16\macros\"
		StrCpy $DCSupDir "C:\DataCAD 16\Support Files"
		StrCpy $DCExcelDir "C:\DataCAD 16\Excel"
	${ElseIf} ${FileExists} "C:\DataCAD 15\macros"
		StrCpy $InstDir "C:\DataCAD 15\macros\"
		StrCpy $DCSupDir "C:\DataCAD 15\Support Files"
		StrCpy $DCExcelDir "C:\DataCAD 15\Excel"
	${ElseIf} ${FileExists} "C:\DataCAD 14\macros"
		StrCpy $InstDir "C:\DataCAD 14\macros\"
		StrCpy $DCSupDir "C:\DataCAD 14\Support Files"
		StrCpy $DCExcelDir "C:\DataCAD 23\Excel"
	${ElseIf} ${FileExists} "C:\DataCAD 13\macros"
		StrCpy $InstDir "C:\DataCAD 13\macros\"
		StrCpy $DCSupDir "C:\DataCAD 13\Support Files"
		StrCpy $DCExcelDir "C:\DataCAD 13\Excel"
	${ElseIf} ${FileExists} "C:\DataCAD 12\macros"
		StrCpy $InstDir "C:\DataCAD 12\macros\"
		StrCpy $DCSupDir "C:\DataCAD 12\Support Files"
		StrCpy $DCExcelDir "C:\DataCAD 12\Excel"
	${Else}
		StrCpy $InstDir "C:\DataCAD\Macros\"
		StrCpy $DCSupDir "C:\DataCAD\Support Files"
		StrCpy $DCExcelDir "C:\DataCAD\Excel"
	${EndIf}
${EndIf}

	StrCpy $0 $DCSupDir "" -1
	StrCmp $0 "\" 0 +2
	StrCpy $DCSupDir $DCSupDir -1
	StrCpy $0 $InstDir "" -1
	StrCmp $0 "\" 0 +2
	StrCpy $InstDir $InstDir -1

  InitPluginsDir
  File /oname=$PLUGINSDIR\Directories.ini "directories.ini"
  WriteIniStr $PLUGINSDIR\Directories.ini "Field 4" "State" "$DCSupDir"
  WriteIniStr $PLUGINSDIR\Directories.ini "Field 2" "State" "$InstDir"
FunctionEnd


Function SetCustom

  ;Display the InstallOptions dialog

  Push ${TEMP1}

    InstallOptions::dialog "$PLUGINSDIR\Directories.ini"
    Pop ${TEMP1}
  	;WriteINIStr ${DCMainDIR} "$PLUGINSDIR\test.ini" "Field 2" "State"

  Pop ${TEMP1}

FunctionEnd




; Request application privileges for Windows Vista
RequestExecutionLevel user

DirText "You should install to your existing DataCAD macro folder.  Ensure that the Destination Folder below is correct before proceeding."  "Enter existing DataCAD Macros Folder (DCX folder in early DataCAD versions)" "" "Browse for Existing DataCAD Macros or DCX Folder:"

;--------------------------------

; Pages

Page license
Page custom SetCustom ValidateCustom ": Install Directories" ;Custom page. InstallOptions gets called in SetCustom.
;Page directory
Page instfiles

; PageEx license
;   LicenseText "ReadMe"
;   LicenseData "ReadMe.txt"
;	 LicenseForceSelection off

; PageExEnd



;--------------------------------
BrandingText  /TRIMCENTER "dhSoftware"
Caption "Spaces Macro v1.0.6.0"
LicenseData "License.txt"
LicenseForceSelection checkbox "I Accept"
;LicenseForceSelection radiobuttons "I Accept" "I Decline"
; The stuff to install
Section "" ;No components page, name is not important

  ; Set output path to the installation directory.
  SetOutPath $INSTDIR
  
  ; Put file there
	File "C:\DataCAD 21.1\Macros\Spaces.dmx"
	File "C:\DCAL\Projects\spaces\Examples\SpacesExample.xlsx"
	File "C:\DataCAD 21.1\Macros\SpaceRefresh.dmx"
	SetOverwrite off
	File "C:\DCAL\Projects\spaces\SpaceRefresh.ini"
	SetOverwrite on
	
	StrCpy $0 $DCSupDir
	StrCpy $1 "\dhsoftware"
	StrCpy $DCSupDir $0$1
  SetOutPath $DCSupDir

  File "C:\DCAL\Projects\Spaces\Spaces.msg"
  File "C:\DCAL\Projects\Spaces\Spaces.lbl"
  File "C:\DCAL\Projects\Spaces\SpaceRefresh.msg"
  File "C:\DCAL\Projects\Spaces\doc\Spaces.pdf"
  
  
SectionEnd ; end the section

Function ValidateCustom

  ReadINIStr $DCSupDir "$PLUGINSDIR\Directories.ini" "Field 4" "State"
  ReadINIStr $INSTDIR "$PLUGINSDIR\Directories.ini" "Field 2" "State"

  
FunctionEnd
