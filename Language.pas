unit Language;

interface

uses Classes, UInterfaces, URecords, UConstants, Dialogs, Controls, System.UITypes,
     vcl.stdCtrls, vcl.ExtCtrls, vcl.ComCtrls, vcl.Menus, System.SysUtils, UDCLibrary;

FUNCTION LangInit (FileName : string; doLblFile : boolean) : boolean;  overload
FUNCTION LangInit (FileName : string) : boolean;  overload
PROCEDURE lplblset (KeyNdx, LblNdx : integer; params : array of shortstring);
PROCEDURE llblset (KeyNdx, LblNdx : integer);
PROCEDURE lplblsett (KeyNdx, LblNdx : integer; toggle : boolean; params : array of shortstring);
PROCEDURE llblsett (KeyNdx, LblNdx : integer; toggle : boolean);
PROCEDURE llblsetf (KeyNdx, LblNdx : integer; state : integer);
PROCEDURE lpwrtlvl (LblNdx : integer; params : array of shortstring);
PROCEDURE lwrtlvl (LblNdx : integer);
PROCEDURE GetLvl (LblNdx : integer; var Lvl : shortstring);    overload;
FUNCTION GetLvl (LblNdx : integer) : string;    overload;
PROCEDURE GetMsgParams (MsgNdx : integer; params : array of shortstring; var msg : shortstring);
PROCEDURE GetMsg (MsgNdx : integer; VAR msg : string);   overload;
PROCEDURE GetMsg (MsgNdx : integer; var msg : shortstring);  overload;
FUNCTION GetMsg (MsgNdx : integer) : string;  overload;
PROCEDURE lwrtmsg (MsgNdx : integer);
PROCEDURE lpwrtmsg (MsgNdx : integer; const params : array of shortstring);
PROCEDURE lwrterr (MsgNdx : integer; doBeep : boolean);  overload;
PROCEDURE lwrterr (MsgNdx : integer);  overload;
PROCEDURE lpwrterr (MsgNdx : integer; params : array of shortstring);
PROCEDURE lpMsg_Error (MsgNdx : integer; params : array of shortstring);
PROCEDURE lpMsg_OK (MsgNdx : integer; const params : array of shortstring);
PROCEDURE lMsg_OK (MsgNdx : integer);
PROCEDURE lStrCat (var str : shortstring; MsgNdx : integer);
PROCEDURE CompoundMsg (Ndx1, Ndx2 : integer; params : array of shortstring);
FUNCTION lpMsg_Confirm (MsgNdx : integer; params : array of shortstring) : boolean;
FUNCTION lMsg_Confirm (MsgNdx : integer) : boolean;
PROCEDURE GetLblParams (LblNdx : integer; params : array of shortstring; var lbl, msg : shortstring);
PROCEDURE GetLbl (LblNdx : integer; var lbl, msg : string);    overload;
PROCEDURE GetLbl (LblNdx : integer; var lbl : string);    overload;
FUNCTION GetLbl (LblNdx : integer) : string;  overload;
PROCEDURE ButtonLblParams(LblNdx : integer;
                          params : array of shortstring;
                          var btn : TButton);
PROCEDURE PanelLblParams(LblNdx : integer;
                         params : array of shortstring;
                         var pnl : TPanel);
PROCEDURE LabelCaption (LblNdx : integer; var lbl : TLabel);
PROCEDURE RadioCaption (LblNdx : integer; var rbtn : TRadioButton);
PROCEDURE CheckCaption (LblNdx : integer; var chb : TCheckBox);
PROCEDURE ButtonCaption (LblNdx : integer; var btn : TButton);
PROCEDURE MenuItemCaption (LblNdx : integer; var itm : TMenuItem);
PROCEDURE TabCaption (LblNdx : integer; var tab : TTabSheet);
PROCEDURE GroupCaption (LblNdx : integer; var grp : TGroupBox);
PROCEDURE TabControlCaptions (var Ctrl : TTabControl; ndxs : array of integer);
PROCEDURE ComboItems (var combo : TComboBox; ndxs : array of integer);

implementation

VAR
	Msgs : TStringList;
	Lbls : TStringList;

PROCEDURE PutParams (var str : string; params : array of shortstring);
VAR
	ndx, pndx : integer;
	delspace : boolean;
BEGIN
	pndx := low(params);
	ndx := pos ('$', str, 1);
	while ndx > 0 do begin
		delete (str, ndx, 1);
		delspace := false;
		if (ndx > 1) then	begin		         // '#' immediately before '$' needs to be deleted, but it indicates to also delete the character
			if str[ndx-1] = '#' then begin   // immediately prior to the '#' if the particular parameter is an empty shortstring
				delspace := true;
				ndx := ndx-1;
				delete (str, ndx, 1);
			end;
		end;
		if ndx > length(str) then begin
			if pndx < length(params) then
				str := str + string(params[pndx])
			else
				str := str + '???';
    end
		else begin
			if delspace and (length(params[pndx]) = 0)then begin
				ndx := ndx-1;
				delete (str, ndx, 1);
			end;
			if pndx < length(params) then
				insert (string(params[pndx]), str, ndx)
			else
				insert ('???', str, ndx);
		end;
		ndx := pos ('$', str, ndx + length(params[pndx]));
		pndx := pndx+1;
	end;
END;

FUNCTION GetMsg (MsgNdx : integer) : string;  overload;
VAR
  ndx : integer;
BEGIN
	if (MsgNdx > Msgs.Count) or (MsgNdx < 1) then
		result := '***'
	else begin
		result := Msgs[MsgNdx-1];
    ndx := pos ('//', result);
    if ndx > 0 then
      setlength (result, ndx-1);
    result := trimright (result);
  end;
END;

PROCEDURE GetMsg (MsgNdx : integer; VAR msg : shortstring);
BEGIN
  msg := shortstring(GetMsg (MsgNdx));
END;

PROCEDURE GetMsg (MsgNdx : integer; VAR msg : string);
BEGIN
  msg := GetMsg(MsgNdx);
END;

PROCEDURE GetMsgParams (MsgNdx : integer; params : array of shortstring; VAR msg : shortstring);
VAR
  s : string;
BEGIN
  msg := ShortString (GetMsg(MsgNdx));
	if msg = '***' then
		exit;

	// replace $ with parameters
	s := string(Msg);
	PutParams (s, params);
  msg := shortstring(s);
END;

PROCEDURE lStrCat (VAR str : shortstring; MsgNdx : integer);
VAR
	mystr : shortstring;
BEGIN
	GetMsg (MsgNdx,mystr);
  str := str + mystr;
END;

PROCEDURE lpMsg_OK (MsgNdx : integer;const params : array of shortstring);
VAR
	str : string;
	ndx	: integer;
BEGIN
	GetMsg (MsgNdx, str);
	ndx := pos ('|', str, 1);
	while ndx > 0 do begin
		delete (str, ndx, 1);
		if ndx > length(str) then
			str := str + sLineBreak
		else
			insert (sLineBreak, str, ndx);

		ndx := pos ('|', str, ndx);
	end;

	PutParams (str, params);

  messagedlg (str, mtCustom, [mbOK], 0);
END;

PROCEDURE lMsg_OK (MsgNdx : integer);
VAR
	Dummy : array [1..1] of shortstring;
BEGIN
	lpMsg_OK (MsgNdx, Dummy);
END;

PROCEDURE lpMsg_Error (MsgNdx : integer; params : array of shortstring);
VAR
	str : string;
	ndx	: integer;
BEGIN
	GetMsg (MsgNdx, str);
	ndx := pos ('|', str, 1);
	while ndx > 0 do begin
		delete (str, ndx, 1);
		if ndx > length(str) then
			str := str + slinebreak
		else
			insert (sLineBreak, str, ndx);
		ndx := pos ('|', str, ndx);
	end;

	PutParams (str, params);

  MessageDlg (str, mtError, [mbOK], 0);
END;

FUNCTION lpMsg_Confirm (MsgNdx : integer; params : array of shortstring) : boolean;
VAR
	str : string;
	ndx	: integer;
BEGIN
	GetMsg (MsgNdx, str);
	ndx := pos ('|', str, 1);
	while ndx > 0 do begin
		delete (str, ndx, 1);
		if ndx > length(str) then
			str := str + slinebreak
		else
			insert (sLineBreak, str, ndx);
		ndx := pos ('|', str, ndx);
	end;

	PutParams (str, params);

  result := MessageDlg (str, mtConfirmation, [mbYes, mbNo], 0) = mrYes;

END;

FUNCTION lMsg_Confirm (MsgNdx : integer) : boolean;
BEGIN
	result := lpMsg_Confirm (MsgNdx, []);
END;

PROCEDURE lwrtmsg (MsgNdx : integer);
VAR
	str : shortstring;
BEGIN
	GetMsg (MsgNdx, str);
	wrtmsg (str);
END;

PROCEDURE lpwrtmsg (MsgNdx : integer; const params : array of shortstring);
VAR
	str : shortstring;
BEGIN
	GetMsgParams (MsgNdx, params, str);
	wrtmsg (str);
END;

PROCEDURE lwrterr (MsgNdx : integer; doBeep : boolean);
VAR
	str : shortstring;
BEGIN
	GetMsg (MsgNdx, str);
  wrterr (str);
  if doBeep then
    beep;
END;

PROCEDURE lwrterr (MsgNdx : integer);
VAR
	str : shortstring;
BEGIN
	GetMsg (MsgNdx, str);
  wrterr (str);
END;

PROCEDURE lpwrterr (MsgNdx : integer; params : array of shortstring);
VAR
	str : shortstring;
BEGIN
	GetMsgParams (MsgNdx, params, str);
	wrterr (str);
END;

PROCEDURE GetLblParams (LblNdx : integer;
                        params : array of shortstring;
                        var lbl, msg : shortstring);
var
	str : string;
	ndx : integer;
BEGIN
	// check that LblNdx is in the allowable range
	if (LblNdx > Lbls.count) or (LblNdx < 1) then begin
		lbl := '***';
		msg := '';
		exit;
	end;

  // get lbl and remove any comment (comments follow '//')
	str := Lbls[LblNdx-1];
  ndx := Pos ('//', str);
  if ndx > 0 then
    SetLength (str, ndx-1);
  str := trimright(str);

	// replace $ with parameters
	PutParams (str, params);

	//split into lbl & msg based on pipe character
	ndx := pos ('|', string(str), 1);
	if (ndx > 41) or ((ndx = 0) and (length(str) > 40)) then
		lbl := shortstring(copy(str,1, 40))
	else if ndx = 0 then
		lbl := shortstring(str)
	else
		lbl := shortstring(copy (str, 1, ndx-1));

	if ndx > 0 then begin
		delete (str, 1, ndx);
		msg := shortstring(str);
  end
	else
		msg := '';
END;

PROCEDURE GetLbl (LblNdx : integer;
                  var lbl, msg : string);    overload;
var
  slbl, smsg : shortstring;
BEGIN
  GetLblParams (LblNdx, [], slbl, smsg);
  lbl := string(slbl);
  msg := string(smsg);
END;

FUNCTION GetLbl (LblNdx : integer) : string;  overload;
var
  slbl, smsg : shortstring;
BEGIN
  GetLblParams (LblNdx, [], slbl, smsg);
  result := string(slbl);
END;

PROCEDURE ButtonLblParams(LblNdx : integer;
                          params : array of shortstring;
                          var btn : TButton);
var
  lbl, msg : shortstring;
BEGIN
  GetLblParams (LblNdx, params, lbl, msg);
  btn.Caption := string(lbl);
  btn.Hint := string(msg);
  btn.showHint := length(msg) > 0;
END;



PROCEDURE lpwrtlvl (LblNdx : integer; params : array of shortstring);
var
	msgstr : shortstring;
	lblstr : shortstring;
BEGIN
	GetLblParams (LblNdx, params, lblstr, msgstr);
	wrtlvl (lblstr);
END;

PROCEDURE lwrtlvl (LblNdx : integer);
var
	msgstr : shortstring;
	lblstr  : shortstring;
	dummy   : array [1..1] of shortstring;
BEGIN
	GetLblParams (LblNdx, dummy, lblstr, msgstr);
	wrtlvl (lblstr);
END;

PROCEDURE GetLvl (LblNdx : integer; var Lvl : shortstring);
VAR
	msgstr :  shortstring;
BEGIN
	GetLblParams (LblNdx, [], Lvl, msgstr);
END;

FUNCTION GetLvl (LblNdx : integer) : string;
VAR
	lvl, msgstr :  shortstring;
BEGIN
	GetLblParams (LblNdx, [], Lvl, msgstr);
  result := string(lvl);
END;


PROCEDURE lplblset (KeyNdx, LblNdx : integer; params : array of shortstring);
var
	msgstr : shortstring;
	lblstr  : shortstring;
BEGIN
	GetLblParams (LblNdx, params, lblstr, msgstr);
	lblset (KeyNdx, shortstring(lblstr));
	lblmsg (KeyNdx, shortstring(msgstr));
END;

PROCEDURE llblset (KeyNdx, LblNdx : integer);
var
	msgstr : shortstring;
	lblstr  : shortstring;
	dummy   : array [1..1] of shortstring;
BEGIN
	GetLblParams (LblNdx, dummy, lblstr, msgstr);
	lblset (KeyNdx, lblstr);
	lblmsg (KeyNdx, msgstr);
END;

PROCEDURE lplblsett (KeyNdx, LblNdx : integer; toggle : boolean; params : array of shortstring);
var
	msgstr : shortstring;
	lblstr  : shortstring;
BEGIN
	GetLblParams (LblNdx, params, lblstr, msgstr);
	lblsett (KeyNdx, shortstring(lblstr), toggle);
	lblmsg (KeyNdx, shortstring(msgstr));
END;

PROCEDURE llblsett (KeyNdx, LblNdx : integer; toggle : boolean);
var
	msgstr : shortstring;
	lblstr  : shortstring;
	dummy   : array [1..1] of shortstring;
BEGIN
	GetLblParams (LblNdx, dummy, lblstr, msgstr);
	lblsett (KeyNdx, shortstring(lblstr), toggle);
	lblmsg (KeyNdx, shortstring(msgstr));
END;

PROCEDURE llblsetf (KeyNdx, LblNdx : integer; state : integer);
var
	msgstr : shortstring;
	lblstr  : shortstring;
	dummy   : array [1..1] of shortstring;
BEGIN
	GetLblParams (LblNdx, dummy, lblstr, msgstr);
	lblsettf (KeyNdx, shortstring(lblstr), state);
	lblmsg (KeyNdx, shortstring(msgstr));
END;

PROCEDURE CompoundMsg (Ndx1, Ndx2 : integer; params : array of shortstring);
VAR
	myparams : array[1..1] of shortstring;
  str : shortstring;
BEGIN
	GetMsgParams (Ndx1, params, str);
  myparams[1] := shortstring(str);
	lpwrtmsg (Ndx2, myparams);
END;


FUNCTION LangInit (FileName : string; doLblFile : boolean) : boolean;
VAR
  tempstr : shortstring;
	flstring : string;
BEGIN
  result := true;
	UInterfaces.getpath (tempstr, pathsup);
	flstring := string(tempstr) + 'dhsoftware\' +  FileName;

  if not fileexists (flstring + '.msg') then begin
    UInterfaces.getpath (tempstr, pathmcr);
    if fileexists (string(tempstr) + 'dhsoftware\' +  FileName + '.msg') then
      flstring := string(tempstr) + 'dhsoftware\' +  FileName;
  end;

  try
    Msgs := TStringList.Create;
    Msgs.LoadFromFile(flstring + '.msg');
  except
    on e:EFOpenError do begin
      MessageDlg ('Error loading message file' + sLineBreak + sLineBreak +
                  e.Message + sLineBreak + sLineBreak + '(Macro will exit)' + sLineBreak +
                  'If you have recently upgraded to a new version of DataCAD '+
                  'then check that the dhsoftware folder has been copied from '+
                  'the Support Files folder in your old version.',
                  mtError, [mbOK], 0);

      result := false;
      exit;
    end
    else begin
      MessageDlg ('Error loading message file (Macro will exit)', mtError, [mbOK], 0);
      result := false;
      exit;
    end;
  end;
  if doLblFile then try
    UInterfaces.getpath (tempstr, pathsup);
    flstring := string(tempstr) + 'dhsoftware\' +  FileName;

    if not fileexists (flstring + '.lbl') then begin
      UInterfaces.getpath (tempstr, pathmcr);
      if fileexists (string(tempstr) + 'dhsoftware\' +  FileName + '.lbl') then
        flstring := string(tempstr) + 'dhsoftware\' +  FileName;
    end;

    Lbls := TStringList.Create;
    Lbls.LoadFromFile(flstring + '.lbl');
  except
    on e:EFOpenError do begin
      MessageDlg ('Error loading label file' + sLineBreak + sLineBreak +
                  e.Message + sLineBreak + sLineBreak + '(Macro will exit)',
                  mtError, [mbOK], 0);
      result := false;
      exit;
    end
    else begin
      MessageDlg ('Error loading label file (Macro will exit)', mtError, [mbOK], 0);
      result := false;
      exit;
    end;
  end;
END;

FUNCTION LangInit (FileName : string) : boolean;
BEGIN
  result := LangInit (FileName, true);
END;


PROCEDURE GetLbl (LblNdx : integer; var lbl : string);    overload;
var
	ndx : integer;
BEGIN
	// check that LblNdx is in the allowable range
	if (LblNdx > Lbls.count) or (LblNdx < 1) then begin
		lbl := '***';
		exit;
	end;

	lbl := Lbls[LblNdx-1];

	// discard anything after a pipe character
	ndx := pos ('|', lbl, 1);
  if ndx > 0 then
    delete (lbl, ndx, length(lbl)-ndx+1);
END;

FUNCTION GetLblMsg (LblNdx : integer; var lbl, msg : string) : boolean;
VAR
  Ndx : integer;
BEGIN
 	result :=  (LblNdx <= Lbls.count) or (LblNdx > 0);
  if not result then
		exit;
	lbl := Lbls[LblNdx-1];
  ndx := pos ('|', lbl, 1);
  if ndx > 0 then begin
    msg := copy (lbl, ndx+1, length(lbl)-ndx);
    lbl := copy (lbl, 1, ndx-1);
  end
  else
    msg := '';
END;


PROCEDURE LabelCaption (LblNdx : integer; var lbl : TLabel);
VAR
  s, s1 : string;
BEGIN
  if GetLblMsg (LblNdx, s, s1) then begin
    lbl.Caption := s;
    if length(s1) > 0 then begin
      lbl.Hint := s1;
      lbl.ShowHint := true;
    end;
  end;
END;

PROCEDURE RadioCaption (LblNdx : integer; var rbtn : TRadioButton);
VAR
  s, s1 : string;
BEGIN
  if GetLblMsg (LblNdx, s, s1) then begin
    rbtn.Caption := s;
    if length(s1) > 0 then begin
      rbtn.Hint := s1;
      rbtn.ShowHint := true;
    end;
  end;
END;

PROCEDURE CheckCaption (LblNdx : integer; var chb : TCheckBox);
VAR
  s, s1 : string;
BEGIN
  if GetLblMsg (LblNdx, s, s1) then begin
    chb.Caption := s;
    if length(s1) > 0 then begin
      chb.Hint := s1;
      chb.ShowHint := true;
    end;
  end;
END;

PROCEDURE ButtonCaption (LblNdx : integer; var btn : TButton);
VAR
  s, s1 : string;
BEGIN
  if GetLblMsg (LblNdx, s, s1) then begin
    btn.Caption := s;
    if length(s1) > 0 then begin
      btn.Hint := s1;
      btn.ShowHint := true;
    end;
  end;
END;

PROCEDURE MenuItemCaption (LblNdx : integer; var itm : TMenuItem);
VAR
  s : string;
BEGIN
  GetLbl(LblNdx, s);
  itm.Caption := s;
END;

PROCEDURE TabCaption (LblNdx : integer; var tab : TTabSheet);
VAR
  s : string;
BEGIN
  GetLbl(LblNdx, s);
  tab.Caption := s;
END;

PROCEDURE GroupCaption (LblNdx : integer; var grp : TGroupBox);
VAR
  s : string;
BEGIN
  GetLbl(LblNdx, s);
  grp.Caption := s;
END;

PROCEDURE PanelLblParams(LblNdx : integer;
                         params : array of shortstring;
                         var pnl : TPanel);
var
  lbl, msg : shortstring;
BEGIN
  GetLblParams (LblNdx, params, lbl, msg);
  pnl.Caption := string(lbl);
  pnl.Hint := string(msg);
  pnl.showHint := length(msg) > 0;
END;

PROCEDURE TabControlCaptions (var Ctrl : TTabControl; ndxs : array of integer);
var
  i : integer;
  s : string;
BEGIN
  for i := 0 to Length(ndxs)-1 do begin
    GetLbl(ndxs[i], s);
    if i > Ctrl.Tabs.Count-1 then
      Ctrl.Tabs.Add(s)
    else
      Ctrl.Tabs[i] := s;
  end;
END;

PROCEDURE ComboItems (var combo : TComboBox; ndxs : array of integer);
var
  i : integer;
  s : string;
BEGIN
  for i := 0 to Length(ndxs)-1 do begin
    GetLbl(ndxs[i], s);
    if i > combo.items.Count-1 then
      combo.items.Add(s)
    else
      combo.items[i] := s;
  end;
END;

end.
