unit ExportSelect;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.Grids, CommonStuff, URecords,
  System.Generics.Collections, UInterfaces, UConstants, Vcl.StdCtrls, Vcl.ExtCtrls, LayerTree,
  System.Generics.Defaults, settings, ExportOptions, ExcelHelper, System.Types, Language;

type

  SpaceListRecType = record
    Num : string;
    Name : string;
    adr : lgl_addr;
    lyr : lgl_addr;
    lyrName : string;
    category : string;
    xlName : string;
    xlRow : smallint;
    DupeSource : boolean;
    Selected : boolean;
  end;

  TfExportSelect = class(TForm)
    Panel1: TPanel;
    StringGrid1: TStringGrid;
    btnByLayer: TButton;
    Label1: TLabel;
    btnSelectAll: TButton;
    btnSelectNone: TButton;
    btnExportSource: TButton;
    btnExportOther: TButton;
    chbOpenExcel: TCheckBox;
    btnCancel: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnByLayerClick(Sender: TObject);
    procedure btnSelectAllClick(Sender: TObject);
    procedure btnSelectNoneClick(Sender: TObject);
    procedure StringGrid1DrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure btnExportSourceClick(Sender: TObject);
    procedure StringGrid1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure chbGridClick(Sender: TObject);
    procedure StringGrid1DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure StringGrid1DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure btnExportOtherClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    NumbersAreNumeric : boolean;
    DragStartRow : integer;
    fmExportOptions: TfmExportOptions;
    procedure PlaceCheckBox (ColNum, RowNum : integer);
    procedure AutoSizeCol(Grid: TStringGrid; Column: integer);
    function SortByColumn(colNum : integer): boolean;
    procedure PopulateStringGrid;
  public
    { Public declarations }
    SpaceList : TList<SpaceListRecType>;
    ExportFileName : string;
    RowsAbove : integer;
    AfterLast : boolean;
    DeleteReplace : boolean;
    UpdateLinks : boolean;
    procedure SetSpaceListSelected (ndx : integer; value : boolean);
  end;

var
  fExportSelect: TfExportSelect;
  fmLayerTree: TfmLayerTree;

const
  mr_ExportLinked = 1;
  mr_ExportSpecify = 200;

implementation

{$R *.dfm}

procedure TfExportSelect.PlaceCheckBox (ColNum, RowNum : integer);

var
  NewCheckBox :TCheckBox;
  Rect        :TRect;
  TempInt     :Integer;

begin
  NewCheckBox := (StringGrid1.Objects[ColNum, RowNum] as TCheckBox);

  if NewCheckBox <> nil then
  begin
    Rect := StringGrid1.CellRect(ColNum, RowNum);
    TempInt := TRUNC((Rect.Right - Rect.Left)/2);
    NewCheckBox.Left  := StringGrid1.Left + Rect.Left + TempInt;
    NewCheckBox.Top   := StringGrid1.Top + Rect.Top+2;
    NewCheckBox.Width := TempInt;
    NewCheckBox.Height := Rect.Bottom - Rect.Top;
    NewCheckBox.Visible := True;
    NewCheckBox.Checked := SpaceList[RowNum-1].Selected;
    NewCheckBox.Name := 'chb' + IntToStr(RowNum);
    NewCheckBox.OnExit := chbGridClick;
  end;
end;

function WholeRecString (rec : SpaceListRecType) : string;
begin
  result := string (rec.Num + rec.Name) + rec.lyrname + rec.xlName + inttostr (rec.xlRow);
end;


function TfExportSelect.SortByColumn(colNum : integer): boolean;
var
  Comparison: TComparison<SpaceListRecType>;
  i : integer;
begin
  Result := false;
  Comparison := nil;
  case colNum of
    1 : begin
          Comparison := function(const Left, Right: SpaceListRecType): integer
                        begin
                          Result := 0;
                          if numbersarenumeric then begin
                            try
                              if strtofloat (Left.Num) > StrToFloat(Right.Num) then
                                Result := 1
                              else if strtofloat (Left.Num) < StrToFloat(Right.Num) then
                                Result := -1;
                            except
                            end;
                          end;
                          if Result = 0 then
                            Result := TComparer<string>.Default.Compare(string(Left.Num),string(Right.Num));
                          //if Result = 0 then
                          //  Result := TComparer<string>.Default.Compare(WholeRecString(Left),WholeRecString(Right));
                        end;
        end;
    2 : begin
          Comparison := function(const Left, Right: SpaceListRecType): integer
                        begin
                          Result := TComparer<string>.Default.Compare(string(Left.Name),string(Right.Name));
                          //if Result = 0 then
                          //  Result := TComparer<string>.Default.Compare(WholeRecString(Left),WholeRecString(Right));
                        end;
        end;
    3 : begin
          Comparison := function(const Left, Right: SpaceListRecType): integer
                        begin
                          Result := TComparer<string>.Default.Compare(Left.lyrName, Right.LyrName);
                          //if Result = 0 then
                          //  Result := TComparer<string>.Default.Compare(WholeRecString(Left),WholeRecString(Right));
                        end;
        end;
    4 : begin
          Comparison := function(const Left, Right: SpaceListRecType): integer
                        begin
                          Result := TComparer<string>.Default.Compare(Left.category, Right.category);
                          //if Result = 0 then
                          //  Result := TComparer<string>.Default.Compare(WholeRecString(Left),WholeRecString(Right));
                        end;
        end;
    5 : begin
          Comparison := function(const Left, Right: SpaceListRecType): integer
                        begin
                          Result := TComparer<string>.Default.Compare(Left.xlName, Right.xlName);
                          if Result = 0 then
                            if Left.xlRow > Right.xlRow then Result := 1
                            else if Left.xlRow < Right.xlRow then Result := -1;
                          //if Result = 0 then
                          //  Result := TComparer<string>.Default.Compare(WholeRecString(Left),WholeRecString(Right));
                        end;
        end;
  end;

  for i := 1 to StringGrid1.RowCount-1 do
    SetSpaceListSelected (i-1, (StringGrid1.Objects[0, i] as TCheckBox).Checked);

  if assigned(Comparison) then begin
    SpaceList.Sort(TComparer<SpaceListRecType>.Construct(Comparison));
    PopulateStringGrid;
  end;
end;

procedure TfExportSelect.StringGrid1DragDrop(Sender, Source: TObject; X,
  Y: Integer);
var
  MousePoint : TPoint;
  ACol, ARow : integer;
  temprec : SpaceListRecType;
  i : integer;
begin
  if (DragStartRow <= 0) then
    exit;

  {make absolutely sure that list seleced fields match grid checkboxes}
  for i := 0 to SpaceList.Count-1 do
    SetSpaceListSelected (i, (StringGrid1.Objects[0, i+1] as TCheckBox).Checked);

  {get X, Y in screen coordinates}
  MousePoint := StringGrid1.ClientToScreen(System.Classes.Point(X, Y));
  {get the cell clicked in}
  StringGrid1.MouseToCell(X, Y, ACol, ARow);

  if ARow = DragStartRow then
    exit;

  {move dragged record}
  TempRec := SpaceList[DragStartRow-1];
  SpaceList.Delete(DragStartRow-1);
  SpaceList.Insert(Arow-1, TempRec);

  PopulateStringGrid;
end;

procedure TfExportSelect.StringGrid1DragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
var
  col, row: Integer;
 begin
  Accept := (Sender = Source) and (DragStartRow > 0) ;
  If Accept Then Begin
    Stringgrid1.MouseToCell( x, y, col, row );
    Accept := (row > 0);
  End;
end;

procedure TfExportSelect.StringGrid1DrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  i : integer;
begin
  if (ACol = 0) then for i := 1 to StringGrid1.RowCount-1 do
    PlaceCheckBox(ACol, i)
  else if (ARow > 0) and  (ACol = 5) and SpaceList[ARow-1].DupeSource then
    with TStringGrid(Sender) do begin
      //paint the background Red for duplicate Excel Source cells
      Canvas.Brush.Color := clRed;
      Canvas.FillRect(Rect);
      Canvas.TextOut(Rect.Left+2,Rect.Top+2,Cells[ACol, ARow]);
    end;

end;

procedure TfExportSelect.StringGrid1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  MousePoint : TPoint;
  ACol : integer;
begin
  {get X, Y in screen coordinates}
  MousePoint := StringGrid1.ClientToScreen(System.Classes.Point(X, Y));
  {get the cell clicked in}
  StringGrid1.MouseToCell(X, Y, ACol, DragStartRow);
  if DragStartRow = 0 then begin
    SortByColumn (ACol);
  end
  else
    StringGrid1.BeginDrag (false, 5);
end;

procedure TfExportSelect.btnSelectAllClick(Sender: TObject);
var
  i : integer;
begin
  for i := 0 to SpaceList.Count-1 do
    (TCheckBox(StringGrid1.Objects[0, i+1])).Checked := true;
end;

procedure TfExportSelect.btnSelectNoneClick(Sender: TObject);
var
  i : integer;
begin
  for i := 0 to SpaceList.Count-1 do
    (TCheckBox(StringGrid1.Objects[0, i+1])).Checked := false;
end;

procedure TfExportSelect.chbGridClick(Sender: TObject);
var
  RowNum : integer;
begin
  if not string((sender as TControl).Name).StartsWith('chb') then
    exit;
  try
    RowNum := strtoint (copy ((Sender as TCheckBox).Name, 4, 10));
  except
    exit;
  end;
  SetSpaceListSelected (RowNum-1, (TCheckBox(StringGrid1.Objects[0, RowNum])).Checked);
end;

procedure TfExportSelect.btnByLayerClick(Sender: TObject);
var
  i : integer;
begin
  fmLayerTree := TfmLayerTree.Create(self);
  try
    fmLayerTree.ShowModal;
    if fmLayerTree.ModalResult = mrOk then begin
      for i := 0 to SpaceList.Count-1 do begin
        // Note: SpaceList[i] corresponds to StringGrid row i+1 (because stringgrid has a header row)
        if fmLayerTree.SelectedLayers.IndexOf(SpaceList[i].lyr) >= 0 then begin
          (TCheckBox(StringGrid1.Objects[0, i+1])).Checked := true;
          SetSpaceListSelected(i, true);
        end
        else begin
          (TCheckBox(StringGrid1.Objects[0, i+1])).Checked := false;
          SetSpaceListSelected(i, false);
        end;
      end;
    end;
  finally
    fmLayerTree.Free;
  end;
end;

procedure TfExportSelect.SetSpaceListSelected (ndx : integer; value : boolean);
var
  temprec : SpaceListRecType;
begin
  temprec := SpaceList[ndx];
  temprec.selected := value;
  SpaceList[ndx] := temprec;
end;

procedure TfExportSelect.btnExportOtherClick(Sender: TObject);
var
  OpenDlg : TOpenDialog;
  s : string;
  ss : shortstring;
  i : integer;
begin
  for i := 1 to StringGrid1.RowCount-1 do
    SetSpaceListSelected (i-1, (StringGrid1.Objects[0, i] as TCheckBox).Checked);

  OpenDlg := TOpenDialog.Create(Application);
  try
    OpenDlg.Filter := 'Excel Files (*.xls, *.xlsx)|*.xls;*.xlsx';
    OpenDlg.FilterIndex := 1;
    GetPath (ss, Pathxls);
    s := string(GetSvdStr ('SpdhLastXl', ss));
    if ansipos ('|pathxls|', s) = 1 then begin  //ExcelPath has been replaced with a tag ... need to substitute with actual Excelpath
      delete (s, 1, 9);
      insert (string(ss), s, 1);
    end;
    OpenDlg.InitialDir := ExtractFilePath (s);
    OpenDlg.FileName := ExtractFileName (s);
    OpenDlg.Options := [ofHideReadOnly];
    if OpenDlg.Execute then begin
      ExportFileName := OpenDlg.FileName;
      fmExportOptions := TfmExportOptions.Create(self);
      fmExportOptions.SetExportFile (ExportFileName);
      fmExportOptions.ShowModal;
      if fmExportOptions.ModalResult = mrOK then begin
        RowsAbove := fmExportOptions.dceRowsAbove.IntValue;
        ExportFileName := ExportFileName + '|' + fmExportOptions.cbSheets.Text;
        SaveColAssignment (ExportFileName, fmExportOptions.sgColMapping, 0);
        DeleteReplace := fmExportOptions.chbReplace.Checked;
        AfterLast := fmExportOptions.chbAddAfter.Checked;
        UpdateLinks := fmExportOptions.chbUpdateLinks.Checked;
        modalresult := mr_ExportSpecify;
      end;
    end;
  finally
    OpenDlg.Free;
  end;
end;

procedure TfExportSelect.btnExportSourceClick(Sender: TObject);
var
  i : integer;
begin
  for i := 1 to StringGrid1.RowCount-1 do
    SetSpaceListSelected (i-1, (StringGrid1.Objects[0, i] as TCheckBox).Checked);
  ModalResult := mr_ExportLinked;
end;

procedure TfExportSelect.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  SaveFormPos (TForm(Sender));
end;

procedure TfExportSelect.FormCreate(Sender: TObject);
var
  ent : entity;
  mode : mode_type;
  adr : lgl_addr;
  atr : attrib;
  pStandardAtr : ^StandardAtr;
  SpaceListRec, TempSLrec : SpaceListRecType;
  CheckBox : TCheckBox;
  i : integer;
  ss : shortstring;
  Sel : TGridRect;
  SomeWithSource : boolean;
begin
  SetFormPos (TForm(Sender));
  numbersarenumeric := true;
  SomeWithSource := false;
  mode_init (mode);
  mode_lyr (mode, lyr_all);
  mode_enttype (mode, entpln);
  mode_atr (mode, STANDARD_ATR_NAME);
  adr := ent_first (mode);
  SpaceList := TList<SpaceListRecType>.Create;

  while ent_get (ent, adr) do begin
    adr := ent_next (ent, mode);
    if atr_entfind (ent, STANDARD_ATR_NAME, atr) and (atr.atrtype = atr_str255) then begin
      pStandardAtr := addr (atr.shstr);
      SpaceListRec.Num := string(pStandardAtr^.Num);
      if numbersarenumeric then
        try
          StrToFloat (SpaceListRec.Num)
        except
          numbersarenumeric := false;
        end;
      SpaceListRec.Name := string(pStandardAtr^.Name);
      SpaceListRec.category := string (pStandardAtr^.Category);
      SpaceListRec.xlRow := pStandardAtr^.ExcelRow;
      SpaceListRec.adr := ent.addr;
      SpaceListRec.lyr := ent.lyr;
      getLongLayerName (ent.lyr, ss);
      SpaceListRec.lyrName := string(ss);
      atr.name := 'SpdhXL' + shortstring(IntToStr(pStandardAtr^.ExcelNdx));
      if atr_sysfind (atr.name, atr) and (atr.atrtype = atr_str255) then begin
        SpaceListRec.xlName := string(atr.shstr);
        if SpaceListRec.xlName.StartsWith('|pathxls|') then begin
          delete (SpaceListRec.xlName, 1, 9);
          insert ('{Excel Path}', SpaceListRec.xlName, 1);
        end;
        SpaceListRec.xlName := SpaceListRec.xlName.Replace('|', ' | ');
        SomeWithSource := SomeWithSource or (SpaceListRec.xlName.Length > 0);
      end;
      SpaceListRec.Selected := true;
      SpaceListRec.DupeSource := false;
      for i := 0 to SpaceList.Count-1 do begin
        // check for (and flag) duplicate excel source details
        if (Length(SpaceListRec.xlName) > 0) and
           (SpaceList[i].xlName = SpaceListRec.xlName) and
           (SpaceList[i].xlRow = SpaceListRec.xlRow) then begin
          SpaceListRec.DupeSource := true;
          tempSLrec := SpaceList[i];
          tempSLrec.DupeSource := true;
          SpaceList[i] := tempSLrec;
        end;
      end;
      SpaceList.Add(SpaceListRec);
    end;
  end;

  StringGrid1.RowCount := SpaceList.Count+1;
  for i := 0 to SpaceList.Count-1 do begin
    CheckBox := TCheckBox.Create(StringGrid1);
    CheckBox.Parent := Panel1;
    CheckBox.Visible := false;
    CheckBox.Color := clWhite; //StringGrid1.Color;
    StringGrid1.Objects[0, i+1] := CheckBox;
  end;
  PopulateStringGrid;

  StringGrid1.Cells[1,0] := GetLbl (325); //'Num';
  StringGrid1.Cells[2,0] := GetLbl (111); // 'Name';
  stringGrid1.Cells[3,0] := GetLbl (94);  //'Layer';
  StringGrid1.Cells[4,0] := GetLbl (113); //'Category';
  StringGrid1.Cells[5,0] := GetLbl (326); //'Linked Excel File / Sheet / Row';
  StringGrid1.Cells[5,0] := StringGrid1.Cells[5,0].Replace('/', '|');

  ButtonLblParams(327, [], btnSelectAll);   //Select All
  ButtonLblParams(328, [], btnSelectNone);   //Select None
  ButtonLblParams(329, [], btnByLayer);   //Select by Layer
  ButtonLblParams(102, [], btnCancel);  // Cancel|Cancel input
  ButtonLblParams(330, [], btnExportSource); //Export to Linked File
  btnExportSource.Enabled := SomeWithSource;
  ButtonLblParams(331, [], btnExportOther); //Export to Other File
  CheckCaption(332, chbOpenExcel);  //Open Excel After Export
  (Sender as TForm).Caption := GetLbl (333);  //Export Space Details to Excel



  for i := 1 to 5 do
    AutoSizeCol (StringGrid1, i);

  Sel.Left := -1;
  Sel.Right := -1;
  Sel.Top := -1;
  Sel.Bottom := -1;
  StringGrid1.Selection := Sel;
end;

procedure TfExportSelect.PopulateStringGrid;
var
  i : integer;
begin
  for i := 0 to SpaceList.Count-1 do begin
    StringGrid1.Cells[1, i+1] := SpaceList[i].Num;
    StringGrid1.Cells[2, i+1] := SpaceList[i].Name;
    StringGrid1.Cells[3, i+1] := SpaceList[i].lyrName;
    StringGrid1.Cells[4, i+1] := SpaceList[i].category;
    if (length(SpaceList[i].xlName) > 0) and (SpaceList[i].xlRow > 0) then
      StringGrid1.Cells[5, i+1] := SpaceList[i].xlName + ' | ' + inttostr (SpaceList[i].xlRow)
    else
      StringGrid1.Cells[5, i+1] := '';
    (StringGrid1.Objects[0, i+1] as TCheckBox).Checked := SpaceList[i].Selected;
  end;
end;

procedure TfExportSelect.AutoSizeCol(Grid: TStringGrid; Column: integer);
var
  i, W, WMax: integer;
begin
  WMax := 0;
  for i := 0 to (Grid.RowCount - 1) do begin
    W := Grid.Canvas.TextWidth(Grid.Cells[Column, i]);
    if W > WMax then
      WMax := W;
  end;
  Grid.ColWidths[Column] := WMax + Grid.DefaultRowHeight;
end;

procedure TfExportSelect.FormDestroy(Sender: TObject);
begin
  SpaceList.Free;
end;

procedure TfExportSelect.FormResize(Sender: TObject);
var
  i : integer;
begin
  if SpaceList <> nil then for i := 0 to SpaceList.Count-1 do begin
    SetSpaceListSelected (i, (StringGrid1.Objects[0, i+1] as TCheckBox).Checked);
    PlaceCheckBox(0, i+1);
  end;
end;

end.
