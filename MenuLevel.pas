unit MenuLevel;

interface
uses CommonStuff, Settings, Language, UInterfaces, UConstants, URecords, SysUtils,
CreateSpaces, Dialogs, StrUtil, ProcessLevel, PlnUtil, SpaceUtil, UDCLibrary,
System.UITypes, System.Math, ContourParam, vcl.Forms, Constants;

FUNCTION doMenuLevel : integer;

implementation




  PROCEDURE SizePrompt (msg : integer; var anchor : byte);
  BEGIN
    DrawAnchor (false);
    lblsinit;
    if (l^.state = State_GetNewX) or (l^.state = State_GetNewY) then
      llblset (1,  56) //Move Anchor|Move Anchor to the next vertex of this space
    else begin
      llblsett (1, 239, anchor=1);		//Anchor Top Lft|Anchor top left corner of space
      llblsett (2, 240, anchor=2);		//Anchor Top Rgt|Anchor top right corner of space
      llblsett (3, 241, anchor=3);		//Anchor Bot Rgt|Anchor bottom right corner of space
      llblsett (4, 242, anchor=4);		//Anchor Bot Lft|Anchor bottom left corner of space
      if l^.DefineByCenter then begin
        llblsett (7, 363, l^.KeyDimCenter);  //Dim Center|The dimension entered will be of the Wall Center
        llblsett (8, 364, not l^.KeyDimCenter);  //Dim Surface|The dimension entered will be of Wall Surface
      end;
      llblsett (10, 62, l^.xAxisAngle<>0.0);		//Angle|Select an angle for the space if it is not parralel to the X & Y axis
    end;
    llblset (20, 20); 		//Exit
    lblson;
    lwrtmsg (msg);
  END;  // SizePrompt

  PROCEDURE GridFKeys;
  BEGIN
    llblset (1, 231);     //Grid Origin|Select new Grid Origin
    llblset (2, 238);     //Grid Size|Set new temporary size for snap grid
    llblset (17, 60);    //Reset Origin|Reset grid origin to 0,0,0 (active layer only)
  END;


  PROCEDURE ShowMainMenu;
  BEGIN
    lwrtlvl (9); //Spaces
    lblsinit;
    llblsett (1, 1, l.InputMode=State_Rectangle);  //Rectangle
    llblsett (2, 2, l.InputMode=State_Pln);        //Polyline
    llblsett (3, 18, l.inputmode=State_3Pt );      //3Pt Rectangle|Create rectangle or parallelogram by selecting 3 points
    if not l^.DefineByCenter then
      llblsett (4, 3, l.inputmode=State_Contour);  //Room Contour|Define space from existing 2D lines

    llblsett (5, 4, l.InputMode=State_Void);       //Add Void
    if l.InputMode > State_3Pt then
      llblsett (6, 130, PGsavevar.srch);    //Layer Search|Search all "on" layers for entities to edit
    llblsett (7, 5, l.InputMode=State_Exist);      //Existing

    llblset (9, 10);     //Edit Spaces
    llblset (10, 59);    //Refresh/Update|Update or Refresh Labels/Reports/Display

    llblset (16, 8);     //Settings|Specify text attributes and visibility of various components
{    if l^.ShowPln in [PLshowCntr, PLshowSurf] then
      llblsetf (11, 135+ord(l^.ShowPln), 3)
    else
      llblsett (11, 135+ord(l^.ShowPln), l^.ShowPln = PLshowAll);  }                              //Settings
  

    llblset (12, 17);  //Reports
    llblset (13, 29);  //Excel|Import/Export space details from/to an Excel spreadsheet
    llblset (14, 131); //Measure
    //
    llblset (18, 13);  //Arrow
    llblset (19, 19);  //Help / About

    llblset (20,20);   //Exit
    lblson;
    case l.inputmode of
      state_rectangle, state_3Pt : lwrtmsg(1);
      state_pln : lwrtmsg(2);
      state_contour : lwrtmsg(3);
      state_exist : lwrtmsg(4);
      state_void : lwrtmsg(5);
    end;
  END;

  PROCEDURE ShowRectangleMenu;
  BEGIN
    lwrtlvl (61);   //2Pt Rectangle
    lblsinit;
    GridFKeys;
    llblset (8, 21);          //Key Size|Use the keyboard to enter room dimensions
    llblset (20, 20);         //Exit

    lblson;
    lwrtmsg (12);        //Select option, or second point of room rectangle
  END;

  PROCEDURE Show3PtMenu;
  BEGIN
    lwrtlvl(63);    //3Pt Rectangle
    lblsinit;
    llblset (4, 64);    //Tangents|Display DataCAD Tangents menu
    llblset (20, 20);   //Exit
    lblson;
    if l.state = State_3Pt then
      lwrtmsg (10)       //Select option, or 2nd point of first side
    else
      lwrtmsg (11);      //Select option, or a point to define second side
  END;

  PROCEDURE ShowMeasures;
  VAR
    pStrings : ^Strings;    // do not use element 26, it is used as a place marker only
    pAtr : ^Attrib;
    pAtrStr : ^StandardAtrStr;
    s_Area, s_AreaUnits,
    s_Perim, s_Perimunits,
    s_OutPerim, s_OutPerimunits,
    s_VoidPerim, s_VoidPerimunits,
    s_Vol, s_VolUnits,
    s_PerimArea, s_PerimAreaUnits,
    s_OutPerimArea, s_OutPerimAreaUnits,
    s_VoidPerimArea, s_VoidPerimAreaUnits : string;

  BEGIN
    pStrings := Addr (l^.ent2);
    pAtr := Addr (pStrings^[26]);
    pAtrstr := Addr (pAtr^.shstr);
    wrtlvl (pAtrstr^.fields.Num + ' ' + pAtrStr^.fields.Name);
    lblsinit;
    AreaString (pAtrstr^.fields.area, Flr, UNITS_CLIP, s_Area, s_AreaUnits);
    DisString (pAtrStr^.fields.OutlinePerim + pAtrstr^.fields.VoidPerim, UNITS_CLIP, s_Perim, s_PerimUnits);
    VolString(pAtrStr^.fields.volume, UNITS_CLIP, s_Vol, s_VolUnits);
    pStrings^[1] := shortstring (Trim((s_Area + ' ' + s_AreaUnits)));
    lplblset (1, 172, [pStrings^[1]]);   //Copy Area|Copy "$" to clipboard
    pStrings^[2] := shortstring (Trim(s_Perim + ' ' + s_PerimUnits));
    lplblset (2, 173, [pStrings^[2]]);  //Copy Perim|Copy "$" to clipboard
    if pAtrStr^.fields.VoidPerim > 0 then begin
      DisString (pAtrStr^.fields.OutlinePerim, UNITS_CLIP, s_OutPerim, s_OutPerimUnits);
      DisString (pAtrstr^.fields.VoidPerim, UNITS_CLIP, s_VoidPerim, s_VoidPerimUnits);
      pStrings^[3] := shortstring (Trim(s_OutPerim + ' ' + s_OutPerimUnits));
      lplblset (3, 177, [pStrings^[3]]); //OutLn Perim|Copy "$" to clipboard
      pStrings^[4] := shortstring (Trim(s_VoidPerim + ' ' + s_VoidPerimUnits));
      lplblset (4, 171, [pStrings^[4]]); // Void Perim|Copy perimeter of void(s) ("$") to clipboard
    end
    else begin
      pStrings^[3] := '';
      pStrings^[4] := '';
    end;
    pStrings^[5] := shortstring (Trim((s_Vol + ' ' + s_VolUnits)));
    lplblset (5, 175, [pStrings^[5]]);
    AreaString (pAtrStr^.fields.OutlinePerimArea + pAtrstr^.fields.VoidPerimArea, Wall, UNITS_CLIP, s_PerimArea, s_PerimAreaUnits);
    pStrings^[6] := shortstring (Trim(s_PerimArea + ' ' + s_PerimAreaUnits));
    lplblset (6, 174, [pStrings^[6]]); //Copy Perim Area|Copy "$" to clipboard
    if pAtrstr^.fields.VoidPerimArea > 0 then begin
      AreaString (pAtrstr^.fields.OutlinePerimArea, Wall, UNITS_CLIP, s_OutPerimArea, s_OutPerimAreaUnits);
      pStrings^[7] := shortstring (Trim(s_OutPerimArea + ' ' + s_OutPerimAreaUnits));
      lplblset (7, 176, [pStrings^[7]]);  //OutLn Perim Area|Copy area of outline perimeter ("$") to clipboard
      AreaString (pAtrstr^.fields.VoidPerimArea, Wall, UNITS_CLIP, s_VoidPerimArea, s_VoidPerimAreaUnits);
      pStrings^[8] := shortstring (Trim(s_VoidPerimArea + ' ' + s_VoidPerimAreaUnits));
      lplblset (8, 176, [pStrings^[8]]);  //OutLn Perim Area|Copy area of outline perimeter ("$") to clipboard
    end
    else begin
      pStrings^[7] := '';
      pStrings^[8] := '';
    end;

    if pAtrStr^.fields.GrossArea <> pAtrStr^.fields.Area then begin
      AreaString (pAtrStr^.fields.GrossArea, Flr, UNITS_CLIP, s_Area, s_AreaUnits);
      pStrings^[9] := shortstring(s_Area + ' ' + s_AreaUnits);
      AreaString (pAtrStr^.fields.GrossArea - pAtrStr^.fields.Area, Flr, UNITS_CLIP, s_Area, s_AreaUnits);
      pStrings^[10] := shortstring(s_Area + ' ' + s_AreaUnits);
    end
    else begin
      pStrings^[9] := '';
      pStrings^[10] := '';
    end;

    if pAtrStr^.fields.GrossVolume <> pAtrStr^.fields.volume then begin
      VolString(pAtrStr^.fields.GrossVolume, UNITS_CLIP, s_Vol, s_VolUnits);
      pStrings^[11] := shortstring(s_Vol + ' ' + s_VolUnits);
      VolString(pAtrStr^.fields.GrossVolume - pAtrStr^.fields.volume, UNITS_CLIP, s_Vol, s_VolUnits);
      pStrings^[12] := shortstring(s_Vol + ' ' + s_VolUnits);
    end
    else begin
      pStrings^[11] := '';
      pStrings^[12] := '';
    end;

    llblsett (18, 178, l^.ShowMeasuresDlg);  //Show Dlg|Display a Dialog Box showing Measurements and Details

    llblset (20, 20); //Exit
    lblson;
    lpwrterr (106, [shortstring(s_Area), shortstring(s_AreaUnits),
                    shortstring(s_Perim), shortstring(s_PerimUnits),
                    shortstring(s_Vol), shortstring(s_VolUnits)]);  //Area=$ #$, Perim=$ #$, Volume=$ #$
    if l^.ShowMeasuresDlg and l^.flagFirst then begin
      redraw;    // hilites do not display before dialog is drawn without this ...
      ShowMeasuresDlg;
    end;
    l^.flagFirst := false;
  END;

  FUNCTION doMenuLevel : integer;
  VAR
    pv : polyvert;
    tempent : entity;
    temppnt : point;
    mode : mode_type;
    d : double;
    atr : attrib;
    s : shortstring;
  BEGIN
    if l^.DoRefresh then begin
      RefreshLabels (true);
      l^.DoRefresh := false;
    end;

    l^.ShowPln := ShowPlnType ( GetSvdInt (SpdhShowPlin, ord (PLshowSurf)));  // need to do this in case they have done an undo (Ctrl/Z) that may have changed this setting

    if (fmContourParam <> nil) and ((l^.state <> State_Main) or (l.inputmode <> State_Contour)) and
       fmContourParam.Visible then
      fmContourParam.Hide;
    if (l^.state = State_Main) and (l.inputmode = State_Contour) then begin
      if (fmContourParam = nil) then
        fmContourParam := TfmContourParam.Create(Application);
      fmContourParam.Show;
    end;


    case l^.state of
      State_Main : begin
        if l^.DoRefresh then
          RefreshLabels (true);
        l^.DoRefresh := false;
        wrtlvl('Spaces');
        if l^.DefineByCenter and (l.inputmode=State_Contour) then
          l.inputmode := State_Rectangle;
        ShowMainMenu;
        getpointp(vmode_orth, false, l.getp, result);
      end;

      State_Rectangle : begin
        ShowRectangleMenu;
        rubbx^ := true;
        setrubpnt (l^.Pnt1);
        getpointp(vmode_orth, false, l.getp, result);
      end;

      State_3Pt, State_3PtA, State_Void3Pt, State_Void3PtA : begin
        Show3PtMenu;
        rubln^ := true;
        if (l.state = State_3PtA) or (l.state = State_Void3PtA) then begin
          SetGridAngle (angle (l.Pnt1, l.Pnt2));
          ent_init (tempent, entln3);
          tempent.ln3pt1 := l.Pnt1;
          tempent.ln3pt2 := l.Pnt2;
          ent_draw (tempent, drmode_white);
          setrubpnt (l.Pnt2);   // not normally needed, unless they have pressed an escape key in the meantime and clicked on another point
        end
        else begin
          setrubpnt (l.Pnt1);   // not normally needed, unless they have pressed an escape key in the meantime and clicked on another point
          l.svGridAng := GridAngle;
        end;
        getpointp(vmode_orth, false, l.getp, result);
      end;

      State_Pln : begin
        ent_init (l.ent, entpln);
        polyvert_init (pv);
        pv.shape := pv_vert;
        pv.pnt := l.getp.curs;
        pv.pnt.z := zbase;
        polyvert_add (pv, l.ent.plnfrst, l.ent.plnlast);
        l.plninit := false;
        l.closd := true;
        lblsinit;
        callGetPlin (1078, vmode_orth, true, true, l.plninit, l.closd,
                 l.ent.plnfrst, l.ent.plnlast, l.docover, l.dorect,
                 true, l.getpln, result);

      end;

      State_Void : begin
        lwrtlvl(4);   // Add Void
        lblsinit;
        llblsett (1, 1, l^.void_inputmode=State_Rectangle); //2Pt Rectangle|Create a rectangle by selecting opposite corners
        llblsett (2, 2, l^.void_InputMode=State_Pln);        //Polyline
        llblsett (3, 18, l^.void_inputmode=State_3Pt);  //3Pt Rectangle|Create rectangle or parallelogram by selecting 3 points
        llblsett (7, 5, l^.void_InputMode=State_Exist);      //Existing
        if l^.void_InputMode = State_Exist then
          llblsett (6, 130, PGsavevar.srch);    //Layer Search|Search all "on" layers for entities to edit
        llblset (20, 20); //exit
        lblson;
        lwrtmsg (97); //Enter Void polyline, or select F10 to use an existing polyline
        getpointp(vmode_orth, false, l.getp, result);
      end;

      State_VoidRectangle : begin
        lblsinit;
        GridFKeys;
        llblset (8, 21);          //Key Size|Use the keyboard to enter room dimensions
        llblset (20, 20);         //Exit

        lblson;
        lwrtmsg (28);        //Select option, or opposite corner of void rectangle
        setrubpnt (l^.Pnt1);
         rubbx^ := true;
        getpointp(vmode_orth, false, l.getp, result);
      end;

      State_VoidPln : begin
        ent_init (l.ent2, entpln);
        polyvert_init (pv);
        pv.shape := pv_vert;
        pv.pnt := l.getp.curs;
        pv.pnt.z := zbase;
        polyvert_add (pv, l.ent2.plnfrst, l.ent2.plnlast);
        l.plninit := false;
        l.closd := true;
        lblsinit;
        callGetPlin (1078, vmode_orth, true, true, l.plninit, l.closd,
                 l.ent2.plnfrst, l.ent2.plnlast, l.docover, l.dorect,
                 true, l.getpln, result);
      end;

      State_Increment : begin
        lwrtmsg (19);
        getint (l^.Increment, l^.geti, result);
      end;

      State_NameIncrement : begin
        lwrtmsg (20);
        getint (l^.NameIncrement, l^.geti, result);
      end;

      State_Esc, State_EscRefresh : begin
        SetGridAngle (l.svGridAng);
        callGlobalEsc(l.getp.key, l.globesc, result);
        l.svGridAng := GridAngle;
        if l^.state = State_EscRefresh then
          l^.state := State_DoRefresh;
      end;

      State_GridOrigin : begin
        lwrtmsg (117);        //Select new snap grid origin
        getpointp(vmode_orth, false, l.getp, result);
      end;

      State_LblMove : begin
        dragModeMove(l^.lblmode, l^.lblcntr);
        lwrtlvl (31); //Place Label
        lwrtmsg (93); //Use cursor to place label
        lblsinit;
        llblset (20, 132);  //Default|Place Label at Polyline Centroid
        llblsett (10, 133, l^.lblrotate);  //Dynamic Rotate|Rotate Label after placement
        if l^.lblvertical then
          llblset (8, 224)   //Horizontal|Rotate to horizontal
        else
          llblset (7, 223);   //Vertical|Rotate to vertical
        lblson;
        getpointp(vmode_orth, false, l.getp, result);
      end;

      State_GridSize : begin
        lwrtmsg (118);  //Enter new size for Snap Grid:
        calldgetdis(l^.gridDis, l.dgetd, result);
      end;

      State_RenumberFirst : begin
        lblsinit;
        llblset (20, 20); //Exit
        lblson;
        l^.ent2.txtstr := shortstring (IncrementNumber(l^.prevnum, l^.Increment));
        lwrtmsg (59); //Select Starting Number:
        calldgetstr255 (l^.ent2.txtstr, 9, l.getstr, result);
      end;

      State_Renumber : begin
        lblsinit;
        llblset (20, 20); //Exit
        lblson;
        l^.ent2.txtstr := shortstring (IncrementNumber(l^.prevnum, l^.Increment));
        lpwrtmsg (60, [l^.ent2.txtstr]); //Select space to renumber as $
        getpointp(vmode_orth, false, l.getp, result);
      end;


      State_LblRotate : begin
        setpoint (temppnt, 0.0);
        dragmoderot (l^.lblmode, l^.lblcntr, temppnt, l^.lblcntr);
        lblsinit;
        llblset (20, 134);  //Default|Place Label at Default Angle
        lblson;
        lwrtmsg (93); //Use cursor to place label
        getpointp(vmode_orth, false, l.getp, result);
      end;

      State_KeySizeX, State_VoidKeySizeX, State_GetNewX : begin
        SizePrompt (115, l^.Anchor);
        calldgetdis(l^.Xdis, l.dgetd, result);
      end;

      State_KeySizeY, State_VoidKeySizeY, State_GetNewY : begin
        SizePrompt (116, l^.Anchor);
        calldgetdis(l^.Ydis, l.dgetd, result);
      end;

      State_KeyAngle : begin
        lwrtmsg(178);  //Enter angle for 'X' side of rectangle:
        callGetang (748, l^.xAxisAngle, false, l^.geta, result);
      end;

      State_Arrow1, State_Arrow2 : begin
        lwrtlvl(156);  //Arrows|(level)
        lblsinit;
        with l^.ArrowSettings do begin
          if l^.state = State_Arrow1 then begin
            llblsett (1, 190, Style=0);	//Open |Use open arrow style
            llblsett (2, 191, Style=1);	//Closed |Use closed arrow style
            llblsett (3, 192, Style=2);	//Bridge |Use bridge arrow style
            llblsett (4, 193, Style=3);	//Dot |Use dot arrow style
            llblsett (5, 194, Style=4);	//Tick |Use tick arrow style

            llblsett (16, 160, LeaderType = Ldr_BSp);   //B-Spline|Draw arrow leader as a B-Spline curve (max 20 points)
            llblsett (17, 158, LeaderType = Ldr_Fillet);   //Fillet|Draw arrow leader as filletted straight lines.
            llblsett (18, 159, LeaderType = Ldr_Pln);   //Polyline|Draw arrow leader using standard polyline interface.
          end;

          if (Style <= 1) or (Style = 3) then begin
            llblsett (7, 196, Knockout);  //Knockout|Apply knockout to arrow head
            if Style > 0 then
              llblsett (8, 195, Filled);  //Filled|Fill arrow head
          end;

          llblset (10, 162);  //Size|Set Head arrow size relative to largest text specified for labels
          if Style < 2 then
            llblset (11, 161);  //Aspect|Set width to length ration of arrow head
          llblset (12, 163);  //Wgt|Set line weight of arrow
          llblsett (13, 164, l^.ArrowSettings.Clr > 0);  //Clr|Draw arrow in a colour other than the current colour
          if l^.ArrowSettings.LeaderType <> Ldr_Pln then
            llblsett (14, 165, Ortho1);		//1st Ortho|Restricts the first leader segment to the ortho grid


          if l^.State = State_Arrow1 then begin
            llblset (20, 20);	//Exit
            lwrtmsg (26);  //Select option or first point of arrow leader
          end
          else begin
            llblset (18, 169);	//Cancel|Cancel arrow input
            lwrtmsg (27);  //Select option or next point to define arrow leader
          end;
        end;

        if (l^.state = State_Arrow2) and (l^.ArrowSettings.LeaderType = Ldr_Pln) then begin
          l.plninit := false;
          l.closd := false;
          callGetPlin (1078, vmode_orth, true, true, l.plninit, l.closd,
                   l.ent.plnfrst, l.ent.plnlast, l.docover, l.dorect,
                   true, l.getpln, result);
        end
        else begin
          lblson;
          getpointp(vmode_orth, false, l.getp, result);
        end;
      end;

      State_ArrowSz: begin
        lblsinit;
        llblset (20, 20); // Exit
        lblson;
        lwrtmsg (22); //Enter Arrow Size (as multiple of largest text size in labels):
        calldgetreal (l^.ArrowSettings.Size, 4, l^.getr, result);
      end;

      State_ArrowAspect: begin
        lblsinit;
        llblset (20, 20); // Exit
        lblson;
        lwrtmsg (21); //Enter Arrow Aspect:
        calldgetreal (l^.ArrowSettings.Aspect, 4, l^.getr, result);
      end;

      State_ArrowWeight : begin
        lwrtmsg (23);  //Enter Arrow Line Weight:
        l^.geti.len := 1;
        getint (l^.ArrowSettings.Wgt, l^.geti, result);
      end;

      State_ArrowClr : begin
        lwrtmsg (24);   //Select Colour for Arrow Head:
        getclr (l^.ArrowSettings.Clr, l^.getclr, result);
      end;

      State_Measure : begin
        lwrtmsg (105);  //Select Space to Measure
        lblsinit;
        llblsett (6, 130, PGsavevar.srch);      //Layer Search|Search all "on" layers
        llblset (20, 20);   //exit;
        lblson;
        getpointp(vmode_orth, false, l.getp, result);
      end;

      State_tangents :
        menutangents (result);

      State_ShowMeasures : begin
        lwrtmsg (105);  //Select Space to Measure
        ShowMeasures;
        ShowSideLengths (l^.ent);
        getpointp(vmode_orth, false, l.getp, result);
      end;

      State_UpdateRefresh : begin
        lblsinit;
        lwrtlvl(32);   //Refresh/Update
        lwrtmsg (74);  //Select Option
        llblset (1, 69);  //Refresh All|Refresh All Space Labels (do not change label format)
        llblset (3, 70);  //Update All|Update All Space Labels Using the current format)
        llblset (4, 72);  //Update Area Lbls|Select are to update labels to current format (on same layer as outline)
        llblset (5, 71);  //Update Some|Select labels to update to current format
        llblsett (11, 139, l^.ShowPln in [PLshowSurf, PLshowAll]);  //Show Surface|Show wall surface polylines
        llblsett (12, 140, l^.ShowPln in [PLshowCntr, PLshowAll]);  //Show Center|Show wall center polylines
        llblset (20, 20);
        lblson;
        getpointp(vmode_orth, false, l.getp, result);
      end;

      State_UpdateSome : begin
        lblsinit;
        lwrtlvl(71);   //Update One Lbl
        llblset (20, 20);
        lblson;
        lwrtmsg (73); //Select Space to Update Label
        getpointp(vmode_orth, false, l.getp, result);
      end;

      State_UpdateArea, State_UpdateArea2 : begin
        lblsinit;
        llblsett (6, 130, pgSaveVar.srch); //Layer Search|Search all "on" layers
        llblset (20, 20);
        lblson;
        lwrtlvl (72);  //Update Area Lbls
        if l^.State = State_UpdateArea then begin
          lwrtmsg (75); //Select first corner of area to Update Labels
        end
        else begin
          lwrtmsg (76); //Select second corner of area to Update Labels
          rubbx^ := true;
        end;
        getpointp(vmode_orth, false, l.getp, result);
      end;

      State_Edit : begin
        lblsinit;
        lwrtlvl (10);   //Edit Spaces
        llblsett (1, 33, l^.EditSelection=1);    //Move|Move a space and optionally rotate by 90
        llblsett (2, 34, l^.EditSelection=2);    //Copy|Copy a space and optionally rotate by 90
        llblsett (3, 35, l^.EditSelection=3);    //Dynamic Rotate|Dynamic Rotate selected space about its centroid.
        llblsett (4, 36, l^.EditSelection=4);    //Rotate 90|Rotate the selected space by 90 degrees.
        llblsett (5, 37, l^.EditSelection=5);    //Move Label|Move the label for selected Space(s)
        llblsett (6, 130, PGsavevar.srch);       //Layer Search|Search all "on" layers for entities to edit
        llblsett (8, 38, l^.EditSelection=8);    //Change Size|Change the size of a rectangular space
        llblsett (9, 39, l^.EditSelection=9);    //Fold Corner|'Fold' a corner in
        llblsett (10, 40, l^.EditSelection=10);  //Move Side/Cnr|Move the side or one corner (vertex) of a space
        llblsett (11, 65, l^.EditSelection=11);  //Del Corner/Cnr|Delete a corner (vertex) of a space
        llblsett (12, 66, l^.EditSelection=12);  //Add Corner|Add a corner (vertex) to a space
        llblsett (13, 67, l^.EditSelection=13);  //Del Void|Delete a void.
        llblsett (15, 42, l^.EditSelection=15);  //Details|Change Name, Number, Category, Fill Clr etc.
        llblsett (16, 43, l^.EditSelection=16);  //ReNumber|ReNumber Spaces, auto-incrementing each time
        llblset (18, 86);   //Copy Dist/Lyr|Copy a space by a fixed distance, or copy to a new layer.
        llblset (20, 20);     //Exit
        lblson;
        case (l^.EditSelection) of
          1 : lwrtmsg (42); //Select space to move.
          2 : lwrtmsg (43); //Select space to copy.
          3,4 : lwrtmsg (44); //Select space to rotate.
          5 : lwrtmsg (45); //Select Space Label to move.
          8 : lwrtmsg (46); //Select a rectangular space to resize.
          9 : lwrtmsg (41); //Select corner to fold.
          10: lwrtmsg (47); //Select side or corner to move.
          11: lwrtmsg (56); //Select corner to delete
          12: lwrtmsg (57); //Select side to add corner into.
       //   13: lwrtmsg (61); //Select first point of indent or protrusion
          15: lwrtmsg (48); //Select Space to change details.
          16: lwrtmsg (49) //Select first space to renumber.
          else lwrtmsg (50); //Select Edit option.
        end;
        getpointp(vmode_orth, false, l.getp, result);
      end;

      State_AddCorner : begin
        if polyvert_get (pv, l^.Adr, l^.ent.plnfrst) then begin
          lblsinit;
          GridFKeys;
          llblset (20, 20);  //Exit
          lblson;
          lwrtmsg (58);   //Select location of new corner.
          drag2Pt (pv.pnt, pv.nextpnt, true, l^.ent.color);
          getpointp(vmode_orth, false, l.getp, result);
        end
        else
          PrevState;
      end;

      State_Move, State_Copy, State_Rotate : begin
        if l^.state = State_Move then
          lwrtlvl (33)   //move
        else if l^.state = State_Copy then
          lwrtlvl (34)  //copy
        else if l^.state = State_Rotate then
          lwrtlvl (35);  //Dynamic Rotate
        lblsinit;
        if l^.state <> State_Rotate then begin
          GridFKeys;
          llblset (4, 44);  //Rotate 90|Rotate the selected space by 90 degrees.
        end;
        d := GetxAxisAngle (l^.ent);
        if realequal (d, 0) or realequal (d, halfpi) then begin
          llblset (7, 45);  //Top Left|Move handle to top left of space
          llblset (8, 46);  //Top Right|Move handle to top right of space
          llblset (9, 47);  //Bottom Left|Move handle to bottom left of space
          llblset (10, 48);  //Bottom Right|Move handle to bottom right of space
        end
        else if l^.state = State_Rotate then
          llblset (6, 49);  //Straighten|Rotate the space so that a straight side is parallel to the x-axis
        if l^.state = State_Rotate then
          llblset (16, 50); //Key Angle|Key the angle to rotate by (+ve is anti-clockwise)
        llblset (20, 15);   //Cancel|Cancel
        lblson;
        mode_init (mode);
        mode_ss (mode, hilite_ss);
        if l^.state = State_Rotate then begin
          setpoint (l^.Pnt2, 0.0);
          mode_enttype (mode, entpln);  // dont show text as result may not match what is shown dynamically
          dragmoderot (mode, l^.Pnt1, l^.Pnt2, l^.Pnt1);
        end
        else begin
          dragmodemove (mode, l^.Pnt1);
          setrubpnt (l^.Pnt1);
          rubln^ := true;
        end;
        getpointp(vmode_orth, false, l.getp, result);
      end;

      State_KeyRotAngle : begin
        lblsinit;
        lblson;
        lwrtmsg (31); //Angle to Rotate Space by (+ve is anti-clockwise):
        l^.d := 0;
        getang (l^.d, l^.geta, result);
      end;

      State_MoveLbl : begin
        lwrtlvl (37); //Move Label
        lblsinit;
        llblset (1, 51); //To Centroid|Move label to space centroid
        llblset (4, 44); //Rotate 90|Rotate the selected entities by 90 degrees.
        llblset (5, 52); //Rotate|Enter an angle to rotate the Label by
        llblset (6, 53); //Rotate To...|Enter a new angle for the Label
        llblset (7, 54); //Horizontal|Rotate Label to a Horizontal orientation
        llblset (8, 55); //Vertical|Rotate Label to a Vertical orientation
        llblset (20, 20);
        lblson;
        lwrtmsg (37);    //Select an option or click new position for label.
        mode_init (mode);
        mode_ss (mode, hilite_ss);
        dragmodemove (mode, l^.Pnt1);
        setrubpnt (l^.Pnt1);
        rubln^ := true;
        getpointp(vmode_orth, false, l.getp, result);
      end;

      State_KeyRotLblAngle : begin
        lblsinit;
        lblson;
        lwrtmsg (38); //Angle to Rotate Label by (+ve is anti-clockwise):
        l^.d := 0;
        getang (l^.d, l^.geta, result);
      end;

      State_KeyLblAngle : begin
        lblsinit;
        lblson;
        lwrtmsg (39); //New Angle for Label:
        mode_init (mode);
        mode_ss (mode, hilite_ss);
        l^.d := LabelAngle (mode);
        getang (l^.d, l^.geta, result);
      end;

      State_Fold : begin
        setrubpnt(l^.Pnt1);
        rubln^ := true;
        lblsinit;
        GridFKeys;
        llblset (20, 20); //Exit
        lblson;
        lwrtmsg (51); //Select point to fold corner to
        getpointp(vmode_orth, false, l.getp, result);
      end;


      State_MoveCorner : begin
        if not polyvert_get (pv, l^.Adr, l^.ent.plnfrst) then
          UDCLibrary.beep
        else begin
          lblsinit;
          GridFKeys;
          llblset (20, 20); //Exit
          lblson;
          lwrtmsg (166);	//Select new position
          setrefpnt (l^.Pnt1);
          drag2Pt (NextPv (l^.ent, pv).pnt, PrevPv (l^.ent, pv).pnt, true, l^.ent.color);
          hi_lite (true, l^.ent, true, true);
          getpointp(vmode_orth, false, l.getp, result);
        end;
      end;

      State_MoveSide : begin
        lblsinit;
        GridFKeys;
        llblsett (5, 57, l^.MovSideStretch);		//Strecth|Stretch ajoining sides to new position of the moved side
        llblsett (6, 58, not l^.MovSideStretch);	//Insert|Insert new sides from the old to the new position of the moved side
        llblset (20, 20);	//Exit
        lblson;
        lwrtmsg (166);	//Select new position
        SetSideDrag (l^.ent, l^.Adr, l^.Pnt1);
        setrefpnt (l^.Pnt1);
        hi_lite (true, l^.ent, true, true);
        getpointp(vmode_orth, false, l.getp, result);
      end;

      State_CopyDisLyr : begin
        lwrtlvl (86); //Copy Dist/Lyr
        lblsinit;
        if (abs(l^.Pnt2.x) > abszero) or (abs(l^.Pnt2.y) > abszero) or
           (abs(l^.Pnt2.z) > abszero) or not isnil (l^.Adr)
        then begin
          llblsett (1, 87, l^.CopySelect < 2); //Single Space|Copy a single space
          llblsett (2, 88, l^.CopySelect > 1); //Area|Copy all spaces in a selected box area
          //llblset (3, 89); //This Layer|Copy all spaces on the current layer
          llblsett (6, 130, PGsavevar.srch);       //Layer Search|Search all "on" layers for entities to edit
          llblset (8, 91);  //New Dist/Lyr|Clear existing Distance & Layer settings
          llblset (9, 92); //Invert|Rotate the x/y distance by 180� (no change to z distance if set)
          llblset (10, 93); //Rotate 90�|Rotate the current distance by 90�
        end;
        s := PointDisStr (pgSaveVar^.LastDist);
        lplblset (5, 90, [s]); //Prev. Dist.|Use previously defined distance ($).

        llblsett (11, 94, not isnil (l^.Adr)); //Layer|Specify a Layer to copy to


        s := DisStr (l^.Pnt2.x);
        lplblsett (13, 95, abs(l^.Pnt2.x) > abszero, [s]);  //x:$|x distance to copy
        s := DisStr (l^.Pnt2.y);
        lplblsett (14, 96, abs(l^.Pnt2.y) > abszero, [s]);  //y:$|y distance to copy
        s := DisStr (l^.Pnt2.z);
        lplblsett (15, 97, abs(l^.Pnt2.z) > abszero, [s]);  //z:$|z distance to copy
        cvdisst (Sqrt(sqr(l^.Pnt2.x)+sqr(l^.Pnt2.y)), s);
        lplblsett (16, 98, (abs(l^.Pnt2.x) > abszero) or (abs(l^.Pnt2.y) > abszero), [s]);  //d:$|x-y distance to copy (does not take into account any z distance)
        cvangst (arctan2 (l^.Pnt2.y, l^.Pnt2.x), s);
        lplblset (17, 99, [s]); //a:$|x-y copy angle (does not take into account any z distance)
        llblset (20,20);
        lblson;

        if (abs(l^.Pnt2.x) > abszero) or (abs(l^.Pnt2.y) > abszero) or
           (abs(l^.Pnt2.z) > abszero) or not isnil (l^.Adr)
        then begin
          if l^.CopySelect < 2 then
            lwrtmsg (92)  //Select an option, or select space to copy
          else
            lwrtmsg (94); //Select an option, or select first corner or area to copy
        end
        else
          lwrtmsg (88); //Select an option or first point of distance to move
        rubln^ := false;
        getpointp(vmode_orth, false, l.getp, result);
      end;

      State_CopyDisLyr2 : begin
        lwrtlvl (86); //Copy Dist/Lyr
        lblsinit;
        s := PointDisStr (pgSaveVar^.LastDist);
        lplblset (5, 90, [s]); //Prev. Dist.|Use previously defined distance ($).
        llblset (20, 15); //Cancel
        lblson;
        lwrtmsg (89); //Select an option or second point of distance to move
        rubln^ := true;
        getpointp(vmode_orth, false, l.getp, result);
      end;

      State_CopyArea : begin
        lwrtlvl (86); //Copy Dist/Lyr
        lblsinit;
        llblset (20, 15); //Cancel
        lblson;
        lwrtmsg (98); //Select second corner of area to copy
        rubbx^ := true;
        getpointp(vmode_orth, false, l.getp, result);
      end;


      State_Excel : begin
        lwrtlvl (29);  //Excel
        lblsinit;
        wrtmsg ('');
        llblset (1, 28);  //Import|Import space definitions from an Excel spreadsheet (Excel must be installed on this computer)
        llblset (3, 30);  //Export|Export space details to an Excel spreadsheet

        llblset (18, 73); //Defaults|Specify default Column Mapping and a template to use when exporting to new files
        llblset (20, 20); //Exit
        lblson;
        getpointp(vmode_orth, false, l.getp, result);
      end;

      State_Report : begin
        lwrtlvl (17);   //Reports
        wrtmsg ('');
        lblsinit;
        llblsett (1, 74, l^.ReportSelection = RPT_ACTIVE);  //Active Layer|Report on all spaces on the current active layer
        llblsett (2, 75, l^.ReportSelection = RPT_ON);  //On Layers|Report on all spaces on Layers that are currently ON
        llblsett (3, 76, l^.ReportSelection = RPT_ALL);  //All Layers|Report on all spaces in drawing (regardless of layer begin on or off)
        llblsett (4, 77, l^.ReportSelection = RPT_AREA);  //Area|Select rectangular area to include in report.
        if (l^.ReportSelection IN [RPT_AREA, RPT_AREA_DEL]) and not isnil (ent_first (l^.RptMode)) then begin
          if l^.ReportSelection = RPT_AREA_DEL then
            llblsett (5, 78, true)  //Rem Area|Remove Spaces from those already selected by area
          else
            llblset (5, 78);  //Rem Area|Remove Spaces from those already selected by area
        end
        else begin
          mode_init (l^.RptMode);
          mode_ss (l^.RptMode, hilite_ss);
        end;
        if l^.ReportSelection = RPT_AREA then
          llblsett (6, 130, PGsavevar.srch);   //Layer Search|Search all "on" layers

        llblset (8, 79); //Create Report|Create New Report or Replace an Existing One
        if atr_sysfind ('SpdhR000', atr) then begin
          llblset (9, 85); //Rebuild All|Rebuild all reports
        end;
        llblset (16, 8);    //Settings|Specify text attributes and visibility of various components
        llblset (18, 345);  //Summary Rpt
        llblset (20, 20);   //Exit
        lblson;
        getpointp(vmode_orth, false, l.getp, result);
      end;

      State_ReportArea : begin
        lwrtlvl (80);   //Report Area
        lblsinit;
        llblsett (6, 130, PGsavevar.srch);   //Layer Search|Search all "on" layers
        llblset (20, 20); //Exit
        lblson;
        if l^.ReportSelection = RPT_AREA then
          lwrtmsg (81)  //Select second corner of area to be reported on
        else
          lwrtmsg (83); //Select second corner of area to be removed from report selection
        rubbx^ := true;
        getpointp(vmode_orth, false, l.getp, result);
      end;

      State_ReportPos : begin
        lblsinit;
        llblset (20, 15); //Cancel
        lblson;
        l^.ent2.linpt1.x := l^.Pnt1.x;
        l^.ent2.linpt1.y := l^.Pnt2.y;   // use Pnt1 as the actual top left drag point
        dragboxmov (l^.ent2.linpt1, l^.Pnt1, l^.Pnt2, clrwhite, true);
        getpointp(vmode_orth, false, l.getp, result);
      end;

      State_SkipBack : begin
        PrevState;
      end

      else begin
        Cleanup;
        result := XDone;
      end;
    end;
  END;
end.

