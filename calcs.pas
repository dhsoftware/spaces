unit calcs;

interface
uses UInterfaces, URecords, UConstants, CommonStuff, PlnUtil, Math;

PROCEDURE DoCalcs (ent : entity; var atrstr : StandardAtrStr);   overload;
PROCEDURE DoCalcs (var atrstr : StandardAtrStr);   overload;
FUNCTION SpaceHasChanged (ent : entity; var atrstr : StandardAtrStr) : boolean;


implementation

PROCEDURE DoCalcs (ent : entity;
                   var atrstr : StandardAtrStr);     overload;
VAR
  centroid, frstmoment, lowleft, upright : point;
  rlyr, svlyr : lgl_addr;
  TempEnt : entity;
  xAxisAngle : double;
  tempTotPerim : double;
BEGIN
  stopgroup;
  // create temp layer for calculation entity
  SvLyr := getlyrcurr;
  lyr_init (rlyr);
  lyr_set (rlyr);
  try
    if Flag(atrstr.fields.flags, IS_SURFACE) then
      ent_copy (ent, Tempent, true, false)    // taking a temp copy as if ent_rotate is called it appears to actually update the entity
    else
      offsetPln (ent, TempEnt, false, true, l^.WallWidth/2);

    pline_centroid (TempEnt.plnfrst, TempEnt.plnlast, atrstr.fields.area,
                    frstmoment, centroid, true, true);
    atrstr.fields.centroid_x := centroid.x;
    atrstr.fields.centroid_y := centroid.y;
    atrstr.fields.area := abs(pline_area (TempEnt.plnfrst, TempEnt.plnlast));

    PlnPerim (TempEnt.plnfrst, TempEnt.plnlast, tempTotPerim, atrstr.fields.VoidPerim, atrstr.fields.OutlinePerim);

    atrstr.fields.this_DCPerim := abs(pline_perim (Ent.plnfrst, Ent.plnlast));
    atrstr.fields.OutlinePerimArea := atrstr.fields.OutlinePerim * (TempEnt.plnhite - TempEnt.plbase);
    atrstr.fields.VoidPerimArea := atrstr.fields.VoidPerim * (TempEnt.plnhite - TempEnt.plbase);
    atrstr.fields.volume := atrstr.fields.area * (TempEnt.plnhite - TempEnt.plbase);

    if atrstr.fields.VoidPerim = 0 then
      atrstr.fields.GrossArea := atrstr.fields.Area
    else
      atrstr.fields.GrossArea := PlnGrossArea (TempEnt.plnfrst, TempEnt.plnlast);
    atrstr.fields.GrossVolume := atrstr.fields.GrossArea * (TempEnt.plnhite - TempEnt.plbase);

    xAxisAngle := getxAxisAngle (TempEnt);
    if (xAxisAngle <> 0.0) and (xAxisAngle <> Pi) then
      ent_rotate (TempEnt, centroid, -xAxisAngle);  // temporary rotation only

    ent_extent (TempEnt, lowleft, upright);

    atrstr.fields.vdim := abs (upright.y - lowleft.y);
    atrstr.fields.hdim := abs (upright.x - lowleft.x);

    atrstr.fields.this_area := abs(pline_area (ent.plnfrst, ent.plnlast));
    atrstr.fields.this_DCperim := abs(pline_perim (ent.plnfrst, ent.plnlast));

    // get rid of temp layer
    lyr_clear (rlyr);
    stopgroup;
    lyr_term (rlyr);
  finally
    lyr_set (SvLyr);
  end;
END;

PROCEDURE DoCalcs (var atrstr : StandardAtrStr);     overload;
BEGIN
  DoCalcs (l^.ent, atrstr);
END;


FUNCTION SpaceHasChanged (ent : entity; var atrstr : StandardAtrStr) : boolean;
VAR
  atr : attrib;
  rlyr, svlyr : lgl_addr;
  TempEnt : entity;
  centroid, lowleft, upright : point;
  xAxisAngle : double;
BEGIN
  result := true;
  if atr_entfind (ent, STANDARD_ATR_NAME, atr) then
    if atr.atrtype = atr_str255 then begin
      move (atr.shstr, atrstr.str, sizeof(atr.shstr));

      if atrstr.fields.this_DCperim <> abs(pline_perim (ent.plnfrst, ent.plnlast)) then
        exit;
      if atrstr.fields.this_area <> abs(pline_area (ent.plnfrst, ent.plnlast)) then
        exit;

      if atrstr.fields.Height1 <> (ent.plnhite - ent.plbase) then
        exit;

      xAxisAngle := getxAxisAngle (ent);
      if not (Flag(atrstr.fields.flags, IS_SURFACE) and RealEqual (xAxisAngle, 0.0)) then begin
        // create temp layer for calculation entity
        SvLyr := getlyrcurr;
        lyr_init (rlyr);
        lyr_set (rlyr);
        try
          if Flag(atrstr.fields.flags, IS_SURFACE) then
            ent_copy (ent, Tempent, true, false)    // taking a temp copy as if ent_rotate is called it appears to actually update the entity
          else
            offsetPln (ent, TempEnt, false, true, l^.WallWidth/2);
          if not RealEqual (xAxisAngle, 0.0) then begin
            centroid.x := atrstr.fields.centroid_x;
            centroid.y := atrstr.fields.centroid_y;
            centroid.z := 0.0;
            ent_rotate (tempent, centroid, -xAxisAngle);
          end;
          ent_extent (tempent, lowleft, upright);

          // get rid of temp layer
          lyr_clear (rlyr);
          lyr_term (rlyr);
        finally
        lyr_set (SvLyr);
        end;
      end
      else
        ent_extent (ent, lowleft, upright);

      if not RealEqual (atrstr.fields.vdim, abs (upright.y - lowleft.y)) then
        exit;
      if not RealEqual (atrstr.fields.hdim, abs (upright.x - lowleft.x)) then
        exit;
    end;

    result := false;
END;

end.
