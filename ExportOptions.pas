unit ExportOptions;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Grids, Vcl.StdCtrls, DcadNumericEdit, CommonStuff,
  Language, ComObj, Settings, UConstants, URecords, ExcelHelper, UInterfaces, System.Types,
  Vcl.Menus;

type
  TfmExportOptions = class(TForm)
    lblSheet: TLabel;
    cbSheets: TComboBox;
    chbReplace: TCheckBox;
    chbUpdateLinks: TCheckBox;
    chbAddAfter: TCheckBox;
    lblRowsToLeave: TLabel;
    dceRowsAbove: TDcadNumericEdit;
    Label3: TLabel;
    sgColMapping: TStringGrid;
    btnOK: TButton;
    btnCancel: TButton;
    PopupMenu1: TPopupMenu;
    procedure sgColMappingMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    filename : string;
    MenuCol : integer;
    procedure SetMapping;
    procedure MenuClick(Sender: TObject);
  public
    { Public declarations }
    procedure SetExportFile (Name : string);
  end;


implementation

{$R *.dfm}




procedure TfmExportOptions.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  SaveFormPos (TForm(Sender));
end;

procedure TfmExportOptions.FormCreate(Sender: TObject);
begin
  SetFormPos(TForm(Sender));
  LabelCaption (334, lblSheet); //Sheet
  CheckCaption (335, chbReplace); //Delete and replace with exported data
  CheckCaption (336, chbAddAfter); //Add export after last row of sheet
  LabelCaption (337, lblRowsToLeave); //Rows to leave above export
  CheckCaption (338, chbUpdateLinks); //Update Space/Excel Links
  ButtonLblParams (101, [], btnOK);      // OK|Accept input
  ButtonLblParams (102, [], btnCancel);  // Cancel|Cancel input
end;

procedure TfmExportOptions.MenuClick(Sender: TObject);
var
  pos, col : integer;
  s : string;
begin
  if (TMenuItem(Sender)).Tag = -1 then begin
    sgColMapping.Cells[MenuCol, 0] := xlsColHeading(MenuCol);
    exit;
  end;
  s := (TMenuItem(Sender)).Caption;
  pos := ansipos ('}', s);
  if pos > 0 then
    delete (s, pos+1, 100);
  pos := ansipos ('&', s);
  if pos > 0 then
    delete (s, pos, 1);
  sgColMapping.Cells[MenuCol, 0] := s;
  for col := 0 to sgColMapping.ColCount-1 do
    if col <> MenuCol then begin
      if sgColMapping.Cells[col, 0] = s then begin
        sgColMapping.Cells[col, 0] := xlsColHeading(col+1);
      end;
    end;
end;

procedure TfmExportOptions.SetMapping;
var
  xlsNum : integer;
  atr : attrib;
  prefix : string;

begin
  xlsNum := FileSheetNdx (str255(filename + '|' + cbSheets.text), false);
  if (xlsNum > 0) and atr_sysfind (atrname('SpdhXL' + inttostr(xlsNum) + 'C0'), atr) and (atr.atrtype = atr_str255) then
    prefix := 'SpdhXL' + inttostr(xlsNum) + 'C'
  else
    prefix := 'SpdhXLdftC';

  SetMapGridHeadings (sgColMapping, prefix, 0);
end;

procedure TfmExportOptions.sgColMappingMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  MousePoint : TPoint;
  ARow : integer;
begin
  {get X, Y in screen coordinates}
  MousePoint := sgColMapping.ClientToScreen(System.Classes.Point(X, Y));
  {get the cell clicked in}
  sgColMapping.MouseToCell(X, Y, MenuCol, ARow);
  if (ARow = 0)  then begin
    PopUpMenu1.PopUp(MousePoint.X, MousePoint.Y);
  end;
end;

procedure TfmExportOptions.SetExportFile(Name: string);
var
  xls, wb : OleVariant;
  ExistingExcel, RequiresSaveAs : boolean;
  i : integer;
  defaultfile : string;
begin
  Caption := GetMsg(123) + ' ' + Name;    //Export to ...
  fileName := name;
  ExistingExcel := false;
  try
    xls := GetActiveOleObject('Excel.Application');
    ExistingExcel := true;
  except
    try
      xls := CreateOleObject('Excel.Application');
      ExistingExcel := false;
    except
      on E : Exception do begin
        lpmsg_ok (62, [shortstring(E.Message)]); //Problem processing Excel file|(check file is valid and that Excel is installed on this computer)
        modalresult := mrCancel;
      end;
    end;
  end;

  if VarIsNull(xls) then begin
    lpMsg_Error(62, ['']);
    exit;
  end;

  if not ExistingExcel then
    xls.Visible := false;

  try
    if FileExists(Name) then
      wb := xls.Workbooks.Open(string(Name))
    else begin
      RequiresSaveAs := true;
      defaultfile := string(getsvdstr ('SpdhXLDeflt', ''));
      if defaultfile > '' then
        try
          wb := xls.Workbooks.Add (defaultfile);
        except
          if CopyFile (PWideChar(defaultfile), PWideChar(Name), true) then begin
            wb := xls.Workbooks.Open(string(Name));
            RequiresSaveAs := false;
          end
          else begin
            wb := xls.Workbooks.Add;
            lpMsg_Error (131, [shortstring(defaultfile)]);
          end;
        end
      else
        wb := xls.Workbooks.Add;

      if RequiresSaveAs then
        wb.SaveAs (Name);
    end;
  except
    on E : Exception do
      lpmsg_ok (62, [shortstring(E.Message)]); //Problem processing Excel file|(check file is valid and that Excel is installed on this computer)
  end;

  for i := 1 to wb.WorkSheets.Count do begin
    cbSheets.Items.Add(wb.WorkSheets[i].Name);
  end;
  cbSheets.ItemIndex := 0;

  if not (ExistingExcel and xls.Visible) then
    wb.Close;

  SetMapping;
  BuildExportPopUpMenu (PopupMenu1);
  for i := 0 to PopupMenu1.Items.Count-1 do
    PopupMenu1.Items[i].OnClick := MenuClick;

  dceRowsAbove.IntValue := GetSvdInt ('SpdhXLDltRow', 0);

end;

end.
