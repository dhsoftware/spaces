unit VoidForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, DcadNumericEdit;

type
  TFmVoidDtl = class(TForm)
    chbVoidBottom: TCheckBox;
    B: TLabel;
    dceVoidBottom: TDcadNumericEdit;
    chbVoidTop: TCheckBox;
    Label1: TLabel;
    dceVoidTop: TDcadNumericEdit;
    Label2: TLabel;
    Label3: TLabel;
    Button1: TButton;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmVoidDtl: TFmVoidDtl;

implementation

{$R *.dfm}

end.
