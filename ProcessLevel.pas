unit ProcessLevel;

interface
uses Windows, Forms, CommonStuff, UConstants, UInterfaces, UInterfacesRecords, URecords,
UDCLibrary, Language, RoomContour, Settings, Rectangle, About, Version, SpaceDetails,
MacroSettings, CreateSpaces, vcl.Controls, PlnUtil, ArrowHead, StrUtils, Dialogs,
System.Generics.Collections, SpaceUtil, calcs, ClipBrd, MeasureDlg, FileUtil, StrUtil,
ImportExcel, SysUtils, Excel, Report, CopyDist, SummaryRpt, ContourParam, UserFields,
DcadEdit, Constants;

PROCEDURE doProcessLevel;
PROCEDURE ShowMeasuresDlg;

implementation

  PROCEDURE ShowMeasuresDlg;
  BEGIN
      Measures := TMeasures.Create(Application);
      Measures.ShowModal;
      Measures.Free;
  END;

  FUNCTION ProcessDetails (NeedToAddEntity : boolean) : boolean;
  VAR
    i : integer;
    atr : attrib;
    adr : lgl_addr;
    SvDefinByCntr, SvSurf, SvCntr : boolean;
    rlyr : RLayer;
  BEGIN
    lyr_get (getlyrcurr, rlyr);
    if rlyr.group <> -1 then begin
      lmsg_ok (20);   //NOT CREATED (Layer is locked).
      result := false;
      exit;
    end;

    fmSpaceProperties := TfmSpaceProperties.Create(Application);
    result := fmSpaceProperties.ShowModal = mrOK;
    if result then begin
      SvDefinByCntr := l^.DefineByCenter;
      l^.DefineByCenter := fmSpaceProperties.rbWallCenter.Checked;
      SvSurf := l^.CreateAtSurface;
      l^.CreateAtSurface := fmSpaceProperties.chbWallSurface.Checked;
      SvCntr := l^.CreateAtCenter;
      l^.CreateAtCenter := fmSpaceProperties.chbWallCenter.Checked;

      ProcessOffsetLine (NeedToAddEntity, fmSpaceProperties.rbWallSurface.Checked,
                         fmSpaceProperties.chbWallSurface.Checked, fmSpaceProperties.chbWallCenter.Checked,
                         fmSpaceProperties.dceWallWidth.NumValue);

      CreateSpace (fmSpaceProperties);

      ProcessVisibility (fmSpaceProperties.rbWallSurface.Checked,
                         fmSpaceProperties.chbWallSurface.Checked,
                         fmSpaceProperties.chbWallCenter.Checked);

      if not l^.AutoPlace then
        l^.State := State_LblMove
      else begin
        DrawLabel;
        GroupLabel;
        PrevState;
      end;

      l^.DefineByCenter := SvDefinByCntr;
      l^.CreateAtSurface := SvSurf;
      l^.CreateAtCenter := SvCntr;

    end;

    fmSpaceProperties.Free;

    if l^.RecalcReport then begin
      i := 0;
      while (i < 1000) and atr_sysfind ('SpdhR' +  atrname(IntToZeroFilledStr(i, 3)), atr) do begin
        if (atr.atrtype = atr_str) and (atr.str[1] = 'A') then begin
          doReport (false, atr.str);
        end;
        i := i+1;
      end;
      adr := atr_lyrfirst (getlyrcurr);
      while atr_get (atr, adr) do begin
        adr := atr_next (atr);
        if (string(atr.name)).StartsWith('SpdhR') then begin
          if atr_sysFind (atr.name, atr) then
            doReport (false, atr.str);
        end;
      end;
    end;

  END;

  PROCEDURE CenterLabel (var lblMode : mode_type; plnent : entity);
  VAR
    ent : entity;
    adr : lgl_addr;
    movdis, lblcntr, spacecntr, temppnt : point;
    area : double;
  BEGIN
    pline_centroid (plnent.plnfrst, plnent.plnlast, area, temppnt, spacecntr, true, true);
    lblcntr := LabelCenter (plnent, spacecntr);
    subpnt (spacecntr,lblcntr,  movdis);
    adr := ent_first (lblMode);
    while ent_get (ent, adr) do begin
      adr := ent_next (ent, lblMode);
      ent_draw (ent, drmode_black);
      ent_move (ent, movdis.x, movdis.y, 0);
      ent_update (ent);
      ent_draw (ent, drmode_white);
    end;
  END;


  FUNCTION ProcessGridKey (key : asint) : boolean;
  VAR
    rlyr : RLayer;
  BEGIN
    result := true;
    case key of
      F1:
        NextState (State_GridOrigin);
      F2:
        NextState (State_GridSize);
      S7: begin
        lyr_get (getlyrcurr, rlyr);
        setpoint (rlyr.gridorg, 0.0);
        lyr_put (rlyr);
      end

      else
        result := false;
    end;
  END;



  FUNCTION ExistEntFound (pnt : point; var ent : entity; forVoid : boolean) : boolean;
  VAR
    mode : mode_type;
    atr : attrib;
  BEGIN
    result := true;
    mode_init1 (mode);
    mode_enttype (mode, entpln);
    if not ent_near (ent, pnt.x, pnt.y, mode, true) then
       result := false
    else if forVoid then
      result := true
    else begin
      if atr_entfind (ent, 'dhSPlannrPln', atr) then begin
        result := lmsg_confirm (120);   //This is an existing Space Planner Space|If converted it will no longer be recognised by the Space Planner macro||Do you wish to convert it to a 'Spaces' space?
        if result then begin
          move (atr.str[2], l.PrevName, 26);
          move (atr.str[28], l.PrevNum, 9);
        end;
      end
      else if atr_entfind (ent, STANDARD_ATR_NAME, atr) then begin
        result := false;
        lmsg_ok (138);  //This polyline is already defined as a Space|(cannot convert to a space)
      end;
    end;
  END;


  PROCEDURE DoSettings;
  BEGIN
    screen.cursor:=crHourglass;
    fmMacroSettings := TfmMacroSettings.Create(Application);
    fmMacroSettings.ShowModal;
    fmMacroSettings.Free;
  END;

  PROCEDURE ShowAboutScreen;
  BEGIN
    fmAbout := TfmAbout.Create(Application);
    fmAbout.ShowModal;
    fmAbout.Free;
  END;

  FUNCTION ProcessBreakKey (key : integer) : boolean;
  BEGIN
    result := FnKeyConv(key) < 0;

    if result then begin
      case key of
        // following is a list of keys that I believe cannot be used to change a space
        9 {tab}, 27 {Esc}, 271 {shift/tab}, 327 {home}, 331 {left arrow},
        333 {right arrow}, 328 {up arrow}, 336 {down arrow}, 329 {PgUp},
        337 {PgDn}, 338 {Ins (input mode)}, 514 {p}, 515 {o}, 517 {u}, 525 {+},
        526 {q (line type)}, 527 {Q (line type back)}, 529 {f (line spacing)},
        531 {x (snapping)}, 539 {w (line wt)}, 540 {W (line wt)}, 543 {k (kolour)},
        544 {K (Kolour)}, 569 {[}, 570 {]}, 571 {Quote (Lyr Srch Tgl)},
        1079 {Alt/o}, 537 {- (oshoot)}, 6500 {extents} :
              NextState (State_Esc)
        else  NextState (State_EscRefresh);
      end;
    end;
  END;

  PROCEDURE DrawArrow;
  VAR
    pv, pvl : polyvert;
    pnew : point;
    HeadEnt : entity;
  BEGIN
    if l^.ArrowSettings.LeaderType = Ldr_BSp then begin
      DoArrowHead (l^.ent.bsppnt[l^.ent.bspnpnt-1], l^.ent.bsppnt[l^.ent.bspnpnt], pnew, HeadEnt);
      if l^.ent.bspnpnt = 3 then begin  // get around bug that draws 3 point bsp curves incorrectly
        l^.ent.bsppnt[4] := l^.ent.bsppnt[3];
        l^.ent.bspnpnt := 4;
        ent_draw (l^.ent, drmode_black);
        l^.ent.bspnpnt := 3;
      end
      else
        ent_draw (l^.ent, drmode_black);
      if ent_get (l^.ent, l^.ent.addr) then begin
        ent_draw (l^.ent, drmode_black);
        if (l^.ent.bspnpnt < 20) and (l^.ArrowSettings.Style = 0) then
          l^.ent.bsppnt[l^.ent.bspnpnt+1] := l^.ent.bsppnt[l^.ent.bspnpnt];
        l^.ent.bsppnt[l^.ent.bspnpnt] := pnew;
        if (l^.ent.bspnpnt < 20) and (l^.ArrowSettings.Style = 0) then
          l^.ent.bspnpnt := l^.ent.bspnpnt + 1;

        if l^.ent.bspnpnt = 3 then begin // there appears to be a bug with 3 point bsp's, so add point 4 as a duplicate of point 3
          l^.ent.bsppnt[4] := l^.ent.bsppnt[3];
          l^.ent.bspnpnt := 4
        end;
        ent_update (l^.ent);
        ent_draw (l^.ent, drmode_white);
      end;
    end
    else if polyvert_get (pv, l^.ent.plnlast, l^.ent.plnfrst) then begin
      if polyvert_get (pvl, pv.prev, l^.ent.plnfrst) then begin
        DoArrowHead (pvl.pnt, pv.pnt, pnew, HeadEnt);
        if not PointsEqual(pv.pnt, pnew) then
          pv.pnt := pnew;
          polyvert_update(pv);
      end;
      ent_draw (l^.ent, drmode_white);
    end;
    ent_draw (HeadEnt, drmode_white);
    PrevState;
  END;

  FUNCTION measureEnt (pnt : point) : boolean;
  VAR
    ent, tempent : entity;
    EntList : TList<entity>;
    pStrings : ^Strings;
    pAtr : ^Attrib;
    pAtrStr : ^StandardAtrStr;
    atr : attrib;
  BEGIN
    unhilite_all (false, true, false);
    ssClear (hilite_ss);

    result := FindSpaceFromPoint (pnt, false, false, l^.ent, true);
    if not result then
      exit;

    EntList := tList<entity>.Create;
    result := FindAllSpaceEnts (l^.Ent, EntList);
    if not result then begin
      EntList.Free;
      exit;
    end;

    for tempent in EntList do begin
      if not ent_get (ent, tempent.addr) then
        ent := tempent;
      if (ent.enttype = entpln) and atr_entfind (ent, 'DC_FILL', atr) then begin
        // don't want to hilite fill
        atr.name := 'DH_FILL';
        atr_update (atr);
        hi_lite (true, ent, true, true);
        if atr_get (atr, atr.addr) then begin
          atr.name := 'DC_FILL';
          atr_update (atr);
        end;
      end
      else
        hi_lite (true, ent, true, true);
      if atr_entfind (ent, STANDARD_ATR_NAME, atr) then
        ShowSideLengths (ent);
    end;
    EntList.Free;

    pStrings := Addr (l^.ent2);
    pAtr := Addr (pStrings^[26]);
    result := atr_entFind (l^.ent, STANDARD_ATR_NAME, pAtr^);
    if not result then
      exit;

    pAtrStr := Addr (pAtr^.shstr);
    DoCalcs (pAtrStr^);
    pAtrStr^.fields.centroid_x := GetxAxisAngle(l^.ent);

  END;


  FUNCTION SpaceOnLockedLyr (var ent : entity) : boolean;
  VAR
    rlyr : RLayer;
    ents : tlist<entity>;
    myent : entity;
  BEGIN
    result := false;
    lyr_get (ent.lyr, rlyr);
    if rlyr.group <> -1 then begin
      result := true;
      exit;
    end;

    ents := tlist<entity>.create;
    try
      FindAllSpaceEnts (ent, ents);
      for myent in ents do begin
        lyr_get (myent.lyr, rlyr);
        if rlyr.group <> -1 then begin
          result := true;
          exit;
        end;
      end;
    finally
      ents.Free;
    end;

  END;



  PROCEDURE EditDetails;
  VAR
    pStandardAtr : ^StandardAtr;
    pFillDef : ^FillDef;
    atr : attrib;
    adr : entaddr;
    num : str9;
    i : integer;
    existatr : boolean;
    StartClr : integer;
  BEGIN
    fmSpaceProperties := TfmSpaceProperties.Create(Application);
    try
      if fmSpaceProperties.ShowModal = mrOK then begin
        if ent_get (l^.ent, l^.ent.addr) then begin
          l^.ent.plnhite := fmSpaceProperties.dcFloorZ.NumValue + fmSpaceProperties.fCeilingOptions.dcCeilingHeight.NumValue;
          l^.ent.plbase := fmSpaceProperties.dcFloorZ.NumValue;
          ent_update (l^.Ent);
        end;
        if atr_entfind (l^.ent, STANDARD_ATR_NAME, atr) then begin
          pStandardAtr := Addr (atr.shstr);
          pStandardAtr^.Name := str25(fmSpaceProperties.cbName.Text);
          pStandardAtr^.Num := str9(fmSpaceProperties.edNumber.Text);
          num := pStandardAtr^.Num;
          pStandardAtr^.WallThick := fmSpaceProperties.dceWallWidth.NumValue;
          pStandardAtr^.Category := str25(fmSpaceProperties.cbCategory.Text);
          setflag (pStandardAtr^.Flags, CEILING_FROM_MODEL,
                   (fmSpaceProperties.fCeilingOptions.cbCeilingType.ItemIndex > 0));
          atr_update (atr);
        end;
        if atr_entfind (l^.ent, 'DC_FILL', atr) or atr_entfind (l^.ent, 'DH_FILL', atr) then begin
          if fmSpaceProperties.chbSolidFill.Checked then begin
            atr.xdata.Data[12] := round(fmSpaceProperties.speFillOpacity.Value*2.55);
            pFillDef := Addr (atr.str[1]);
            StartClr := pFillDef^.Fill_color;
            pFillDef^.Fill_color := RGBtoDCADRGB(fmSpaceProperties.Colour1.Color);
            if (StartClr <> pFillDef^.Fill_color) and not l^.Options.CustomClr then begin
              // convert RGB to Pallete Colour
              with fmSpaceProperties.Colour1 do
                pFillDef^.Fill_color := getNearestColorPalleteIndex (
                                                          GetRValue(Color),
                                                          GetGValue(Color),
                                                          GetBValue(Color));
            end;

            atr.SPBvisible := fmSpaceProperties.chbOutline.Checked;
            atr_update (atr);
          end
          else
            atr_delent (l^.ent, atr);
        end
        else if fmSpaceProperties.chbSolidFill.Checked then begin
          StartClr := RGBtoDCADRGB(fmSpaceProperties.Colour1.Color);
          if not l^.Options.CustomClr then begin
              // convert RGB to Pallete Colour
              with fmSpaceProperties.Colour1 do
                StartClr := getNearestColorPalleteIndex (GetRValue(Color),
                                                         GetGValue(Color),
                                                         GetBValue(Color));
            end;


          atr := InitSolidFillAttribute (StartClr, 0, 0);
          atr.SPBvisible := fmSpaceProperties.chbOutline.checked;
          atr.xdata.Data[12] := round(fmSpaceProperties.speFillOpacity.Value*2.55);
          atr_add2ent (l^.ent, atr)
        end;

        for i := 0 to length(fmSpaceProperties.CatCustFields)-1 do begin
          existatr := atr_entfind (l^.ent, 'SpdhCF'+fmSpaceProperties.CatCustFields[i].tag, atr);
          if not existatr then begin
            atr_init (atr, EditFieldTypeToAtrType(fmSpaceProperties.CatCustFields[i].Input.NumberType));
            atr.name := 'SpdhCF'+fmSpaceProperties.CatCustFields[i].tag;
          end;

          case fmSpaceProperties.CatCustFields[i].Input.NumberType of
            FieldType.Distance : atr.dis := fmSpaceProperties.CatCustFields[i].Input.NumValue;
            DecDegrees, DegMinSec : atr.ang := fmSpaceProperties.CatCustFields[i].Input.NumValue;
            IntegerNum : atr.int := fmSpaceProperties.CatCustFields[i].Input.IntValue;
            DecimalNum, Area : atr.rl := fmSpaceProperties.CatCustFields[i].Input.NumValue;
            else atr.str := str80(fmSpaceProperties.CatCustFields[i].Input.Text);
          end;

          if existatr then
            atr_update (atr)
          else
            atr_add2ent (l^.ent, atr);

        end;

        ent_draw (l^.ent, drmode_white);
        UpdateCenterEnt;
        RefreshLabel (l^.ent);
        wrtmsg ('');
        adr := l^.ent.addr;
        if FindByNumber (num, adr, l^.ent2) then begin
          if lpMsg_Confirm (55, [num]) then begin  //There is an existing space numbered '$'.|Do you wish to increment it (and any subsequent numbers)?
            while FindByNumber (num, adr, l^.ent2) do begin
              num := str9(IncrementNumber (num,1));
              adr := l^.ent2.addr;
              if atr_entfind (l^.ent2, STANDARD_ATR_NAME, atr) then begin
                pStandardAtr := Addr (atr.shstr);
                pStandardAtr^.Num := num;
                atr_update (atr);
                RefreshLabel (l^.ent2);
              end;
            end;
          end;
        end;
      end;
    finally
      fmSpaceProperties.Free;
    end;

  END;

  PROCEDURE CopyParams;
  var
    fmCopyDist: TfmCopyDist;
    msg : string;
  BEGIN
    fmCopyDist := TfmCopyDist.Create(Application);
    try
      if fmCopyDist.ShowModal = mrOK then begin
        l^.Pnt2.x := fmCopyDist.dceX.NumValue;
        l^.Pnt2.y := fmCopyDist.dceY.NumValue;
        l^.Pnt2.z := fmCopyDist.dceZ.NumValue;

        if blankstring (fmCopyDist.cbLayer.Text) then
          setnil(l^.Adr)
        else begin
          getmsg (96, msg);
          if (string(fmCopyDist.cbLayer.Text)).ToUpper = msg.ToUpper then
            setnil (l^.Adr)
          else begin
            if not lyr_find (shortstring(fmCopyDist.cbLayer.Text), l^.Adr) then begin
              if lMsg_Confirm(95) then begin
                if not (lyr_create (shortstring(fmCopyDist.cbLayer.Text), l^.Adr) and
                        lyr_find (shortstring(fmCopyDist.cbLayer.Text), l^.Adr)) then
                  setnil (l^.Adr);
              end
              else
                setnil (l^.adr);
            end;
          end;
        end;
      end;
    finally
      fmCopyDist.Free;
    end;
  END;

  PROCEDURE CopyArea;
  VAR
    SvLyr : lyraddr;
    adr : entaddr;
    LyrOn : boolean;
    copyflag : byte;
    tempent : entity;
    mode : mode_type;
    EntList : TList<lgl_addr>;
    i : integer;
  BEGIN
    SvLyr := getlyrcurr;
    if not isnil(l^.Adr) then begin
      LyrOn := lyr_ison (l^.Adr);
      lyr_set (l^.Adr);
    end
    else
      LyrOn := true;
    if isnil(l^.Adr) then
      copyflag := COPY_ORIGLYR
    else
      copyflag := COPY_TOLYR;

    EntList := TList<lgl_addr>.Create;
    try
      mode_init1 (mode);
      mode_box (mode, l^.Pnt1.x, l^.Pnt1.y, l^.getp.curs.x, l^.getp.curs.y);
      mode_enttype (mode, entpln);
      mode_atr (mode, STANDARD_ATR_NAME);
      adr := ent_first (mode);
      while ent_get (l^.ent, adr) do begin
        EntList.add (l^.ent.addr);
        adr := ent_next (l^.ent, mode);
      end;

      if EntList.Count < 1 then begin
        lpwrterr (99, ['No']); //$ spaces copied
        beep;
        exit;
      end;

      for i := 0 to EntList.Count-1 do begin
        if ent_get (tempent, EntList[i]) then begin
          SpaceEnts_to_hiliteSS (tempent, copyflag, false , false, l^.ent2);
          mode_init (mode);
          mode_ss (mode, hilite_ss);
          adr := ent_first (mode);
          while ent_get (tempent, adr) do begin
            adr := ent_next (tempent, mode);
            ent_move (tempent, l^.Pnt2.x, l^.Pnt2.y, l^.Pnt2.z);
            ent_update (tempent);
            if LyrOn then
              ent_draw (tempent, drmode_white);
          end;
          ssClear (hilite_ss);
        end;
      end;
      lpwrterr (99, [shortstring(inttostr(EntList.Count))]); //$ spaces copied
    finally
      EntList.Free;
    end;
  END; //Copy Area

  PROCEDURE doProcessLevel;
  VAR
    key : integer;
    pv : polyvert;
    adr : lgl_addr;
    tempent : entity;
    rlyr : Rlayer;
    atr : attrib;
    atrstr : StandardAtrStr;
    pStrings : ^Strings;
    mode : mode_type;
    TempFile : lglFile;
    d : double;
    b : boolean;
    pStandardAtr : ^StandardAtr;
    SvLyr : lyraddr;
    copyflag : byte;
    LyrOn : boolean;
    NotCopiedFlag : boolean;
  BEGIN
    case l.state of
      State_Main :
      begin
        unhilite_all (false, true, false);
        if l.getp.Result = res_normal then begin
          l.Pnt1 := l.getp.curs;
          case l.inputmode of
            state_Contour : begin
              if fmContourParam <> nil then begin
                if fmContourParam.dceOpening.valid then
                  l^.maxopening := fmContourParam.dceOpening.NumValue;
                if fmContourParam.dceThick.valid then
                  l^.WallThick := fmContourParam.dceThick.NumValue;
              end;
              if ProcessRoom (l.Pnt1, l.ent, l.wallthick , l.maxopening) then begin
                ent_draw (l.ent, drmode_white);
                if not ProcessDetails (false) then begin
                  ent_draw (l.ent, drmode_black);
                  ent_del (l.ent);
                end;
              end;
            end;

            state_Exist : begin
              if ExistEntFound (l.Pnt1, l.ent, false) then begin
                ProcessDetails (false);
              end;
            end;

            state_Void : begin
              if FindSpaceFromPoint (l^.Pnt1, false, false, l^.ent, true) then begin
                NextState (state_Void);
                b := atr_entfind (l^.ent, 'DC_FILL', atr);
                if b then begin
                  atr.name := 'DH_FILL';
                  atr_update (atr);
                end;
                hi_lite (true, l^.ent, true, true);
                if b then begin
                  atr.name := 'DC_FILL';
                  atr_update (atr);
                end;

              end;
            end

            else
              NextState (l.inputmode);
          end;
        end
        else begin
          if not ProcessBreakKey (l.getp.key) then begin
            case l.getp.key of
              F1: begin
                l.inputmode := State_Rectangle;
                SaveInt (SpdhInptMode, State_Rectangle);
              end;

              F2: begin
                l.inputmode := State_Pln;
                SaveInt (SpdhInptMode, State_Pln);
              end;

              F3: begin
                l.inputmode := State_3Pt;
                SaveInt (SpdhInptMode, State_3Pt);
              end;

              F4: begin
                l.inputmode := State_Contour;
                SaveInt (SpdhInptMode, State_Contour);
                {if fmContourParam = nil then
                  fmContourParam := TfmContourParam.Create(Application);
                if not fmContourParam.Visible then
                  fmContourParam.Show;}
              end;

              F5: begin
                l.inputmode := State_Void;
                l^.Xdis := 0;
                l^.Ydis := 0;
                SaveInt (SpdhInptMode, State_Void);
              end;

              F6:
                if l.inputmode > State_Pln then
                  PGsavevar.srch := not PGsavevar.srch;
              F7: begin
                l.inputmode := State_Exist;
                SaveInt (SpdhInptMode, State_Exist);
              end;

              F9:
                NextState (State_Edit);
              F0:
                NextState (State_UpdateRefresh);
//            S1:
//              NextState (State_ShowPln);
              S2: begin
                    NextState (State_Report);
                    unhilite_all (true, true, true);
                    ssClear (hilite_ss);
                    l^.ReportSelection := RPT_NONE;
                  end;
              S3:
                NextState (State_Excel); //ExcelImport;
              S4:
                NextState (State_Measure);
              S6:
                DoSettings;
              S8:
                NextState (State_Arrow1);
              S9:
                ShowAboutScreen;
              S0: begin
                NextState (State_Exit);
              end
              else
                beep;
            end;
          end;
        end;
      end;

      State_Rectangle : begin
        if l^.getp.Result = res_normal then begin
          l^.ent := CreateRectangle(l^.Pnt1, l^.getp.curs);
          if not ProcessDetails (true) then begin
            PrevState;
          end;
        end
        else begin
          if not ProcessBreakKey (l.getp.key) then begin
            if not ProcessGridKey (l.getp.key) then begin
              case l.getp.key of
                F8: begin
                  l^.Anchor := 1;
                  l^.Xdis := 0;
                  l^.Ydis := 0;
                  NextState (State_KeySizeX);
                end;
                S0:
                  PrevState;
                else
                  beep;
              end;
            end;
          end;
        end;
      end;

      State_3Pt, State_Void3Pt : begin
        if l.getp.Result = res_normal then begin
          l.Pnt2 := l.getp.curs;
          NextState (l^.State + 1);
        end
        else begin
          if not ProcessBreakKey (l.getp.key) then
            case l.getp.key of
              f4: NextState (State_tangents);
              S0: begin
                PrevState;
                if l^.State = State_Void3Pt then
                  hi_lite (false, l^.ent, true, true);
                SetGridAngle (l.svGridAng);
              end
              else
                beep;
            end;
        end;
      end;

      State_3PtA, State_Void3PtA : begin
        if l.getp.Result = res_normal then begin
          SetGridAngle (l.svGridAng);
          if l^.state = State_3PtA then begin
            l.ent := Create3Pnt (l.Pnt1, l.Pnt2, l.getp.curs);
            l^.xAxisAngle := angle (l.Pnt1, l.Pnt2);
            ent_draw (l^.ent, drmode_black);
            if not ProcessDetails (true) then
              PrevState;
            PrevState;
          end
          else begin
            l^.ent2 := Create3Pnt (l.Pnt1, l.Pnt2, l.getp.curs);
            l^.xAxisAngle := angle (l.Pnt1, l.Pnt2);
            if plnAddVoid (l^.ent, l.ent2) then begin
              if atr_entfind (l^.ent, STANDARD_ATR_NAME, atr) then
                if atr.atrtype = atr_str255 then begin
                  move (atr.shstr, atrstr.str, sizeof(atr.shstr));
                  RefreshLabel (l^.ent, atrstr);
                end;
              PrevState;
              PrevState;
              ssClear (hilite_ss);
              ent_draw (l^.ent, drmode_white);
            end;
          end;
        end
        else begin
          if not ProcessBreakKey (l.getp.key) then
            case l.getp.key of
              f4: NextState (State_tangents);
              S0: begin
                PrevState;
                ent_init (tempent, entln3);
                tempent.ln3pt1 := l^.Pnt1;
                tempent.ln3pt2 := l^.Pnt2;
                ent_draw (tempent, drmode_black);
                SetGridAngle (l.svGridAng);
              end
              else
                beep;
            end;
        end;
      end;

      State_Pln : begin
        if l.getpln.result = res_normal then begin
          l.ent.plnclose := true;
          if l^.DefineByCenter and (not l^.CreateAtCenter) then
            ent_draw (l^.ent, drmode_black);
          ProcessDetails (true);
        end;
        PrevState;
      end;

      State_Void : begin
        if l.getp.result = res_normal then begin
          l^.Pnt1 := l^.getp.curs;
          case l^.void_inputmode of
            State_Rectangle : NextState (State_VoidRectangle);
            State_Pln : NextState (State_VoidPln);
            State_3Pt : NextState (State_Void3pt);
            State_Exist :
              begin
                if ExistEntFound (l.Pnt1, l.ent2, true) then begin
                  if plnAddVoid (l^.ent, l.ent2) then begin
                    if atr_entfind (l^.ent, STANDARD_ATR_NAME, atr) then
                      if atr.atrtype = atr_str255 then begin
                        move (atr.shstr, atrstr.str, sizeof(atr.shstr));
                        RefreshLabel (l^.ent, atrstr);
                      end;
                    PrevState;
                    ssClear (hilite_ss);
                    ent_draw (l^.ent, drmode_white);
                  end;
               end
                else
                  beep;
              end;
          end;
        end
        else if not ProcessBreakKey (l.getp.key) then begin
          case l.getp.key of
            f1 : l^.void_inputmode := State_Rectangle;
            f2: l^.void_inputmode := State_Pln;
            f3: l^.void_inputmode := State_3Pt;
            f6: PGsavevar.srch := not PGsavevar.srch;
            f7: l^.void_inputmode := State_Exist;
            s0: begin
                  unhilite_all (false, true, false);
                  ssClear (hilite_ss);
                  PrevState;
                end;
          end;
          SaveInt (SpdhVInptMod, l^.void_inputmode);
        end;
      end;

      State_VoidPln : begin
        if l.getpln.result = res_normal then begin
          l.ent2.plnclose := true;
          if plnAddVoid (l^.ent, l^.ent2) then begin
            if atr_entfind (l^.ent, STANDARD_ATR_NAME, atr) then
              if atr.atrtype = atr_str255 then begin
                move (atr.shstr, atrstr.str, sizeof(atr.shstr));
                RefreshLabel (l^.ent, atrstr);
              end;
          end;
        end;
        PrevState;
        PrevState;
        rubln^ := false;
        ssClear (hilite_ss);
        ent_draw (l^.ent, drmode_white);
      end;

      State_VoidRectangle : begin
        if l.getp.result = res_normal then begin
          l^.Pnt2 := l^.getp.curs;
          ent_init (tempent, entpln);
          polyvert_init (pv);
          pv.shape := pv_bulge;
          pv.pnt := l^.Pnt1;
          polyvert_add (pv, tempent.plnfrst, tempent.plnlast);
          polyvert_init (pv);
          pv.shape := pv_bulge;
          pv.pnt := l^.Pnt1;
          pv.pnt.x := l^.Pnt2.x;
          polyvert_add (pv, tempent.plnfrst, tempent.plnlast);
          polyvert_init (pv);
          pv.shape := pv_bulge;
          pv.pnt := l^.Pnt2;
          polyvert_add (pv, tempent.plnfrst, tempent.plnlast);
          polyvert_init (pv);
          pv.shape := pv_bulge;
          pv.pnt := l^.Pnt1;
          pv.pnt.y := l^.Pnt2.y;
          polyvert_add (pv, tempent.plnfrst, tempent.plnlast);
          unhilite_all (true, true, false);
          if ent_get (l^.ent, l^.ent.addr) then
            ent_draw (l^.ent, drmode_black);
          if plnAddVoid (l^.ent, tempent) then begin
            if atr_entfind (l^.ent, STANDARD_ATR_NAME, atr) then
              if atr.atrtype = atr_str255 then begin
                move (atr.shstr, atrstr.str, sizeof(atr.shstr));
                RefreshLabel (l^.ent, atrstr);
              end;
          end;
          ssClear (hilite_ss);
          ent_draw (l^.ent, drmode_white);
          rubbx^ := false;
          prevstate;
          prevstate;
        end
        else if not ProcessBreakKey (l.getp.key) then
          if not ProcessGridKey (l.getp.key) then
            case l.getp.key of
              F8: begin
                l^.Anchor := 1;
                NextState (State_VoidKeySizeX);
              end;
              S0: begin
                PrevState;
                unhilite_all (true, true, false);
              end
              else
                beep;
            end;
      end;

      State_Exist : begin

      end;

      State_Increment : begin
        l^.State := State_NameIncrement;
        SaveInt (dhSpNumInc, l^.Increment, false);
      end;

      State_NameIncrement : begin
        PrevState;
        SaveInt (dhSpNameInc, l^.NameIncrement, false);
      end;

      State_GridOrigin : begin
        if l.getp.Result = res_normal then begin
          if lyr_get (getlyrcurr, rlyr) then begin
            rlyr.gridorg := l.getp.curs;
            lyr_put (rlyr);
          end;
          PrevState;
        end
        else if not ProcessBreakKey (l.getp.key) then begin
          if l.getp.key = s0 then
            PrevState
          else
            beep;
        end;
      end;

      State_GridSize : begin
        lyr_get (getlyrcurr, rlyr);
        rlyr.snapdivs.x := l^.gridDis;
        rlyr.snapdivs.y := l^.gridDis;
        lyr_put (rlyr);
        PrevState;
      end;


      State_LblMove : begin
        if l.getp.Result = res_normal then begin
          PlaceLabel (l^.lblcntr, l.getp.curs, false);
          l^.lblcntr := l.getp.curs;
          if l^.lblrotate then begin
            l^.State := State_LblRotate;
          end
          else begin
            DrawLabel;
            GroupLabel;
            PrevState;
          end;
        end
        else begin
          if not ProcessBreakKey (l.getp.key) then
            case l.getp.key of
              F0:
                l^.lblrotate := not l^.lblrotate;
              F7:
                if l^.lblvertical then
                  beep
                else begin
                  RotateLabel (halfpi);
                  l^.lblvertical := true;
                end;
              F8:
                if l^.lblvertical then begin
                  RotateLabel (-halfpi);
                  l^.lblvertical := false;
                end
                else
                  beep;
              S0: begin
                DrawLabel;
                GroupLabel;
                PrevState;
              end
              else
                beep;
            end;
        end;
      end;

      State_LblRotate : begin
        if l.getp.Result = res_normal then begin
          if PointsEqual (l^.getp.curs, l^.lblcntr) then
            beep
          else begin
            RotateLabel (angle(l^.lblcntr, l^.getp.curs));
            DrawLabel;
            GroupLabel;
            PrevState;
          end;
        end
        else begin
          if not ProcessBreakKey (l.getp.key) then
            case l.getp.key of
              S0: begin
                DrawLabel;
                GroupLabel;
                PrevState;
              end
              else
                beep;
            end;
        end;
      end;

      State_Esc, State_EscRefresh : begin
        PrevState;
      end;

      State_DoRefresh : begin
        l^.DoRefresh := l^.RecalcLabelsAtStart;
        PrevState;
      end;

      State_KeySizeX, State_VoidKeySizeX, State_GetNewX : begin
        if l^.dgetd.result = res_normal then begin
          if (l^.state = State_KeySizeX) and l^.DefineByCenter and not l^.KeyDimCenter then
            l^.Xdis := l^.Xdis + l^.WallWidth;
          NextState (l^.state+1);
        end
        else begin
          if not ProcessBreakKey (l.dgetd.key) then
            if (l^.state = State_KeySizeX) and (l^.dgetd.key = F7) and (l^.DefineByCenter) then
              l^.KeyDimCenter := true
            else if (l^.state = State_KeySizeX) and (l^.dgetd.key = F8) and (l^.DefineByCenter) then
              l^.KeyDimCenter := false
            else case l.dgetd.key of
              F1: if l^.state = State_GetNewX then
                    l^.Anchor := l^.Anchor mod 4 + 1
                  else
                    l^.Anchor := 1;
              F2..F4: if l^.state <> State_GetNewX then
                        l^.Anchor := fnkeyconv (l.dgetd.key);
              F0: if l^.state <> State_GetNewX then
                    NextState (State_KeyAngle);
              s0: begin
                    DrawAnchor (true);
                    PrevState;
              end
              else
                beep;
            end;
        end;
      end;

      State_KeySizeY, State_VoidKeySizeY, State_GetNewY : begin
        if l^.dgetd.result = res_normal then begin
          if l^.state = State_KeySizeY then begin
            if (l^.state = State_KeySizeY) and l^.DefineByCenter and not l^.KeyDimCenter then
              l^.Ydis := l^.Ydis + l^.WallWidth;

            l^.getp.curs := l^.Pnt1;
            if (l^.Anchor = 2) or (l^.Anchor = 3) then
              l^.getp.curs.x := l^.getp.curs.x - l^.Xdis
            else
              l^.getp.curs.x := l^.getp.curs.x + l^.Xdis;
            if (l^.Anchor < 3) then
              l^.getp.curs.y := l^.getp.curs.y - l^.Ydis
            else
              l^.getp.curs.y := l^.getp.curs.y + l^.Ydis;

            l^.ent := CreateRectangle(l^.Pnt1, l^.getp.curs);

            if l^.xAxisAngle <> 0 then
              ent_rotate (l^.ent, l^.Pnt1, l^.xAxisAngle);


{            l^.ent := CreateDimRectangle (l^.Pnt1, l^.Xdis, l^.Ydis, l^.xAxisAngle, l^.Anchor);
            if (l^.CreateAtSurface) and (l^.CreateAtCenter) then
              ProcessOffsetLine (false, not l^.DefineByCenter, true, true, l^.WallWidth);
}            wrtmsg ('');
              if not ProcessDetails (true) then
              PrevState;
          end
          else if l^.State = State_GetNewY then begin   // process resize of the existing space
            ent_draw (l^.ent, drmode_black);
            b := IsWallCenterEnt (l^.ent, d);
            if b then begin
              l^.Xdis := l^.Xdis + d;
              l^.Ydis := l^.Ydis + d;
            end;
            ResizeRectangle (l^.ent, l^.Anchor, l^.anchorbox, l^.Xdis, l^.Ydis);
            ent_draw (l^.ent, drmode_white);
            if (not b) and FindWallCenterEnt (l^.ent, l^.ent2) then begin
              ent_draw (l^.ent2, drmode_black);
              OffsetPln (l^.ent, l^.ent2, true, false, d/2, false);
              ent_draw (l^.ent2, drmode_white);
            end;
            RefreshLabel (l^.ent);
            wrtmsg ('');
          end
          else begin
            CreateDimVoid (l^.ent, l^.Pnt1, l^.Xdis, l^.Ydis, l^.xAxisAngle, l^.Anchor);
            wrtmsg ('');
          end;

          DrawAnchor(true);
          PrevState;
          PrevState;
        end
        else begin
          if not ProcessBreakKey (l.dgetd.key) then
            if l^.dgetd.key = s0 then
              PrevState
            else if l^.state = State_GetNewY then begin
              if l^.dgetd.key = F1 then
                l^.Anchor := l^.Anchor mod 4 + 1
              else
                beep;
            end
            else if (l^.state = State_KeySizeY) and (l^.dgetd.key = F7) then
              l^.KeyDimCenter := true
            else if (l^.state = State_KeySizeY) and (l^.dgetd.key = F8) then
              l^.KeyDimCenter := false
            else case l.dgetd.key of
              F1: l^.Anchor := 1;
              F2: l^.Anchor := 2;
              F3: l^.Anchor := 3;
              F4: l^.Anchor := 4;
              F0: NextState (State_KeyAngle);
              else
                beep;
            end;
        end;
      end;

      State_KeyAngle : begin
        PrevState;
      end;

      State_Arrow1, State_Arrow2: begin
        key := 0;
        if l^.state = State_Arrow1 then begin
          if l.getp.Result = res_normal then begin
            setrubpnt (l.getp.curs);
            rubln^ := true;
            l^.SvOrtho := PGSavevar.orthmode;
            if l^.ArrowSettings.Ortho1 then
              PGSavevar.orthmode := true;
            NextState (State_Arrow2);
            if l^.ArrowSettings.LeaderType = Ldr_BSp then begin
              ent_init (l^.ent, entbsp);
              l^.ent.bsppnt[1] := l.getp.curs;
              l^.ent.bspnpnt := 1;
              l^.ent.bspbase := zbase;
            end
            else begin
              ent_init (l^.ent, entpln);
              polyvert_init (pv);
              pv.shape := pv_vert;
              pv.pnt := l.getp.curs;
              polyvert_add (pv, l.ent.plnfrst, l.ent.plnlast);
            end;
          end
          else if not ProcessBreakKey (l.getp.key) then
            key := fnkeyconv(l.getp.key);
        end
        else begin
          case l^.ArrowSettings.LeaderType of
            Ldr_Pln:
              if l.getpln.result = res_normal then begin
                PGSavevar.orthmode := l^.SvOrtho;
                DrawArrow;
              end
              else if not ProcessBreakKey (l.getpln.key) then
                key := fnkeyconv (l.getpln.key);
            Ldr_Fillet :
              if l.getp.result = res_normal then begin
                PGSavevar.orthmode := l^.SvOrtho;
                ent_draw (l^.ent, drmode_black);
                polyvert_init (pv);
                pv.shape := pv_vert;
                pv.pnt := l.getp.curs;
                polyvert_add (pv, l.ent.plnfrst, l.ent.plnlast);
                AddFillet (l^.ent, 0, 25);
                ent_draw (l^.ent, drmode_white);
                setrubpnt (l.getp.curs);
                rubln^ := true;
              end
              else if not ProcessBreakKey (l.getp.key) then
                key := fnkeyconv (l.getp.key);
            Ldr_BSp:
              if l.getp.result = res_normal then begin
                PGSavevar.orthmode := l^.SvOrtho;
                if l^.ent.bspnpnt > 1 then begin
                  if l^.ent.bspnpnt = 3 then begin
                    // add a 4th point temporarily to overcome a display but with 3 point bsplines
                    l^.ent.bsppnt[4] := l^.ent.bsppnt[3];
                    l^.ent.bspnpnt := 4;
                    ent_draw (l^.ent, drmode_black);
                    l^.ent.bspnpnt := 3;
                  end
                  else
                    ent_draw (l^.ent, drmode_black);
                end;
                if l^.ent.bspnpnt < 20 then begin
                  l^.ent.bspnpnt := l^.ent.bspnpnt + 1;
                  l^.ent.bsppnt[l^.ent.bspnpnt] := l^.getp.curs;
                  if l^.ent.bspnpnt = 3 then begin
                    // add a 4th point temporarily to overcome a display bug with 3 point bsplines
                    l^.ent.bsppnt[4] := l^.ent.bsppnt[3];
                    l^.ent.bspnpnt := 4;
                    ent_draw (l^.ent, drmode_white);
                    l^.ent.bspnpnt := 3;
                  end
                  else
                    ent_draw (l^.ent, drmode_white);
                  setrubpnt (l.getp.curs);
                  rubln^ := true;
                end;
                if l^.ent.bspnpnt >= 20 then begin  // max points reached, force drawing of arrow head
                  key := fnkeyconv (s0);
                end;
              end
              else if not ProcessBreakKey (l.getp.key) then
                key := fnkeyconv (l.getp.key);
          end;
        end;

        if key > 0 then with l^.ArrowSettings  do begin

          case key of
            1..5 : if l^.state = State_Arrow1 then begin
                      Style := fnkeyconv(l.getp.key) - 1;
                      SaveInt ('SpdhArrwStyl', Style);
                   end
                   else
                     beep;

            7 :  if (Style <= 1) or (Style = 3) then begin
                    Knockout := not Knockout;
                    SaveBln ('SpdhArrwKnok', Knockout);
                 end
                 else
                    beep;
            8 :  if (Style = 1) or (Style = 3) then begin
                    Filled := not Filled;
                    SaveBln ('SpdhArrwFill', Filled);
                 end
                 else
                    beep;
            10 : NextState (State_ArrowSz);
            11 : if Style < 2 then
                    NextState (State_ArrowAspect)
                 else
                    beep;
            12 : NextState (State_ArrowWeight);
            13 : if l^.ArrowSettings.Clr > 0 then
                    l^.ArrowSettings.Clr := -l^.ArrowSettings.Clr
                 else
                   NextState (State_ArrowClr);
            14 : begin
                    Ortho1 := not Ortho1;
                    SaveBln ('SpdhArrwOrth', Ortho1);
                 end;
            16 : if l^.state = State_Arrow1 then begin
                    LeaderType := Ldr_BSp;
                    SaveStr ('SpdhArrwLdr', 'Bsp');
                 end
                 else
                    Beep;
            17 : if l^.state = State_Arrow1 then begin
                    LeaderType := Ldr_Fillet;
                    SaveStr ('SpdhArrwLdr', 'Fillet');
                 end
                 else
                    Beep;
            18 : if l^.state = State_Arrow1 then begin
                    LeaderType := Ldr_Pln;
                    SaveStr ('SpdhArrwLdr', 'Pln');
                 end
                 else
                    PrevState;
            20: begin
                  if l^.state = State_Arrow1 then
                    PrevState
                  else if ((l^.ArrowSettings.LeaderType = Ldr_BSp) and (l^.ent.bspnpnt > 1)) or
                          ((l^.ArrowSettings.LeaderType = Ldr_Fillet) and not addr_equal(l^.ent.plnfrst, l^.ent.plnlast))
                  then begin
                    ent_draw (l^.ent, drmode_black);
                    ent_add (l^.ent);
                    //ent_draw (l^.ent, drmode_white);
                    DrawArrow;
                  end
                  else
                    PrevState;

            end
            else
              beep;
          end;

        end;
      end;

 
      State_ArrowSz : begin
        if l.getr.result = res_normal then begin
          SaveRl('SpdhArrwSize', l^.ArrowSettings.Size);
          PrevState;
        end
        else if not ProcessBreakKey (l.getr.key) then begin
          if l.getr.key = s0 then
            PrevState
          else
            beep;
        end;
      end;

      State_ArrowAspect : begin
        if l.getr.result = res_normal then begin
          SaveRl('SpdhArrwAspt', l^.ArrowSettings.Aspect);
          PrevState;
        end
        else if not ProcessBreakKey (l.getr.key) then begin
          if l.getr.key = s0 then
            PrevState
          else
            beep;
        end;
      end;

      State_ArrowWeight : begin
        if (l^.ArrowSettings.Wgt < 1) or (l^.ArrowSettings.Wgt > 9) then begin
          lMsg_OK(25);   //Arrow weight must be between 1 and 9 inclusive.
        end
        else begin
          SaveInt ('SpdgArrwWght', l^.ArrowSettings.Wgt);
          PrevState;
        end;
      end;

      State_ArrowClr : begin
        SaveInt ('SpdhArrwClr', l^.ArrowSettings.Clr);
        PrevState;
      end;

      State_Tangents :
        PrevState;

      State_Measure: begin
        if l.getp.Result = res_normal then begin
          if measureEnt (l.getp.curs) then begin
            if l^.state = State_Measure then begin
              PrevState;
              NextState (State_ShowMeasures);
              l^.flagFirst := true;
            end;
          end
          else
            beep;
        end
        else if not ProcessBreakKey(l.getp.key) then begin
          case l.getp.key of
            f6 : PGsavevar.srch := not PGsavevar.srch;
            s0 : begin
                  PrevState;
                  unhilite_all (false, true, false);
                  ssClear (hilite_ss);
            end;
          end;
        end;
      end;

      State_ShowMeasures : begin
        if l.getp.Result = res_normal then begin
          if measureEnt (l.getp.curs) then
            l^.flagFirst := true
          else
            beep;
        end
        else if not ProcessBreakKey(l.getp.key) then begin
          case fnkeyconv (l.getp.key) of
            1..8 : begin
              pStrings := Addr (l^.ent2);
              if length (pStrings^[fnkeyconv (l.getp.key)]) > 0 then
                Clipboard.AsText := string (pStrings^[fnkeyconv (l.getp.key)]);
            end;
            18 : begin
              l^.ShowMeasuresDlg := not l^.ShowMeasuresDlg;
              SaveBln (SpdhMsgDlg, l^.ShowMeasuresDlg);
              if l^.ShowMeasuresDlg then begin
                ShowMeasuresDlg;
              end;
            end;
            20 : begin
                  PrevState;
                  unhilite_all (false, true, false);
                  ssClear (hilite_ss);
            end;
          end;
        end;
      end;

      State_Edit : begin
        if l.getp.Result = res_normal then begin
          if FindSpaceFromPoint (l.getp.curs, l^.EditSelection in [10, 12],
                                 l^.EditSelection in [9, 11], l^.ent, true) then begin
            if SpaceOnLockedLyr (l^.ent) then
              lMsg_ok (127)  //   Unable to edit this space as some entities (either outline or label) are on locked layers.
            else
              case l^.EditSelection of
                1: begin  //move
                    l^.Pnt1 := l.getp.curs;
                    SpaceEnts_to_hiliteSS (l^.ent, false, true, l^.ent2);
                    NextState (State_Move);
                end;
                2: begin  //copy
                    l^.Pnt1 := l.getp.curs;
                    SpaceEnts_to_hiliteSS (l^.ent, COPY_ORIGLYR, true , false, l^.ent2);
                    NextState (State_Copy);
                end;
                3: begin  //dynamic rotate
                    l^.Pnt1 := l.getp.curs;
                    SpaceEnts_to_hiliteSS (l^.ent, true, true, l^.ent2);
                    NextState (State_Rotate);
                end;
                4: begin  //rotate 90
                    SpaceRotate (l^.ent, l^.getp.curs, halfpi, true);
                end;
                5: begin  //move label
                    l^.Pnt1 := l.getp.curs;
                    if SpaceEnts_to_hiliteSS (l^.ent, COPY_NONE, true, true, l^.ent2, true) then
                      NextState (State_MoveLbl);
                end;
                8: begin  //change size
                    if isRectangle (l^.ent, l^.AnchorBox, l^.xAxisAngle, l^.Xdis, l^.Ydis) then
                          // note: fields in l^.ent2 are used to store information about the rectangle
                          //l^.ent2.plypnt[5].z, l^.ent2.plypnt[5].x, l^.ent2.plypnt[5].y) then
                    begin
                      l^.Anchor := 1;
                      NextState (State_GetNewX);
                    end
                    else begin
                      beep;
                      lwrterr (40); //Unsuitable space (not rectangular)
                    end;
                end;
                9: begin  //fold corner
                    if FindPvPntFromPln (l^.ent, l.getp.curs, l^.Pnt1, pv) then begin
                      b := NextPv(l^.ent, pv).shape = pv_vert;
                      if b then
                        b := PrevPv(l^.ent, pv).shape = pv_vert;
                      if b then
                        NextState (State_Fold)
                      else
                        lwrterr (52); //Cannot fold corners ajacent to curved sides
                    end
                    else
                      lwrterr (53); //Corner not found within miss distance
                end;
                10: begin //move corner or side
                  l^.Pnt1 := l.getp.curs;
                  if FindPvPntFromPln (l^.ent, l.getp.curs, true, l^.Pnt2, pv) then begin
                    NextState (State_MoveCorner);
                    l^.Adr := pv.addr;
                  end
                  else if FindPvSideFromPln (l^.ent, l.getp.curs, l^.Pnt2, pv) then begin
                    NextState (State_MoveSide);
                    l^.Adr := pv.addr;
                  end
                  else
                    lwrterr (29, true); //Existing space not found within miss distance of point entered
                end;
                11: begin  //delete corner
                    if FindPvPntFromPln (l^.ent, l.getp.curs, l^.Pnt1, pv) then begin
                      DeleteVertex (l^.ent, pv);
                      RefreshLabel(l^.ent);
                      UpdateCenterEnt (l^.ent);
                    end
                    else
                      lwrterr (53); //Corner not found within miss distance
                end;
                12: begin //add corner
                  l^.Pnt1 := l.getp.curs;
                  if FindPvSideFromPln (l^.ent, l.getp.curs, l^.Pnt2, pv) then begin
                      NextState (State_AddCorner);
                      l^.Adr := pv.addr;
                  end
                  else
                    lwrterr (29); //Existing space not found within miss distance of point entered
                end;
                13: begin //delete void
                  l^.Pnt1 := l.getp.curs;
                  if FindPvSideFromPln (l^.ent, l.getp.curs, l^.Pnt2, pv) then begin
                    DeleteVoid (l^.ent, pv);
                    RefreshLabel (l^.ent);
                  end
                  else
                    lwrterr (29); //Existing space not found within miss distance of point entered
                end;
                15: begin //edit details
                    EditDetails;
                end;
                16: begin //renumber
                    NextState (State_RenumberFirst);
                end
                else begin
                  lwrterr (30);  //No changes have been specified
                  beep;
                end;
              end
          end
          else
            lwrterr (29); //Existing space not found within miss distance of point entered
        end
        else if not ProcessBreakKey(l.getp.key) then begin
          case fnkeyconv(l.getp.key) of
            1..5, 8..13, 15, 16  : begin
              if fnkeyconv(l.getp.key) = l^.EditSelection then
                l^.EditSelection := 0
              else
                l^.EditSelection := fnkeyconv(l.getp.key);
              SaveInt (SpdhEditSel, l^.EditSelection);
            end;
            6 : PGsavevar.srch := not PGsavevar.srch;
            18: begin
                  NextState (State_CopyDisLyr);
                  SetPoint (l^.Pnt2, 0.0);
                  setnil (l^.Adr);
                end;
            20: PrevState
            else Beep;
          end;
        end;

      end;

      State_RenumberFirst : begin
        if l.getstr.result = res_normal then begin
          if atr_entfind (l^.ent, STANDARD_ATR_NAME, atr) then begin
            pStandardAtr := Addr (atr.shstr);
            move (l.getstr.pstr^, pStandardAtr^.Num, l.getstr.len);
            atr_update (atr);
            RefreshLabel(l^.ent);
            l^.prevnum := pStandardAtr^.Num;
            NextState (State_Renumber);
          end;
        end
        else if not ProcessBreakKey (l.getstr.key) then
          case l.getstr.key of
            s0 : PrevState
            else beep;
          end;
      end;

      State_Renumber : begin
        if l^.getp.result = res_normal then begin
          if FindSpaceFromPoint (l.getp.curs, false, false, l^.ent, true) and
             atr_entfind (l^.ent, STANDARD_ATR_NAME, atr) then begin
            pStandardAtr := Addr (atr.shstr);
            pStandardAtr^.Num := str9(l^.ent2.txtstr);
            atr_update (atr);
            RefreshLabel(l^.ent);
            l^.prevnum := pStandardAtr^.Num;
          end
          else
            lwrterr (29, true); //Existing space not found within miss distance of point entered
        end
        else if not ProcessBreakKey (l.getp.key) then
          case l^.getp.key of
            s0 : begin
                  PrevState;
                  PrevState;
                 end
            else beep;
          end;
      end;

      State_AddCorner : begin
        if l.getp.Result = res_normal then begin
          polyvert_init (pv);
          pv.pnt := l.getp.curs;
          ent_draw (l^.ent, drmode_black);
          polyvert_ins (pv, l^.Adr, l^.ent.plnfrst, l^.ent.plnlast);
          ent_update (l^.ent);
          ent_draw (l^.ent, drmode_white);
          RefreshLabel(l^.ent);
          UpdateCenterEnt;
          PrevState;
        end
        else if not ProcessBreakKey (l.getp.key) then
          if not ProcessGridKey (l.getp.key) then begin
            case l.getp.key of
              s0 : PrevState
              else beep;
            end;
          end;

      end;

      State_Move, State_Copy, State_Rotate : begin
        if l.getp.Result = res_normal then begin
          if l^.state = State_Rotate then begin
            d := angle (l^.Pnt1, l.getp.curs);
            SpaceRotate (l^.ent, l^.Pnt1, d, true);
          end
          else begin
            subpnt (l.getp.curs, l^.Pnt1, l^.Pnt2);
            mode_init (mode);
            mode_ss (mode, hilite_ss);
            adr := ent_first (mode);
            if l^.state in [State_Move, State_Rotate] then begin
              while ent_get (tempent, adr) do begin
                ent_draw (tempent, drmode_black);
                adr := ent_next (tempent, mode);
              end;
              adr := ent_first (mode);
            end;
            while ent_get (tempent, adr) do begin
              adr := ent_next (tempent, mode);
              ent_move (tempent, l^.Pnt2.x, l^.Pnt2.y, 0);
              ent_update (tempent);
              ent_draw (tempent, drmode_white);
            end;
          end;

          ssClear(hilite_ss);
          if l^.state = State_Copy then begin
            OpenTempDwgFile (TempFile, false);
            while not Eof(TempFile) do begin
              // following is necessary to undo the hilite
              Read (TempFile, tempent.addr);
              if ent_get (tempent, tempent.addr) then
                ent_draw_dl (tempent, drmode_white, true);
            end;
            DeleteTempDwgFile (TempFile);
          end;
          stopgroup;
          PrevState;
        end
        else if not ProcessBreakKey (l.getp.key) then
          if not ProcessGridKey (l.getp.key) then begin
            d := GetxAxisAngle (l^.ent);
            b := realequal (d, 0) or realequal (d, halfpi);
            case l.getp.key of
              f4 : if l^.state <> State_Rotate then SpaceRotate (l^.ent, l^.Pnt1, -halfpi, false);
              f6 : if (l^.state = State_Rotate) and not b then begin
                     SpaceRotate (l^.ent, l^.Pnt1, -d, true);
                     PrevState;
                   end;
              f7 : if b then begin
                    if isnil (l^.ent2.addr) then
                      ent_extent (l^.ent, l^.Pnt1, l^.Pnt2)
                    else
                      ent_extent (l^.ent2, l^.Pnt1, l^.Pnt2);
                    l^.Pnt1.y := l^.Pnt2.y;
                   end;
              f8 : if b then begin
                     if isnil (l^.ent2.addr) then
                       ent_extent (l^.ent, l^.Pnt2, l^.Pnt1)
                     else
                       ent_extent (l^.ent2, l^.Pnt2, l^.Pnt1);
                   end;
              f9 : if b then begin
                     if isnil (l^.ent2.addr) then
                       ent_extent (l^.ent, l^.Pnt1, l^.Pnt2)
                     else
                       ent_extent (l^.ent2, l^.Pnt1, l^.Pnt2);
                   end;
              f0 : if b then begin
                    if isnil (l^.ent2.addr) then
                      ent_extent (l^.ent, l^.Pnt1, l^.Pnt2)
                    else
                      ent_extent (l^.ent2, l^.Pnt1, l^.Pnt2);
                    l^.Pnt1.x := l^.Pnt2.x;
                   end;
              s6 : NextState (State_KeyRotAngle);
              s0 : begin
                    if l^.state = State_Copy then begin
                      unhilite_all (false, true, false);
                      mode_init (mode);
                      mode_ss (mode, hilite_ss);
                      adr := ent_first (mode);
                      while ent_get (tempent, adr) do begin
                        adr := ent_next (tempent, mode);
                        ent_del (tempent);
                      end;
                    end;
                    PrevState;
                    unhilite_all (true, true, false);
                   end;
            end;
          end;
      end;

      State_KeyRotAngle : begin
        SpaceRotate (l^.ent, l^.Pnt1, l^.d, true);
        unhilite_all (true, true, false);
        PrevState;
        if l^.d <> 0 then
          PrevState;
      end;

      State_MoveLbl : begin
        if l.getp.Result = res_normal then begin
          subpnt (l.getp.curs, l^.Pnt1, l^.Pnt2);
          mode_init (mode);
          mode_ss (mode, hilite_ss);
          adr := ent_first (mode);
          while ent_get (tempent, adr) do begin
            adr := ent_next (tempent, mode);
            ent_move (tempent, l^.Pnt2.x, l^.Pnt2.y, 0);
            ent_update (tempent);
            ent_draw (tempent, drmode_white);
          end;
          unhilite_all (true, true, false);
          ssClear(hilite_ss);
          PrevState;
        end
        else if not ProcessBreakKey (l.getp.key) then begin
            case l.getp.key of
              f1 : begin
                     mode_init (mode);
                     mode_ss (mode, hilite_ss);
                     CenterLabel(mode, l^.ent);
                     unhilite_all (true, true, false);
                     ssClear(hilite_ss);
                     PrevState;
                   end;
              F4 : begin
                     mode_init (mode);
                     mode_ss (mode, hilite_ss);
                     d := LabelAngle(mode) + halfpi;
                     if (d > halfpi) and (d < 3*halfpi) then
                       d := -halfpi
                     else
                       d := halfpi;
                     adr := ent_first (mode);
                     l^.Pnt2 := LabelCenter(l^.ent);
                     while ent_get (tempent, adr) do begin
                       adr := ent_next (tempent, mode);
                       ent_draw (tempent, drmode_black);
                       ent_rotate (tempent, l^.Pnt2, d);
                       ent_update (tempent);
                       ent_draw (tempent, drmode_white);
                     end;
                   end;
              F5 : NextState (State_KeyRotLblAngle);
              F6 : NextState (State_KeyLblAngle);
              F7, F8 : begin
                     mode_init (mode);
                     mode_ss (mode, hilite_ss);
                     if l.getp.key = F7 then
                       d := - LabelAngle(mode)
                     else
                       d := halfpi - LabelAngle(mode);
                     adr := ent_first (mode);
                     l^.Pnt2 := LabelCenter(l^.ent);
                     while ent_get (tempent, adr) do begin
                       adr := ent_next (tempent, mode);
                       ent_draw (tempent, drmode_black);
                       ent_rotate (tempent, l^.Pnt2, d);
                       ent_update (tempent);
                       ent_draw (tempent, drmode_white);
                     end;
                   end;
              s0 : begin
                    PrevState;
                    unhilite_all (true, true, false);
                   end;
            end;
        end;

      end;

      State_KeyRotLblAngle : begin
        if l^.d <> 0 then begin
           mode_init (mode);
           mode_ss (mode, hilite_ss);
           d := LabelAngle(mode) + d;
           if (d > halfpi) and (d < 3*halfpi) then
             d := l^.d + pi
           else
             d := l^.d;
           adr := ent_first (mode);
           l^.Pnt2 := LabelCenter(l^.ent);
           while ent_get (tempent, adr) do begin
             adr := ent_next (tempent, mode);
             ent_draw (tempent, drmode_black);
             ent_rotate (tempent, l^.Pnt2, d);
             ent_update (tempent);
             ent_draw (tempent, drmode_white);
           end;
        end;
        PrevState;
      end;

      State_KeyLblAngle : begin
         mode_init (mode);
         mode_ss (mode, hilite_ss);
         d := l^.d - LabelAngle(mode);

         adr := ent_first (mode);
         l^.Pnt2 := LabelCenter(l^.ent);
         while ent_get (tempent, adr) do begin
           adr := ent_next (tempent, mode);
           ent_draw (tempent, drmode_black);
           ent_rotate (tempent, l^.Pnt2, d);
           ent_update (tempent);
           ent_draw (tempent, drmode_white);
         end;
         PrevState;
      end;

      State_Fold : begin
          if l.getp.Result = res_normal then begin
            FoldCorner (l^.ent, l^.Pnt1, l.getp.curs);
            UpdateCenterEnt;
            RefreshLabel (l^.ent);
            wrtmsg ('');

            PrevState;
          end
          else if not ProcessBreakKey (l.getp.key) then begin
            if l.getp.key = s0 then
              PrevState
            else
              beep;
          end;
      end;

      State_MoveSide : begin
        unhilite_all (true, true, false);
        ssClear(hilite_ss);
        ent_get (l^.ent, l^.ent.addr);
        ent_draw (l^.ent, drmode_white);
        if l.getp.Result = res_normal then begin
          MoveSide (l^.ent, l^.Adr, l^.pnt1, l.getp.curs);
          UpdateCenterEnt;
          RefreshLabel (l^.ent);
          PrevState;
        end
        else if not ProcessBreakKey (l.getp.key) then
          if not ProcessGridKey (l.getp.key) then begin
            case l.getp.key of
              f5, f6 : begin
                     l^.MovSideStretch := (l.getp.key = f5);
                     SaveFlags;
                   end;
              s0 : begin
                    PrevState;
                   end;
            end;
          end;
        end;

      State_MoveCorner : begin
        unhilite_all (true, true, false);
        ssClear(hilite_ss);
        ent_draw (l^.ent, drmode_white);
        if l.getp.Result = res_normal then begin
          MoveVertex (l^.ent, l^.Adr, l^.getp.curs);
          UpdateCenterEnt;
          RefreshLabel (l^.ent);
          PrevState;
        end
        else if not ProcessBreakKey (l.getp.key) then
          if not ProcessGridKey (l.getp.key) then begin
            case l.getp.key of
              s0 : begin
                    PrevState;
                   end;
            end;
          end;
      end;

      State_CopyDisLyr : begin
        b := (abs(l^.Pnt2.x) > abszero) or (abs(l^.Pnt2.y) > abszero) or
             (abs(l^.Pnt2.z) > abszero) or not isnil (l^.Adr);
        if l.getp.Result = res_normal then begin
          if b then begin
            SvLyr := getlyrcurr;
            if l^.CopySelect < 2 then begin
              if FindSpaceFromPoint (l.getp.curs, false, false, l^.ent, true) then begin
                LyrOn := true;
                if not isnil(l^.Adr) then begin
                  LyrOn := lyr_ison (l^.Adr);
                  lyr_set (l^.Adr);
                end;
                pgSaveVar^.lastdist := l^.Pnt2;
                SaveInt ('SpdhCpySel', l^.CopySelect);
                l^.Pnt1 := l.getp.curs;
                if isnil(l^.Adr) then
                  copyflag := COPY_ORIGLYR
                else
                  copyflag := COPY_TOLYR;
                NotCopiedFlag := false;
                SpaceEnts_to_hiliteSS (l^.ent, copyflag, false , false, l^.ent2);
                mode_init (mode);
                mode_ss (mode, hilite_ss);
                adr := ent_first (mode);
                while ent_get (tempent, adr) do begin
                  adr := ent_next (tempent, mode);
                  if (copyflag = COPY_ORIGLYR) and SpaceOnLockedLyr(tempent) then
                    NotCopiedFlag := true
                  else begin
                    ent_draw (tempent, drmode_black);
                    ent_move (tempent, l^.Pnt2.x, l^.Pnt2.y, l^.Pnt2.z);
                    ent_update (tempent);
                    if LyrOn then begin
                      ent_draw (tempent, drmode_black);
                      ent_draw (tempent, drmode_white);
                    end;
                  end;
                end;
                ssClear (hilite_ss);
                PrevState;
                lyr_set (SvLyr);
                if not LyrOn then
                  lyr_seton (l^.Adr, false, false);
                if NotCopiedFlag then
                  lmsg_ok (128);  //Some spaces could not be copied due to being on locked layers.
              end;
            end
            else begin
              l^.Pnt1 := l.getp.curs;
              NextState (State_CopyArea);
            end;
          end
          else begin
            l^.Pnt1 := l^.getp.curs;
            NextState (State_CopyDisLyr2);
          end;

        end
        else if not ProcessBreakKey (l.getp.key) then begin
          case l.getp.key of
            f1 : if b then l^.CopySelect := 1;
            f2 : if b then l^.CopySelect := 2;
            f5: l^.Pnt2 := pgSaveVar^.LastDist;
            f6: if b then PGsavevar.srch := not PGsavevar.srch;
            f8: begin
                  SetPoint (l^.Pnt2, 0.0);
                  setnil (l^.Adr);
                end;
            f9: begin
                  l^.Pnt2.x := -l^.Pnt2.x;
                  l^.Pnt2.y := -l^.Pnt2.y;
                end;
            f0: begin
                  d := -l^.Pnt2.y;
                  l^.Pnt2.y := l^.Pnt2.x;
                  l^.Pnt2.x := d;
                end;
            s1, s3 .. s7: CopyParams;
            s0: begin
                  PrevState;
                end;
          end;
        end;
      end;

      State_CopyArea : begin
        if l.getp.Result = res_normal then begin
          CopyArea;
          PrevState;
        end
        else if l.getp.key = s0 then
            PrevState
        else
          beep;
      end;

      State_CopyDisLyr2 : begin
        if l.getp.Result = res_normal then begin
            subpnt (l^.getp.curs, l^.Pnt1, l^.Pnt2);
            PrevState;
        end
        else if not ProcessBreakKey (l.getp.key) then begin
          case l.getp.key of
            f5: begin
                  l^.Pnt2 := pgSaveVar^.LastDist;
                  PrevState;
                end;
            s0: begin
                  PrevState;
                end;
          end;
        end;

      end;

      State_UpdateRefresh : begin
        if l.getp.Result = res_normal then begin
        end
        else if not ProcessBreakKey (l.getp.key) then begin
          case l.getp.key of
            f1: begin
                  screen.Cursor := crHourglass;
                  RefreshLabels (false);
                  screen.Cursor := crDefault;
                end;
            f3: begin
                  screen.Cursor := crHourglass;
                  mode_init (mode);
                  mode_lyr (mode, lyr_all);
                  mode_atr (mode, STANDARD_ATR_NAME);
                  mode_enttype (mode, entpln);
                  adr := ent_first (mode);
                  while ent_get (l^.ent, adr) do begin
                    adr := ent_next (l^.ent, mode);
                    UpdateLabel (l^.ent);
                    UpdateLabel (l^.ent);
                  end;
                  screen.Cursor := crDefault;
                end;
            f4: NextState (State_UpdateArea);
            f5: NextState (State_UpdateSome);
            s1: begin
                  if l^.DCADver >= 3 then
                    undoStartTransaction;

                  case l^.ShowPln of
                    PLshowAll   : l^.ShowPln := PLShowCntr;
                    PLshowSurf  : l^.ShowPln := PLshowNone;
                    PLshowNone  : l^.ShowPln := PLShowSurf;
                    PLshowCntr  : l^.ShowPln := PLShowAll;
                  end;
                  DisplaySurfaces (l^.ShowPln in [PLShowSurf, PLshowAll]);

                  SaveInt (SpdhShowPlin, ord (l^.ShowPln));

                  if l^.DCADver >= 3 then
                    undoEndTransaction;
                end;
            s2: begin
                  if l^.DCADver >= 3 then
                    undoStartTransaction;

                  case l^.ShowPln of
                    PLshowAll   : l^.ShowPln := PLShowSurf;
                    PLshowSurf  : l^.ShowPln := PLshowAll;
                    PLshowNone  : l^.ShowPln := PLShowCntr;
                    PLshowCntr  : l^.ShowPln := PLShowNone;
                  end;
                  DisplayCenters (l^.ShowPln in [PLShowCntr, PLshowAll]);

                  SaveInt (SpdhShowPlin, ord (l^.ShowPln));

                  if l^.DCADver >= 3 then
                    undoEndTransaction;

                end;
            s0: begin
                  PrevState;
                end;
          end;
        end;
      end;

      State_UpdateSome : begin
        if l.getp.Result = res_normal then begin
          if FindSpaceFromPoint (l.getp.curs, false, false, l^.ent, true) then begin
            UpdateLabel (l^.ent);
            UpdateLabel (l^.ent);
          end
          else
            beep;
        end
        else if not ProcessBreakKey (l.getp.key) then begin
          case l.getp.key of
            s0: begin
                  PrevState;
                end;
          end;
        end;
      end;

      State_UpdateArea : begin
        if l.getp.Result = res_normal then begin
          l^.Pnt1 := l.getp.curs;
          NextState (State_UpdateArea2);
        end
        else if not ProcessBreakKey (l.getp.key) then begin
          case l.getp.key of
            f6: PGSavevar.srch := not PGSavevar.srch;
            s0: begin
                  PrevState;
                end;
          end;
        end;
      end;

      State_UpdateArea2 : begin
        if l.getp.Result = res_normal then begin
          l^.Pnt2 := l.getp.curs;
          mode_init1 (mode);
          mode_box (mode, l^.Pnt1.x, l^.Pnt1.y, l^.Pnt2.x, l^.Pnt2.y);
          mode_atr (mode, STANDARD_ATR_NAME);
          adr := ent_first (mode);
          while ent_get (l^.ent, adr) do begin
            adr := ent_next (l^.ent, mode);
            UpdateLabel (l^.ent);
            UpdateLabel (l^.ent);
          end;
          PrevState;
        end
        else if not ProcessBreakKey (l.getp.key) then begin
          case l.getp.key of
            f6: PGSavevar.srch := not PGSavevar.srch;
            s0: begin
                  PrevState;
                end;
          end;
        end;
      end;

      State_Excel : begin
        if l.getp.Result = res_normal then begin
        end
        else if not ProcessBreakKey (l.getp.key) then
          case l.getp.key of
            f1: ExcelImport;
            f3: ExcelExport;
            s8: SetExcelDefaults;
            s0: PrevState
            else Beep;
          end;
      end;

      State_Report : begin
        if l.getp.Result = res_normal then begin
          case l^.ReportSelection of
            RPT_AREA, RPT_AREA_DEL :
              begin
                l^.Pnt1 := l^.getp.curs;
                NextState (State_ReportArea);
              end;
          end;
        end
        else if not ProcessBreakKey (l.getp.key) then
          case l.getp.key of
            f1: begin
                  HiliteSelected (l^.RptMode, false);
                  if l^.ReportSelection = RPT_ACTIVE then
                    l^.ReportSelection := RPT_NONE
                  else begin
                    l^.ReportSelection := RPT_ACTIVE;
                    mode_init (l^.RptMode);
                    mode_lyr (l^.RptMode, lyr_curr);
                    HiliteSelected (l^.RptMode, true);
                  end;
                end;
            f2: begin
                  HiliteSelected (l^.RptMode, false);
                  if l^.ReportSelection = RPT_ON then
                    l^.ReportSelection := RPT_NONE
                  else begin
                    l^.ReportSelection := RPT_ON;
                    mode_init (l^.RptMode);
                    mode_lyr (l^.RptMode, lyr_on);
                    HiliteSelected (l^.RptMode, true);
                  end;
                end;
            f3: begin
                  HiliteSelected (l^.RptMode, false);
                  if l^.ReportSelection = RPT_ALL then
                    l^.ReportSelection := RPT_NONE
                  else begin
                    l^.ReportSelection := RPT_ALL;
                    mode_init (l^.RptMode);
                    mode_lyr (l^.RptMode, lyr_all);
                    HiliteSelected(l^.RptMode, true);
                  end;
                end;
            f4: begin
                  HiliteSelected(l^.RptMode, false);
                  if l^.ReportSelection = RPT_AREA then begin
                    l^.ReportSelection := RPT_NONE;
                    ssClear (hilite_ss);
                  end
                  else begin
                    l^.ReportSelection := RPT_AREA;
                    lwrtmsg (80); //Select first corner of area to be reported on
                  end;
                end;
            f5: if (l^.ReportSelection IN [RPT_AREA]) and not isnil (ent_first (l^.RptMode)) then begin
                  l^.ReportSelection := RPT_AREA_DEL;
                  lwrtmsg (82); //Select first corner of area to be removed from report selection
                end
                else if l^.ReportSelection = RPT_AREA_DEL then
                  l^.ReportSelection := RPT_AREA
                else
                  beep;
            f6: if l^.ReportSelection = RPT_AREA then
                  PGsavevar.srch := not PGsavevar.srch;
            f8: begin
                  DoReport (true, '');
                end;
            f9: if atr_sysfind ('SpdhR000', atr) then begin
                  if l^.ReportSelection > RPT_NONE then
                    HiliteSelected (l^.RptMode, false);
                  l^.ReportSelection := 0;
                  DoAllReports;
                  l^.ReportSelection := 0;
                end;
            s6: begin
                  DoSettings;
                end;
            s8: begin
                  fmSummary := TfmSummary.Create(Application);
                  try
                    fmSummary.ShowModal;
                  finally
                    fmSummary.Free;
                  end;
                end;
            s0: begin
                  if l^.ReportSelection = RPT_AREA_DEL then
                    l^.ReportSelection := RPT_AREA
                  else begin
                    PrevState;
                    mode_init (l^.RptMode);
                    mode_ss (l^.RptMode, hilite_ss);
                    HiliteSelected(l^.RptMode, false);
                  end;
                end
            else Beep;
          end;
      end;

      State_ReportArea : begin
        if l.getp.Result = res_normal then begin
          mode_init1 (mode);
          mode_atr (mode, STANDARD_ATR_NAME);
          mode_enttype (mode, entpln);
          mode_box (mode, l^.Pnt1.x, l^.Pnt1.y, l^.getp.curs.x, l^.getp.curs.y);
          adr := ent_first (mode);
          while ent_get (l^.ent, adr) do begin
            adr := ent_next (l^.ent, mode);
            if l^.ReportSelection = RPT_AREA then
              ssAdd (hilite_ss, l^.ent)
            else begin
              ssDel (hilite_ss, l^.ent);
              l^.ReportSelection := RPT_AREA;
              hi_lite (false, l^.ent, true, true);
            end;
          end;
          mode_init (l^.RptMode);
          mode_ss (l^.RptMode, hilite_ss);
          HiliteSelected (l^.RptMode, true);
          wrtmsg ('');
          PrevState;
        end
        else if not ProcessBreakKey (l.getp.key) then
          case l.getp.key of
            f6: PGsavevar.srch := not PGsavevar.srch;
            s0: begin
                  PrevState;
                  wrtmsg ('');
                end
            else Beep;
          end;
      end;

      State_ReportPos : begin
        if l.getp.Result = res_normal then begin
          subpnt (l^.getp.curs, l^.ent2.linpt1, l^.ent2.linpt2);
          adr := ent_first (l^.lblmode);
          while ent_get (l^.ent, adr) do begin
            adr := ent_next (l^.ent, l^.lblmode);
            ent_move (l^.ent, l^.ent2.linpt2.x, l^.ent2.linpt2.y, zero);
            ent_update (l^.ent);
            ent_draw (l^.ent, drmode_white);
          end;
          PrevState;
        end
        else if l.getp.key = s0 then begin
          adr := ent_first (l^.lblmode);
          while ent_get (l^.ent, adr) do begin
            adr := ent_next (l^.ent, l^.lblmode);
            ent_del (l^.ent);
          end;
          PrevState;
        end
        else if not ProcessBreakKey(l.getp.key) then
          beep;

      end;
    end;
  END;

end.

