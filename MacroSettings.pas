unit MacroSettings;

{$WARN UNIT_PLATFORM OFF}

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls,
  UInterfaces, UConstants, URecords, StrUtils, Vcl.FileCtrl, Vcl.Buttons, Vcl.Menus,
  Vcl.ExtCtrls, Settings, DcadEdit, DcadNumericEdit, CommonStuff, Language, StrUtil,
  UserFields, Vcl.Imaging.pngimage, Catgries, CeilingOptions, System.UITypes, ClrUtil,
  SpaceUtil, Report, System.Types, Constants, Vcl.Samples.Spin, Version;

type
  TagBtns = array [1 .. NumTags] of TButton;
  CustBtns = array of TButton;
  CatCustField = record
    created : boolean;
    Dft : TCheckBox;
    Input : TDcadEdit;
    key : atrname;
  end;

  TfmMacroSettings = class(TForm)
    tsMain: TPageControl;
    tsLabels: TTabSheet;
    btnSaveExit: TButton;
    rbLabelText: TRadioButton;
    rbLabelSymbol: TRadioButton;
    OpenDialog1: TOpenDialog;
    tsReportData: TTabSheet;
    pReportData: TPanel;
    Label8: TLabel;
    Label9: TLabel;
    Panel5: TPanel;
    LblHeadings: TLabel;
    LblData: TLabel;
    lblSubTots: TLabel;
    LblTots: TLabel;
    LblTitle: TLabel;
    LblSubHeads: TLabel;
    ScrollBox1: TScrollBox;
    Shape1: TShape;
    lblCol1: TLabel;
    lblCol2: TLabel;
    lblCol3: TLabel;
    lblCol4: TLabel;
    lblCol5: TLabel;
    lblCol6: TLabel;
    lblCol7: TLabel;
    lblCol8: TLabel;
    lblCol9: TLabel;
    lblCol10: TLabel;
    tsReportFormat: TTabSheet;
    btnSave: TButton;
    btnCancel: TButton;
    EdTitle: TEdit;
    EdSubHdg: TEdit;
    chbRptAllCaps: TCheckBox;
    lblFont2: TLabel;
    lblHeight2: TLabel;
    lblAspect2: TLabel;
    lblSlant2: TLabel;
    lblWeight2: TLabel;
    lblClr2: TLabel;
    Label20: TLabel;
    Label12: TLabel;
    Label21: TLabel;
    Label18: TLabel;
    Label13: TLabel;
    Label19: TLabel;
    MainMenu1: TMainMenu;
    Options: TMenuItem;
    TTFfirst: TMenuItem;
    SHXfirst: TMenuItem;
    FontOrder1: TMenuItem;
    lblSpaceAbove2: TLabel;
    lblSpaceBelow2: TLabel;
    tsUnits: TTabSheet;
    tcDimUnits: TTabControl;
    cbDimScaleType: TComboBox;
    pcDimSettings: TPageControl;
    tsDimBlank: TTabSheet;
    tsDimFtInch: TTabSheet;
    lblDimFtInchRounding: TLabel;
    cbDimRoundFeetInch: TComboBox;
    tsDimDecimal: TTabSheet;
    lblDimdecRounding: TLabel;
    lblDimDecUnitDisp: TLabel;
    lblDimDecPlaces: TLabel;
    cbDimRoundDec: TComboBox;
    edDimUnitDispDec: TEdit;
    dcnDimDecPlacesDec: TDcadNumericEdit;
    chbDimRemZeroDec: TCheckBox;
    tsDimCustom: TTabSheet;
    lblDimCustomRounding: TLabel;
    lblDimCustomUnitDisp: TLabel;
    lblDimDecPlacesCust: TLabel;
    lblDimCustomUnitSz: TLabel;
    cbDimRoundCustom: TComboBox;
    edDimUnitDispCustom: TEdit;
    dcnDimDecPlacesCustom: TDcadNumericEdit;
    chbDimRemZeroCustom: TCheckBox;
    lblDimensions: TLabel;
    tcAreaUnits: TTabControl;
    cbAreaScaleType: TComboBox;
    pcAreaSettings: TPageControl;
    tsAreaBlank: TTabSheet;
    tsAreaDecimal: TTabSheet;
    lblAreaDecRounding: TLabel;
    lblAreaDecUnitDisp: TLabel;
    lblAreaDecPlaces: TLabel;
    cbAreaRoundDec: TComboBox;
    edAreaUnitDispDec: TEdit;
    dcnAreaDecPlacesDec: TDcadNumericEdit;
    chbAreaRemZeroDec: TCheckBox;
    tsAreaCustom: TTabSheet;
    lblAreaCustomRounding: TLabel;
    lblAreaCustomUnitDisp: TLabel;
    lblAreaDecPlacesCust: TLabel;
    cbAreaRoundCustom: TComboBox;
    edAreaUnitDispCustom: TEdit;
    dcnAreaDecPlacesCustom: TDcadNumericEdit;
    chbAreaRemZeroCustom: TCheckBox;
    dcnAreaUnitSz: TDcadNumericEdit;
    lblAreas: TLabel;
    tcVolUnits: TTabControl;
    cbVolScaleType: TComboBox;
    pcVolSettings: TPageControl;
    tsVolBlank: TTabSheet;
    tsVolDecimal: TTabSheet;
    lblVolDecRounding: TLabel;
    lblVolDecUnitDisp: TLabel;
    lblVolDecPlaces: TLabel;
    cbVolRoundDec: TComboBox;
    edVolUnitDispDec: TEdit;
    dcnVolDecPlacesDec: TDcadNumericEdit;
    chbVolRemZeroDec: TCheckBox;
    tsVolCustom: TTabSheet;
    lblVolCustRounding: TLabel;
    lblVolCustUnitDisp: TLabel;
    lblVolCustDecPlaces: TLabel;
    cbVolRoundCustom: TComboBox;
    edVolUnitDispCustom: TEdit;
    dcnVolDecPlacesCustom: TDcadNumericEdit;
    chbVolRemZeroCustom: TCheckBox;
    dcnVolUnitSz: TDcadNumericEdit;
    lblVolumes: TLabel;
    lblAltAreas: TLabel;
    tcWAreaUnits: TTabControl;
    cbWAreaScaleType: TComboBox;
    pcWAreaSettings: TPageControl;
    tsWAreaBlank: TTabSheet;
    tsWAreaDecimal: TTabSheet;
    lblAltAreaDecRounding: TLabel;
    lblAltAreaDecUnitDisp: TLabel;
    lblAltAreaDecPlaces: TLabel;
    cbWAreaRoundDec: TComboBox;
    edWAreaUnitDispDec: TEdit;
    dcnWAreaDecPlacesDec: TDcadNumericEdit;
    chbWAreaRemZeroDec: TCheckBox;
    tsWAreaCustom: TTabSheet;
    lblAltAreaCustRounding: TLabel;
    lblAltAreaCustUnitDisp: TLabel;
    lblAltAreaCustDecPlaces: TLabel;
    lblAltAreaCustUnitSz: TLabel;
    cbWAreaRoundCustom: TComboBox;
    edWAreaUnitDispCustom: TEdit;
    dcnWAreaDecPlacesCustom: TDcadNumericEdit;
    chbWAreaRemZeroCustom: TCheckBox;
    dcnWAreaUnitSz: TDcadNumericEdit;
    lblAltArea: TLabel;
    chbWalls: TCheckBox;
    chbWindows: TCheckBox;
    chbDoors: TCheckBox;
    chbCeilings: TCheckBox;
    tsUserDefined: TTabSheet;
    lblUniqueTag: TLabel;
    lblDescription: TLabel;
    lblFormat: TLabel;
    sbCustom: TScrollBox;
    btnNewCustField: TButton;
    ppTagBtnsRpt: TPanel;
    scbTagBtnsRpt: TScrollBar;
    tsCat: TTabSheet;
    lblCatCategory: TLabel;
    cbCategory: TComboBox;
    Shape4: TShape;
    btnCategoryMaintain: TButton;
    pCatDefaults: TPanel;
    chbCatSolidFill: TCheckBox;
    pCatColour: TPanel;
    lblCatDefaults: TLabel;
    sbCatUserFields: TScrollBox;
    pCatCeiling: TPanel;
    CustomFillColours1: TMenuItem;
    lblDefault: TLabel;
    pTagBtnsRpt: TPanel;
    Button2_1: TButton;
    Button2_2: TButton;
    Button2_3: TButton;
    Button2_4: TButton;
    Button2_5: TButton;
    Button2_6: TButton;
    Button2_7: TButton;
    Button2_8: TButton;
    Button2_9: TButton;
    Button2_10: TButton;
    Button2_11: TButton;
    Button2_12: TButton;
    Button2_13: TButton;
    Button2_14: TButton;
    Button2_15: TButton;
    Button2_16: TButton;
    Button2_17: TButton;
    Button2_18: TButton;
    pcLabel: TPageControl;
    tsFromSymbol: TTabSheet;
    lblSymbolFile: TLabel;
    SpeedButton2: TSpeedButton;
    edSymFileName: TEdit;
    tsSpecifyText: TTabSheet;
    edRptHead1: TEdit;
    edRptHead2: TEdit;
    edRptHead3: TEdit;
    edRptHead4: TEdit;
    edRptHead5: TEdit;
    edRptHead6: TEdit;
    edRptHead7: TEdit;
    edRptHead8: TEdit;
    edRptHead9: TEdit;
    edRptHead10: TEdit;
    edRptData1: TEdit;
    edRptData2: TEdit;
    edRptData3: TEdit;
    edRptData4: TEdit;
    edRptData5: TEdit;
    edRptData6: TEdit;
    edRptData7: TEdit;
    edRptData8: TEdit;
    edRptData9: TEdit;
    edRptData10: TEdit;
    edRptSubTot1: TEdit;
    edRptSubTot2: TEdit;
    edRptSubTot3: TEdit;
    edRptSubTot4: TEdit;
    edRptSubTot5: TEdit;
    edRptSubTot6: TEdit;
    edRptSubTot7: TEdit;
    edRptSubTot8: TEdit;
    edRptSubTot9: TEdit;
    edRptSubTot10: TEdit;
    edRptTot1: TEdit;
    edRptTot2: TEdit;
    edRptTot3: TEdit;
    edRptTot4: TEdit;
    edRptTot5: TEdit;
    edRptTot6: TEdit;
    edRptTot7: TEdit;
    edRptTot8: TEdit;
    edRptTot9: TEdit;
    edRptTot10: TEdit;
    cbRptFont6: TComboBox;
    cbRptFont7: TComboBox;
    cbRptFont8: TComboBox;
    cbRptFont9: TComboBox;
    cbRptFont10: TComboBox;
    cbRptFont11: TComboBox;
    dcnRptHght6: TDcadNumericEdit;
    dcnRptHght7: TDcadNumericEdit;
    dcnRptHght8: TDcadNumericEdit;
    dcnRptHght9: TDcadNumericEdit;
    dcnRptHght10: TDcadNumericEdit;
    dcnRptHght11: TDcadNumericEdit;
    dcnRptAspect6: TDcadNumericEdit;
    dcnRptAspect7: TDcadNumericEdit;
    dcnRptAspect8: TDcadNumericEdit;
    dcnRptAspect9: TDcadNumericEdit;
    dcnRptAspect10: TDcadNumericEdit;
    dcnRptAspect11: TDcadNumericEdit;
    pRptClr6: TPanel;
    pRptClr7: TPanel;
    pRptClr8: TPanel;
    pRptClr9: TPanel;
    pRptClr10: TPanel;
    pRptClr11: TPanel;
    dcnSpaceAbove6: TDcadNumericEdit;
    dcnSpaceAbove7: TDcadNumericEdit;
    dcnSpaceAbove8: TDcadNumericEdit;
    dcnSpaceAbove9: TDcadNumericEdit;
    dcnSpaceAbove10: TDcadNumericEdit;
    dcnSpaceAbove11: TDcadNumericEdit;
    dcnSpaceBelow6: TDcadNumericEdit;
    dcnSpaceBelow7: TDcadNumericEdit;
    dcnSpaceBelow8: TDcadNumericEdit;
    dcnSpaceBelow9: TDcadNumericEdit;
    dcnSpaceBelow10: TDcadNumericEdit;
    dcnSpaceBelow11: TDcadNumericEdit;
    cbItalic6: TComboBox;
    cbBold6: TComboBox;
    dcnSlant6: TDcadNumericEdit;
    dcnWeight6: TDcadNumericEdit;
    cbItalic7: TComboBox;
    cbBold7: TComboBox;
    cbItalic8: TComboBox;
    cbBold8: TComboBox;
    cbItalic9: TComboBox;
    cbBold9: TComboBox;
    cbItalic10: TComboBox;
    cbBold10: TComboBox;
    cbItalic11: TComboBox;
    cbBold11: TComboBox;
    dcnWeight7: TDcadNumericEdit;
    dcnSlant7: TDcadNumericEdit;
    dcnWeight8: TDcadNumericEdit;
    dcnSlant8: TDcadNumericEdit;
    dcnWeight9: TDcadNumericEdit;
    dcnSlant9: TDcadNumericEdit;
    dcnWeight10: TDcadNumericEdit;
    dcnSlant10: TDcadNumericEdit;
    dcnWeight11: TDcadNumericEdit;
    dcnSlant11: TDcadNumericEdit;
    pLblSpecifyText: TPanel;
    Label3: TLabel;
    lblTxtPattern: TLabel;
    lblFont: TLabel;
    lblHeight: TLabel;
    lblAspect: TLabel;
    lblColour: TLabel;
    lblSlant: TLabel;
    lblWeight: TLabel;
    lblAlignment: TLabel;
    lblSpaceBelow: TLabel;
    chbLblAllCaps: TCheckBox;
    cbLblAlignment: TComboBox;
    scbTagBtns: TScrollBar;
    ppTagBtns: TPanel;
    pTagBtns: TPanel;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    Button8: TButton;
    Button9: TButton;
    Button10: TButton;
    Button11: TButton;
    Button12: TButton;
    Button13: TButton;
    Button14: TButton;
    Button15: TButton;
    Button16: TButton;
    Button17: TButton;
    Button18: TButton;
    edLblPtn1: TEdit;
    edLblPtn2: TEdit;
    edLblPtn3: TEdit;
    edLblPtn4: TEdit;
    edLblPtn5: TEdit;
    cbLblFont1: TComboBox;
    cbLblFont2: TComboBox;
    cbLblFont3: TComboBox;
    cbLblFont4: TComboBox;
    cbLblFont5: TComboBox;
    dcnHeight1: TDcadNumericEdit;
    dcnHeight2: TDcadNumericEdit;
    dcnHeight3: TDcadNumericEdit;
    dcnHeight4: TDcadNumericEdit;
    dcnHeight5: TDcadNumericEdit;
    dcnAspect1: TDcadNumericEdit;
    dcnAspect2: TDcadNumericEdit;
    dcnAspect3: TDcadNumericEdit;
    dcnAspect4: TDcadNumericEdit;
    dcnAspect5: TDcadNumericEdit;
    pnlLblColour1: TPanel;
    pnlLblColour2: TPanel;
    pnlLblColour3: TPanel;
    pnlLblColour4: TPanel;
    pnlLblColour5: TPanel;
    dcnSpaceBelow1: TDcadNumericEdit;
    dcnSpaceBelow2: TDcadNumericEdit;
    dcnSpaceBelow3: TDcadNumericEdit;
    dcnSpaceBelow4: TDcadNumericEdit;
    dcnSlant1: TDcadNumericEdit;
    dcnWeight1: TDcadNumericEdit;
    cbItalic1: TComboBox;
    cbBold1: TComboBox;
    cbItalic2: TComboBox;
    cbBold2: TComboBox;
    cbItalic3: TComboBox;
    cbBold3: TComboBox;
    cbItalic4: TComboBox;
    cbBold4: TComboBox;
    cbItalic5: TComboBox;
    cbBold5: TComboBox;
    dcnSlant2: TDcadNumericEdit;
    dcnWeight2: TDcadNumericEdit;
    dcnSlant3: TDcadNumericEdit;
    dcnWeight3: TDcadNumericEdit;
    dcnSlant4: TDcadNumericEdit;
    dcnWeight4: TDcadNumericEdit;
    dcnSlant5: TDcadNumericEdit;
    dcnWeight5: TDcadNumericEdit;
    CustomDesc0: TEdit;
    CustomTag0: TEdit;
    CustomNum0: TComboBox;
    CustomDflt0: TDcadEdit;
    CustomDel0: TButton;
    CustomDesc1: TEdit;
    CustomTag1: TEdit;
    CustomNum1: TComboBox;
    CustomDflt1: TDcadEdit;
    CustomDel1: TButton;
    cbUserDefault1: TCheckBox;
    dceUserFieldValue1: TDcadEdit;
    cbUserDefault2: TCheckBox;
    dceUserFieldValue2: TDcadEdit;
    lblCatUserDefinedFields: TLabel;
    lblCatFieldName: TLabel;
    lblCatUseDefault: TLabel;
    lblCatValue: TLabel;
    Shape2: TShape;
    chbTxtScaleLbl: TCheckBox;
    chbTxtScaleRpt: TCheckBox;
    lblAreaDecFractions: TLabel;
    cbAreaFractions: TComboBox;
    lblVolDecFractions: TLabel;
    cbVolFractions: TComboBox;
    lblAltAreaDecFractions: TLabel;
    cbAltAreaFractions: TComboBox;
    lblAreaCustomFrac: TLabel;
    cbCustAreaFractions: TComboBox;
    lblVolCustFractions: TLabel;
    lblAltAreaCustFractions: TLabel;
    cbCustVolFractions: TComboBox;
    cbCustAltAreaFractions: TComboBox;
    lblDimCustomFrac: TLabel;
    cbCustDimFractions: TComboBox;
    lblHint: TLabel;
    lblCustUnitsAreaLbl: TLabel;
    lblVolCustUnitSzLbl: TLabel;
    lblAltAreaCustUnitSzLbl: TLabel;
    dcnDimUnitSz: TDcadNumericEdit;
    lblSaving: TLabel;
    tsWallOptions: TTabSheet;
    Panel1: TPanel;
    rbWallSurface: TRadioButton;
    rbWallCenter: TRadioButton;
    pnlCenter: TPanel;
    chbWallSurface: TCheckBox;
    dceWallWidth: TDcadEdit;
    lblThickness: TLabel;
    chbWallCenter: TCheckBox;
    lblSymbolNotes: TLabel;
    tsProcOptions: TTabSheet;
    gbAutoRecalculation: TGroupBox;
    chbAutoLabelStart: TCheckBox;
    lblAutoNote: TLabel;
    chbAutoRptStart: TCheckBox;
    chbAutoRpt: TCheckBox;
    gbAutoPositioning: TGroupBox;
    lblAutoPosNote: TLabel;
    chbAutoLblRecenter: TCheckBox;
    Button19: TButton;
    Button20: TButton;
    Button2_19: TButton;
    Button2_20: TButton;
    Panel2: TPanel;
    rbVoidNoSpecifyZ: TRadioButton;
    rbVoidSpecifyZ: TRadioButton;
    gbKnockout: TGroupBox;
    chbKnockoutBorder: TCheckBox;
    pnlKOentity: TPanel;
    lblXEnl: TLabel;
    dcnKO_X: TDcadNumericEdit;
    dcnKO_Y: TDcadEdit;
    lblYenl: TLabel;
    pnlKObox: TPanel;
    lblKOmargin: TLabel;
    lblKOclr: TLabel;
    dcnKOmargin: TDcadNumericEdit;
    pnlKnockoutClr: TPanel;
    pnlKOnone: TPanel;
    rbKnockoutNone: TRadioButton;
    rbKnockoutEntity: TRadioButton;
    rbKnockoutBox: TRadioButton;
    chbAutoPlace: TCheckBox;
    gbIncrements: TGroupBox;
    dceNumIncrement: TDcadEdit;
    dceNameIncrement: TDcadEdit;
    lblNumInc: TLabel;
    lblNameInc: TLabel;
    lblNameIncNote: TLabel;
    chbShowSurface: TCheckBox;
    chbShowCenter: TCheckBox;
    lblNoteText: TLabel;
    lblNote: TLabel;
    chbDrawLines: TCheckBox;
    chbLinesCurrentClr: TCheckBox;
    pnlRptLineClr: TPanel;
    lblSpaceLR: TLabel;
    dcnSpaceLR: TDcadNumericEdit;
    cbAlign1: TComboBox;
    cbAlign2: TComboBox;
    cbAlign3: TComboBox;
    cbAlign4: TComboBox;
    cbAlign5: TComboBox;
    cbAlign6: TComboBox;
    cbAlign7: TComboBox;
    cbAlign8: TComboBox;
    cbAlign9: TComboBox;
    cbAlign10: TComboBox;
    cbTitleAlign: TComboBox;
    cbSubHeadAlign: TComboBox;
    lblRptLyr: TLabel;
    cbReportLyr: TComboBox;
    memInstructions: TMemo;
    lblCustUnitsArea: TLabel;
    lblVolCustUnitSz: TLabel;
    btnColour1: TButton;
    btnColour2: TButton;
    btnColour3: TButton;
    btnColour4: TButton;
    btnColour5: TButton;
    btnKnockoutClr: TButton;
    btnRptClr6: TButton;
    btnRptClr7: TButton;
    btnRptClr8: TButton;
    btnRptClr9: TButton;
    btnRptClr10: TButton;
    btnRptClr11: TButton;
    btnLineClr: TButton;
    btnCatClr: TButton;
    ColMenu: TPopupMenu;
    Insert1: TMenuItem;
    Delete1: TMenuItem;
    Clear1: TMenuItem;
    chbUserDefined: TCheckBox;
    lblOpacity: TLabel;
    speOpacity: TSpinEdit;
    lblPct: TLabel;

    procedure CreateFields(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure rbLabelTextClick(Sender: TObject);
    procedure rbLabelSymbolClick(Sender: TObject);
    procedure SHXClick(Sender: TObject);
    procedure LoadFonts;
    procedure enableLblButtons (enable : boolean);
    procedure edLblPatternEnter(Sender: TObject);
    procedure edExit(Sender: TObject);
    procedure btnClick(Sender: TObject);
    procedure ItalicChange(Sender: TObject);
    procedure BoldChange(Sender: TObject);
    procedure SlantChange(Sender: TObject);
    procedure WeightChange(Sender: TObject);
    procedure pClrClick(Sender: TObject);
    procedure chbLblAllCapsClick(Sender: TObject);
    procedure chbRptAllCapsClick(Sender: TObject);
    procedure cbsFontChange(Sender: TObject);
    procedure btnSaveExitClick(Sender: TObject);
    procedure Save(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure enableRptButtons (enable : boolean);
    procedure DisableBtns(Sender: TObject);
    procedure EnableBtns(Sender: TObject);
    procedure EdSubHdgEnter(Sender: TObject);
    procedure EdSubTotEnter(Sender: TObject);
    procedure EdTotEnter(Sender: TObject);
    procedure TTFClick(Sender: TObject);
    procedure TextScaleClick(Sender: TObject);
    procedure EdSubHdgExit(Sender: TObject);
    procedure cbDimScaleTypeChange(Sender: TObject);
    procedure cbAreaScaleTypeChange(Sender: TObject);
    procedure cbWAreaScaleTypeChange(Sender: TObject);
    procedure cbVolScaleTypeChange(Sender: TObject);
    procedure DimUnits (units : integer);
    procedure AreaUnits (units : integer);
    procedure WAreaUnits (units : integer);
    procedure VolUnits (units : integer);
    procedure tcDimUnitsChange(Sender: TObject);
    procedure tcAreaUnitsChange(Sender: TObject);
    procedure tcVolUnitsChange(Sender: TObject);
    procedure tcWAreaUnitsChange(Sender: TObject);
    procedure CustEditClick (Sender: TObject);
    procedure CustDelClick (Sender: TObject);
    procedure CustDescEnter (Sender: TObject);
    procedure CustDefaultEnter (Sender : TObject);
    procedure CustDescExit (Sender: TObject);
    procedure CustDefaultExit (Sender: TObject);
    procedure CustNoEditEnter (Sender: TObject);
    procedure btnNewCustFieldClick(Sender: TObject);
    procedure CreateCustButtons (ndx : integer);
    procedure CustTagExit (Sender: TObject);
    procedure tsUserDefinedExit(Sender: TObject);
    procedure UpdateCustomBtns ();
    procedure scbTagBtnsChange(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure scbTagBtnsRptChange(Sender: TObject);
    procedure chbCatSolidFillClick(Sender: TObject);
    procedure CreateCatCustFields ();
    procedure SaveCatCustFields ();
    procedure cbCategoryChange(Sender: TObject);
    procedure btnCategoryMaintainClick(Sender: TObject);
    procedure CustomFillColours1Click(Sender: TObject);
    procedure pCatColourClick(Sender: TObject);
    procedure NumericChange (Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CustomDescChange(Sender: TObject);
    procedure cbUserDefaultClick(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure cbCustDimFractionsChange(Sender: TObject);
    procedure cbCustAreaFractionsChange(Sender: TObject);
    procedure cbCustVolFractionsChange(Sender: TObject);
    procedure cbCustAltAreaFractionsChange(Sender: TObject);
    procedure cbAltAreaFractionsChange(Sender: TObject);
    procedure cbVolFractionsChange(Sender: TObject);
    procedure cbAreaFractionsChange(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure rbWallCenterClick(Sender: TObject);
    procedure KnockoutVisibility;
    procedure rbKnockoutClick(Sender: TObject);
    procedure CustomTagKeyPress(Sender: TObject; var Key: Char);
    procedure chbDrawLinesClick(Sender: TObject);
    procedure chbLinesCurrentClrClick(Sender: TObject);
    procedure cbAlignChange(Sender: TObject);
    procedure cbLblAlignmentChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure lblColMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Insert1Click(Sender: TObject);
    procedure Delete1Click(Sender: TObject);
    procedure Clear1Click(Sender: TObject);
    procedure chbWallSurfaceClick(Sender: TObject);
    procedure chbWallCenterClick(Sender: TObject);
    procedure speOpacityExit(Sender: TObject);
  private
    init : boolean;
    txtscale : boolean;
    LastEd : TEdit;
    LastCsr : integer;
    PrevDim : integer;
    PrevArea : integer;
    PrevWArea : integer;
    PrevVol : integer;
    PrevValue : string;
    fCeilingOptions : TfCeilingOptions;
    prevCatNdx : integer;
    CatCustFields : array of CatCustField;
    btnsLblTag : TagBtns;
    btnsLblCust : CustBtns;
    btnsRptTag : TagBtns;
    btnsRptCust : CustBtns;
    CurrCol : integer;
    CustNumericBtnFlags : array of boolean;

    edsLblPattern : array [1..5] of TEdit;
    cbsFont : array [1..11] of TComboBox;
    dcnsHeight : array [1..11] of TDcadNumericEdit;
    dcnsAspect : array [1..11] of TDcadNumericEdit;
    cbsItalic : array [1..11] of TComboBox;
    cbsBold : array [1..11] of TComboBox;
    dcnsSlant : array [1..11] of TDcadNumericEdit;
    dcnsWeight : array [1..11] of TDcadNumericEdit;
    pnlsColour : array [1..13] of TPanel;
    Colours : array [1..13] of integer;

    cbsTextAlign : array [1..12] of TComboBox;
    edsRptHeads : array [1..10] of TEdit;
    edsRptData : array [1..10] of TEdit;
    edsRptSubTot : array [1..10] of TEdit;
    edsRptTot : array [1..10] of TEdit;

    dcnsSpaceAbove : array [6..11] of TDcadNumericEdit;
    dcnsSpaceBelow : array [1..11] of TDcadNumericEdit;

    btnsCustDelete : array of TButton;
    edsCustDesc : array of TEdit;
    edsCustTag : array of TEdit;
    cbsCustNumeric : array of TComboBox;
    dceCustDefault : array of TDcadEdit;

    SaveDone : boolean;

    procedure EnableCatEdit (value : boolean);
    procedure RememberPrevDimTab;
    procedure RememberPrevAreaTab;
    procedure RememberPrevVolTab;
    procedure RememberPrevAltAreaTab;
    function CatFind (txt : string) : integer;
  public
    { Public declarations }
  end;

var
  fmMacroSettings: TfmMacroSettings;

  LocalDimSettings : array [UNITS_LBL .. UNITS_CLIP] of DimensionsRec;
  LocalAreaSettings : array [AreaRecords, UNITS_LBL .. UNITS_CLIP] of AreasRec;
  LocalVolSettings : array [UNITS_LBL .. UNITS_CLIP] of VolumesRec;


implementation

{$R *.dfm}

procedure TfmMacroSettings.EnableCatEdit (value : boolean);
var
  i : integer;
begin
  pCatCeiling.Enabled := value;
  fCeilingOptions.Enable (value);
  chbCatSolidFill.enabled := value;
  pCatColour.enabled := value;
  pCatDefaults.Enabled := value;
  speOpacity.Enabled := value;
  lblOpacity.Enabled := value;
  lblPct.Enabled := value;
  if l^.UserFields.Count > 0 then
    for i := 0 to l^.UserFields.Count-1 do
      if length(CatCustFields) > i then begin
        CatCustFields[i].Dft.Enabled := value;
        CatCustFields[i].Input.Enabled := value;
      end;
  pCatColour.Font.Color := PnlFontClr (pCatColour.Color);
  if not value then begin
    if pCatColour.Font.Color = clBlack then
      pCatColour.Font.Color := $00555555
    else
      pCatColour.Font.Color := $00BBBBBB;
  end;
end;

procedure AddNumericItems (var cb : TComboBox);
var
  i : integer;
  s : string;
begin
  cb.Items.Clear;
  for i := 14 to 19 do begin
    GetMsg (i, s);
    cb.Items.Add(s);
  end;
end;


procedure TfmMacroSettings.LoadFonts;
var
  fontpath : shortstring;
  searchResult : TSearchRec;
  i : integer;
  ExistingSelections : array[1..11] of string;
  s : shortstring;
begin
  for i := 1 to 5 do begin
    if init then begin
      if cbsFont[i].ItemIndex >= 0 then begin
        ExistingSelections[i] := cbsFont[i].Items[cbsFont[i].ItemIndex]
      end
      else begin
        ExistingSelections[i] := '';
      end;
    end;
    cbsFont[i].Sorted := (not TTFfirst.Checked) and (not SHXfirst.Checked);
  end;
  for i := 6 to 11 do begin
    if init then begin
      if cbsFont[i].ItemIndex >= 0 then begin
        ExistingSelections[i] := cbsFont[i].Items[cbsFont[i].ItemIndex]
      end
      else begin
        ExistingSelections[i] := '';
      end;
    end;
    cbsFont[i].Sorted := (not TTFfirst.Checked) and (not SHXfirst.Checked);
  end;

  cbsFont[1].Items.BeginUpdate;
  try
    cbsFont[1].Items.Clear;
    if TTFfirst.Checked then
      for i := 0 to Screen.Fonts.Count - 1 do
        if not AnsiStartsStr ('@', Screen.Fonts[i]) then
          cbsFont[1].Items.Add(Screen.Fonts[i] + ' (TTF)');

    getpath (fontpath, pathchr);
    fontpath := fontpath + '*.SHX';
    if FindFirst(string(fontpath), faAnyFile, searchResult) =0 then
      repeat
        cbsFont[1].Items.Add ((string(searchResult.Name)).subString (0, length(searchResult.Name)-4))
      until FindNext(searchResult) <> 0;

    if not TTFfirst.Checked then
      for i := 0 to Screen.Fonts.Count - 1 do
        if not AnsiStartsStr ('@', Screen.Fonts[i]) then
          cbsFont[1].Items.Add(Screen.Fonts[i] + ' (TTF)');

  finally
    cbsFont[1].Items.EndUpdate;
    for i := 2 to 11 do
      cbsFont[i].Items := cbsFont[1].Items;
  end;

  for i := 1 to 11 do begin
    if not init then begin
      s := GetSvdStr ('SpdhSl' + inttostr(i) + 'Font', '');
      if length(s) = 0 then
        if i < 10 then
          s := GetSvdStr ('dhSPStl' + inttostr(i) + 'Font', PGsaveVar^.fontname)
        else
          s:= PGsaveVar^.fontname;
      ExistingSelections[i] := string(s);
    end;
    if length (ExistingSelections[i]) > 0 then begin
      if i < 6 then begin
        cbsFont[i].ItemIndex := cbsFont[i].Items.IndexOf(ExistingSelections[i]);
        if not init then
          cbsFontChange(cbsFont[i]);
      end
      else begin
        cbsFont[i].ItemIndex := cbsFont[i].Items.IndexOf(ExistingSelections[i]);
        if not init then
          cbsFontChange(cbsFont[i]);
      end;
    end;
  end;
end;

procedure TfmMacroSettings.RememberPrevDimTab;
begin
  if init then with LocalDimSettings[prevDim] do begin
    DimTyp := DimensionType (cbDimScaleType.ItemIndex);
    case DimTyp of
      FeetInch : Rounding := RoundingType (cbDimRoundFeetInch.ItemIndex);
      DecFt .. Mm: begin
          Rounding := RoundingType(cbDimRoundDec.ItemIndex);
          DecUnitDisplay := str9(edDimUnitDispDec.Text);
          DecPlaces := dcnDimDecPlacesDec.IntValue;
          RemTrailingZero := chbDimRemZeroDec.Checked;
        end;
      CustomDim : begin
          CustUnitSize := dcnDimUnitSz.NumValue;
          Rounding := RoundingType (cbDimRoundCustom.ItemIndex);
          CustUnitDisplay := str9 (edDimUnitDispCustom.Text);
          DecPlaces := dcnDimDecPlacesCustom.IntValue;
          RemTrailingZero := chbDimRemZeroCustom.Checked;
          Fractions := FractionType(cbCustDimFractions.ItemIndex);
        end;
    end;
  end;
end;


procedure TfmMacroSettings.Clear1Click(Sender: TObject);
begin
  edsRptHeads[CurrCol].Text := '';
  edsRptData[CurrCol].Text := '';
  edsRptSubTot[CurrCol].Text := '';
  edsRptTot[CurrCol].Text := '';
end;

procedure TfmMacroSettings.DimUnits (units : integer);
var
  s : string;
begin
  RememberPrevDimTab;

  if units = UNITS_LBL then begin
    while cbDimScaleType.Items.Count > 7 do begin
      cbDimScaleType.Items.Delete(cbDimScaleType.Items.Count-1);
    end;
  end;
  if units = UNITS_RPT then begin
    while cbDimScaleType.Items.Count > 8 do begin
      cbDimScaleType.Items.Delete(cbDimScaleType.Items.Count-1);
    end;
  end;
  if units <> UNITS_LBL then begin
    if cbDimScaleType.Items.Count < 8 then begin
      getlbl (269, s);  //Same as Labels
      cbDimScaleType.Items.Add(s);
    end;
  end;
  if units = UNITS_CLIP then begin
    if cbDimScaleType.Items.Count < 9 then begin
      GetLbl (270, s);   //Same as Reports
      cbDimScaleType.Items.Add(s);
    end;
  end;

  tcDimUnits.TabIndex := units;
  with LocalDimSettings[units] do begin
    cbDimScaleType.ItemIndex := ord(DimTyp);
    dcnDimUnitSz.NumValue := CustUnitSize;
    edDimUnitDispDec.Text := string(DecUnitDisplay);
    edDimUnitDispCustom.Text := string (CustUnitDisplay);
    cbDimRoundFeetInch.ItemIndex := ord (Rounding);
    cbDimRoundDec.ItemIndex := ord (Rounding);
    cbDimRoundCustom.ItemIndex := ord (Rounding);
    dcnDimDecPlacesDec.IntValue := DecPlaces;
    dcnDimDecPlacesCustom.IntValue := DecPlaces;
    chbDimRemZeroDec.Checked := RemTrailingZero;
    chbDimRemZeroCustom.Checked := RemTrailingZero;
    cbCustDimFractions.ItemIndex := ord (Fractions);
    chbDimRemZeroCustom.Visible := (Fractions = Fr_Decimal);
    chbDimRemZeroDec.Visible := (Fractions = Fr_Decimal);
    lblDimDecPlacesCust.Visible := (Fractions = Fr_Decimal);
    dcnDimDecPlacesCustom.Visible := (Fractions = Fr_Decimal);
    lblDimDecPlaces.Visible := (Fractions = Fr_Decimal);
    dcnDimDecPlacesDec.Visible := (Fractions = Fr_Decimal);

    case DimTyp of
      FeetInch : pcDimSettings.ActivePageIndex := 1;
      DecFt .. Mm : pcDimSettings.ActivePageIndex := 2;
      CustomDim : pcDimSettings.ActivePageIndex := 3;
      else pcDimSettings.ActivePageIndex := 0;
    end;
  end;

  PrevDim := units;

  if not init then
    cbDimScaleTypeChange (cbDimScaleType);
end;

procedure TfmMacroSettings.RememberPrevAreaTab;
BEGIN
  if init then with LocalAreaSettings[MainAreaRec, prevArea] do begin
    AreaTyp := AreaType(cbAreaScaleType.ItemIndex);
    case LocalAreaSettings[MainAreaRec, prevArea].AreaTyp of
      SqFt .. Hectares : begin
          Rounding := RoundingType (cbAreaRoundDec.ItemIndex);
          DecUnitDisplay := str9 (edAreaUnitDispDec.Text);
          DecPlaces := dcnAreaDecPlacesDec.IntValue;
          RemTrailingZero := chbAreaRemZeroDec.Checked;
          Fractions := FractionType (cbAreaFractions.ItemIndex);
        end;
      CustomArea : begin
          case PGSaveVar^.scaletype of
            0,1,2,4,5 : CustUnitSize := dcnAreaUnitSz.NumValue * 147456
            else CustUnitSize := dcnAreaUnitSz.NumValue * 1587203.1744063488126976253952508;
          end;
          Rounding := RoundingType (cbAreaRoundCustom.ItemIndex);
          CustUnitDisplay := str9 (edAreaUnitDispCustom.Text);
          DecPlaces := dcnAreaDecPlacesCustom.IntValue;
          RemTrailingZero :=  chbAreaRemZeroCustom.Checked;
          Fractions := FractionType (cbCustAreaFractions.ItemIndex);
        end;
    end;
  end;
END;

procedure TfmMacroSettings.AreaUnits (units : integer);
var
  s : string;
begin
  RememberPrevAreaTab;
  if units = UNITS_LBL then begin
    while cbAreaScaleType.Items.Count > 6 do begin
      cbAreaScaleType.Items.Delete(cbAreaScaleType.Items.Count-1);
    end;
  end;
  if units = UNITS_RPT then begin
    while cbAreaScaleType.Items.Count > 7 do begin
      cbAreaScaleType.Items.Delete(cbAreaScaleType.Items.Count-1);
    end;
  end;
  if units <> UNITS_LBL then begin
    if cbAreaScaleType.Items.Count < 7 then begin
      getlbl (269, s);  //Same as Labels
      cbAreaScaleType.Items.Add(s);
    end;
  end;
  if units = UNITS_CLIP then begin
    if cbAreaScaleType.Items.Count < 8 then begin
      getlbl (270, s);  //Same as Reports
      cbAreaScaleType.Items.Add(s);
    end;
  end;


  tcAreaUnits.TabIndex := units;
  with LocalAreaSettings[MainAreaRec, units] do begin
    cbAreaScaleType.ItemIndex := ord(AreaTyp);
    case PGSaveVar^.scaletype of
      0,1,2,4,5 : dcnAreaUnitSz.NumValue := CustUnitSize / 147456
      else dcnAreaUnitSz.NumValue := CustUnitSize / 1587203.1744063488126976253952508;
    end;
    edAreaUnitDispDec.Text := string(DecUnitDisplay);
    edAreaUnitDispCustom.Text := string(CustUnitDisplay);
    cbAreaRoundDec.ItemIndex := ord (Rounding);
    cbAreaRoundCustom.ItemIndex := ord (Rounding);
    dcnAreaDecPlacesDec.IntValue := DecPlaces;
    dcnAreaDecPlacesCustom.IntValue := DecPlaces;
    chbAreaRemZeroDec.Checked := RemTrailingZero;
    chbAreaRemZeroCustom.Checked := RemTrailingZero;
    cbAreaFractions.ItemIndex := ord (Fractions);
    cbCustAreaFractions.ItemIndex := ord (Fractions);
    chbAreaRemZeroDec.Visible := (Fractions = Fr_Decimal);
    chbAreaRemZeroCustom.Visible := (Fractions = Fr_Decimal);
    dcnAreaDecPlacesDec.Visible := (Fractions = Fr_Decimal);
    dcnAreaDecPlacesCustom.Visible := (Fractions = Fr_Decimal);
    lblAreaDecPlaces.Visible := (Fractions = Fr_Decimal);
    lblAreaDecPlaces.Visible := (Fractions = Fr_Decimal);

    case AreaTyp of
      SqFt .. Hectares : pcAreaSettings.ActivePageIndex := 1;
      CustomArea : pcAreaSettings.ActivePageIndex := 2
      else pcAreaSettings.ActivePageIndex := 0;
    end;
  end;

  PrevArea := units;

  if not init then
    cbAreaScaleTypeChange (cbAreaScaleType);
end;


procedure TfmMacroSettings.RememberPrevAltAreaTab;
begin
  if init then with LocalAreaSettings[AltAreaRec, PrevWArea] do begin
    if cbWAreaScaleType.ItemIndex > 0 then
      AreaTyp := AreaType(cbWAreaScaleType.ItemIndex - 1)
    else
      AreaTyp := AltSameAsMain;
    case AreaTyp of
      SqFt, Squares, Acres, SqMtr, Hectares : begin
          Rounding := RoundingType (cbWAreaRoundDec.ItemIndex);
          DecUnitDisplay := str9(edWAreaUnitDispDec.Text);
          DecPlaces := dcnWAreaDecPlacesDec.IntValue;
          RemTrailingZero := chbWAreaRemZeroDec.Checked;
          Fractions := FractionType (cbAltAreaFractions.ItemIndex);
        end;
      CustomArea : begin
          case PGSaveVar^.scaletype of
            0,1,2,4,5 : CustUnitSize := dcnWAreaUnitSz.NumValue * 147456
            else CustUnitSize := dcnWAreaUnitSz.NumValue * 1587203.1744063488126976253952508;
          end;
          Rounding := RoundingType (cbWAreaRoundCustom.ItemIndex);
          CustUnitDisplay := str9 (edWAreaUnitDispCustom.Text);
          DecPlaces := dcnWAreaDecPlacesCustom.IntValue;
          RemTrailingZero := chbWAreaRemZeroCustom.Checked;
          Fractions := FractionType (cbCustAltAreaFractions.ItemIndex);
        end;
    end;
  end;
end;

procedure TfmMacroSettings.WAreaUnits (units : integer);
var
  s : string;
begin
  RememberPrevAltAreaTab;

  if units = UNITS_LBL then begin
    while cbWAreaScaleType.Items.Count > 7 do begin
      cbWAreaScaleType.Items.Delete(cbWAreaScaleType.Items.Count-1);
    end;
  end;
  if units = UNITS_RPT then begin
    while cbWAreaScaleType.Items.Count > 8 do begin
      cbWAreaScaleType.Items.Delete(cbWAreaScaleType.Items.Count-1);
    end;
  end;
  if units <> UNITS_LBL then begin
    if cbWAreaScaleType.Items.Count < 8 then begin
      getlbl (269, s);  //Same as Labels
      cbWAreaScaleType.Items.Add(s);
    end;
  end;
  if units = UNITS_CLIP then begin
    if cbWAreaScaleType.Items.Count < 9 then begin
      getlbl (270, s);  //Same as Reports
      cbWAreaScaleType.Items.Add(s);
    end;
  end;


  tcWAreaUnits.TabIndex := units;
  with LocalAreaSettings[AltAreaRec, Units] do begin
    if AreaTyp = AltSameAsMain then
      cbWAreaScaleType.ItemIndex := 0
    else
      cbWAreaScaleType.ItemIndex := Ord(AreaTyp)+1;
    case PGSaveVar^.scaletype of
      0,1,2,4,5 : dcnWAreaUnitSz.NumValue := CustUnitSize / 147456
      else dcnWAreaUnitSz.NumValue := CustUnitSize / 1587203.1744063488126976253952508;
    end;
    edWAreaUnitDispDec.Text := string(DecUnitDisplay);
    edWAreaUnitDispCustom.Text := string(CustUnitDisplay);
    cbWAreaRoundDec.ItemIndex := Ord(Rounding);
    cbWAreaRoundCustom.ItemIndex := Ord(Rounding);
    dcnWAreaDecPlacesDec.IntValue := DecPlaces;
    dcnWAreaDecPlacesCustom.IntValue := DecPlaces;
    chbWAreaRemZeroDec.Checked := RemTrailingZero;
    chbWAreaRemZeroCustom.Checked := RemTrailingZero;
    cbAltAreaFractions.ItemIndex := ord (Fractions);
    cbCustAltAreaFractions.ItemIndex := ord (Fractions);
    chbAreaRemZeroDec.visible := (Fractions = Fr_Decimal);
    chbWAreaRemZeroCustom.visible := (Fractions = Fr_Decimal);
    lblAltAreaDecPlaces.visible := (Fractions = Fr_Decimal);
    dcnWAreaDecPlacesDec.visible := (Fractions = Fr_Decimal);
    dcnWAreaDecPlacesCustom.visible := (Fractions = Fr_Decimal);
    lblAltAreaCustDecPlaces.visible := (Fractions = Fr_Decimal);

    case AreaTyp of
      SqFt, Squares, Acres, SqMtr, Hectares : pcWAreaSettings.ActivePageIndex := 1;
      CustomArea : pcWAreaSettings.ActivePageIndex := 2;
      else pcWAreaSettings.ActivePageIndex := 0;
    end;
  end;

  PrevWArea := units;

  if not init then
    cbWAreaScaleTypeChange (cbWAreaScaleType);
end;

procedure TfmMacroSettings.RememberPrevVolTab;
begin
  if init then with LocalVolSettings[prevVol] do begin
    VolTyp := VolumeType (cbVolScaleType.ItemIndex);
    case VolTyp of
      CuFt .. CuMtr : begin
          Rounding := RoundingType (cbVolRoundDec.ItemIndex);
          DecUnitDisplay := str9(edVolUnitDispDec.Text);
          DecPlaces := dcnVolDecPlacesDec.IntValue;
          RemTrailingZero := chbVolRemZeroDec.Checked;
          Fractions := FractionType (cbVolFractions.ItemIndex);
        end;
      CustomVol : begin
          case PGSaveVar^.scaletype of
            0,1,2,4,5 : CustUnitSize := dcnVolUnitSz.NumValue * 56623104
            else CustUnitSize := dcnVolUnitSz.NumValue * 1999626046.4961874805639375058278;
          end;
          Rounding := RoundingType (cbVolRoundCustom.ItemIndex);
          CustUnitDisplay := str9 (edVolUnitDispCustom.Text);
          DecPlaces := dcnVolDecPlacesCustom.IntValue;
          RemTrailingZero := chbVolRemZeroCustom.Checked;
          Fractions := FractionType (cbCustVolFractions.ItemIndex);
        end;
    end;
  end;
end;

procedure TfmMacroSettings.VolUnits (units : integer);
var
  s : string;
begin
  RememberPrevVolTab;

  if units = UNITS_LBL then begin
    while cbVolScaleType.Items.Count > 5 do begin
      cbVolScaleType.Items.Delete(cbVolScaleType.Items.Count-1);
    end;
  end;
  if units = UNITS_RPT then begin
    while cbVolScaleType.Items.Count > 6 do begin
      cbVolScaleType.Items.Delete(cbVolScaleType.Items.Count-1);
    end;
  end;
  if units <> UNITS_LBL then begin
    if cbVolScaleType.Items.Count < 6 then begin
      getlbl (269, s);  //Same as Labels
      cbVolScaleType.Items.Add(s);
    end;
  end;
  if units = UNITS_CLIP then begin
    if cbVolScaleType.Items.Count < 7 then begin
      getlbl (270, s);  //Same as Reports
      cbVolScaleType.Items.Add(s);
    end;
  end;

  tcVolUnits.TabIndex := units;
  with LocalVolSettings[units] do begin
    cbVolScaleType.ItemIndex := ord (VolTyp);
    case PGSaveVar^.scaletype of
      0,1,2,4,5 : dcnVolUnitSz.NumValue := CustUnitSize / 56623104
      else dcnVolUnitSz.NumValue := CustUnitSize / 1999626046.4961874805639375058278;
    end;
    edVolUnitDispDec.Text := string(DecUnitDisplay);
    edVolUnitDispCustom.Text := string (CustUnitDisplay);
    cbVolRoundDec.ItemIndex := ord (Rounding);
    cbVolRoundCustom.ItemIndex := ord (Rounding);
    dcnVolDecPlacesDec.IntValue := DecPlaces;
    dcnVolDecPlacesCustom.IntValue := DecPlaces;
    chbVolRemZeroDec.Checked := RemTrailingZero;
    chbVolRemZeroCustom.Checked := RemTrailingZero;
    cbVolFractions.ItemIndex := ord(Fractions);
    cbCustVolFractions.ItemIndex := ord(Fractions);
    chbVolRemZeroDec.Visible := (Fractions = Fr_Decimal);
    chbVolRemZeroCustom.Visible := (Fractions = Fr_Decimal);
    lblVolDecPlaces.Visible := (Fractions = Fr_Decimal);
    dcnVolDecPlacesDec.Visible := (Fractions = Fr_Decimal);
    dcnVolDecPlacesCustom.Visible := (Fractions = Fr_Decimal);
    lblVolCustDecPlaces.Visible := (Fractions = Fr_Decimal);

    case VolTyp of
      CuFt, CuYard, Litre, CuMtr : pcVolSettings.ActivePageIndex := 1;
      CustomVol : pcVolSettings.ActivePageIndex := 2;
      else pcVolSettings.ActivePageIndex := 0;
    end;
  end;

  PrevVol := units;

  if not init then
    cbVolScaleTypeChange (cbVolScaleType);
end;

procedure TfmMacroSettings.enableLblButtons (enable : boolean);
var
  i : integer;
//  caption : string;
begin
  for i := 1 to NumTags do begin
//    caption := (string(btnsLblTag[i].Caption)).ToUpper;
    btnsLblTag[i].Enabled := enable and
                             not (i in [tag_rareapct, tag_dareapct, tag_grp,
                                        tag_count]);
                            {(caption <> Tags[tag_rareapct, 1].ToUpper) and
                            (caption <> Tags[tag_dareapct, 1].ToUpper) and
                            (caption <> Tags[tag_grp, 1].ToUpper);}
  end;
  if Length (BtnsLblCust) > 0 then
    for i := 0 to Length(BtnsLblCust)-1 do
      BtnsLblCust[i].Enabled := enable;
end;

procedure TfmMacroSettings.edLblPatternEnter(Sender: TObject);
begin
  enableLblButtons (true);
  if Sender = LastEd then begin
    (TEdit(Sender)).SelStart := LastCsr;
    (TEdit(Sender)).SelLength := 0;
  end;
end;

procedure TfmMacroSettings.edExit(Sender: TObject);
begin
  LastEd := TEdit(Sender);
  LastCsr := (TEdit(Sender)).SelStart;
end;

procedure TfmMacroSettings.DisableBtns(Sender: TObject);
begin
  enableRptButtons (false);
  enableLblButtons (false);
end;

procedure TfmMacroSettings.EdSubHdgEnter(Sender: TObject);
var
  i : integer;
begin
  for i := 1 to NumTags do
    btnsRptTag[i].Enabled := ((string(btnsRptTag[i].Caption)).ToUpper = Tags[tag_grp, 1].ToUpper);
  for i := 0 to Length(btnsRptCust) - 1 do
    btnsRptCust[i].Enabled := false;
  if Sender = LastEd then begin
    (TEdit(Sender)).SelStart := LastCsr;
    (TEdit(Sender)).SelLength := 0;
  end;

end;

procedure TfmMacroSettings.EdSubHdgExit(Sender: TObject);
begin
  LastEd := TEdit(Sender);
  LastCsr := (TEdit(Sender)).SelStart;
end;

procedure TfmMacroSettings.EdSubTotEnter(Sender: TObject);
var
  i : integer;
//  caption : string;
begin
  for i := 1 to NumTags do begin
//    caption := (string(btnsRptTag[i].Caption)).ToUpper;
    btnsRptTag[i].Enabled := not (i in [tag_nbr, tag_lyr, tag_cat, tag_name,
                                        tag_flr, tag_ceiling]);
    {(caption <> Tags[tag_nbr, 1].ToUpper) and
                             (caption <> rTags[tag_lyr, 1].ToUpper) and
                             (caption <> Tags[tag_cat, 1].ToUpper) and
                             (caption <> Tags[tag_nametag_flr, 1].ToUpper) and
                             (caption <> Tags[tag_flr, 1].ToUpper) and
                             (caption <> Tags[tag_ceiling, 1].ToUpper);   }
    end;
  for i := 0 to Length(btnsRptCust)-1 do
    btnsRptCust[i].Enabled := CustNumericBtnFlags[i];
  if Sender = LastEd then begin
    (TEdit(Sender)).SelStart := LastCsr;
    (TEdit(Sender)).SelLength := 0;
  end;
end;

procedure TfmMacroSettings.EdTotEnter(Sender: TObject);
var
  i : integer;
//  caption : string;
begin
  for i := 1 to NumTags do begin
//    caption := (string(btnsRptTag[i].Caption)).ToUpper;

    btnsRptTag[i].Enabled := not (i in [tag_nbr, tag_lyr, tag_cat, tag_name,
                                        tag_flr, tag_ceiling]);

  {
    btnsRptTag[i].Enabled := (caption <> Tags[tag_nbr, 1].ToUpper) and
                             (caption <> Tags[tag_lyr, 1].ToUpper) and
                             (caption <> Tags[tag_cat, 1].ToUpper) and
                             (caption <> Tags[tag_name, 1].ToUpper) and
                             (caption <> Tags[tag_flr, 1].ToUpper) and
                             (caption <> Tags[tag_ceiling, 1].ToUpper);
                             }
  end;
  for i := 0 to Length(btnsRptCust)-1 do
    btnsRptCust[i].Enabled := CustNumericBtnFlags[i];
end;

procedure TfmMacroSettings.EnableBtns(Sender: TObject);
begin
  enableRptButtons (true);
  btnsRptTag[tag_grp].Enabled := false;
  btnsRptTag[tag_count].Enabled := false;
  if Sender = LastEd then begin
    (TEdit(Sender)).SelStart := LastCsr;
    (TEdit(Sender)).SelLength := 0;
  end;
end;

procedure TfmMacroSettings.btnClick(Sender: TObject);
var
  s : string;
begin
  if LastCsr >= 0 then begin
    s := LastEd.Text;
    if LastEd.SelLength > 0 then begin
      delete (s, LastEd.SelStart+1, LastEd.SelLength);
    end;
    insert ((TButton(Sender)).Caption, s, LastCsr+1);
    LastEd.Text := s;
    LastCsr := LastCsr + length((TButton(Sender)).Caption);
    LastEd.SetFocus;
  end;
end;


procedure TfmMacroSettings.CreateCustButtons (ndx : integer);
var
  posy : integer;
  tabndx : integer;
begin
  if ndx = 0 then begin
    edsCustDesc[0] := CustomDesc0;
    edsCustTag[0] := CustomTag0;
    cbsCustNumeric[0] := CustomNum0;
    dceCustDefault[0] := CustomDflt0;
    btnsCustDelete[0] := CustomDel0;
  end
  else if ndx = 1 then begin
    edsCustDesc[1] := CustomDesc1;
    edsCustTag[1] := CustomTag1;
    cbsCustNumeric[1] := CustomNum1;
    dceCustDefault[1] := CustomDflt1;
    btnsCustDelete[1] := CustomDel1;
  end;
  if ndx > 1 then begin
    posy := ndx*CustomDesc1.top;
    tabndx := 8 + ndx;


    edsCustDesc[ndx] := TEdit.Create(self);
    edsCustDesc[ndx].Name := 'CustomDesc' + inttostr(ndx);
    edsCustDesc[ndx].Parent := sbCustom;
    edsCustDesc[ndx].Left := CustomDesc1.left;
    edsCustDesc[ndx].Top := posy;
    edsCustDesc[ndx].width := CustomDesc1.width;
    edsCustDesc[ndx].hint := CustomDesc1.hint;
    edsCustDesc[ndx].showhint := true;
    edsCustDesc[ndx].maxlength := 30;
    edsCustDesc[ndx].height := CustomDesc1.height;
    edsCustDesc[ndx].OnEnter :=CustDescEnter;
    edsCustDesc[ndx].OnChange :=CustomDescChange;
    edsCustDesc[ndx].TabOrder := tabndx;
    tabndx := tabndx+1;

    edsCustTag[ndx] := TEdit.Create(self);
    edsCustTag[ndx].Name := 'CustomTag' + inttostr(ndx);
    edsCustTag[ndx].Parent := sbCustom;
    edsCustTag[ndx].Left := CustomTag1.left;
    edsCustTag[ndx].Top := posy;
    edsCustTag[ndx].width := CustomTag1.width;
    edsCustTag[ndx].maxlength := 6;
    edsCustTag[ndx].height := CustomTag1.height;
    edsCustTag[ndx].OnKeyPress := CustomTag1.OnKeyPress;
    edsCustTag[ndx].TabOrder := tabndx;
    tabndx := tabndx+1;

    cbsCustNumeric[ndx] := TComboBox.Create(self);
    cbsCustNumeric[ndx].Name := 'CustomNum' + inttostr(ndx);
    cbsCustNumeric[ndx].Parent := sbCustom;
    cbsCustNumeric[ndx].Left := CustomNum1.left;
    cbsCustNumeric[ndx].Top := posy;
    cbsCustNumeric[ndx].Width := CustomNum1.width;
    cbsCustNumeric[ndx].height := CustomNum1.height;
    cbsCustNumeric[ndx].Style := csDropDownList;
    cbsCustNumeric[ndx].TabOrder := tabndx;
    tabndx := tabndx+1;

    dceCustDefault[ndx] := TDcadEdit.Create(self);
    dceCustDefault[ndx].Name := 'CustomDflt' + inttostr(ndx);
    dceCustDefault[ndx].Parent := sbCustom;
    dceCustDefault[ndx].Left := CustomDflt1.left;
    dceCustDefault[ndx].Width := CustomDflt1.width;
    dceCustDefault[ndx].Top := posy;
    dceCustDefault[ndx].OnEnter := CustDefaultEnter;
    dceCustDefault[ndx].Height := CustomDflt1.height;
    dceCustDefault[ndx].TabOrder := tabndx;
    tabndx := tabndx+1;

    btnsCustDelete[ndx] := TButton.Create(self);
    btnsCustDelete[ndx].Name := 'CustomDel' + inttostr(ndx);
    btnsCustDelete[ndx].Parent := sbCustom;
    btnsCustDelete[ndx].Left := CustomDel1.left;
    btnsCustDelete[ndx].Top := posy;
    btnsCustDelete[ndx].width := CustomDel1.width;
    btnsCustDelete[ndx].height := CustomDel1.height;
    btnsCustDelete[ndx].OnClick := CustDelClick;
    btnsCustDelete[ndx].TabOrder := tabndx;
end
  else begin
    edsCustDesc[ndx].visible := true;
    edsCustTag[ndx].visible := true;
    cbsCustNumeric[ndx].Visible := true;
    btnsCustDelete[ndx].Visible := true;
    dceCustDefault[ndx].Visible := true;
  end;

  if ndx < l^.UserFields.Count then
    ButtonLblParams(11, [l^.UserFields[Ndx].desc], btnsCustDelete[ndx])
  else
    ButtonLblParams(11, [], btnsCustDelete[ndx]);

  AddNumericItems (cbsCustNumeric[ndx]);
  if ndx > 1 then
    btnNewCustField.Top := (ndx+1)*edsCustDesc[1].Top;
end;

procedure TfmMacroSettings.btnNewCustFieldClick(Sender: TObject);
var
  NewArrayLen : integer;
begin
  NewArrayLen := Length(btnsCustDelete)+1;
  SetLength (btnsCustDelete, NewArrayLen);
  SetLength (edsCustDesc, NewArrayLen);
  SetLength (edsCustTag, NewArrayLen);
  SetLength (cbsCustNumeric, NewArrayLen);
  SetLength (dceCustDefault, NewArrayLen);

  CreateCustButtons (NewArrayLen-1);
  cbsCustNumeric[NewArrayLen-1].TabStop:= true;
  cbsCustNumeric[NewArrayLen-1].ItemIndex := 0;
  cbsCustNumeric[NewArrayLen-1].OnChange := NumericChange;
  cbsCustNumeric[NewArrayLen-1].Name := 'CustomNum' + inttostr(NewArrayLen-1);
  edsCustDesc[NewArrayLen-1].Text := '';
  edsCustTag[NewArrayLen-1].Text := '';
  dceCustDefault[NewArrayLen-1].Text := '';
  edsCustTag[NewArrayLen-1].OnExit := CustTagExit;

  edsCustDesc[NewArrayLen-1].Font.Style := edsCustDesc[NewArrayLen-1].Font.Style + [fsBold];
  edsCustTag[NewArrayLen-1].Font.Style := edsCustTag[NewArrayLen-1].Font.Style + [fsBold];
  dceCustDefault[NewArrayLen-1].Font.Style := dceCustDefault[NewArrayLen-1].Font.Style + [fsBold];
  dceCustDefault[NewArrayLen-1].NumberType := TextStr;
//  (TButton(Sender)).top := (TButton(Sender)).top  + CustomDesc1.top;

  edsCustDesc[NewArrayLen-1].SetFocus;
end;

procedure TfmMacroSettings.Insert1Click(Sender: TObject);
var
  i : integer;
begin
  if not (BlankString (edsRptHeads[10].Text) and
          BlankString (edsRptData[10].Text) and
          BlankString (edsRptSubTot[10].Text) and
          BlankString (edsRptTot[10].Text))
  then begin
    if not lMsg_Confirm (124) then  //Existing entries in Column 10 will be removed. OK to continue?
      exit;
  end;

  for i := 10 downto (CurrCol+1) do begin
    cbsTextAlign[i].ItemIndex := cbsTextAlign[i-1].ItemIndex;
    edsRptHeads[i].Text := edsRptHeads[i-1].Text;
    edsRptData[i].Text := edsRptData[i-1].Text;
    edsRptSubTot[i].Text := edsRptSubTot[i-1].Text;
    edsRptTot[i].Text := edsRptTot[i-1].Text;
  end;

  Clear1Click (Sender);
end;

procedure TfmMacroSettings.ItalicChange(Sender: TObject);
var
  ndx : integer;
begin
  ndx := (TComboBox(Sender)).tag;
  if (TComboBox(Sender)).ItemIndex = 1 then begin
    dcnsSlant[Ndx].NumValue := Pi/12;
  end
  else begin
    dcnsSlant[Ndx].NumValue := 0;
  end;
end;

procedure TfmMacroSettings.SlantChange(Sender: TObject);
var
  ndx : integer;
begin
  ndx := (TDcadNumericEdit(Sender)).tag;
  if (TDcadNumericEdit(Sender)).NumValue > 0 then begin
    cbsItalic[Ndx].ItemIndex := 1;
  end
  else begin
    cbsItalic[Ndx].ItemIndex := 0;
  end;
end;

procedure TfmMacroSettings.BoldChange(Sender: TObject);
var
  ndx : integer;
begin
  ndx := (TComboBox(Sender)).tag;
  if (TComboBox(Sender)).ItemIndex = 0 then begin
    dcnsWeight[Ndx].Text := '1';
  end
  else begin
    dcnsWeight[Ndx].Text := '3';
  end;
  dcnsWeight[Ndx].valid;   // cause numValue to be updated from text
end;

procedure TfmMacroSettings.WeightChange(Sender: TObject);
var
  ndx : integer;
begin
  ndx := (TDcadNumericEdit(Sender)).tag;
  if (TDcadNumericEdit(Sender)).NumValue > 1 then begin
    cbsBold[Ndx].ItemIndex := 1;
  end
  else begin
    cbsBold[Ndx].ItemIndex := 0;
  end;
end;

procedure TfmMacroSettings.pCatColourClick(Sender: TObject);
var
  ClrD : TColorDialog;
  TempClr  : longint;
  Ndx : integer;
  RGBClr : TColor;
  Cat : CategoryDef;
begin
  ndx := cbCategory.ItemIndex;
  Cat := l^.Categories[ndx];
  Cat.Standard.Data.DoFill := true;  // should already be true, but just in case
  RGBClr := DCADRGBtoRGB (Cat.Standard.Data.FillColor);
  if l.Options.CustomClr then begin
    ClrD := TColorDialog.Create(Application);
    ClrD.Color := RGBClr;
    ClrD.Options := [cdFullOpen];
    if ClrD.Execute then begin
      Cat.Standard.Data.FillColor := (RGBtoDCADRGB(ClrD.Color));
      l^.Categories[ndx] := Cat;
    end;
    ClrD.Free;
  end
  else begin
    TempClr := getNearestColorPalleteIndex (GetRValue(RGBClr),
                                            GetGValue(RGBClr),
                                            GetBValue(RGBClr));
    GetColorIndex(TempClr);
    Cat.Standard.Data.FillColor := TempClr;
    l^.Categories[ndx] := Cat;
  end;

  FillClrPnlCaption (TPanel(Sender), Cat.Standard.Data.FillColor);

end;

procedure TfmMacroSettings.pClrClick(Sender: TObject);
var
  clr : TColor;
  s : shortstring;
  tag : integer;
begin
  if Sender is TPanel then
    tag := (TPanel(Sender)).tag
  else if Sender is TButton then
    tag := (TButton(Sender)).tag
  else
    exit;
  GetColorIndex(Colours[tag]);
  if Colours[tag] = 0 then   //if no colour was set and they cancelled out of colour picker then make sure result is not zero.
    Colours[tag]:= clrwhite;
  clr := DCADRGBtoRGB(Colours[tag]);
  if (tag >= 1) and (tag <= 13) then begin
    pnlsColour[tag].Color := clr;
    ClrGetName (Colours[tag], s);
    if tag = 13 then
      pnlsColour[tag].Caption := GetLbl (243) + ' ' +  string(s)
    else
      pnlsColour[tag].Caption := string(s);
    pnlsColour[tag].font.Color := PnlFontClr(clr);
  end
end;

procedure TfmMacroSettings.enableRptButtons (enable : boolean);
var
  i : integer;
begin
  for i := 1 to NumTags do
    btnsRptTag[i].Enabled := enable;
  for i := 0 to Length(btnsRptCust)-1 do
    btnsRptCust[i].Enabled := enable;

end;

procedure TfmMacroSettings.Save(Sender: TObject);
var
  i, j, NewLength, initCustomCount : integer;
  UF : UserFieldDef;
  s : string;

begin
  SaveDone := false;
//  lblSaving.Caption := 'Saving ...';
  lblSaving.Visible := true;
  lblSaving.Repaint;
  Screen.Cursor := crHourGlass;

  initCustomCount := l^.UserFields.Count;

  // check that custom tag values are all valid
  if length(edsCustDesc) > initCustomCount then
    for i := initCustomCount to length(edsCustDesc)-1 do begin
      if edsCustTag[i].enabled and not BlankString(edsCustDesc[i].Text) then begin
        if BlankString(edsCustTag[i].Text) then begin
          lpMsg_Error(110, []); //Unique tag for User Defined Fields can NOT be blank||Save aborted
          edsCustTag[i].SetFocus;
          exit;
        end;
        if i > 0 then for j := 0 to i-1 do begin
          if UpperCase(Trim(edsCustTag[j].Text)) =  UpperCase(Trim(edsCustTag[i].Text)) then begin
            lpMsg_Error (121, []);  //Unique Tags must be unique||Save aborted
            edsCustTag[i].SetFocus;
            exit;
          end;
        end;
      end;
    end;

  l^.LabelFromSymbol := rbLabelSymbol.Checked;
  l^.AutoPlace := chbAutoPlace.Checked;
  SaveBln (SpdhAutoPlac, l^.AutoPlace);
  SaveStr ('SpdhLbSymbol', edSymFileName.Text);
  SaveStr ('SpdhRptTitle', edTitle.Text);
  for i := 1 to 11 do begin
    SaveRl ('SpdhSl' + inttostr(i) + 'Sln', dcnsSlant[i].NumValue, true);
    SaveRl ('SpdhSl' + inttostr(i) + 'Asp', dcnsAspect[i].NumValue, true);
    SaveInt ('SpdhSl' + inttostr(i) + 'Wgt', round(dcnsWeight[i].NumValue));
    SaveInt ('SpdhSl' + inttostr(i) + 'Clr',
             getNearestColorPalleteIndex (GetRValue(pnlsColour[i].Color),
                                          GetGValue(pnlsColour[i].Color),
                                          GetBValue(pnlsColour[i].Color)));
    SaveRl ('SpdhSl' + inttostr(i) + 'Siz', dcnsHeight[i].NumValue, true);
    SaveStr ('SpdhSl' + inttostr(i) + 'Font', cbsFont[i].Items[cbsFont[i].ItemIndex]);
    if i = 6 then
      SaveRl ('SpdhSpLR' + inttostr(i), dcnSpaceLR.NumValue);
    if i > 5 then
      SaveRl ('SpdhSpAbv' + inttostr(i), dcnsSpaceAbove[i].NumValue);
    if i <> 5 then
      SaveRl ('SpdhSpBlw' + inttostr(i), dcnsSpaceBelow[i].NumValue);
    if i < 6 then
      SaveStr ('SpdhLine' + inttostr(i) + 'Ptn', edsLblPattern[i].Text);
  end;
  for i := 1 to 10 do begin
    SaveStr ('SpdhRptHed' + inttostr(i), edsRptHeads[i].Text);
    SaveStr ('SpdhRptDat' + inttostr(i), edsRptData[i].Text);
    SaveStr ('SpdhRptSuT' + inttostr(i), edsRptSubTot[i].Text);
    SaveStr ('SpdhRptTot' + inttostr(i), edsRptTot[i].Text);
  end;
  for i := 1 to 12 do begin
    case cbsTextAlign[i].ItemIndex of
      1 : l^.RptTxtAlign[i] := 'C';
      2 : l^.RptTxtAlign[i] := 'R'
      else l^.RptTxtAlign[i] := 'L';
    end;
  end;
  SaveStr (SpdhRptTxAln, string(l^.RptTxtAlign));

  SaveStr ('SpdhRptSubHd', EdSubHdg.Text);

  SaveBln ('SpdhLblCAPS', chbLblAllCaps.Checked);
  SaveBln ('SpdhRptCAPS', chbRptAllCaps.Checked);

  RememberPrevDimTab;
  move (LocalDimSettings, l^.DimensionSettings, SizeOf (l^.DimensionSettings));
  SaveDimSettings;

  RememberPrevAreaTab;
  RememberPrevAltAreaTab;
  move (LocalAreaSettings, l^.AreaSettings, SizeOf (l^.AreaSettings));
  SaveAreaSettings;

  RememberPrevVolTab;
  move (LocalVolSettings, l^.VolSettings, SizeOf (l^.VolSettings));
  SaveVolSettings;

  l^.AltWallArea := chbWalls.Checked;
  l^.AltCustomArea := chbUserDefined.Checked;
  l^.AltCeilingArea := chbCeilings.Checked;
  l^.AltWindArea := chbWindows.Checked;
  l^.AltDoorArea := chbDoors.Checked;

  SaveAllCategory;

  if not chbDrawLines.Checked then
    i := -1
  else if chbLinesCurrentClr.Checked then
    i := 0
  else
    i := Colours[13];
  SaveInt (SpdhRptLnClr, i);
  l^.RptLineClr := i;

  for i := 0 to initCustomCount-1 do begin
    UF := l^.UserFields[i];
    UF.desc := str30(edsCustDesc[i].Text);
    case UF.numeric of
      str : UF.defaultstr := str30(dceCustDefault[i].Text);
      int : UF.defaultint := dceCustDefault[i].IntValue;
      rl  : UF.defaultrl := dceCustDefault[i].NumValue;
      dis : UF.defaultdis := dceCustDefault[i].NumValue;
      ang : UF.defaultang := dceCustDefault[i].NumValue;
      area: UF.defaultarea := dceCustDefault[i].NumValue;
    end;
    UF.Deleted := not edsCustDesc[i].enabled;
    l^.UserFields[i] := UF;
  end;
  if initCustomCount < length (edsCustDesc) then begin
    // new custom records to add
    for i := initCustomCount to length(edsCustDesc)-1 do begin
      if edsCustDesc[i].enabled and (not BlankString(edsCustDesc[i].Text)) then begin
        UF.tag := shortstring(edsCustTag[i].Text);
        UF.desc := shortstring(edsCustDesc[i].Text);
        case cbsCustNumeric[i].ItemIndex of
          1 : begin
                UF.numeric := int;
                UF.defaultint := dceCustDefault[i].IntValue;
              end;
          2 : begin
                UF.numeric := rl;
                UF.defaultrl := dceCustDefault[i].NumValue;
              end;
          3 : begin
                UF.numeric := dis;
                UF.defaultdis := dceCustDefault[i].NumValue;
              end;
          4 : begin
                UF.numeric := ang;
                UF.defaultang := dceCustDefault[i].NumValue;
              end;
          5 : begin
                UF.numeric := area;
                UF.defaultarea := dceCustDefault[i].NumValue;
              end
          else begin
            UF.numeric := str;
            UF.defaultstr := str25 (dceCustDefault[i].Text);
          end;
        end;
        UF.deleted := false;
        l^.UserFields.Add(UF);
      end;
    end;
  end;
  SaveUserFieldDefs;
  NewLength := Length(btnsCustDelete);
  for i := Length(btnsCustDelete)-1 downto 0 do begin
    if btnsCustDelete[i].Caption = 'UnDelete' then begin
      if i < Length(btnsCustDelete)-1 then begin
        for j := i to NewLength-2 do begin
          btnsCustDelete[j].caption := btnsCustDelete[j+1].caption;
          edsCustDesc[j].Text := edsCustDesc[j+1].Text;
          edsCustTag[j].Text := edsCustTag[j+1].Text;
          cbsCustNumeric[j].ItemIndex := cbsCustNumeric[j+1].ItemIndex;
          dceCustDefault[j].Text := dceCustDefault[j+1].Text;
        end;
      end;

      if Length(btnsCustDelete)> 2 then begin
        btnsCustDelete[Length(btnsCustDelete)-1].Free;
        edsCustDesc[Length(btnsCustDelete)-1].Free;
        edsCustTag[Length(btnsCustDelete)-1].Free;
        cbsCustNumeric[Length(btnsCustDelete)-1].Free;
        dceCustDefault[Length(btnsCustDelete)-1].Free;
      end
      else begin
        btnsCustDelete[Length(btnsCustDelete)-1].visible := false;
        edsCustDesc[Length(btnsCustDelete)-1].visible := false;
        edsCustTag[Length(btnsCustDelete)-1].visible := false;
        cbsCustNumeric[Length(btnsCustDelete)-1].visible := false;
        dceCustDefault[Length(btnsCustDelete)-1].visible := false;
      end;
      btnNewCustField.Top := btnNewCustField.Top - CustomDesc1.top;

      NewLength := NewLength-1;
    end;
  end;
  SetLength (btnsCustDelete, NewLength);
  SetLength (edsCustDesc, NewLength);
  SetLength (edsCustTag, NewLength);
  SetLength (cbsCustNumeric, NewLength);
  SetLength (dceCustDefault, NewLength);

  if l^.state = State_Report then
    SaveInt ('SpdhTabNdxRp', tsMain.TabIndex)
  else
    SaveInt ('SpdhTabNdx', tsMain.TabIndex);

  SaveInt ('SpdhStgCat', cbCategory.ItemIndex);
  if pCatCeiling.Enabled then begin      // if this panel is enabled then there may be an unsaved edit of the category
    UpdateCatInfo (cbCategory.ItemIndex, chbCatSolidFill.Checked, //pCatColour.Color,
                   fCeilingOptions);
    SaveCategory (cbCategory.ItemIndex);
  end;
  SaveCatCustFields;
  RequiredFields (l^.NameRqd,l^.NbrRqd);   // in case label or report definition has changed

  l^.LabelTextAlignment := AlignmentType (cbLblAlignment.ItemIndex);

  PGSavevar.txtuseplt := TxtScale;
  SaveBln ('SpdhTxtScale', TxtScale);
  PGSavevar.txtuseplt := TxtScale;

  l^.WallWidth := dceWallWidth.NumValue;
  SaveRl (SpdhWalWidth, l^.WallWidth, false);
  l^.DefineByCenter := rbWallCenter.Checked;
  SaveBln (SpdhDefCntr, l^.DefineByCenter);
  l^.CreateAtSurface := chbWallSurface.Checked;
  SaveBln (SpdhCrtSurf, l^.CreateAtSurface);
  l^.CreateAtCenter := chbWallCenter.Checked;
  SaveBln (SpdhCrtCent, l^.CreateAtCenter);
//  l^.FillInner := rbFillInner.Checked;
//  SaveBln ('SpdhFilInner', l^.FillInner);

  l^.RecalcLabelsAtStart := chbAutoLabelStart.Checked;
  l^.RecalcRptAtStart := chbAutoRptStart.Checked;
  l^.RecalcReport := chbAutoRpt.checked;

  l^.ReCenterLabel := chbAutoLblRecenter.Checked;

//  if cbReportTopLeft.Checked then
//    l^.ReportAnchor := TopLeft
//  else
//    l^.ReportAnchor := Cnter;

  l^.VoidSpecifyZ := rbVoidSpecifyZ.Checked;

  if rbKnockoutEntity.Checked and chbKnockoutBorder.Checked then
    l^.Knockout := KOentityBorder
  else if rbKnockoutEntity.Checked then
    l^.Knockout := KOentity
  else if rbKnockoutBox.Checked and chbKnockoutBorder.Checked then
    l^.Knockout := KOboxBorder
  else if rbKnockoutBox.Checked then
    l^.Knockout := KObox
  else
    l^.Knockout := KOnone;
  l^.KnockoutClr := Colours[12];
  SaveInt ('SpdhKOtype', ord (l^.Knockout));

  l^.KnockoutEnlX := dcnKO_X.NumValue;
  l^.KnockoutEnlY := dcnKO_Y.NumValue;
  l^.KnockoutMargin := dcnKOmargin.NumValue;
  SaveRl ('SpdhKOEnlrgX', l^.KnockoutEnlX);
  SaveRl ('SpdhKOEnlrgY', l^.KnockoutEnlY);
  SaveRl ('SpdhKOmargin', l^.KnockoutMargin);

  l^.increment := dceNumIncrement.IntValue;
  SaveInt (dhSpNumInc, l^.increment);
  l^.NameIncrement := dceNameIncrement.IntValue;
  SaveInt (dhSpNameInc, l^.NameIncrement);

  if (chbShowSurface.Checked and not (l^.ShowPln in [PLshowAll, PLshowSurf])) or
     ((not chbShowSurface.Checked) and (l^.ShowPln in [PLshowAll, PLshowSurf])) then
    DisplaySurfaces (chbShowSurface.Checked);
  if (chbShowCenter.Checked and not (l^.ShowPln in [PLshowAll, PLshowCntr])) or
     ((not chbShowCenter.Checked) and (l^.ShowPln in [PLshowAll, PLshowCntr])) then
    DisplayCenters (chbShowCenter.Checked);
  if chbShowCenter.Checked and chbShowSurface.Checked then
    l^.ShowPln := PLshowAll
  else if chbShowCenter.Checked then
    l^.ShowPln := PLshowCntr
  else if chbShowSurface.Checked then
    l^.ShowPln := PLshowSurf
  else
    l^.ShowPln := PLshowNone;
  SaveInt (SpdhShowPlin, ord (l^.ShowPln));

  chbShowCenter.Checked := (l^.ShowPln in [PLshowAll, PLshowCntr]);

  GetMsg(85, s);  // ** Use Currently Active Layer **
  if s.ToUpper = string(cbReportLyr.Text).ToUpper then
    s := ''
  else
    s := cbReportLyr.Text;
  SaveStr ('SpdhRptLyr', s);


  SaveFlags;

  SaveInt ('SpdhKOClr', l^.KnockoutClr);

//  lblSaving.Caption := '';
  lblSaving.Visible := false;

  saveDone := true;

  Screen.Cursor := crDefault;
end;  // Save


procedure TfmMacroSettings.scbTagBtnsChange(Sender: TObject);
begin
  pTagBtns.Top := - round( (pTagBtns.Height - ppTagBtns.Height) * (TScrollBar(Sender)).Position / (TScrollBar(Sender)).Max);
end;

procedure TfmMacroSettings.scbTagBtnsRptChange(Sender: TObject);
begin
  pTagBtnsRpt.Top := - round( (pTagBtnsRpt.Height - ppTagBtnsRpt.Height) * (TScrollBar(Sender)).Position / (TScrollBar(Sender)).Max);
end;

procedure TfmMacroSettings.btnSaveExitClick(Sender: TObject);
begin
  Save (Sender);
  if SaveDone then
    FmMacroSettings.Close;
end;

procedure TfmMacroSettings.btnCancelClick(Sender: TObject);
begin
  FmMacroSettings.Close;
end;

function TfmMacroSettings.CatFind (txt : string) : integer;
var
  i :integer;
begin
  result := -1;
  for i := 0 to cbCategory.Items.Count - 1 do
    if UpperCase(txt) = UpperCase (cbCategory.Items[i]) then begin
      result := i;
      exit;
    end;
end;

procedure TfmMacroSettings.btnCategoryMaintainClick(Sender: TObject);
begin
  pCatDefaults.Visible := true;
  EnableCatEdit(true);
  if (TComboBox(cbCategory)).ItemIndex < 0 then begin
    // add new category
    AddCategory ((TComboBox(cbCategory)).Text, chbCatSolidFill.checked,
                 RGBtoDCADRGB(pCatColour.Color), byte(speOpacity.value), fCeilingOptions);
    PopulateCategories (cbCategory);
    prevCatNdx := cbCategory.ItemIndex;
    CreateCatCustFields;
  end
  else begin
    // maintain existing cateogory
  end;
end;

procedure TfmMacroSettings.cbDimScaleTypeChange(Sender: TObject);
begin
  case cbDimScaleType.ItemIndex of
    0,7,8 :
        pcDimSettings.ActivePageIndex := 0;
    1 : pcDimSettings.ActivePageIndex := 1;
    6 : pcDimSettings.ActivePageIndex := 3;
    else
        pcDimSettings.ActivePageIndex := 2;
  end;
  LocalDimSettings [tcDimUnits.TabIndex].DimTyp := DimensionType (cbDimScaleType.ItemIndex);
end;

procedure TfmMacroSettings.cbLblAlignmentChange(Sender: TObject);
var
  Alignment : TAlignment;
begin
  case cbLblAlignment.ItemIndex of
    1 : Alignment := taCenter;
    2 : Alignment := taRightJustify;
    else Alignment := taLeftJustify;
  end;
  edLblPtn1.Alignment := Alignment;
  edLblPtn2.Alignment := Alignment;
  edLblPtn3.Alignment := Alignment;
  edLblPtn4.Alignment := Alignment;
  edLblPtn5.Alignment := Alignment;
end;

procedure TfmMacroSettings.cbAlignChange(Sender: TObject);
VAR
  ndx : integer;
  align : TAlignment;
begin
  ndx := (Sender as TComboBox).Tag;
  case (Sender as TComboBox).ItemIndex of
    1 : align := taCenter;
    2 : align := taRightJustify
    else align := taLeftJustify;
  end;
  case ndx of
    1..10:begin
            edsRptData[ndx].Alignment := align;
            edsRptHeads[ndx].Alignment := align;
            edsRptSubTot[ndx].Alignment := align;
            edsRptTot[ndx].Alignment := align;
          end;
    11:   edTitle.Alignment := align;
    12:   EdSubHdg.Alignment := align;
  end;
end;

procedure TfmMacroSettings.cbAltAreaFractionsChange(Sender: TObject);
begin
  chbWAreaRemZeroDec.Visible := (cbAltAreaFractions.ItemIndex=0);
  dcnWAreaDecPlacesDec.visible := (cbAltAreaFractions.ItemIndex = 0);
  lblAltAreaDecPlaces.visible := (cbAltAreaFractions.ItemIndex = 0);
end;

procedure TfmMacroSettings.cbAreaFractionsChange(Sender: TObject);
begin
  chbAreaRemZeroDec.Visible := (cbAreaFractions.ItemIndex = 0);
  dcnAreaDecPlacesDec.visible := (cbAreaFractions.ItemIndex = 0);
  lblAreaDecPlaces.visible := (cbAreaFractions.ItemIndex = 0);
end;

procedure TfmMacroSettings.cbAreaScaleTypeChange(Sender: TObject);
begin
  case cbAreaScaleType.ItemIndex of
    6,7 :
        pcAreaSettings.ActivePageIndex := 0;
    5 : pcAreaSettings.ActivePageIndex := 2;
    else
        pcAreaSettings.ActivePageIndex := 1;
  end;
  LocalAreaSettings[MainAreaRec, tcAreaUnits.TabIndex].AreaTyp := AreaType(cbAreaScaleType.ItemIndex);

  cbCustAreaFractionsChange(Sender);
  cbAreaFractionsChange(Sender);
end;

procedure TfmMacroSettings.cbCategoryChange(Sender: TObject);
var
  ndx : integer;

begin
  if pCatCeiling.Enabled and (prevCatNdx >= 0) then    // if this panel is enabled then there may be an unsaved edit of the category
    UpdateCatInfo (prevCatNdx, chbCatSolidFill.Checked, //pCatColour.Color,
                   fCeilingOptions);
  PopulateCeilingOptions ((TComboBox(Sender)).ItemIndex, fCeilingOptions);
  if BlankString((TComboBox(Sender)).Text) then begin
    btnCategoryMaintain.Visible := false;
    pCatDefaults.Visible := false;
    ndx := -1;
  end
  else begin
    ndx := (TComboBox(Sender)).ItemIndex;
    if ndx < 0 then
      ndx := catfind ((TComboBox(Sender)).Text); // in case they have manually entered text that matches an existing category
    if ndx >=0 then begin
      btnCategoryMaintain.Caption := 'Edit';
      pCatDefaults.Visible := true;
      pCatColour.Visible := l^.Categories[ndx].Standard.Data.DoFill;
      btnCatClr.Visible := pCatColour.Visible;
      chbCatSolidFill.Checked := l^.Categories[ndx].Standard.Data.DoFill;
      pCatColour.Color := DCADRGBtoRGB (l^.Categories[ndx].Standard.Data.FillColor);
      FillClrPnlCaption (pCatColour, l^.Categories[ndx].Standard.Data.FillColor);
      speOpacity.Value := round (l^.Categories[ndx].Standard.Data.FillOpacity/2.55)
    end
    else begin
      btnCategoryMaintain.Caption := 'Add';
      pCatDefaults.Visible := false;
    end;
    btnCategoryMaintain.Visible := true;
  end;
  EnableCatEdit(false);
  prevCatNdx := ndx;
  CreateCatCustFields;
end;


procedure TfmMacroSettings.cbCustAltAreaFractionsChange(Sender: TObject);
begin
  chbWAreaRemZeroCustom.Visible := (cbCustAltAreaFractions.ItemIndex = 0);
  dcnWAreaDecPlacesCustom.visible := (cbCustAltAreaFractions.ItemIndex = 0);
  lblAltAreaCustDecPlaces.visible := (cbCustAltAreaFractions.ItemIndex = 0);
end;


procedure TfmMacroSettings.cbCustAreaFractionsChange(Sender: TObject);
begin
  chbAreaRemZeroCustom.Visible := (cbCustAreaFractions.ItemIndex = 0);
  dcnAreaDecPlacesCustom.visible := (cbCustAreaFractions.ItemIndex = 0);
  lblAreaDecPlacesCust.visible := (cbCustAreaFractions.ItemIndex = 0);
end;

procedure TfmMacroSettings.cbCustDimFractionsChange(Sender: TObject);
begin
  chbDimRemZeroCustom.visible := (cbCustDimFractions.ItemIndex = 0);
  dcnDimDecPlacesCustom.visible := (cbCustDimFractions.ItemIndex = 0);
  lblDimDecPlacesCust.visible := (cbCustDimFractions.ItemIndex = 0);
end;

procedure TfmMacroSettings.cbCustVolFractionsChange(Sender: TObject);
begin
  chbVolRemZeroCustom.Visible := (cbCustVolFractions.ItemIndex = 0);
  dcnVolDecPlacesCustom.visible := (cbCustVolFractions.ItemIndex = 0);
  lblVolCustDecPlaces.visible := (cbCustVolFractions.ItemIndex = 0);
end;

procedure TfmMacroSettings.cbWAreaScaleTypeChange(Sender: TObject);
begin
  case cbWAreaScaleType.ItemIndex of
    0,7,8:
        pcWAreaSettings.ActivePageIndex := 0;
    6 : pcWAreaSettings.ActivePageIndex := 2;
    else
        pcWAreaSettings.ActivePageIndex := 1;
  end;
  if cbWAreaScaleType.ItemIndex = 0 then
    LocalAreaSettings [AltAreaRec, tcWAreaUnits.TabIndex].AreaTyp := AltSameAsMain
  else
    LocalAreaSettings [AltAreaRec, tcWAreaUnits.TabIndex].AreaTyp := AreaType(cbWAreaScaleType.ItemIndex -1);

  cbCustAltAreaFractionsChange(Sender);
  cbAltAreaFractionsChange(Sender);
end;

procedure TfmMacroSettings.cbVolFractionsChange(Sender: TObject);
begin
  chbVolRemZeroDec.Visible := (cbVolFractions.ItemIndex = 0);
  dcnVolDecPlacesDec.visible := (cbCustVolFractions.ItemIndex = 0);
  lblVolDecPlaces.visible := (cbCustVolFractions.ItemIndex = 0);
end;

procedure TfmMacroSettings.cbVolScaleTypeChange(Sender: TObject);
begin
  case cbVolScaleType.ItemIndex of
    6,5 :
        pcVolSettings.ActivePageIndex := 0;
    4 : pcVolSettings.ActivePageIndex := 2;
    else
        pcVolSettings.ActivePageIndex := 1;
  end;
  LocalVolSettings [tcVolUnits.TabIndex].VolTyp := VolumeType (cbVolScaleType.ItemIndex);

  cbVolFractionsChange(Sender);
  cbCustVolFractionsChange(Sender);
end;

procedure TfmMacroSettings.cbsFontChange(Sender: TObject);
var
  ndx : integer;
  TTF : boolean;
begin
  ndx := (TComboBox(Sender)).tag;
  TTF := (TComboBox(Sender)).Items[(TComboBox(Sender)).ItemIndex].EndsWith('(TTF)');
  dcnsSlant[ndx].tabstop := not TTF;
  dcnsWeight[ndx].tabstop := not TTF;
  cbsItalic[ndx].tabstop := TTF;
  cbsBold[ndx].tabstop := TTF;
  cbsItalic[ndx].SelLength := 0;
  cbsBold[ndx].SelLength := 0;
  dcnsWeight[ndx].Visible := not TTF;
  dcnsSlant[ndx].Visible := not TTF;
  cbsItalic[ndx].Visible := TTF;
  cbsBold[ndx].Visible := TTF;
end;

procedure TfmMacroSettings.cbUserDefaultClick(Sender: TObject);
var
  fieldndx, i : integer;
  name : string;
begin
  name := (TCheckBox(Sender)).Name;
  fieldndx := StrToInt (name.subString (13, length(name)-13)) - 1;
  CatCustFields[fieldndx].Input.visible := not (TCheckBox(Sender)).checked;
    if l^.Categories[cbCategory.ItemIndex].Custom.Count > 0 then begin
    for i := l^.Categories[cbCategory.ItemIndex].Custom.Count-1 downto 0 do
      if l^.Categories[cbCategory.ItemIndex].Custom[i].tag = l^.UserFields[fieldndx].tag then begin
        if (TCheckBox(Sender)).checked then begin
           l^.Categories[cbCategory.ItemIndex].Custom.Delete(i);
        end
        else begin
        end;
      end;
  end;
end;

procedure TfmMacroSettings.chbCatSolidFillClick(Sender: TObject);
begin
    pCatColour.Visible := chbCatSolidFill.Checked;
    btnCatClr.Visible := pCatColour.Visible;
    lblOpacity.Visible := pCatColour.Visible and DataCADVersionEqOrGtr(22,2,1,0);
    speOpacity.Visible := lblOpacity.Visible;
    lblPct.Visible := lblOpacity.Visible;
end;


procedure TfmMacroSettings.chbDrawLinesClick(Sender: TObject);
begin
  chbLinesCurrentClr.Visible := chbDrawLines.Checked;
  pnlRptLineClr.visible := chbDrawLines.Checked and (not chbLinesCurrentClr.Checked);
  btnLineClr.visible := pnlRptLineClr.visible;
end;

procedure TfmMacroSettings.chbLblAllCapsClick(Sender: TObject);
var
  i : integer;
begin
  if not init then
    exit;

  if chbLblAllCaps.Checked then begin
    for i := 1 to NumTags do
      btnsLblTag[i].Caption := UpperCase (btnsLblTag[i].Caption);
  end
  else begin
    for i := 1 to NumTags do
      btnsLblTag[i].Caption := LowerCase (btnsLblTag[i].Caption);
  end;
end;

procedure TfmMacroSettings.chbLinesCurrentClrClick(Sender: TObject);
begin
  pnlRptLineClr.visible := not chbLinesCurrentClr.Checked;
  btnLineClr.visible := pnlRptLineClr.visible;
end;

procedure TfmMacroSettings.chbRptAllCapsClick(Sender: TObject);
var
  i : integer;
begin
  if not init then
    exit;

  if chbRptAllCaps.Checked then begin
    for i := 1 to NumTags do
      btnsRptTag[i].Caption := UpperCase (btnsRptTag[i].Caption);
    for i := 0 to Length(btnsRptCust)-1 do
      btnsRptCust[i].Caption := UpperCase (btnsRptCust[i].Caption);
  end
  else begin
    for i := 1 to NumTags do
      btnsRptTag[i].Caption := LowerCase (btnsRptTag[i].Caption);
    for i := 0 to Length(btnsRptCust)-1 do
      btnsRptCust[i].Caption := LowerCase (btnsRptCust[i].Caption);
  end;
end;


procedure TfmMacroSettings.chbWallCenterClick(Sender: TObject);
begin
  if not chbWallCenter.Checked then
    chbWallSurface.Checked := true;
end;

procedure TfmMacroSettings.chbWallSurfaceClick(Sender: TObject);
begin
  if not chbWallSurface.Checked then
    chbWallCenter.Checked := true;
end;

procedure TfmMacroSettings.SHXClick(Sender: TObject);
begin
  if init then begin
    SHXfirst.Checked := not SHXfirst.Checked;
    if SHXfirst.Checked then
      TTFfirst.Checked := false;
    SaveBln ('Spdh_ShxFrst', SHXfirst.Checked, true, false);
    SaveBln ('Spdh_TtfFrst', TTFfirst.Checked, true, false);
    LoadFonts;
  end;
end;

procedure TfmMacroSettings.TextScaleClick(Sender: TObject);
var
  scale : double;
  tempstr : shortstring;
  i : integer;
begin
  if not init then
    exit;

  TxtScale := not TxtScale;

  chbTxtScaleLbl.OnClick := nil;
  chbTxtScaleRpt.OnClick := nil;
  chbTxtScaleLbl.Checked := TxtScale;
  chbTxtScaleRpt.Checked := TxtScale;
  chbTxtScaleLbl.OnClick := TextScaleClick;
  chbTxtScaleRpt.OnClick := TextScaleClick;

  scale_get (PGSavevar.plt.scalei , scale, tempstr);
  if chbTxtScaleLbl.Checked then begin
    for i := 1 to 11 do
      dcnsHeight[i].NumValue := dcnsHeight[i].NumValue * scale;
  end
  else begin
    for i := 1 to 11 do
      dcnsHeight[i].NumValue := dcnsHeight[i].NumValue / scale;
  end;
end;

procedure TfmMacroSettings.TTFClick(Sender: TObject);
begin
  if init then begin
    TTFfirst.Checked := not TTFfirst.Checked;
    if TTFfirst.Checked then
      SHXfirst.Checked := false;
    SaveBln ('Spdh_TtfFrst', TTFfirst.Checked, true, false);
    SaveBln ('Spdh_ShxFrst', SHXfirst.Checked, true, false);
    LoadFonts;
  end;
end;

procedure TfmMacroSettings.UpdateCustomBtns ();
var
  i, btnNum, btnNdx, Lbly {, Rpty} : integer;
begin
  Lbly := btnsLblTag[NumTags].Top + Button2.Top + 2;
  btnNum := 0;
  for i := 0 to (Length(edsCustDesc)-1) do begin
    if edsCustDesc[i].Enabled then begin
      btnNum := btnNum+1;
      btnNdx := btnNum-1;
      if btnNum > Length (BtnsLblCust) then begin
        SetLength (BtnsLblCust, btnNum);
        SetLength (btnsRptCust, btnNum);
        SetLength (CustNumericBtnFlags, btnNum);
        BtnsLblCust[btnNdx] := TButton.Create(self);
        BtnsRptCust[btnNdx] := TButton.Create(self);
        BtnsLblCust[btnNdx].Parent := pTagBtns;
        BtnsRptCust[btnNdx].Parent := pTagBtnsRpt;
      end;
      BtnsLblCust[btnNdx].Left := BtnsLblCust[1].Left;
      BtnsLblCust[btnNdx].Top := Lbly;
      BtnsLblCust[btnNdx].Anchors := [akRight, akTop];
      BtnsLblCust[btnNdx].Width := Button1.width;
      BtnsLblCust[btnNdx].Height := Button1.Height;
      BtnsLblCust[btnNdx].Caption := '{' + edsCustTag[btnNdx].Text + '}';
      BtnsLblCust[btnNdx].ShowHint := true;
      BtnsLblCust[btnNdx].Hint := edsCustDesc[btnNdx].Text;
      BtnsLblCust[btnNdx].OnClick := btnClick;
      BtnsLblCust[btnNdx].TabStop := false;

      BtnsRptCust[btnNdx].Left := 0;
      BtnsRptCust[btnNdx].Top := Lbly;
      BtnsRptCust[btnNdx].Anchors := [akRight, akTop];
      BtnsRptCust[btnNdx].Width := 75;
      BtnsRptCust[btnNdx].Height := 17;
      BtnsRptCust[btnNdx].Caption := '{' + edsCustTag[btnNdx].Text + '}';
      BtnsRptCust[btnNdx].ShowHint := true;
      BtnsRptCust[btnNdx].Hint := edsCustDesc[btnNdx].Text;
      BtnsRptCust[btnNdx].OnClick := btnClick;
      BtnsRptCust[btnNdx].TabStop := false;

      CustNumericBtnFlags[btnNdx] := cbsCustNumeric[btnNdx].ItemIndex > 0;
      Lbly := Lbly + Button2.Top;
    end;
  end;
  if btnNum > length (BtnsLblCust) then begin
    for i := btnNum to length (BtnsLblCust)-1 do begin
      BtnsLblCust[i-1].Visible := false;
      BtnsLblCust[i-1].Destroy;
    end;
    setLength (BtnsLblCust, btnNum);
  end;
  if btnNum > length (BtnsRptCust) then begin
    for i := btnNum to length (BtnsRptCust)-1 do begin
      BtnsRptCust[i-1].Visible := false;
      BtnsRptCust[i-1].Destroy;
    end;
    setLength (BtnsRptCust, btnNum);
  end;
  pTagBtns.Height := Lbly;
  pTagBtnsRpt.Height := Lbly;
end;

procedure TfmMacroSettings.SaveCatCustFields ();
var
  i : integer;
  atr : attrib;
  atr_existing : boolean;
begin
  if high (CatCustFields) < 0 then
    exit;
  for i := 0 to high (CatCustFields) do begin
    if CatCustFields[i].Dft.Visible and CatCustFields[i].Dft.Enabled then begin
      atr_existing := atr_sysfind (CatCustFields[i].key, atr);
      if atr_existing and CatCustFields[i].Dft.Checked then
        atr_delsys (atr)
      else if not CatCustFields[i].Dft.Checked then begin
        case l^.UserFields[i].numeric of
          str : begin
                  if atr_existing then
                    atr.atrtype := atr_str
                  else begin
                    atr_init (atr, atr_str);
                    atr.name := CatCustFields[i].key;
                  end;
                  atr.str := str80(CatCustFields[i].Input.Text);
                end;
          int : begin
                  if atr_existing then
                    atr.atrtype := atr_int
                  else begin
                    atr_init (atr, atr_int);
                    atr.name := CatCustFields[i].key;
                  end;
                  atr.int := CatCustFields[i].Input.IntValue;
                end;
          rl  : begin
                  if atr_existing then
                    atr.atrtype := atr_rl
                  else begin
                    atr_init (atr, atr_rl);
                    atr.name := CatCustFields[i].key;
                  end;
                  atr.rl := CatCustFields[i].Input.NumValue;
                end;
          dis : begin
                  if atr_existing then
                    atr.atrtype := atr_dis
                  else begin
                    atr_init (atr, atr_dis);
                    atr.name := CatCustFields[i].key;
                  end;
                  atr.rl := CatCustFields[i].Input.NumValue;
                end;
          ang : begin
                  if atr_existing then
                    atr.atrtype := atr_ang
                  else begin
                    atr_init (atr, atr_ang);
                    atr.name := CatCustFields[i].key;
                  end;
                  atr.ang := CatCustFields[i].Input.NumValue;
                end;
          area: begin
                  if atr_existing then
                    atr.atrtype := atr_area
                  else begin
                    atr_init (atr, atr_area);
                    atr.name := CatCustFields[i].key;
                  end;
                  atr.rl := CatCustFields[i].Input.NumValue;
                end;
        end;
        if atr_existing then
          atr_update (atr)
        else
          atr_add2sys (atr);
      end;
    end
    else if atr_sysfind (CatCustFields[i].key, atr) then
      atr_delsys (atr);
  end

end;

procedure TfmMacroSettings.Delete1Click(Sender: TObject);
var
  i : integer;
begin
  if CurrCol < 10 then for i := CurrCol to 9 do begin
    cbsTextAlign[i].ItemIndex := cbsTextAlign[i+1].ItemIndex;
    edsRptHeads[i].Text := edsRptHeads[i+1].Text;
    edsRptData[i].Text := edsRptData[i+1].Text;
    edsRptSubTot[i].Text := edsRptSubTot[i+1].Text;
    edsRptTot[i].Text := edsRptTot[i+1].Text;
  end;
  edsRptHeads[10].Text := '';
  edsRptData[10].Text := '';
  edsRptSubTot[10].Text := '';
  edsRptTot[10].Text := '';
end;

procedure TfmMacroSettings.CreateCatCustFields ();
var
  i : integer;
  atr : attrib;
  prevlen : integer;
begin
  if cbCategory.ItemIndex < 0 then
    exit;
  prevlen := length(CatCustFields);
  if prevlen < l^.UserFields.Count then
    SetLength (CatCustFields, l^.UserFields.Count)
  else if length(CatCustFields) > l^.UserFields.Count then
    for i := l^.UserFields.Count to (length(CatCustFields) - 1) do begin
      CatCustFields[i].Dft.Visible := false;
      CatCustFields[i].Input.Visible := false;
    end;

  cbUserDefault1.Visible := (l^.UserFields.Count > 0);
  dceUserFieldValue1.Visible := cbUserDefault1.Visible;
  cbUserDefault2.Visible := (l^.UserFields.Count > 1);
  dceUserFieldValue2.Visible := cbUserDefault2.Visible;

  for i := 0 to l^.UserFields.Count-1 do begin
    if i=0 then begin
      CatCustFields[i].Dft := cbUserDefault1;
      CatCustFields[i].Input := dceUserFieldValue1;
    end
    else if i=1 then begin
      CatCustFields[i].Dft := cbUserDefault2;
      CatCustFields[i].Input := dceUserFieldValue2;
    end
    else begin
      if i >= prevlen  then begin
        CatCustFields[i].Dft := TCheckBox.Create(cbUserDefault1.Owner);
        CatCustFields[i].Dft.Parent := cbUserDefault1.Parent;
        CatCustFields[i].Dft.Top := cbUserDefault2.Top + (i-1)*(cbUserDefault2.Top - cbUserDefault1.Top);
        CatCustFields[i].Dft.Left := cbUserDefault1.Left;
        CatCustFields[i].Dft.Width := cbUserDefault1.Width;
        CatCustFields[i].Dft.Height := cbUserDefault1.Height;
        CatCustFields[i].Dft.Alignment := cbUserDefault1.Alignment;
        CatCustFields[i].Dft.Name := 'cbUserDefault' + IntToStr(i+1);
        CatCustFields[i].Dft.OnClick := cbUserDefault1.OnClick;

        CatCustFields[i].Input := TDcadEdit.Create(dceUserFieldValue1.Owner);
        CatCustFields[i].Input.Parent := dceUserFieldValue1.Parent;
        CatCustFields[i].Input.Top := CatCustFields[i].Dft.Top - 2;
        CatCustFields[i].Input.Left := dceUserFieldValue1.Left;
        CatCustFields[i].Input.Width := dceUserFieldValue1.Width;
        CatCustFields[i].Input.Anchors := dceUserFieldValue1.Anchors;
        CatCustFields[i].Input.Height := dceUserFieldValue1.Height;
        CatCustFields[i].Input.Name := 'dceUserFieldValue' + IntToStr(i+1);
      end
      else begin
        CatCustFields[i].Dft.Visible := true;
        CatCustFields[i].Input.Visible := true;
      end;
    end;
    CatCustFields[i].Dft.Caption := string(l^.UserFields[i].desc);

    CatCustFields[i].key := 'SdhC' + l^.Categories[cbCategory.ItemIndex].num + l^.UserFields[i].tag;
    case l^.UserFields[i].numeric of
      str : begin
              CatCustFields[i].Input.NumberType := DCADEdit.TextStr;
            end;
      int : begin
              CatCustFields[i].Input.NumberType := DCADEdit.IntegerNum;
            end;
      rl  : begin
              CatCustFields[i].Input.NumberType := DCADEdit.DecimalNum;
            end;
      dis : begin
              CatCustFields[i].Input.NumberType := DCADEdit.Distance;
            end;
      ang : begin
              if pgSaveVar^.angstyl = 3 then
                CatCustFields[i].Input.NumberType := DCADEdit.DecDegrees
              else
                CatCustFields[i].Input.NumberType := DCADEdit.DegMinSec;
            end;
      area: begin
              CatCustFields[i].Input.NumberType := DCADEdit.Area;
            end;
    end;

    if atr_sysfind (CatCustFields[i].key, atr ) then begin
      case l^.UserFields[i].numeric of
        str : begin
                if atr.atrtype = atr_str then
                  CatCustFields[i].Input.Text := string (atr.str)
                else
                  CatCustFields[i].Input.Text := '';
              end;
        int : begin
                if atr.atrtype = atr_int then
                  CatCustFields[i].Input.IntValue := atr.int
                else
                  CatCustFields[i].Input.Text := '';
              end;
        rl  : begin
                if atr.atrtype = atr_rl then
                  CatCustFields[i].Input.NumValue := atr.rl
                else
                  CatCustFields[i].Input.Text := '';
              end;
        dis : begin
                if atr.atrtype = atr_dis then
                  CatCustFields[i].Input.NumValue := atr.dis
                else
                  CatCustFields[i].Input.Text := '';
              end;
        ang : begin
                if atr.atrtype = atr_ang then
                  CatCustFields[i].Input.NumValue := atr.ang
                else
                  CatCustFields[i].Input.Text := '';
              end;
        area: begin
                if atr.atrtype = atr_area then
                  CatCustFields[i].Input.NumValue := atr.rl
                else
                  CatCustFields[i].Input.Text := '';
              end;
      end;
      CatCustFields[i].Dft.Checked := false;
      CatCustFields[i].Input.Visible := true;
    end
    else begin
      CatCustFields[i].Dft.Checked := true;
      CatCustFields[i].Input.Visible := false;
      case l^.UserFields[i].numeric of
        str : begin
                CatCustFields[i].Input.Text := string(l^.UserFields[i].defaultstr);
              end;
        int : begin
                CatCustFields[i].Input.IntValue := l^.UserFields[i].defaultint;
              end;
        rl  : begin
                CatCustFields[i].Input.NumValue := l^.UserFields[i].defaultrl;
              end;
        dis : begin
                CatCustFields[i].Input.NumValue := l^.UserFields[i].defaultdis;
              end;
        ang : begin
                CatCustFields[i].Input.NumValue := l^.UserFields[i].defaultang;
              end;
        area: begin
                CatCustFields[i].Input.NumValue := l^.UserFields[i].defaultarea;
              end;
      end;
    end;
  end;
end;


procedure TfmMacroSettings.KnockoutVisibility;
begin
  pnlKOnone.visible := not (rbKnockoutEntity.Checked or rbKnockoutBox.Checked);
  pnlKObox.Visible := rbKnockoutBox.Checked;
  pnlKOentity.Visible := rbKnockoutEntity.Checked;
end;



procedure TfmMacroSettings.lblColMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  MousePoint : TPoint;
begin
  {get X, Y in screen coordinates}
  MousePoint := (Sender as TControl).ClientToScreen(System.Classes.Point(X, Y));
  ColMenu.PopUp(MousePoint.X, MousePoint.Y);
  CurrCol := (Sender as TControl).Tag;
end;
procedure TfmMacroSettings.CreateFields(Sender: TObject);
var
  i, posy, weight : integer;
  height, slant, Aspect, space, spacebelow, spacebeside : double;
  s : shortstring;
  btnwidth, btnhght : integer;
  ColAlign : TAlignment;
  dfltTxtSz, scale : double;

  procedure   CreateTagButtons (Parent: TWinControl; var Btns : TagBtns; var Cust : CustBtns; px, py : integer; Caps : boolean);
  var
    i : integer;
    pyInc : integer;
  begin
    LastCsr:= -1;
    posy := py;

    for i := 1 to NumTags do begin
      if Caps then
        Btns[i].Caption := UpperCase (Tags[i][1])
      else
        Btns[i].Caption := Tags[i][1];
      Btns[i].Hint := Tags[i][2];
      Btns[i].Visible := true;
      Btns[i].Enabled := false;
    end;

     // user defined buttons
    pyInc := Btns[2].Top - Btns[1].top;
    py := Btns[NumTags].Top + pyInc + 2;
    btnwidth := Btns[1].width;
    btnhght := Btns[1].Height;
    setlength (Cust, l^.UserFields.Count);
    setLength (CustNumericBtnFlags, l^.UserFields.Count);
    if l^.UserFields.Count > 0 then begin
      for i := 0 to l^.UserFields.Count-1 do begin
        CustNumericBtnFlags[i] := l^.UserFields[i].numeric <> str;
        Cust[i] := TButton.Create(self);
        Cust[i].Parent := Parent;
        Cust[i].Left := 0;
        Cust[i].Top := py;
        Cust[i].Anchors := [akLeft, akTop];
        py := py + pyInc;
        Cust[i].Width := BtnWidth;
        Cust[i].Height := BtnHght;
        if Caps then
          Cust[i].Caption := '{' + UpperCase (string(l^.UserFields[i].tag)) + '}'
        else
          Cust[i].Caption := '{' + string(l^.UserFields[i].tag) + '}';
        Cust[i].Hint := string(l^.UserFields[i].desc);
        Cust[i].ShowHint := true;
        Cust[i].OnClick := btnClick;
        Cust[i].TabStop := false;
      end;
    end;
    Parent.Height := py;
  end;

begin   // CreateFields
  if init then
    exit;
  init := false;

  TTFfirst.Checked := GetIniBln ('Spdh_TtfFrst', false);
  SHXfirst.Checked := GetIniBln ('Spdh_ShxFrst', true);

  CheckTxtScale;
  TxtScale := PGSavevar.txtuseplt;
  chbTxtScaleRpt.Checked := TxtScale;
  chbTxtScaleLbl.Checked := TxtScale;

  edsLblPattern[1] := edLblPtn1;
  edsLblPattern[2] := edLblPtn2;
  edsLblPattern[3] := edLblPtn3;
  edsLblPattern[4] := edLblPtn4;
  edsLblPattern[5] := edLblPtn5;

  cbsFont[1] := cbLblFont1;
  cbsFont[2] := cbLblFont2;
  cbsFont[3] := cbLblFont3;
  cbsFont[4] := cbLblFont4;
  cbsFont[5] := cbLblFont5;

  dcnsHeight[1] := dcnHeight1;
  dcnsHeight[2] := dcnHeight2;
  dcnsHeight[3] := dcnHeight3;
  dcnsHeight[4] := dcnHeight4;
  dcnsHeight[5] := dcnHeight5;

  dcnsAspect[1] := dcnAspect1;
  dcnsAspect[2] := dcnAspect2;
  dcnsAspect[3] := dcnAspect3;
  dcnsAspect[4] := dcnAspect4;
  dcnsAspect[5] := dcnAspect5;

  cbsItalic[1] := cbItalic1;
  cbsItalic[2] := cbItalic2;
  cbsItalic[3] := cbItalic3;
  cbsItalic[4] := cbItalic4;
  cbsItalic[5] := cbItalic5;

  cbsBold[1] := cbBold1;
  cbsBold[2] := cbBold2;
  cbsBold[3] := cbBold3;
  cbsBold[4] := cbBold4;
  cbsBold[5] := cbBold5;

  dcnsSlant[1] := dcnSlant1;
  dcnsSlant[2] := dcnSlant2;
  dcnsSlant[3] := dcnSlant3;
  dcnsSlant[4] := dcnSlant4;
  dcnsSlant[5] := dcnSlant5;

  dcnsWeight[1] := dcnWeight1;
  dcnsWeight[2] := dcnWeight2;
  dcnsWeight[3] := dcnWeight3;
  dcnsWeight[4] := dcnWeight4;
  dcnsWeight[5] := dcnWeight5;

  pnlsColour[1] := pnlLblColour1;
  pnlsColour[2] := pnlLblColour2;
  pnlsColour[3] := pnlLblColour3;
  pnlsColour[4] := pnlLblColour4;
  pnlsColour[5] := pnlLblColour5;

  dcnsSpaceBelow[1] := dcnSpaceBelow1;
  dcnsSpaceBelow[2] := dcnSpaceBelow2;
  dcnsSpaceBelow[3] := dcnSpaceBelow3;
  dcnsSpaceBelow[4] := dcnSpaceBelow4;

  posy := 58;

  dfltTxtSz := pgSaveVar^.txtsiz;
  if PGSavevar.txtuseplt then begin
    scale_get (PGSavevar.plt.scalei , scale, s);
    dfltTxtSz := dfltTxtSz*scale;
  end;

  for i := 1 to 5 do begin
    s := GetSvdStr ('SpdhLine' + inttostr(i) + 'Ptn', '!@#$%');
    if s = '!@#$%' then
      s := GetSvdStr ('Line' + inttostr(i) + 'Ptn', defaultPtn[i]);
    edsLblPattern[i].Text := string(s);

    height := GetSvdRl ('SpdhSl' + inttostr(i) + 'Siz', 0);
    if height = 0 then
      height := GetSvdRl ('dhSPStl' + inttostr(i) + 'Siz', dfltTxtSz);
    dcnsHeight[i].NumValue := height;

    Aspect := GetSvdRl ('SpdhSl' + inttostr(i) + 'Asp', 0.0);
    if RealEqual(Aspect, 0.0) then
      Aspect := GetSvdRl ('dhSPStl' + inttostr(i) + 'Asp', 1.0);
    dcnsAspect[i].NumValue := Aspect;


    slant := getsvdRl ('SpdhSl' + inttostr(i) + 'Sln', 999.0);
    if slant = 999.0 then
      if i < 10 then
        slant := getsvdRl ('dhSPStl' + inttostr(i) + 'Sln', 0.0)
      else
        slant := 0.0;
    weight := getsvdInt ('SpdhSl' + inttostr(i) + 'Wgt', -999);
    if weight = -999 then
      if i < 10 then
        weight := getsvdInt ('dhSPStl' + inttostr(i) + 'Wgt', 1)
      else
        weight := 1;

    if slant > 0.0 then
      cbsItalic[i].ItemIndex := 1
    else
      cbsItalic[i].ItemIndex := 0;

    if weight > 1 then
      cbsBold[i].ItemIndex := 1
    else
      cbsBold[i].ItemIndex := 0;

    dcnsSlant[i].NumValue := slant;
    dcnsWeight[i].NumValue := Weight;

    Colours[i] := GetSvdInt ('SpdhSl' + inttostr(i) + 'Clr', 0);
    if Colours[i] = 0 then
      if i < 10 then
        Colours[i] := GetSvdInt ('dhSPStl' + inttostr(i) + 'Clr', clrWhite)
      else
        Colours[i] := clrWhite;
    pnlsColour[i].Color := DCADRGBtoRGB (Colours[i]);
    clrGetName (Colours[i], s);
    pnlsColour[i].Caption := string(s);
    pnlsColour[i].font.Color := PnlFontClr(pnlsColour[i].Color);

    space := GetSvdRl ('SpdhSpBlw'+inttostr(i), 0.25);
    if i < 5 then
      dcnsSpaceBelow[i].NumValue := space;
  end;

  cbsTextAlign[1] := cbAlign1;
  cbsTextAlign[2] := cbAlign2;
  cbsTextAlign[3] := cbAlign3;
  cbsTextAlign[4] := cbAlign4;
  cbsTextAlign[5] := cbAlign5;
  cbsTextAlign[6] := cbAlign6;
  cbsTextAlign[7] := cbAlign7;
  cbsTextAlign[8] := cbAlign8;
  cbsTextAlign[9] := cbAlign9;
  cbsTextAlign[10] := cbAlign10;
  cbsTextAlign[11] := cbTitleAlign;
  cbsTextAlign[12] := cbSubHeadAlign;

  edsRptHeads[1] := edRptHead1;
  edsRptHeads[2] := edRptHead2;
  edsRptHeads[3] := edRptHead3;
  edsRptHeads[4] := edRptHead4;
  edsRptHeads[5] := edRptHead5;
  edsRptHeads[6] := edRptHead6;
  edsRptHeads[7] := edRptHead7;
  edsRptHeads[8] := edRptHead8;
  edsRptHeads[9] := edRptHead9;
  edsRptHeads[10] := edRptHead10;

  edsRptData[1] := edRptData1;
  edsRptData[2] := edRptData2;
  edsRptData[3] := edRptData3;
  edsRptData[4] := edRptData4;
  edsRptData[5] := edRptData5;
  edsRptData[6] := edRptData6;
  edsRptData[7] := edRptData7;
  edsRptData[8] := edRptData8;
  edsRptData[9] := edRptData9;
  edsRptData[10] := edRptData10;

  edsRptSubTot[1] := edRptSubTot1;
  edsRptSubTot[2] := edRptSubTot2;
  edsRptSubTot[3] := edRptSubTot3;
  edsRptSubTot[4] := edRptSubTot4;
  edsRptSubTot[5] := edRptSubTot5;
  edsRptSubTot[6] := edRptSubTot6;
  edsRptSubTot[7] := edRptSubTot7;
  edsRptSubTot[8] := edRptSubTot8;
  edsRptSubTot[9] := edRptSubTot9;
  edsRptSubTot[10] := edRptSubTot10;

  edsRptTot[1] := edRptTot1;
  edsRptTot[2] := edRptTot2;
  edsRptTot[3] := edRptTot3;
  edsRptTot[4] := edRptTot4;
  edsRptTot[5] := edRptTot5;
  edsRptTot[6] := edRptTot6;
  edsRptTot[7] := edRptTot7;
  edsRptTot[8] := edRptTot8;
  edsRptTot[9] := edRptTot9;
  edsRptTot[10] := edRptTot10;

  edTitle.TabOrder := 0;
  s := GetSvdStr ('SpdhRptTitle', '!@#$');
  if s = '!@#$' then
    s := GetSvdStr ('dhSPRptTitle', '');
  edTitle.Text := String (s);
  case l^.RptTxtAlign[11] of
    'C' : begin
            cbTitleAlign.ItemIndex := 1;
            edTitle.Alignment := taCenter;
          end;
    'R' : begin
            cbTitleAlign.ItemIndex := 2;
            edTitle.Alignment := taRightJustify;
          end
    else  begin
            cbTitleAlign.ItemIndex := 0;
            edTitle.Alignment := taLeftJustify;
          end;
  end;

  for i := 1 to 10 do begin
    case l^.RptTxtAlign[i] of
      'C' : begin
              cbsTextAlign[i].ItemIndex := 1;
              ColAlign := taCenter;
            end;
      'R' : begin
              cbsTextAlign[i].ItemIndex := 2;
              ColAlign := taRightJustify;
            end
      else  begin
              cbsTextAlign[i].ItemIndex := 0;
              ColAlign := taLeftJustify;
            end;
    end;

    edsRptHeads[i].Text := ReportHeading (i);
    edsRptHeads[i].Alignment := ColAlign;
    edsRptData[i].Text := ReportDataPattern(i);
    edsRptData[i].Alignment := ColAlign;
    edsRptSubTot[i].Text := ReportSubTotPattern(i);
    edsRptSubTot[i].Alignment := ColAlign;
    edsRptTot[i].Text := ReportTotPattern(i);
    edsRptTot[i].Alignment := ColAlign;
  end;

  EdSubHdg.Text := ReportSubHeading;
  case l^.RptTxtAlign[12] of
    'C' : begin
            cbSubHeadAlign.ItemIndex := 1;
            EdSubHdg.Alignment := taCenter;
          end;
    'R' : begin
            cbSubHeadAlign.ItemIndex := 2;
            EdSubHdg.Alignment := taRightJustify;
          end
    else  begin
            cbSubHeadAlign.ItemIndex := 0;
            EdSubHdg.Alignment := taLeftJustify;
          end;
  end;


  cbsFont[6] := cbRptFont6;
  cbsFont[7] := cbRptFont7;
  cbsFont[8] := cbRptFont8;
  cbsFont[9] := cbRptFont9;
  cbsFont[10] := cbRptFont10;
  cbsFont[11] := cbRptFont11;

  dcnsHeight[6] := dcnRptHght6;
  dcnsHeight[7] := dcnRptHght7;
  dcnsHeight[8] := dcnRptHght8;
  dcnsHeight[9] := dcnRptHght9;
  dcnsHeight[10] := dcnRptHght10;
  dcnsHeight[11] := dcnRptHght11;

  dcnsAspect[6] := dcnRptAspect6;
  dcnsAspect[7] := dcnRptAspect7;
  dcnsAspect[8] := dcnRptAspect8;
  dcnsAspect[9] := dcnRptAspect9;
  dcnsAspect[10] := dcnRptAspect10;
  dcnsAspect[11] := dcnRptAspect11;

  dcnsSlant[6] := dcnSlant6;
  dcnsSlant[7] := dcnSlant7;
  dcnsSlant[8] := dcnSlant8;
  dcnsSlant[9] := dcnSlant9;
  dcnsSlant[10] := dcnSlant10;
  dcnsSlant[11] := dcnSlant11;

  dcnsWeight[6] := dcnWeight6;
  dcnsWeight[7] := dcnWeight7;
  dcnsWeight[8] := dcnWeight8;
  dcnsWeight[9] := dcnWeight9;
  dcnsWeight[10] := dcnWeight10;
  dcnsWeight[11] := dcnWeight11;

  cbsItalic[6] := cbItalic6;
  cbsItalic[7] := cbItalic7;
  cbsItalic[8] := cbItalic8;
  cbsItalic[9] := cbItalic9;
  cbsItalic[10] := cbItalic10;
  cbsItalic[11] := cbItalic11;

  cbsBold[6] := cbBold6;
  cbsBold[7] := cbBold7;
  cbsBold[8] := cbBold8;
  cbsBold[9] := cbBold9;
  cbsBold[10] := cbBold10;
  cbsBold[11] := cbBold11;

  pnlsColour[6] := pRptClr6;
  pnlsColour[7] := pRptClr7;
  pnlsColour[8] := pRptClr8;
  pnlsColour[9] := pRptClr9;
  pnlsColour[10] := pRptClr10;
  pnlsColour[11] := pRptClr11;

  dcnsSpaceAbove[6] := dcnSpaceAbove6;
  dcnsSpaceAbove[7] := dcnSpaceAbove7;
  dcnsSpaceAbove[8] := dcnSpaceAbove8;
  dcnsSpaceAbove[9] := dcnSpaceAbove9;
  dcnsSpaceAbove[10] := dcnSpaceAbove10;
  dcnsSpaceAbove[11] := dcnSpaceAbove11;

  dcnsSpaceBelow[6] := dcnSpaceBelow6;
  dcnsSpaceBelow[7] := dcnSpaceBelow7;
  dcnsSpaceBelow[8] := dcnSpaceBelow8;
  dcnsSpaceBelow[9] := dcnSpaceBelow9;
  dcnsSpaceBelow[10] := dcnSpaceBelow10;
  dcnsSpaceBelow[11] := dcnSpaceBelow11;

  for i := 6 to 11 do begin
    height := GetSvdRl ('SpdhSl' + inttostr(i) + 'Siz', 0);
    if height = 0 then
      if i < 10 then
        height := GetSvdRl ('dhSPStl' + inttostr(i) + 'Siz', dfltTxtSz)
      else
        height := dfltTxtSz;
    dcnsHeight[i].NumValue := height;

    Aspect := GetSvdRl ('SpdhSl' + inttostr(i) + 'Asp', 0.0);
    if Aspect = 0 then
      Aspect := GetSvdRl ('dhSPStl' + inttostr(i) + 'Asp', 1.0);
    dcnsAspect[i].NumValue := Aspect;

    slant := getsvdRl ('SpdhSl' + inttostr(i) + 'Sln', 999.0);
    if slant = 999.0 then
      slant := getsvdRl ('dhSPStl' + inttostr(i) + 'Sln', 0.0);
    weight := getsvdInt ('SpdhSl' + inttostr(i) + 'Wgt', -999);
    if weight = -999 then
      weight := getsvdInt ('dhSPStl' + inttostr(i) + 'Wgt', 1);

    if slant > 0.0 then
      cbsItalic[i].ItemIndex := 1
    else
      cbsItalic[i].ItemIndex := 0;

    if weight > 1 then
      cbsBold[i].ItemIndex := 1
    else
      cbsBold[i].ItemIndex := 0;

    dcnsSlant[i].NumValue := Slant;
    dcnsWeight[i].NumValue := Weight;



    Colours[i] := GetSvdInt ('SpdhSl' + inttostr(i) + 'Clr', 0);
    if Colours[i] = 0 then
      Colours[i] := GetSvdInt ('dhSPStl' + inttostr(i) + 'Clr', clrWhite);
    pnlsColour[i].Color := DCADRGBtoRGB (Colours[i]);
    clrGetName (Colours[i], s);
    pnlsColour[i].Caption := string(s);
    pnlsColour[i].font.Color := PnlFontClr(pnlsColour[i].Color);

    SpaceAroundText (i, space, spacebelow, spacebeside);
    dcnsSpaceAbove[i].NumValue := space;
    dcnsSpaceBelow[i].NumValue := spacebelow;
    if i= 6 then
      dcnSpaceLR.NumValue := spacebeside;
  end;

  LoadFonts;

  chbLblAllCaps.Checked := GetSvdBln ('SpdhLblCAPS', false);
  chbRptAllCaps.Checked := GetSvdBln ('SpdhRptCAPS', false);
  BtnsLblTag[1] := Button1;
  BtnsLblTag[2] := Button2;
  BtnsLblTag[3] := Button3;
  BtnsLblTag[4] := Button4;
  BtnsLblTag[5] := Button5;
  BtnsLblTag[6] := Button6;
  BtnsLblTag[7] := Button7;
  BtnsLblTag[8] := Button8;
  BtnsLblTag[9] := Button9;
  BtnsLblTag[10] := Button10;
  BtnsLblTag[11] := Button11;
  BtnsLblTag[12] := Button12;
  BtnsLblTag[13] := Button13;
  BtnsLblTag[14] := Button14;
  BtnsLblTag[15] := Button15;
  BtnsLblTag[16] := Button16;
  BtnsLblTag[17] := Button17;
  BtnsLblTag[18] := Button18;
  BtnsLblTag[19] := Button19;
  BtnsLblTag[20] := Button20;
  for i := 21 to NumTags do begin
    BtnsLblTag[i] := TButton.Create(self);
    BtnsLblTag[i].Parent := Button20.Parent;
    BtnsLblTag[i].Left := Button20.Left;
    BtnsLblTag[i].Top := Button20.Top + (i-20)* (Button20.Top - Button19.Top);
    BtnsLblTag[i].Anchors := [akLeft, akTop];
    BtnsLblTag[i].Width := Button20.Width;
    BtnsLblTag[i].Height := Button20.Height;
    BtnsLblTag[i].ShowHint := true;
    BtnsLblTag[i].OnClick := Button20.OnClick;
    BtnsLblTag[i].TabStop := false;
  end;
  CreateTagButtons (pTagBtns, BtnsLblTag, BtnsLblCust, 0, 0, chbLblAllCaps.Checked);
  scbTagBtns.Max := NumTags + Length(btnsLblCust);

  btnsRptTag[1] := Button2_1;
  btnsRptTag[2] := Button2_2;
  btnsRptTag[3] := Button2_3;
  btnsRptTag[4] := Button2_4;
  btnsRptTag[5] := Button2_5;
  btnsRptTag[6] := Button2_6;
  btnsRptTag[7] := Button2_7;
  btnsRptTag[8] := Button2_8;
  btnsRptTag[9] := Button2_9;
  btnsRptTag[10] := Button2_10;
  btnsRptTag[11] := Button2_11;
  btnsRptTag[12] := Button2_12;
  btnsRptTag[13] := Button2_13;
  btnsRptTag[14] := Button2_14;
  btnsRptTag[15] := Button2_15;
  btnsRptTag[16] := Button2_16;
  btnsRptTag[17] := Button2_17;
  btnsRptTag[18] := Button2_18;
  btnsRptTag[19] := Button2_19;
  btnsRptTag[20] := Button2_20;

  for i := 21 to NumTags do begin
    btnsRptTag[i] := TButton.Create(self);
    btnsRptTag[i].Parent := Button2_20.Parent;
    btnsRptTag[i].Left := Button2_20.Left;
    btnsRptTag[i].Top := Button2_20.Top + (i-20)* (Button2_20.Top - Button2_19.Top);
    btnsRptTag[i].Anchors := [akLeft, akTop];
    btnsRptTag[i].Width := Button2_20.Width;
    btnsRptTag[i].Height := Button2_20.Height;
    btnsRptTag[i].ShowHint := true;
    btnsRptTag[i].OnClick := Button2_20.OnClick;
    btnsRptTag[i].TabStop := false;
  end;


  CreateTagButtons (pTagBtnsRpt, btnsRptTag, btnsRptCust, 0, 0, chbRptAllCaps.Checked);
  scbTagBtnsRpt.Max := NumTags + Length(btnsRptCust);


  if l^.LabelFromSymbol then begin
    rbLabelSymbol.Checked := true;
    pcLabel.ActivePage := tsFromSymbol;
  end
  else begin
    rbLabelText.Checked := true;
    pcLabel.ActivePage := tsSPecifyText;
  end;

  edSymFileName.Text := string (GetSvdStr ('SpdhLbSymbol', ''));

  chbWalls.Checked := l^.AltWallArea;
  chbCeilings.Checked := l^.AltCeilingArea;
  chbWindows.Checked := l^.AltWindArea;
  chbDoors.Checked := l^.AltDoorArea;

  if l^.Categories.Count < 2 then begin
    CustomDesc1.Visible := false;
    CustomTag1.Visible := false;
    CustomNum1.Visible := false;
    CustomDflt1.Visible := false;
    CustomDel1.Visible := false;
  end;
  if l^.Categories.Count < 1 then begin
    CustomDesc0.Visible := false;
    CustomTag0.Visible := false;
    CustomNum0.Visible := false;
    CustomDflt0.Visible := false;
    CustomDel0.Visible := false;
  end;

  setlength (btnsCustDelete, l^.UserFields.Count);
  setlength (edsCustDesc, l^.UserFields.Count);
  setlength (edsCustTag, l^.UserFields.Count);
  setlength (cbsCustNumeric, l^.UserFields.Count);
  setlength (dceCustDefault, l^.UserFields.Count);
  if l^.UserFields.Count = 0 then begin
    CustomDesc0.Visible := false;
    CustomTag0.Visible := false;
    CustomNum0.Visible := false;
    CustomDflt0.Visible := false;
    CustomDel0.Visible := false;
    CustomDesc1.Visible := false;
    CustomTag1.Visible := false;
    CustomNum1.Visible := false;
    CustomDflt1.Visible := false;
    CustomDel1.Visible := false;
  end
  else for i := 0 to l^.UserFields.Count-1 do begin
    CreateCustButtons (i);

    edsCustDesc[i].Text := string(l^.UserFields[i].desc);
    edsCustDesc[i].OnExit := CustDescExit;
    edsCustDesc[i].OnEnter :=CustDescEnter;

    edsCustTag[i].Text := string(l^.UserFields[i].tag);
    edsCustTag[i].TabStop := false;
    edsCustTag[i].OnEnter := CustNoEditEnter;

    cbsCustNumeric[i].ItemIndex := ord(l^.UserFields[i].numeric);
    cbsCustNumeric[i].tabstop := false;
    cbsCustNumeric[i].OnEnter := CustNoEditEnter;

    case l^.UserFields[i].numeric of
      str : begin
              dceCustDefault[i].NumberType := TextStr;
              dceCustDefault[i].Text := string(l^.UserFields[i].defaultstr);
            end;
      int : begin
              dceCustDefault[i].NumberType := DcadEdit.IntegerNum;
              dceCustDefault[i].IntValue := l^.UserFields[i].defaultint;
            end;
      rl  : begin
              dceCustDefault[i].NumberType := DcadEdit.DecimalNum;
              dceCustDefault[i].NumValue := l^.UserFields[i].defaultrl;
            end;
      dis : begin
              dceCustDefault[i].NumberType := DcadEdit.Distance;
              dceCustDefault[i].NumValue := l^.UserFields[i].defaultdis;
            end;
      ang : begin
              if pgSaveVar^.angstyl = 3 then
                dceCustDefault[i].NumberType := DCADEdit.DecDegrees
              else
                dceCustDefault[i].NumberType := DcadEdit.DegMinSec;
              dceCustDefault[i].NumValue := l^.UserFields[i].defaultang;
            end;
      area: begin
              dceCustDefault[i].NumberType := DcadEdit.Area;
              dceCustDefault[i].NumValue := l^.UserFields[i].defaultrl;
            end;
    end;
    dceCustDefault[i].OnEnter := CustDefaultEnter;
    dceCustDefault[i].OnExit := CustDefaultExit;

  end;

  PopulateCategories (cbCategory);
  btnCategoryMaintain.Visible := (l^.Categories.Count > 0);

  if l^.state = State_Report then
    tsMain.TabIndex := GetSvdInt ('SpdhTabNdxRp', 1)
  else
    tsMain.TabIndex := GetSvdInt ('SpdhTabNdx', 0);

  cbCategory.ItemIndex := GetSvdInt ('SpdhStgCat', -1);

  fCeilingOptions := TfCeilingOptions.Create(self);
  fCeilingOptions.Parent := pCatCeiling;
  fCeilingOptions.Top := 0;
  fCeilingOptions.Left := 0;
  fCeilingOptions.Visible := true;
  pCatDefaults.visible := (cbCategory.ItemIndex >= 0);
  btnCategoryMaintain.visible := false;
  PopulateCeilingOptions (cbCategory.ItemIndex, fCeilingOptions);
    if cbCategory.ItemIndex >= 0 then begin
    FillClrPnlCaption (pCatColour, l^.Categories[cbCategory.ItemIndex].Standard.Data.FillColor);
    cbCategory.Text := string(l^.Categories[cbCategory.ItemIndex].Standard.Data.name);
    chbCatSolidFill.Checked := l^.Categories[cbCategory.ItemIndex].Standard.Data.DoFill;
    btnCategoryMaintain.Caption := 'Edit';
    btnCategoryMaintain.Visible := true;
    pCatColour.Visible := chbCatSolidFill.Checked;
    lblOpacity.Visible := pCatColour.Visible and DataCADVersionEqOrGtr(22,2,1,0);
    speOpacity.Visible := lblOpacity.Visible;
    speOpacity.Value := round(l^.Categories[cbCategory.ItemIndex].Standard.Data.FillOpacity / 2.55);
    lblPct.Visible := lblOpacity.Visible;

  end
  else begin
    if l^.FillColours[1] <> 0 then
      FillClrPnlCaption (pCatColour, l^.FillColours[1])
    else
      FillClrPnlCaption (pCatColour, CurrentState^.color);
    btnCategoryMaintain.Visible := false;
    pCatColour.visible := false;
    lblOpacity.Visible := false;
    speOpacity.Visible := false;
    speOpacity.Value := round(l^.FillOpacity / 2.55);
    lblPct.Visible := false;
  end;
  btnCatClr.Visible := pCatColour.visible;
  fCeilingOptions.Show;

  CreateCatCustFields;
  EnableCatEdit(false);

  CustomFillColours1.checked := l.Options.CustomClr;

  cbLblAlignment.ItemIndex := ord (l^.LabelTextAlignment);

  with l^.DimensionSettings[UNITS_LBL] do begin
    cbDimScaleType.ItemIndex := Ord(DimTyp);
    cbDimRoundFeetInch.ItemIndex := Ord(Rounding);
    cbDimRoundDec.ItemIndex := Ord(Rounding);
    cbDimRoundCustom.ItemIndex := Ord(Rounding);
    dcnDimDecPlacesDec.IntValue := DecPlaces;
    dcnDimDecPlacesCustom.IntValue := DecPlaces;
    chbDimRemZeroDec.Checked := RemTrailingZero;
    chbDimRemZeroCustom.Checked := RemTrailingZero;
    dcnDimUnitSz.NumValue := CustUnitSize;
    edDimUnitDispCustom.Text := string (CustUnitDisplay);
    edDimUnitDispDec.Text := string(DecUnitDisplay);
    lblDimDecPlacesCust.Visible := (cbCustDimFractions.ItemIndex = 0);
    dcnDimDecPlacesCustom.Visible := (cbCustDimFractions.ItemIndex = 0);
  end;

  cbDimScaleTypeChange(self);

  move (l^.DimensionSettings, LocalDimSettings, SizeOf (LocalDimSettings));
  move (l^.AreaSettings, LocalAreaSettings, SizeOf (LocalAreaSettings));
  move (l^.VolSettings, LocalVolSettings, SizeOf (LocalVolSettings));

  DimUnits (UNITS_LBL);
  AreaUnits (UNITS_LBL);
  VolUnits (UNITS_LBL);
  WAreaUnits (UNITS_LBL);


  dceWallWidth.NumValue := l^.WallWidth;
  rbWallCenter.Checked := l^.DefineByCenter;
  rbWallSurface.Checked:= not rbWallCenter.Checked;
  chbWallSurface.Checked := l^.CreateAtSurface;
  chbWallCenter.Checked := l^.CreateAtCenter;
//  rbFillInner.Checked := l^.FillInner;

  chbAutoLabelStart.Checked := l^.RecalcLabelsAtStart;
  chbAutoRptStart.Checked := l^.RecalcRptAtStart;
  chbAutoRpt.Checked := l^.RecalcReport;

  chbAutoLblRecenter.Checked := l^.ReCenterLabel;
//  if l^.ReportAnchor = TopLeft then
//    cbReportTopLeft.Checked := true
//  else
//    rbReportCenter.Checked := true;

  Colours[12] := l^.KnockoutClr;
  pnlKnockoutClr.Color := DCADRGBtoRGB (Colours[12]);
  clrGetName (Colours[12], s);
  pnlKnockoutClr.Caption := string(s);
  pnlKnockoutClr.font.Color := PnlFontClr(pnlKnockoutClr.Color);

  rbKnockoutENtity.Checked := (l^.Knockout in [KOentity, KOentityBorder]);
  rbKnockoutBox.Checked := (l^.Knockout in [KObox, KOboxBorder]);
  rbKnockoutNone.Checked := l^.Knockout = KOnone;
  chbKnockoutBorder.Checked := (l^.Knockout in [KOentityBorder, KOboxBorder]);
  dcnKO_X.NumValue := l^.KnockoutEnlX;
  dcnKO_Y.NumValue := l^.KnockoutEnlY;
  dcnKOmargin.NumValue := l^.KnockoutMargin;
  KnockoutVisibility;

  rbVoidSpecifyZ.Checked := l^.VoidSpecifyZ;
  // not implementing void options in this version
  rbVoidNoSpecifyZ.Visible := false;
  rbVoidSpecifyZ.Visible := false;

  chbAutoPlace.Checked := l^.AutoPlace;

  dceNumIncrement.IntValue := l^.increment;
  dceNameIncrement.IntValue := l^.NameIncrement;

  chbShowSurface.Checked := (l^.ShowPln in [PLshowAll, PLshowSurf]);
  chbShowCenter.Checked := (l^.ShowPln in [PLshowAll, PLshowCntr]);

  chbDrawLines.Checked := (l^.RptLineClr >= 0);
  chbLinesCurrentClr.Visible := chbDrawLines.Checked;
  chbLinesCurrentClr.Checked := (l^.RptLineClr = 0);
  if l^.RptLineClr > 0 then
    Colours[13] := l^.RptLineClr
  else
    Colours[13] := clrwhite;
  pnlRptLineClr.Color := DCADRGBtoRGB (Colours[13]);
  clrGetName (Colours[13], s);
  pnlRptLineClr.Caption := GetLbl (243) + ' ' +  string(s);
  pnlRptLineClr.font.Color := PnlFontClr(pnlRptLineClr.Color);
  pnlRptLineClr.visible := chbDrawLines.Checked and not chbLinesCurrentClr.Checked;
  btnLineClr.Visible := pnlRptLineClr.visible;


  init := true;
  screen.cursor:= crDefault;
end;    //CreateFields

procedure TfmMacroSettings.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  SaveFormPos (TForm(Sender));
end;

procedure TfmMacroSettings.FormCreate(Sender: TObject);
var
  LblNo : integer;
  lblCaption, FieldHint : string;
  s : string;
begin
  SetFormPos(TForm(Sender));
  MenuItemCaption (110, Options); //Options
  MenuItemCaption (126, CustomFillColours1);  //Custom Fill Colours
  MenuItemCaption (179, FontOrder1);  //Font Order
  MenuItemCaption (180, TTFfirst);  //TTF First
  MenuItemCaption (181, SHXfirst);  //SHX First
  LabelCaption (182, lblSaving);  //Saving ...
  TabCaption (129, tsUserDefined);  //User Defined Fields
  TabCaption (183, tsLabels); //Labels
  TabCaption (184, tsReportData); //Reports (Page 1)
  TabCaption (185, tsReportFormat); //Reports (Page 2)
  TabCaption (186, tsUnits);  //Units
  TabCaption (187, tsCat);  //Categories
  TabCaption (188, tsWallOptions);  //Wall Options
  TabCaption (189, tsProcOptions);  //Processing Options
  RadioCaption (197, rbLabelText); //Specify Text
  RadioCaption (198, rbLabelSymbol); //From Symbol
  CheckCaption (199, chbAutoPlace);  //Auto-place labels at Space Center
  CheckCaption (200, chbLblAllCaps);  //All Caps
  LabelCaption (201, lblAlignment);  //Label Alignment
  cbLblAlignment.Items.Clear;
  getlbl (202, s);   //Left Align
  cbLblAlignment.Items.Add (s);
  getlbl (203, s);   //Center Align
  cbLblAlignment.Items.Add (s);
  getlbl (204, s);   //Right Align
  cbLblAlignment.Items.Add (s);
  cbTitleAlign.Items := cbLblAlignment.Items;
  cbAlign1.Items := cbLblAlignment.Items;
  cbAlign2.Items := cbLblAlignment.Items;
  cbAlign3.Items := cbLblAlignment.Items;
  cbAlign4.Items := cbLblAlignment.Items;
  cbAlign5.Items := cbLblAlignment.Items;
  cbAlign6.Items := cbLblAlignment.Items;
  cbAlign7.Items := cbLblAlignment.Items;
  cbAlign8.Items := cbLblAlignment.Items;
  cbAlign9.Items := cbLblAlignment.Items;
  cbAlign10.Items := cbLblAlignment.Items;
  cbSubHeadAlign.Items := cbLblAlignment.Items;
  CheckCaption (205, chbTxtScaleLbl); //Text Scale
  LabelCaption (206, lblTxtPattern);  //Text Pattern
  LabelCaption (207, lblFont);  //Font
  LabelCaption (208, lblHeight);  //Height
  LabelCaption (209, lblAspect);  //Aspect
  LabelCaption (210, lblSlant);  //Slant
  LabelCaption (211, lblWeight);  //Weight
  LabelCaption (212, lblColour);  //Colour
  LabelCaption (213, lblSpaceBelow);  //Space Below
  cbItalic1.Items.Clear;
  cbBold1.Items.Clear;
  getlbl (214,s);  //Regular
  cbItalic1.Items.Add(s);
  cbBold1.Items.Add(s);
  getlbl (215,s);  //Italic
  cbItalic1.Items.Add(s);
  cbItalic2.Items := cbItalic1.Items;
  cbItalic3.Items := cbItalic1.Items;
  cbItalic4.Items := cbItalic1.Items;
  cbItalic5.Items := cbItalic1.Items;
  cbItalic6.Items := cbItalic1.Items;
  cbItalic7.Items := cbItalic1.Items;
  cbItalic8.Items := cbItalic1.Items;
  cbItalic9.Items := cbItalic1.Items;
  cbItalic10.Items := cbItalic1.Items;
  cbItalic11.Items := cbItalic1.Items;
  getLbl (216, s);  //Bold
  cbBold1.Items.Add(s);
  cbBold2.Items := cbBold1.Items;
  cbBold3.Items := cbBold1.Items;
  cbBold4.Items := cbBold1.Items;
  cbBold5.Items := cbBold1.Items;
  cbBold6.Items := cbBold1.Items;
  cbBold7.Items := cbBold1.Items;
  cbBold8.Items := cbBold1.Items;
  cbBold9.Items := cbBold1.Items;
  cbBold10.Items := cbBold1.Items;
  cbBold11.Items := cbBold1.Items;

  GroupCaption (217, gbKnockout); //Knockout
  RadioCaption (218, rbKnockoutNone); //No Knockout
  RadioCaption (219, rbKnockoutEntity); //Knockout by Entity
  RadioCaption (220, rbKnockoutBox); //Knockout Box
  LabelCaption (221, lblKOmargin);  //Margin
  LabelCaption (222, lblKOclr);   //Knockout Colour
  LabelCaption (225, lblXEnl);  //X Enlargement
  LabelCaption (226, lblYEnl);  //Y Enlargement

  LabelCaption (227, lblTitle); //Report Title:
  LabelCaption (228, lblHeadings);  //Col Headings:
  LabelCaption (229, lblSubHeads);  //Sub-Heading:
  LabelCaption (230, lblData);  //Data Line:
  LabelCaption (232, lblSubTots); //Sub-Totals:
  LabelCaption (233, lblTots); //Totals:
  CheckCaption (200, chbRptAllCaps);  //All Caps
  CheckCaption (205, chbTxtScaleRpt); //Text Scale
  LabelCaption (207, lblFont2);  //Font
  LabelCaption (208, lblHeight2);  //Height
  LabelCaption (209, lblAspect2);  //Aspect
  LabelCaption (210, lblSlant2);  //Slant
  LabelCaption (211, lblWeight2);  //Weight
  LabelCaption (212, lblClr2);  //Colour
  LabelCaption (213, lblSpaceBelow2);  //Space Below
  LabelCaption (234, lblSpaceAbove2);   //Space Above
  LabelCaption (235, lblSpaceLR);   //Space Left/Right
  CheckCaption (236, chbDrawLines);   //Draw Lines
  CheckCaption (237, chbLinesCurrentClr);   //Use Current Colour for Lines
  PanelLblParams(243, [], pnlRptLineClr);   //Line Colour|Select Colour to use for lines
  btnLineClr.Hint := pnlRptLineClr.Hint;
  btnLineClr.ShowHint := pnlRptLineClr.ShowHint;

  LabelCaption (244, lblRptLyr);  //Report Layer

  LabelCaption (245, lblDimensions);    //Dimensions
  LabelCaption (246, lblAreas);    //Areas
  LabelCaption (247, lblVolumes);    //Volumes
  LabelCaption (248, lblAltAreas);    //Alternate Areas
  TabControlCaptions (tcDimUnits, [249, 250, 251]); //Labels, Reports, Clipboard
  TabControlCaptions (tcAreaUnits, [249, 250, 251]); //Labels, Reports, Clipboard
  TabControlCaptions (tcVolUnits, [249, 250, 251]); //Labels, Reports, Clipboard
  TabControlCaptions (tcWAreaUnits, [249, 250, 251]); //Labels, Reports, Clipboard
  ComboItems(cbDimScaleType, [252, 253, 254, 255, 256, 257, 258]);  //Current Scale Type, Feet/Inches (rounded), Decimal Feet, Meters, Centimeter, Millimeter, Custom
  ComboItems(cbAreaScaleType, [259, 260, 261, 262, 263, 258]);  //Square Feet, Squares, Acres, Square Meter, Hectares, Custom
  ComboItems(cbVolScaleType, [264, 265, 266, 267, 258]);  //Cubic Feet, Cubic Yards, Litres, Cubic Meters, Custom
  ComboItems(cbWAreaScaleType, [268, 259, 260, 261, 262, 263, 258]);  // Same as Main Areas setting, Square Feet, Squares, Acres, Square Meter, Hectares, Custom, Square Feet, Squares, Acres, Square Meter, Hectares, Custom

  LabelCaption (271, lblDimCustomUnitSz); //Unit Size
  LabelCaption (272, lblDimCustomUnitDisp); //Unit Display
  LabelCaption (273, lblDimCustomRounding); //Rounding
  LabelCaption (274, lblDimCustomFrac); //Fractions
  LabelCaption (275, lblDimDecPlacesCust); //Decimal Places
  CheckCaption (276, chbDimRemZeroCustom);  //Remove trailing zeros after decimal point
  ComboItems (cbDimRoundCustom, [277, 278, 279]);   //  Round Down, Round Nearest, Round Up
  ComboItems (cbCustDimFractions, [280]);     //Decimal (leave other items as designed)

  LabelCaption (273, lblDimdecRounding); //Rounding
  LabelCaption (272, lblDimDecUnitDisp); //Unit Display
  LabelCaption (275, lblDimDecPlaces); //Decimal Places
  CheckCaption (276, chbDimRemZeroDec);  //Remove trailing zeros after decimal point
  ComboItems (cbDimRoundDec, [277, 278, 279]);   //  Round Down, Round Nearest, Round Up

  LabelCaption (273, lblDimFtInchRounding); //Rounding
  ComboItems (cbDimRoundFeetInch, [277, 278, 279]);   //  Round Down, Round Nearest, Round Up

  LabelCaption (271, lblCustunitsArea);    //Unit Size
  //LabelCaption (281, lblCustunitsAreaLbl); //(Square Ft)
  LabelCaption (272, lblAreaCustomUnitDisp); //Unit Display
  LabelCaption (273, lblAreaCustomRounding); //Rounding
  LabelCaption (274, lblAreaCustomFrac); //Fractions
  LabelCaption (275, lblAreaDecPlacesCust); //Decimal Places
  CheckCaption (276, chbAreaRemZeroCustom);  //Remove trailing zeros after decimal point
  ComboItems (cbCustAreaFractions, [280]);     //Decimal (leave other items as designed)
  ComboItems (cbAreaRoundCustom, [277, 278, 279]);   //  Round Down, Round Nearest, Round Up

  LabelCaption (272, lblAreaDecUnitDisp); //Unit Display
  LabelCaption (273, lblAreaDecRounding); //Rounding
  LabelCaption (275, lblAreaDecPlaces); //Decimal Places
  CheckCaption (276, chbAreaRemZeroDec);  //Remove trailing zeros after decimal point
  ComboItems (cbAreaFractions, [280]);     //Decimal (leave other items as designed)
  ComboItems (cbAreaRoundDec, [277, 278, 279]);   //  Round Down, Round Nearest, Round Up

  LabelCaption (271, lblVolCustUnitSz); //Unit Size
  //LabelCaption (282, lblVolCustUnitSzLbl); //(cubic ft)
  LabelCaption (272, lblVolCustUnitDisp); //Unit Display
  LabelCaption (273, lblVolCustRounding); //Rounding
  LabelCaption (274, lblVolCustFractions); //Fractions
  LabelCaption (275, lblVolCustDecPlaces); //Decimal Places
  CheckCaption (276, chbVolRemZeroCustom);  //Remove trailing zeros after decimal point
  ComboItems (cbVolRoundCustom, [277, 278, 279]);   //  Round Down, Round Nearest, Round Up
  ComboItems (cbCustVolFractions, [280]);     //Decimal (leave other items as designed)

  LabelCaption (272, lblVolDecUnitDisp); //Unit Display
  LabelCaption (273, lblVolDecRounding); //Rounding
  LabelCaption (274, lblVolDecFractions); //Fractions
  LabelCaption (275, lblVolDecPlaces); //Decimal Places
  ComboItems (cbVolFractions, [280]);     //Decimal (leave other items as designed)
  ComboItems (cbVolRoundDec, [277, 278, 279]);   //  Round Down, Round Nearest, Round Up
  CheckCaption (276, chbVolRemZeroDec);  //Remove trailing zeros after decimal point

  LabelCaption (271, lblAltAreaCustUnitSz); //Unit Size
  LabelCaption (272, lblAltAreaCustUnitDisp); //Unit Display
  LabelCaption (273, lblAltAreaCustRounding); //Rounding
  LabelCaption (274, lblAltAreaCustFractions); //Fractions
  LabelCaption (275, lblAltAreaCustDecPlaces); //Decimal Places
  CheckCaption (276, chbWAreaRemZeroCustom);  //Remove trailing zeros after decimal point
  ComboItems (cbWAreaRoundCustom, [277, 278, 279]);   //  Round Down, Round Nearest, Round Up
  ComboItems (cbCustAltAreaFractions, [280]);     //Decimal (leave other items as designed)

  LabelCaption (272, lblAltAreaCustUnitDisp); //Unit Display
  LabelCaption (273, lblAltAreaDecRounding); //Rounding
  LabelCaption (274, lblAltAreaDecFractions); //Fractions
  LabelCaption (275, lblAltAreaDecPlaces); //Decimal Places
  CheckCaption (276, chbWAreaRemZeroDec);   //Remove trailing zeros after decimal point
  ComboItems (cbWAreaRoundDec, [277, 278, 279]);   //  Round Down, Round Nearest, Round Up
  ComboItems (cbAltAreaFractions, [280]);     //Decimal (leave other items as designed)

  LabelCaption (281, lblAltArea);   //Use Alternate Area Settings for:
  CheckCaption (282, chbWalls);   //Wall Areas
  CheckCaption (365, chbUserDefined); //User Defined Fields|Use Alternate Area units for User Defined Area Fields
  CheckCaption (283, chbWindows);   //Window Areas
  CheckCaption (284, chbDoors);   //Door Areas
  CheckCaption (285, chbCeilings);   //Ceiling Areas

  LabelCaption (286, lblDescription); //Description
  GetLbl (287, lblCaption, FieldHint);  //Unique Tag|Enter a tag of up to 6 characters.  You can then use that tag to include the field in Labels and Reports
  lblUniqueTag.Caption := lblCaption;
  CustomTag0.Hint := FieldHint;
  CustomTag1.Hint := FieldHint;
  LabelCaption (288, lblFormat); //Format
  LabelCaption (289, lblDefault); //Default Value

//  PanelLblParams (106, [], pCatColour);   //Select Colour|Select Fill Colour
  LabelCaption (129, lblCatUserDefinedFields);    //User Defined Fields
  LabelCaption (291, lblCatFieldName); //Field Name
  LabelCaption (144, lblCatUseDefault); //Use Default
  LabelCaption (145, lblCatValue);    //Value
  RadioCaption (292, rbWallSurface);    //Define Spaces by Wall Surface
  RadioCaption (293, rbWallCenter);    //Define Spaces by Wall Center
  LabelCaption (120, lblThickness);  //Wall Thickness
  CheckCaption (121, chbWallSurface);   //Create polyline at Wall Surface
  CheckCaption (122, chbWallCenter);   //Create polyline at Wall Center
  LabelCaption (294, lblNote);    // Note:
  GetMsg (111, lblCaption);   //Created polylines can be visible or invisible as per the options below (these options will apply to all spaces regardless of what the setting was at the time the space was created).
  lblNoteText.Caption := lblCaption;
  CheckCaption (295, chbShowSurface); //Wall Surface Polylines are Visible
  CheckCaption (296, chbShowCenter); //Wall Center Polylines are Visible
  GroupCaption (297, gbIncrements);  //Increments
  GroupCaption (298, gbAutoRecalculation);  //Auto Recalculation
  GroupCaption (299, gbAutoPositioning);  //Auto Positioning
  LabelCaption (300, lblNumInc);  //Room Number Increment
  LabelCaption (301, lblNameInc);  //Room Name Increment
  GetMsg (112, lblCaption); //(name of next room will be defaulted if previous room name ended with a number)
  lblNameIncNote.Caption := lblCaption;

  CheckCaption (302, chbAutoLabelStart);  //Auto Recalc Labels when macro starts
  CheckCaption (303, chbAutoRptStart);  //Auto Recalc Reports when macro starts
  CheckCaption (304, chbAutoRpt);  //Add to Reports when adding spaces
  GetMsg (113, lblCaption); //The above options determine if labels or reports are automatically updated. Labels are always updated if you use the macro to change a space.  You can use F9 Refresh/Update from the main menu to update labels at any time.  Pleases see the manual for complete details.
  lblAutoNote.Caption := lblCaption;
  CheckCaption (305, chbAutoLblRecenter); //Auto Recenter Labels on Recalc

  GetMsg (114, LblCaption); //If a space is changed and the label was previously at the center then the recalculated label will automatically be placed at the new center (label is not moved on recalc if this option is disabled)
  lblAutoPosNote.Caption := LblCaption;

  LabelCaption (313, lblSymbolFile);
  GetMsg (109, lblCaption); //The symbol used can contain most entity types (2D or 3D lines, curves 3D solids etc), in addition to text. The text in the symbol may contain tags (e.g. '{nbr}', '{name}' etc) in addition to actual text. The tags should be part of the actual text in the symbol (do NOT create the tags as symbol attribute text).
  lblSymbolNotes.Caption := lblCaption;

  case PGSaveVar^.scaletype of
    0, 1, 2, 4, 5: LblNo := 24
    else LblNo := 25;
  end;
  GetLbl (LblNo, lblCaption, FieldHint);
  lblCustUnitsAreaLbl.caption := lblCaption;
  lblAltAreaCustUnitSzLbl.Caption := lblCaption;
  dcnAreaUnitSz.Hint := FieldHint;
  dcnAreaUnitSz.ShowHint := true;
  dcnWAreaUnitSz.Hint := FieldHint;
  dcnWAreaUnitSz.ShowHint := true;

  case PGSaveVar^.scaletype of
    0, 1, 2, 4, 5: LblNo := 26
    else LblNo := 27;
  end;
  GetLbl (LblNo, lblCaption, FieldHint);
  lblVolCustUnitSzLbl.Caption := lblCaption;
  dcnVolUnitSz.Hint := FieldHint;
  dcnVolUnitSz.ShowHint := true;

  PopulateLayerList (85, cbReportLyr);

  s := string(GetSvdStr('SpdhRptLyr', ''));
  if BlankString(s) then
    cbReportLyr.ItemIndex := 0
  else
    cbReportLyr.Text := s;

  memInstructions.Lines.Clear;
  GetMsg(90, s); //Enter required text for each entry.  Leave a column blank if it is not required (blank columns on the right will not be added to report). Tags (in curly braces) will be replaced by the appropriate data when the report is produced.
  memInstructions.Lines.Add(s);
  memInstructions.Lines.Add('');
  GetMsg(91, s); //Available tags for each field have their corresponding buttons to the left enabled (tags are not valid in Report Title or Col Headings).
  memInstructions.Lines.Add(s);

  CheckCaption (121, chbWallSurface); // Create polyline at Wall Surface
  CheckCaption (122, chbWallCenter); // Create polyline at Wall Center

  LabelCaption (113, lblCatCategory);  //Category
  LabelCaption (290, lblCatDefaults);   //Defaults:

  MenuItemCaption(347, Clear1);  //Clear
  MenuItemCaption(353, Delete1);  //Delete
  MenuItemCaption(354, Insert1);  //Insert

  init := false;

  pnlsColour[12] := pnlKnockoutClr;
  pnlsColour[13] := pnlRptLineClr;


  CreateFields (Sender);

  cbLblAlignmentChange (cbLblAlignment);

end;

procedure TfmMacroSettings.FormDestroy(Sender: TObject);
var
  i : integer;
begin
    if length (btnsLblCust) > 0 then for i := 0 to Length(btnsLblCust)-1 do
      btnsLblCust[i].Free;
    Finalize (btnsLblCust);

    if length (btnsRptCust) > 0 then for i := 0 to Length(btnsRptCust)-1 do
      btnsRptCust[i].Free;
    Finalize (btnsRptCust);

    Finalize (CustNumericBtnFlags);

    if Length(CatCustFields) > 2 then for i := 2 to Length(CatCustFields)-1 do begin
      CatCustFields[i].Dft.free;
      CatCustFields[i].Input.free;
    end;
    Finalize (CatCustFields);

    if Length(btnsCustDelete) > 2 then for i := 2 to Length(btnsCustDelete)-1 do
      btnsCustDelete[i].Free;
    Finalize (btnsCustDelete);

    if Length(edsCustDesc) > 2 then for i := 2 to Length(edsCustDesc)-1 do
      edsCustDesc[i].Free;
    Finalize (edsCustDesc);

    if Length(edsCustTag) > 2 then for i := 2 to Length(edsCustTag)-1 do
      edsCustTag[i].Free;
    Finalize (edsCustTag);

    if Length(cbsCustNumeric) > 2 then for i := 2 to Length(cbsCustNumeric)-1 do
      cbsCustNumeric[i].Free;
    Finalize (cbsCustNumeric);

    if Length(dceCustDefault) > 2 then for i := 2 to Length(dceCustDefault)-1 do
      dceCustDefault[i].Free;
    Finalize (dceCustDefault);
end;

procedure TfmMacroSettings.FormPaint(Sender: TObject);
begin
    screen.cursor:= crDefault;
    SetWindowPos(Handle, HWND_TOP, Left, Top, Width, Height, SWP_NOACTIVATE or SWP_NOMOVE or SWP_NOSIZE);
end;

procedure TfmMacroSettings.FormResize(Sender: TObject);
begin
  scbTagBtns.Visible := pTagBtns.Height > ppTagBtns.Height;
  if not scbTagBtns.Visible then
    pTagBtns.top := 0;

  scbTagBtnsRpt.Visible := pTagBtnsRpt.Height > ppTagBtnsRpt.Height;
  if not scbTagBtnsRpt.Visible then
    pTagBtnsRpt.top := 0;

end;

procedure TfmMacroSettings.rbKnockoutClick(Sender: TObject);
begin
  KnockoutVisibility;
  if init and PGSaveVar^.stakfrac and ((Sender as TRadioButton).Name = 'rbKnockoutEntity') then
    lMsg_OK(36);  //Knockout by Entity is NOT recommended when using stacked fractions
end;

procedure TfmMacroSettings.rbLabelSymbolClick(Sender: TObject);
begin
  pcLabel.ActivePage := tsFromSymbol;
end;

procedure TfmMacroSettings.rbLabelTextClick(Sender: TObject);
begin
  pcLabel.ActivePage := tsSpecifyText;
end;

procedure TfmMacroSettings.rbWallCenterClick(Sender: TObject);
begin
  pnlCenter.visible := true;  //rbWallCenter.Checked;
end;

procedure TfmMacroSettings.SpeedButton1Click(Sender: TObject);
begin
  OpenDialog1.InitialDir := '';
  if not Length (edSymFileName.Text) > 0 then
    OpenDialog1.InitialDir := ExtractFilePath(edSymFileName.Text);
  if Length (OpenDialog1.InitialDir) = 0 then
    OpenDialog1.InitialDir := string(pgSaveVar^.SymbolFolder);
  if OpenDialog1.Execute then
    if FileExists(OpenDialog1.FileName) then
       edSymFileName.Text := OpenDialog1.FileName;
end;

procedure TfmMacroSettings.speOpacityExit(Sender: TObject);
var
  Ndx : integer;
  Cat : CategoryDef;
begin
  ndx := cbCategory.ItemIndex;
  Cat := l^.Categories[ndx];
  Cat.Standard.Data.DoFill := true;  // should already be true, but just in case
  Cat.Standard.Data.FillOpacity := round(speOpacity.Value * 2.55);
  Cat.updated := true;
  l^.Categories[ndx] := Cat;
end;

procedure TfmMacroSettings.tcVolUnitsChange(Sender: TObject);
begin
  VolUnits (tcVolUnits.TabIndex);
end;

procedure TfmMacroSettings.tcDimUnitsChange(Sender: TObject);
begin
  DimUnits (tcDimUnits.TabIndex);
end;

procedure TfmMacroSettings.tcAreaUnitsChange(Sender: TObject);
begin
  AreaUnits (tcAreaUnits.TabIndex);
end;

procedure TfmMacroSettings.tcWAreaUnitsChange(Sender: TObject);
begin
  WAreaUnits (tcwAreaUnits.TabIndex);
end;

procedure TfmMacroSettings.CustEditClick (Sender: TObject);
var
  fieldndx : integer;
  name : string;
begin
  name := (TButton(Sender)).Name;
  fieldndx := StrToInt (name.subString (8, length(name)-8));
  edsCustDesc[fieldndx].Enabled := true;
  edsCustDesc[fieldndx].SetFocus;
  (TButton(Sender)).visible := false;
end;

procedure TfmMacroSettings.CustDelClick (Sender: TObject);
var
  fieldndx : integer;
  name : string;
begin
  name := (TButton(Sender)).Name;
  fieldndx := StrToInt (name.subString (9, length(name)-8));
  edsCustDesc[fieldndx].Enabled := not edsCustDesc[fieldndx].Enabled;
  edsCustTag[fieldndx].Enabled := edsCustDesc[fieldndx].Enabled;
  cbsCustNumeric[fieldndx].Enabled := edsCustDesc[fieldndx].Enabled;
  dceCustDefault[fieldndx].Enabled := edsCustDesc[fieldndx].Enabled;
  if edsCustDesc[fieldndx].Enabled then begin
    edsCustDesc[fieldndx].Font.Color := clWindowText;
    edsCustDesc[fieldndx].Font.Style := edsCustDesc[fieldndx].Font.Style - [fsStrikeOut];
    edsCustTag[fieldndx].Font.Color := clWindowText;
    edsCustTag[fieldndx].Font.Style := edsCustTag[fieldndx].Font.Style - [fsStrikeOut];
    dceCustDefault[fieldndx].Font.Style := dceCustDefault[fieldndx].Font.Style - [fsStrikeOut];
    ButtonLblParams(11, [shortstring(edsCustDesc[fieldndx].Text)], btnsCustDelete[fieldndx]);
  end
  else begin
    edsCustDesc[fieldndx].Font.Color := clRed;
    edsCustDesc[fieldndx].Font.Style := edsCustDesc[fieldndx].Font.Style + [fsStrikeOut];
    edsCustTag[fieldndx].Font.Color := clRed;
    edsCustTag[fieldndx].Font.Style := edsCustTag[fieldndx].Font.Style + [fsStrikeOut];
    dceCustDefault[fieldndx].Font.Style := dceCustDefault[fieldndx].Font.Style + [fsStrikeOut];
    ButtonLblParams(12, [shortstring(edsCustDesc[fieldndx].Text)], btnsCustDelete[fieldndx]);
  end;

end;

procedure TfmMacroSettings.CustDescEnter (Sender: TObject);
begin
  PrevValue := (TEdit(Sender)).Text;
end;

procedure TfmMacroSettings.CustDescExit (Sender: TObject);
begin
  if PrevValue <> (TEdit(Sender)).Text then begin
    (TEdit(Sender)).Font.Style := (TEdit(Sender)).Font.Style + [fsBold];
  end;
end;

procedure TfmMacroSettings.CustDefaultEnter (Sender : TObject);
begin
  PrevValue := (TDcadEdit(Sender)).Text;
end;

procedure TfmMacroSettings.CustDefaultExit (Sender: TObject);
begin
  if PrevValue <> (TDcadEdit(Sender)).Text then begin
    (TDcadEdit(Sender)).Font.Style := (TDcadEdit(Sender)).Font.Style + [fsBold];
  end;
end;

procedure TfmMacroSettings.CustNoEditEnter (Sender: TObject);
var
  fieldndx : integer;
  name : string;
begin
  name := (TButton(Sender)).Name;
  fieldndx := StrToInt (name.subString (9, length(name)-8));
  edsCustDesc[fieldndx].SetFocus;
end;

procedure TfmMacroSettings.CustomDescChange(Sender: TObject);
var
  fieldndx : integer;
  name : string;
begin
  name := (TEdit(Sender)).Name;
  fieldndx := StrToInt (name.subString (10, length(name)-10));
  if edsCustDesc[fieldndx].Enabled then begin
    ButtonLblParams(11, [shortstring(edsCustDesc[fieldndx].Text)], btnsCustDelete[fieldndx]);
  end
  else begin
    ButtonLblParams(12, [shortstring(edsCustDesc[fieldndx].Text)], btnsCustDelete[fieldndx]);
  end;
end;

procedure TfmMacroSettings.CustomFillColours1Click(Sender: TObject);
begin
  l.Options.CustomClr := not l.Options.CustomClr;
  CustomFillColours1.checked := l.Options.CustomClr;
  SaveOptions;
end;

procedure TfmMacroSettings.CustomTagKeyPress(Sender: TObject; var Key: Char);
begin
  if (key = '&') or (key = '{') or (key = '}') then begin
    key := #0;     //do not allow ampersand in tags
    beep;
  end;
end;

procedure TfmMacroSettings.CustTagExit (Sender: TObject);
var
  i, fieldndx : integer;
  name : string;
  warninggiven : boolean;
begin
  if screen.ActiveControl <> btnCancel then begin
    name := (TButton(Sender)).Name;
    WarningGiven := false;
    fieldndx := StrToInt (name.subString (9, length(name)-8));
    for i := 0 to Length(edsCustTag)-1 do begin
      if (i <> FieldNdx) and
         (CompareText (edsCustTag[i].Text, edsCustTag[FieldNdx].Text) = 0) and
         not WarningGiven
      then begin
        lpMsg_Error (6, [shortstring(edsCustTag[i].Text)]);
        (TEdit(Sender)).SetFocus;
        WarningGiven := true;
      end;
    end;
    if not WarningGiven then begin
      name := '{' + edsCustTag[FieldNdx].Text + '}';
      for i := 1 to NumTags do begin
        if (not WarningGiven) and (CompareText (name, Tags[i][1]) = 0) then begin
          lpMsg_Error (8, [shortstring(edsCustTag[FieldNdx].Text), shortstring(Tags[i][2])]);
          WarningGiven := true;
          (TEdit(Sender)).SetFocus;
        end;
      end;
    end;
  end;
end;

procedure TfmMacroSettings.tsUserDefinedExit(Sender: TObject);
var
  i : integer;
  b, b1, b2 : boolean;
begin
  if screen.ActiveControl <> btnCancel then begin
    for i := 0 to Length(edsCustTag)-1 do begin
      b:= edsCustTag[i].enabled;
      b1 := BlankString (edsCustTag[i].Text);
      b2 := BlankString (edsCustDesc[i].Text);
      if b and (b1 xor b2) then begin
        if b1 then begin
          lpMsg_Error(110, []);   //Unique tag for User Defined Fields can NOT be blank
          edsCustDesc[i].SetFocus
        end
        else begin
          lpMsg_Error (121, []);  //Unique Tags must be unique
          edsCustTag[i].SetFocus;
        end;
      end
      else begin
        UpdateCustomBtns;
      end;
    end;
  end;
end;

procedure TfmMacroSettings.NumericChange(Sender: TObject);
var
  Ndx : integer;
begin
  Ndx := strtoint (string((TComboBox(Sender).name)).substring(9));
  case (TComboBox(Sender)).ItemIndex of
    0 : dceCustDefault[Ndx].NumberType := TextStr;
    1 : dceCustDefault[Ndx].NumberType := DcadEdit.IntegerNum;
    2 : dceCustDefault[Ndx].NumberType := DcadEdit.DecimalNum;
    3 : dceCustDefault[Ndx].NumberType := DcadEdit.Distance;
    4 : if pgSaveVar^.angstyl = 3 then
          dceCustDefault[Ndx].NumberType := DCADEdit.DecDegrees
        else
          dceCustDefault[Ndx].NumberType := DcadEdit.DegMinSec;
    5 : dceCustDefault[Ndx].NumberType := DcadEdit.Area;
  end;
end;


end.

