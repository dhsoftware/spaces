unit Constants;

interface

const
  SpdhInptMode = 'SpdhInptMode';
  SpdhVInptMod = 'SpdhVInptMod';
  SpdhSpaceNum = 'SpdhSpaceNum';
  dhSpNumInc = 'dhSpNumInc';
  dhSpNameInc = 'dhSpNameInc';
  SpdhAutoPlac = 'SpdhAutoPlac';
  SpdhShowPlin = 'SpdhShowPlin';
  SpdhWllThick = 'SpdhWllThick';
  SpdhMaxOpeng = 'SpdhMaxOpeng';
  SpdhPlnFill = 'SpdhPlnFill';
  SpdhFilOutLn = 'SpdhFilOutLn';
  SpdhWalWidth = 'SpdhWalWidth';
  SpdhDefCntr = 'SpdhDefCntr';
  SpdhCrtSurf = 'SpdhCrtSurf';
  SpdhCrtCent = 'SpdhCrtCent';
  SpdhMsgDlg = 'SpdhMsgDlg';
  SpdhEditSel = 'SpdhEditSel';
  SpdhRptSbTot = 'SpdhRptSbTot';
  SpdhRptTxAln = 'SpdhRptTxAln';
  SpdhRptLnClr = 'SpdhRptLnClr';

  LicParagraph1 = 'This software is distributed free of charge and on an "AS IS" basis. To the extent ' +
                  'permitted by law, I disclaim all warranties of any kind, either express or implied, ' +
                  'including but not limited to, warranties of merchantability, fitness for a particular purpose, ' +
                  'and non-infringement of third-party rights.';
  LicParagraph2 = 'I will not be liable for any direct, indirect, actual, exemplary or any other damages ' +
                  'arising from the use or inability to use this software, even if I have been advised of the ' +
                  'possibility of such damages.';
  LicParagraph3 = 'Whilst I do solicit contributions toward the cost of developing and distributing my ' +
                  'software, such contributions are entirely at your discretion and in no way constitute a ' +
                  'payment for the software.';
  LicParagraph4 = 'You may distribute this software to others provided that you distribute the complete ' +
                  'unaltered zip file provided by me at the dhsoftware.com.au web site, and that you do ' +
                  'so free of charge. This includes not charging for any other software, service or ' +
                  'product that you associate with this software in such a way as to imply that a purchase ' +
                  'is required in order to obtain this software (without limitation, examples of unacceptable ' +
                  'charges would be charging for distribution media or for any accompanying software that is ' +
                  'on the same media or contained in the same download or distribution file).  If you wish ' +
                  'to make any charge at all you need to obtain specific permission from me.';
  LicParagraph5 = 'Whilst it is free (or because of this), I would like and expect that if you can think ' +
                  'of any improvements or spot any bugs (or even spelling or formatting errors in the ' +
                  'documentation) that you would let me know. Your feedback will help with future ' +
                  'development of the macro.';



implementation

end.
