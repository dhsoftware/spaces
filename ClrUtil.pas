unit ClrUtil;

interface

  uses
    Winapi.Windows, Vcl.Graphics;

  function PnlFontClr (bgClr : TColor) : TColor;

implementation

  function PnlFontClr (bgClr : TColor) : TColor;
  // returns white for dark backgroundes, otherwise retunns black
  var
    brightness : double;
  begin
    brightness := sqrt(0.241*sqr(GetRValue(bgClr)) +
                       0.691*sqr(GetGValue(bgClr)) +
                       0.068*sqr(GetBValue(bgClr)));
    if brightness > 128 then
      result := clBlack
    else
      result := clWhite;
  end;

end.
