object fmContourParam: TfmContourParam
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Contour Parameters'
  ClientHeight = 59
  ClientWidth = 199
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poDesigned
  OnClose = FormClose
  OnCreate = FormCreate
  OnHide = FormHide
  DesignSize = (
    199
    59)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 69
    Height = 13
    Caption = 'Wall Thickness'
  end
  object Label2: TLabel
    Left = 8
    Top = 35
    Width = 63
    Height = 13
    Caption = 'Max Opening'
  end
  object dceThick: TDcadEdit
    Left = 96
    Top = 5
    Width = 97
    Height = 21
    AllowNegative = False
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    OnExit = dceThickExit
  end
  object dceOpening: TDcadEdit
    Left = 96
    Top = 30
    Width = 97
    Height = 21
    AllowNegative = False
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 1
    OnExit = dceOpeningExit
  end
end
